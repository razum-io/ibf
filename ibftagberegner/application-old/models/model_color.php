<?
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class Model_color extends CI_Model {
	function get($data=""){
		$query = $this->db->order_by('name');
		if($data=="")
			$data = array();
			
		$data['deleted'] = 0;
		
		$query = $this->db->get_where('color', $data);
		return $query->result(); 
		// selekte pec uzstaditajiem laukiem $data
		// atgriez datus masiva (iespejams vairakas rindas)
	}
	
	function insert($data){
		$this->db->insert("color", $data);
		// nem datus no $data masiva, lieto CI insert funkciju
	}
	
	function update($data,$id){
		$this->db->update("color",$data, "id = ".$id);
	}
	
	function delete($where){
		$data = array();
		$data['deleted'] = 1;
		$this->db->update("color",$data, $where);
	}
	
	function show($where){
		$data = array();
		$data['hidden'] = 0;
		$this->db->update("color",$data, $where);
	}

	function hide($where){
		$data = array();
		$data['hidden'] = 1;
		$this->db->update("color",$data, $where);
	}

	function get_id($id){
		$data = array('id'=>$id);
		$query = $this->db->get_where('color', $data);
		$result = $query->result(); 
		return get_object_vars($result[0]);
	}
}
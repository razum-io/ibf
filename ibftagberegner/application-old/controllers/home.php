<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class Home extends CI_Controller {
	function __construct() {
		parent::__construct ();
		//$this->output->enable_profiler ( TRUE );
	}
	public function nuberegning() {
		$this->session->sess_destroy ();
		redirect ( 'home' );
	}
	public function index($form = '') {
		//$this->session->sess_destroy();
		session_start();
		
		// global variables from VB
		$_SESSION['dobbeltving'] = 0;
		$_SESSION['dobbeltving'] = 0;
		
		$data ['title'] = 'vælg tagsten';
		$this->load->view ( '/include/header', $data );
		$this->load->view ( 'view_home' );
		if ($form == 'beregn') {
			$this->session->set_userdata ( array (
					'sentfrberegn' => "YES" 
			) );
		}
	}
	public function kunde() { 
		$this->model_ibf->checkSession();
		$data = array ();
		$this->CheckBack ( 'kundenavn', $data );
		$this->CheckBack ( 'adresse', $data );
		$this->CheckBack ( 'by', $data );
		$this->CheckBack ( 'telefon', $data );
		$this->CheckBack ( 'tilbudsnr', $data );
		$this->CheckBack ( 'udfort', $data );
		$this->CheckBack ( 'beregningstidspunkt', $data );
		$data ['title'] = 'kunde';
		$this->load->view ( '/include/header', $data );
		$this->load->view ( 'view_kunde', $data );
	}
	public function farvevalg() {
		$this->model_ibf->checkSession();
		// get collors from db
		$vars ['colors'] = $this->model_ibf->getFarve ();
		$this->CheckBack ( 'farvevalgnr', $vars );
		// get session data for displaying farve images
		$vars ['stenvalgtnr'] = $this->session->userdata ( 'stenvalgtnr' );
		// load views
		$data ['title'] = 'vælg farve';
		$this->load->view ( '/include/header', $data );
		$this->load->view ( 'view_farvevalg', $vars );
	}
	
	public function tagtype() {
		$this->model_ibf->checkSession();
		// load views
		$vars = array ();
		$this->CheckBack ( 'tag', $vars );
		$this->CheckBack ( 'antalVinklerVinkel', $vars );
		$this->CheckBack ( 'antalTilbygninger', $vars );
		$this->CheckBack ( 'antalKnaster', $vars );
		$this->CheckBack ( 'antalVinkler', $vars );
		$this->CheckBack ( 'antalkviste', $vars );
		$data ['title'] = 'Valg af hus';
		$this->load->view ( '/include/header', $data );
		$this->load->view ( 'view_tagtype', $vars );
	}
	public function valm() {
		$this->model_ibf->checkSession();
		$data ['tag'] = $this->session->userdata ( 'tag' );
		$data ['dynValmType'] = $this->session->userdata ( 'dynValmType' );
		$data ['dynValmPlacering'] = $this->session->userdata ( 'dynValmPlacering' );
		$data ['hoved'] = $this->session->userdata ( 'hoved' );
		$data ['valm'] = $this->session->userdata ( 'valm' );
		$data ['antalvinklervinkel'] = $this->session->userdata ( 'antalVinklerVinkel' );
		$data ['antalKnaster'] = $this->session->userdata ( 'antalKnaster' );
		$data ['antalVinkler'] = $this->session->userdata ( 'antalVinkler' );
		$data ['antaltilbygninger'] = $this->session->userdata ( 'antalTilbygninger' );
		$data ['laengehus'] = $this->session->userdata ( 'laengehusbckup' );
		$this->CheckBack ( 'dynValmType', $data );
		$this->CheckBack ( 'dynValmPlacering', $data );
		$this->CheckBack ( 'valm_retning', $data );
		$data ['title'] = 'Vælg gavltype';
		$this->load->view ( '/include/header', $data );
		$this->load->view ( 'view_valm', $data );
	}
	public function vinkel() {
		$this->model_ibf->checkSession();
		$data ['hoved'] = $this->session->userdata ( 'hoved' );
		$data ['tilbyg'] = $this->session->userdata ( 'tilbyg' );
		$data ['title'] = 'Angiv antal aftræk';
		$this->load->view ( '/include/header', $data );
		$this->CheckBack ( 'udluftningshaetter', $data );
		$this->load->view ( 'view_vinkel', $data );
	}
	public function hvhus1() {
		$this->model_ibf->checkSession(); 
		$data ['dynValmType'] = $this->session->userdata ( 'dynValmType' );
		$this->model_ibf->deleteberegn ();
		$data ['stenvalgt'] = $this->session->userdata ( 'stenvalgtnr' );
		$data ['hoved'] = $this->session->userdata ( 'hoved' );
		$data ['antalvinklervinkel'] = $this->session->userdata ( 'antalVinklerVinkel' );
		$data ['antalvinkler'] = $this->session->userdata ( 'antalVinklerVinkel' );
		$data ['antaltilbygninger'] = $this->session->userdata ( 'antalTilbygninger' );
		$data ['antalknaster'] = $this->session->userdata ( 'antalKnaster' );
		$data ['tag'] = $this->session->userdata ( 'tag' );
		$data ['title'] = 'Målsæt hovedhus';
		$this->CheckBack ( 'tagunder', $data, true );
		$this->CheckBack ( 'facon', $data, true );
		$this->CheckBack ( 'hl', $data, true );
		$this->CheckBack ( 'hb', $data, true );
		$this->CheckBack ( 'hv', $data, true );
		$this->load->view ( '/include/header', $data );
		$this->load->view ( 'ml/view_hvhus1', $data );
	}
	public function hvhus2() {
		$this->model_ibf->checkSession();
		$data ['dynValmType'] = $this->session->userdata ( 'dynValmType' );
		$data ['hoved'] = $this->session->userdata ( 'hoved' );
		$data ['stenvalgt'] = $this->session->userdata ( 'stenvalgtnr' );
		$data ['tilbyg'] = $this->session->userdata ( 'tilbyg' );
		$data ['antalvinklervinkel'] = $this->session->userdata ( 'antalVinklerVinkel' );
		$data ['antalvinkler'] = $this->session->userdata ( 'antalVinklerVinkel' );
		$data ['antaltilbygninger'] = $this->session->userdata ( 'antalTilbygninger' );
		$data ['antalknaster'] = $this->session->userdata ( 'antalKnaster' );
		$data ['tag'] = $this->session->userdata ( 'tag' );
		$data ['title'] = 'Målsæt hovedhus';
		$this->CheckBack ( 'tagunder', $data, true );
		$this->CheckBack ( 'facon', $data, true );
		$this->CheckBack ( 'hl', $data, true );
		$this->CheckBack ( 'hb', $data, true );
		$this->CheckBack ( 'hv', $data, true );
		$this->CheckBack ( 'hl_v', $data, true );
		$this->CheckBack ( 'hb_v', $data, true );
		$this->CheckBack ( 'hv_V', $data, true );
		$this->CheckBack ( 'hl_t', $data, true );
		$this->CheckBack ( 'hb_t', $data, true );
		$this->CheckBack ( 'hv_t', $data, true );
		$this->load->view ( '/include/header', $data );
		$this->load->view ( 'ml/view_hvhus2', $data );
	}
	public function hvhus3() {
		$this->model_ibf->checkSession();
		$data ['dynValmType'] = $this->session->userdata ( 'dynValmType' );
		$data ['antalvinklervinkel'] = $this->session->userdata ( 'antalVinklerVinkel' );
		$data ['antalvinkler'] = $this->session->userdata ( 'antalVinklerVinkel' );
		$data ['antaltilbygninger'] = $this->session->userdata ( 'antalTilbygninger' );
		$data ['antalknaster'] = $this->session->userdata ( 'antalKnaster' );
		$data ['tag'] = $this->session->userdata ( 'tag' );
		$data ['title'] = 'Målsæt hovedhus';
		$this->CheckBack ( 'tagunder', $data, true );
		$this->CheckBack ( 'facon', $data, true );
		$this->CheckBack ( 'hl', $data, true );
		$this->CheckBack ( 'hb', $data, true );
		$this->CheckBack ( 'hv', $data, true );
		$this->load->view ( '/include/header', $data );
		$this->load->view ( 'ml/view_hvhus3', $data );
	}
	public function vinkel_45() {
		$this->model_ibf->checkSession();
		$data ['dynValmType'] = $this->session->userdata ( 'dynValmType' );
		$data ['antalvinklervinkel'] = $this->session->userdata ( 'antalVinklerVinkel' );
		$data ['antalvinkler'] = $this->session->userdata ( 'antalVinklerVinkel' );
		$data ['antaltilbygninger'] = $this->session->userdata ( 'antalTilbygninger' );
		$data ['antalknaster'] = $this->session->userdata ( 'antalKnaster' );
		$data ['tag'] = $this->session->userdata ( 'tag' );
		$data ['stenvalgt'] = $this->session->userdata ( 'stenvalgtnr' );
		$data ['antalvinkler'] = $this->session->userdata ( 'antalVinkler' );
		$data ['title'] = 'Målsæt hovedhus';
		$this->CheckBack ( 'tagunder', $data, true );
		$this->CheckBack ( 'facon', $data, true );
		$this->CheckBack ( 'hl', $data, true );
		$this->CheckBack ( 'hb', $data, true );
		$this->CheckBack ( 'hv', $data, true );
		$this->CheckBack ( 'hl_v', $data, true );
		$this->CheckBack ( 'hb_v', $data, true );
		$this->CheckBack ( 'hl_t', $data, true );
		$this->CheckBack ( 'hb_t', $data, true );
		$this->load->view ( '/include/header', $data );
		$this->load->view ( 'ml/view_vinkel_45', $data );
	}
	public function h_hus() {
		$this->model_ibf->checkSession();
		$data ['stenvalgt'] = $this->session->userdata ( 'stenvalgtnr' );
		$data ['dynValmType'] = $this->session->userdata ( 'dynValmType' );
		$data ['antalvinklervinkel'] = $this->session->userdata ( 'antalVinklerVinkel' );
		$data ['antalvinkler'] = $this->session->userdata ( 'antalVinklerVinkel' );
		$data ['antaltilbygninger'] = $this->session->userdata ( 'antalTilbygninger' );
		$data ['antalknaster'] = $this->session->userdata ( 'antalKnaster' );
		$data ['tag'] = $this->session->userdata ( 'tag' );
		$data ['title'] = 'Målsæt hovedhus';
		$this->CheckBack ( 'tagunder', $data, true );
		$this->CheckBack ( 'facon', $data, true );
		$this->CheckBack ( 'hl', $data, true );
		$this->CheckBack ( 'hb', $data, true );
		$this->CheckBack ( 'hv', $data, true );
		$this->CheckBack ( 'hl_v', $data, true );
		$this->CheckBack ( 'hb_v', $data, true );
		$this->CheckBack ( 'hv_V', $data, true );
		$this->CheckBack ( 'hl_t', $data, true );
		$this->CheckBack ( 'hb_t', $data, true );
		$this->CheckBack ( 'hv_t', $data, true );
		$this->load->view ( '/include/header', $data );
		$this->load->view ( 'ml/view_h_hus', $data );
	}
	public function tagmetode($spec_met = 0) {
		$this->model_ibf->checkSession();
		if ($spec_met !== 0) {
			$this->session->set_userdata ( array (
					
					'spec_met' => 'kvist' 
			) );
		}
		$data ['title'] = 'Vælg metode til tagopbygning';
		$this->load->view ( '/include/header', $data );
		$this->load->view ( 'view_tagmetode' );
	}
	public function maalsaet() {
		$this->model_ibf->checkSession();
		$data ['dynValmType'] = $this->session->userdata ( 'dynValmType' );
		$data ['antalvinkler'] = $this->session->userdata ( 'antalVinkler' );
		$data ['antalknaster'] = $this->session->userdata ( 'antalKnaster' );
		$data ['tag'] = $this->session->userdata ( 'tag' );
		$data ['dynValmType'] = $this->session->userdata ( 'dynValmType' );
		$data ['dynValmPlacering'] = $this->session->userdata ( 'dynValmPlacering' );
		$data ['antalvinklervinkel'] = $this->session->userdata ( 'antalVinklerVinkel' );
		$data ['antaltilbygninger'] = $this->session->userdata ( 'antalTilbygninger' );
		$data ['tag'] = $this->session->userdata ( 'tag' );
		$data ['laengehus'] = $this->session->userdata ( 'laengehus' );
		$data ['globalValmTypeMaalsaet'] = $this->session->userdata ( 'globalValmTypeMaalsaet' );
		// create back button
		$hoved = $this->session->userdata ( 'hoved' );
		$tilbyg = $this->session->userdata ( 'tilbyg' );
		switch ($hoved) {
			case 1 :
				if ($tilbyg == 1) {
					$data ['back'] = base_url ( 'home/hvhus1' );
				} else {
					$data ['back'] = base_url ( 'home/hvhus2' );
				}
				break;
			case 2 :
				$data ['back'] = base_url ( 'home/hvhus2' );
				break;
			case 3 :
			case 4 :
				$data ['back'] = base_url ( 'home/hvhus3' );
				break;
			case 5 :
				$data ['back'] = base_url ( 'home/vinkel_45' );
				break;
			case 6 :
				if ($tilbyg == 1) {
					$data ['back'] = base_url ( 'home/hvhus1' );
				} else {
					$data ['back'] = base_url ( 'home/hvhus2' );
				}
				break;
			case 7 :
				$data ['back'] = base_url ( 'home/h_hus' );
				break;
		}
		if ($data ['globalValmTypeMaalsaet'] == 2) {
			$data ['title'] = "IBF Tagsten - Målsætning af helvalm";
		} else {
			$data ['title'] = "IBF Tagsten - Målsætning af halvvalm";
		}
		$this->load->view ( '/include/header', $data );
		$this->load->view ( 'view_maalsaet', $data );
	}
	public function kvist() {
		$this->model_ibf->checkSession();
		$data ['kvist'] = $this->session->userdata ( 'kvist' );
		$dynValmType = ( array ) json_decode ( $this->session->userdata ( 'dynValmType' ) );
		// create back button
		
		if ($this->session->userdata ( 'globalValmTypeMaalsaet' ) !== False) {
			$data ['back'] = base_url ( 'home/maalsaet' );
		} else {
			$hoved = $this->session->userdata ( 'hoved' );
			$tilbyg = $this->session->userdata ( 'tilbyg' );
			switch ($hoved) {
				case 1 :
					if ($tilbyg == 1) {
						$data ['back'] = base_url ( 'home/hvhus1' );
					} else {
						$data ['back'] = base_url ( 'home/hvhus2' );
					}
					break;
				case 2 :
					$data ['back'] = base_url ( 'home/hvhus2' );
					break;
				case 3 :
				case 4 :
					$data ['back'] = base_url ( 'home/hvhus3' );
					break;
				case 5 :
					$data ['back'] = base_url ( 'home/vinkel_45' );
					break;
				case 6 :
					if ($tilbyg == 1) {
						$data ['back'] = base_url ( 'home/hvhus1' );
					} else {
						$data ['back'] = base_url ( 'home/hvhus2' );
					}
					break;
				case 7 :
					$data ['back'] = base_url ( 'home/h_hus' );
					break;
			}
		}
		$data ['title'] = 'Angiv mål for kvist(e)';
		$this->load->view ( '/include/header', $data );
		$this->load->view ( 'view_kvist', $data );
	}
	public function met2q() {
		$this->model_ibf->checkSession();
		$sess = $this->session->all_userdata ();
		foreach ( $sess as $key => $value ) {
			if (substr ( $key, 0, 4 ) == 'tmp_') {
				$data [substr ( $key, 4 )] = $value;
			}
		}
		$data ['title'] = 'Tagopbygning';
		$this->load->view ( '/include/header', $data );
		$this->load->view ( 'met/2q', $data );
	}
	public function met3q() {
		$this->model_ibf->checkSession();
		$sess = $this->session->all_userdata ();
		foreach ( $sess as $key => $value ) {
			if (substr ( $key, 0, 4 ) == 'tmp_') {
				$data [substr ( $key, 4 )] = $value;
			}
		}
		$data ['title'] = 'Tagopbygning';
		$this->load->view ( '/include/header', $data );
		$this->load->view ( 'met/3q', $data );
	}
	public function beregn() {
		$this->model_ibf->checkSession();
		global $stenvalgt, $beregn, $farvevalg, $tilgTable_stand;
		$rygbeslag = 0;
		$stenvalgt = $this->session->userdata ( 'stenvalgtnr' );
		$farvevalg = $this->session->userdata ( 'farvevalgnr' );
		$tagunder = $this->session->userdata ( 'tagunder' );
		$maxLegte = $this->session->userdata ( 'maxLegte' );
		$mal = $this->session->userdata ( 'mal' );
		$udluftningshaetter = $this->session->userdata ( 'udluftningshaetter' );
		$sessId = $this->session->userdata ( 'sessuser_id' );
		$qry = "SELECT
		SUM(`normalsten`) AS normalsten ,
		SUM(`Dobbeltvinget`) AS Dobbeltvinget ,
		SUM(`halvsten`) AS halvsten ,
		SUM(`VindskH31`) AS VindskH31,
		SUM(`VindskH33`) AS VindskH33,
		SUM(`VindskH35`) AS VindskH35,
		SUM(`VindskV31`) AS VindskV31,
		SUM(`VindskV33`) AS VindskV33,
		SUM(`VindskV35`) AS VindskV35,
		Pultsten ,
		PultvindskH ,
		PultvindskV ,
		SUM(`Rygning`) AS Rygning ,
		SUM(`Faconrygning`) AS Faconrygning ,
		SUM(`Rygafslut`) AS Rygafslut ,
		SUM(`FaconRygAfslut`) AS FaconRygAfslut ,
		SUM(`Valmbegynder`) AS Valmbegynder ,
		SUM(`Valmkappe`) AS Valmkappe ,
		SUM(`KVM`) AS KVM ,
		SUM(`legte`) AS legte ,
		SUM(`AntalRekker`) AS AntalRekker ,
		SUM(`AntalKolonner`) AS AntalKolonner ,
		SUM(`Rygstart`) AS Rygstart ,
		SUM(`faconrygstart`) AS faconrygstart ,
		Pultdobb ,
		SUM(`skotrende`) AS skotrende ,
		SUM(`fuglegitter`) AS fuglegitter ,
		SUM(`alurullerGrater`) AS alurullerGrater ,
		SUM(`alurullerRygninger`) AS alurullerRygninger ,
		SUM(`Pyramidesten`) AS Pyramidesten ,
		SUM(`vinkelRygning`) AS vinkelRygning
		FROM `beregn` where `sessid` = ?";
		$query = $this->db->query ( $qry, array (
				$sessId 
		) );
		//echo $sessId;
		$beregn = $query->result ()[0];
		
		$tilgTable_stand = json_decode ( $this->session->userdata ( 'tilgengelig' ) );
		
		// ##########################################
		// # find edit and create final view data ###
		// ##########################################
		
		if ($stenvalgt == 0) {
			$stenvalgstart = "80";
			$faktorforudl = 200;
			$faktorunderstryg = 1.01;
		} // Dobbelt-S
		if ($stenvalgt == 1) {
			$stenvalgstart = "91";
			$faktorforudl = 250;
			$faktorunderstryg = 1.41;
		} // Vinge Classic
		if ($stenvalgt == 2) {
			$stenvalgstart = "90";
			$faktorforudl = 250;
			$faktorunderstryg = 1.41;
		} // Vinge Sten
		if ($stenvalgt == 3) {
			$stenvalgstart = "92";
			$faktorforudl = 125;
			$faktorunderstryg = 1.31;
		} // Vinge Å²konomi
		if ($stenvalgt == 4) {
			$stenvalgstart = "93";
			$faktorforudl = 125;
			$faktorunderstryg = 1.31;
		} // Vinge Å²konomi Plus
		$prev = $stenvalgstart;
		$rekkenummer = 1;
		
		// #skotrende
		if (round ( $beregn->skotrende > 0 )) {
			$this->model_ibf->Vis_Flex ( (""), ("skotrende"), (( string ) (- 1 * (Floor ( ((- 1 * (2 * round ( $beregn->skotrende ))) / 1000) )))) );
		}
		
		// #legte
		if (( float ) $beregn->legte > 0) {
			$data ['frmlegte'] = ( string ) ($this->model_ibf->todec ( $beregn->legte ));
		}
		// #fuglegitter
		if (( float ) ($beregn->fuglegitter / 1000) > 0) {
			$this->model_ibf->Vis_Flex ( (""), ("fuglegitter"), (( string ) (- 1 * (Floor ( (((- 1 * $beregn->fuglegitter) / 1000) / 5) )))) );
			$data ['frmtagrende'] = ( string ) ($this->model_ibf->todec ( $beregn->fuglegitter / 1000 ));
		}
		$grater_meter = 0;
		$rygning_meter = 0;
		// #alurullerGrater
		if (( float ) ($beregn->alurullerGrater / 1000) > 0) {
			
			$grater_meter = ( float ) ($beregn->alurullerGrater / 1000);
		}
		// #alurullerRygninger
		if (( float ) ($beregn->alurullerRygninger / 1000) > 0) {
			
			$rygning_meter = ( float ) ($beregn->alurullerRygninger / 1000);
		}
		if ($tagunder == "understrygning") {
			// Ved understrygning:
			$this->model_ibf->Vis_Flex ( (""), ("alurullerGrater"), (( string ) (- 1 * (Floor ( ((- 1 * ($grater_meter + $rygning_meter)) / 5) )))) );
		} else {
			// Ved undertag:
			$this->model_ibf->Vis_Flex ( (""), ("alurullerRygninger"), (( string ) (- 1 * (Floor ( ((- 1 * ($grater_meter + $rygning_meter)) / 5) )))) );
		}
		
		if ($udluftningshaetter > 0) {
			
			$this->model_ibf->Vis_Flex ( ("82"), ("udlHaetter"), (( string ) ($udluftningshaetter)) );
			//$this->model_ibf->Vis_Flex ( ("950290"), (""), (( string ) ($udluftningshaetter)) ); // overgangsstykke
			$this->model_ibf->Vis_Flex ( ("950020"), (""), (( string ) ($udluftningshaetter)) ); // multiovergang
			$this->model_ibf->Vis_Flex ( ("950410"), (""), (( string ) ($udluftningshaetter)) ); // Taghettekrave
		}
		// #kvm
		$data ['frmflade'] = '';
		if (( float ) ($beregn->KVM) > 0) {
			$data ['frmflade'] = ( string ) ($this->model_ibf->todec ( $beregn->KVM ));
			
			if ($tagunder == "understrygning") {
				$faktorunderstryg = $faktorunderstryg . (($maxLegte - $mal) * 0.25);
				$iiii = (- 1 * (Floor ( ((- 1 * $beregn->KVM) / 25) )));
				$this->model_ibf->Vis_Flex ( (""), ("UNDERSTRYGNINGSKIT"), (( string ) ($iiii * 25)) );
			}
		}
		$fastgorelse = 0;
		$fastgorelseNakkeklemmer = 0;
		
		// Beregning af "normalsten"
		// #normalsten
		if (( float ) ($beregn->normalsten) > 0) {
			$this->model_ibf->Vis_Flex ( ("00"), ("normalsten"), (( string ) (- 1 * (Floor ( (- 1 * ($beregn->normalsten * 1.03)) )))) );
			if ($tagunder == "understrygning") {
				$this->model_ibf->Vis_Flex ( ("02"), ("tagrumsudl"), (( string ) (- 1 * (Floor ( (- 1 * ($beregn->normalsten / $faktorforudl)) )))) );
			}
			$fastgorelse = ( float ) ($beregn->normalsten / 3);
		}
		
		// fastgĆørelse:
		// ---------------------
		// #AntalRekker
		if (( float ) ($beregn->AntalRekker) > 0) {
			$fastgorelse = $fastgorelse + (4 * ( float ) ($beregn->AntalRekker));
		}
		
		// #AntalKolonner
		if (( float ) ($beregn->AntalKolonner) > 0) {
			$fastgorelse = $fastgorelse + (2 * ( float ) ($beregn->AntalKolonner));
		}
		
		if ($stenvalgt == 0) {
			// 0 Dobbelt-S
			// Man bruer ikke tagstenssĆøm lengere - rettet 24-09-2007:
			// $this->model_ibf->Vis_Flex( ("975000"), (""), ((string)(-1 * (Floor(((-1 * (fastgorelse)) / 250)))) // DS TagstenssĆøm
			$this->model_ibf->Vis_Flex ( ("975155"), (""), (( string ) (- 1 * (Floor ( ((- 1 * ($fastgorelse)) / 500) )))) ); // D.S. Bindere
		}
		if ($stenvalgt == 3 || $stenvalgt == 4) {
			$this->model_ibf->Vis_Flex ( ("975255"), (""), (( string ) (- 1 * (floor ( ((- 1 * ($fastgorelse)) / 500) )))) ); // Å²KO Bindere
		}
		// ---------------------
		// Beregning af halve sten
		// #halvsten
		if (( float ) ($beregn->halvsten) > 0) {
			$this->model_ibf->Vis_Flex ( ("01"), ("halve_sten"), (( string ) ($beregn->halvsten)) );
		}
		
		// Beregning af dobbeltvinget
		// #dobbeltvinget
		if (( float ) ($beregn->Dobbeltvinget) > 0) {
			$dobbvinget_paa_udskrift = true;
			$this->model_ibf->Vis_Flex ( ("11"), ("Dobbeltvinget"), (( string ) ($beregn->Dobbeltvinget)) );
			$fastgorelseNakkeklemmer = $fastgorelseNakkeklemmer + ( float ) ($beregn->Dobbeltvinget);
		} else {
			$dobbvinget_paa_udskrift = false;
		}
		
		// #Rygning
		
		if (( float ) ($beregn->Rygning) > 0) {
			
			$iii = (- 1 * (Floor ( (- 1 * (($grater_meter + $rygning_meter) * 2.7) * 1.03) )));
			$this->model_ibf->Vis_Flex ( ("70"), ("rygningssten"), (( string ) ($iii)) );
			$rygbeslag = $rygbeslag + ( float ) ($iii);
		}
		// Beregning af Pyramidesten
		// #Pyramidesten
		if (( float ) ($beregn->Pyramidesten) > 0) {
			$this->model_ibf->Vis_Flex ( ("75"), ("Pyramidesten"), (( string ) ($beregn->Pyramidesten)) );
			// rygbeslag = rygbeslag + (float)($beregn->)
		}
		
		// Beregning af VinkelRygninger
		// #vinkelRygning
		if (( float ) ($beregn->vinkelRygning) > 0) {
			$this->model_ibf->Vis_Flex ( ("78"), ("vinkelRygning"), (( string ) ($beregn->vinkelRygning)) );
		}
		
		// #faconrygning
		if (( float ) ($beregn->Faconrygning) > 0) {
			$iii = (- 1 * (Floor ( (- 1 * (($grater_meter + $rygning_meter) * 3.33) * 1.03) )));
			
			$this->model_ibf->Vis_Flex ( ("50"), ("Faconrygninger"), (( string ) ($iii)) );
			$rygbeslag = $rygbeslag + ( float ) ($iii);
		}
		
		// Beregning af rygningsstart
		// #rygstart
		if (( float ) ($beregn->Rygstart) > 0) {
			$this->model_ibf->Vis_Flex ( ("71"), ("rygningsstart"), (( string ) ($beregn->Rygstart)) );
			$rygbeslag = $rygbeslag + ( float ) ($beregn->Rygstart);
		}
		
		// Beregning af rygningsslut
		// #rygafslut
		if (( float ) ($beregn->Rygafslut) > 0) {
			$this->model_ibf->Vis_Flex ( ("72"), ("rygningsslut"), (( string ) ($beregn->Rygafslut)) );
		}
		
		// Beregning af faconrygningsstart
		// #faconrygstart
		if (( float ) ($beregn->faconrygstart) > 0) {
			$this->model_ibf->Vis_Flex ( ("51"), ("faconrygningstart"), (( string ) ($beregn->faconrygstart)) );
			$rygbeslag = $rygbeslag + ( float ) ($beregn->faconrygstart);
		}
		
		// Beregning af faconrygning med afslutning
		// #faconRygAfslut
		if (( float ) ($beregn->FaconRygAfslut) > 0) {
			$this->model_ibf->Vis_Flex ( ("52"), ("faconrygningslut"), (( string ) ($beregn->FaconRygAfslut)) );
		}
		
		$vindd = 0;
		// Beregning af vindskedesten hĆøjre
		// #vindskH31
		if (( float ) ($beregn->VindskH31) > 0) {
			$this->model_ibf->Vis_Flex ( ("12"), ("vindskedesten_h"), (( string ) ($beregn->VindskH31)) );
			$vindd = $vindd + ( float ) ($beregn->VindskH31);
		}
		
		// #vindskH33
		if (( float ) ($beregn->VindskH33) > 0) {
			$this->model_ibf->Vis_Flex ( ("14"), ("vindskedesten_h"), (( string ) ($beregn->VindskH33)) );
			$vindd = $vindd + ( float ) ($beregn->VindskH33);
		}
		
		// #vindskH35
		if (( float ) ($beregn->VindskH35) > 0) {
			$this->model_ibf->Vis_Flex ( ("16"), ("vindskedesten_h"), (( string ) ($beregn->VindskH35)) );
			$vindd = $vindd + ( float ) ($beregn->VindskH35);
		}
		
		// Beregning af vindskedesten venstre
		// #vindskV31
		if (( float ) ($beregn->VindskV31) > 0) {
			$this->model_ibf->Vis_Flex ( ("13"), ("vindskedesten_v"), (( string ) ($beregn->VindskV31)) );
			$vindd = $vindd + ( float ) ($beregn->VindskV31);
			$fastgorelseNakkeklemmer = $fastgorelseNakkeklemmer + ( float ) ($beregn->VindskV31);
		}
		
		// #vindskV33
		if (( float ) ($beregn->VindskV33) > 0) {
			$this->model_ibf->Vis_Flex ( ("15"), ("vindskedesten_v"), (( string ) ($beregn->VindskV33)) );
			$vindd = $vindd + ( float ) ($beregn->VindskV33);
			$fastgorelseNakkeklemmer = $fastgorelseNakkeklemmer + ( float ) ($beregn->VindskV33);
		}
		// #vindskV35
		if (( float ) ($beregn->VindskV35) > 0) {
			$this->model_ibf->Vis_Flex ( ("17"), ("vindskedesten_v"), (( string ) ($beregn->VindskV35)) );
			$vindd = $vindd + ( float ) ($beregn->VindskV35);
			$fastgorelseNakkeklemmer = $fastgorelseNakkeklemmer + ( float ) ($beregn->VindskV35);
		}
		if ($stenvalgt == 2) {
			// 2 Vinge
			$this->model_ibf->Vis_Flex ( ("975355"), (""), (( string ) (- 1 * (Floor ( ((- 1 * ($fastgorelse + $fastgorelseNakkeklemmer)) / 100) )))) ); // Rustfri nakkeklemmer 38X73 (100/PK)
		} else {
			if ($fastgorelseNakkeklemmer > 0) {
				$this->model_ibf->Vis_Flex ( ("975355"), (""), (( string ) (- 1 * (floor ( ((- 1 * ($fastgorelseNakkeklemmer)) / 100) )))) ); // Nakkekroge
			}
		}
		if ($vindd > 0) {
			$vindskede_paa_udskrift = true;
		} else {
			$vindskede_paa_udskrift = false;
		}
		
		// Beregning af valmstart
		// #Valmbegynder
		if (( float ) ($beregn->Valmbegynder) > 0) {
			$this->model_ibf->Vis_Flex ( ("76"), ("valmstart"), (( string ) ($beregn->Valmbegynder)) );
			$rygbeslag = $rygbeslag + ( float ) ($beregn->Valmbegynder);
		}
		$this->model_ibf->Vis_Flex ( ("9755"), (""), (( string ) (- 1 * (floor ( ((- 1 * $rygbeslag)) )))) ); // Rygningsbeslag
		$this->model_ibf->Vis_Flex ( ("975645"), (""), (( string ) (- 1 * (floor ( ((- 1 * (2 * $rygbeslag)) / 100) )))) ); // AlusĆøm 45mm
		$this->model_ibf->Vis_Flex ( ("975665"), (""), (( string ) (- 1 * (floor ( ((- 1 * $rygbeslag) / 100) )))) ); // AlusĆøm 65mm
		                                                                                                              
		// Beregning af valmkappe
		                                                                                                              // #Valmkappe
		if (( float ) ($beregn->Valmkappe) > 0) {
			$this->model_ibf->Vis_Flex ( ("77"), ("valmkappe"), (( string ) ($beregn->Valmkappe)) );
		}
		
		// Antal hele pultsten
		if ($beregn->Pultsten > 0) {
			$this->model_ibf->Vis_Flex ( ("20"), ("pultsten"), (( string ) ($beregn->Pultsten)) );
		}
		if ($beregn->PultvindskH > 0) {
			$this->model_ibf->Vis_Flex ( ("24"), ("pult_vindskede_h"), (( string ) ($beregn->PultvindskH)) );
		}
		
		// Antal pult vindskedesten venstre
		if ($beregn->PultvindskV > 0) {
			$this->model_ibf->Vis_Flex ( ("25"), ("pult_vindskede_v"), (( string ) ($beregn->PultvindskV)) );
		}
		
		// Antal dobbeltvingede pultsten
		if ($beregn->Pultdobb > 0) {
			$this->model_ibf->Vis_Flex ( ("21"), ("pult_dobbvingede"), (( string ) ($beregn->Pultdobb)) );
		}
		
		// #######################################
		// ######### end of main data ###########
		// #######################################
		function dataSort($a, $b) {
			return $a->Rekkefulge == $b->Rekkefulge ? 0 : ($a->Rekkefulge > $b->Rekkefulge) ? 1 : - 1;
		}
		usort ( $tilgTable_stand, 'dataSort' );
		$data ['tilgTable_stand'] = $tilgTable_stand;
		
		$data ['tilgTable_stand'] = $this->str_replace_json ( "ų", "ø", $data ['tilgTable_stand'] );
		$data ['tilgTable_stand'] = $this->str_replace_json ( "ę", "æ", $data ['tilgTable_stand'] );
		// gather info for KUNDE
		$data ['Navn'] = $this->session->userdata ( 'kundenavn' );
		$data ['adresse'] = $this->session->userdata ( 'adresse' );
		$data ['by'] = $this->session->userdata ( 'by' );
		$data ['telefon'] = $this->session->userdata ( 'telefon' );
		$data ['tilbudsnummer'] = $this->session->userdata ( 'tilbudsnr' );
		$data ['beregningstidspunkt'] = $this->session->userdata ( 'beregningstidspunkt' );
		
		// ----
		// set session variables for tagopbygning form
		$sesset = array (
				'grater_meter' => $grater_meter,
				'rygning_meter' => $rygning_meter,
				'fladen' => $data ['frmflade'] 
		);
		$this->session->set_userdata ( $sesset );
		// ----
		// backup result data
		$this->session->set_userdata ( array (
				'result' => $data ['tilgTable_stand'] 
		) );
		
		foreach($data['tilgTable_stand'] as $part)
		{
			$ibf = $part->IBF ;
			$navn = $part->Navn ;
			$tun = $part->TUN ;
			
			$custom_color_id = $this->session->userdata('custom_color');
			$q = $this->db->query("select * from color_parts where color_id = ".$custom_color_id." and ibf1 = '".$ibf."'");
			
			$result = $q->result();
			if (count($result))
			{
				$part->IBF = $result[0]->ibf2;
				$part->Navn = $result[0]->navn2;
				$part->TUN = $result[0]->tun2;
			}
		}
		
		// create back button
		$hoved = $this->session->userdata ( 'hoved' );
		$tilbyg = $this->session->userdata ( 'tilbyg' );
		switch ($hoved) {
			case 1 :
				if ($tilbyg == 1) {
					$data ['back'] = base_url ( 'home/hvhus1' );
				} else {
					$data ['back'] = base_url ( 'home/hvhus2' );
				}
				break;
			case 2 :
				$data ['back'] = base_url ( 'home/hvhus2' );
				break;
			case 3 :
			case 4 :
				$data ['back'] = base_url ( 'home/hvhus3' );
				break;
			case 5 :
				$data ['back'] = base_url ( 'home/vinkel_45' );
				break;
			case 6 :
				if ($tilbyg == 1) {
					$data ['back'] = base_url ( 'home/hvhus1' );
				} else {
					$data ['back'] = base_url ( 'home/hvhus2' );
				}
				break;
			case 7 :
				$data ['back'] = base_url ( 'home/h_hus' );
				break;
		}
		// for illustration
		$data ['dynValmType'] = $this->session->userdata ( 'dynValmType' );
		$data ['antalvinklervinkel'] = $this->session->userdata ( 'antalVinklerVinkel' );
		$data ['antalvinkler'] = $this->session->userdata ( 'antalVinkler' );
		$data ['antaltilbygninger'] = $this->session->userdata ( 'antalTilbygninger' );
		$data ['antalknaster'] = $this->session->userdata ( 'antalKnaster' );
		$data ['tag'] = $this->session->userdata ( 'tag' );
		
		$data ['title'] = 'Beregnigsprogram';
		$this->load->view ( '/include/header', $data );
		$this->load->view ( 'view_beregn', $data );
	}
	public function str_replace_json($search, $replace, $subject) {
		return json_decode ( str_replace ( $search, $replace, json_encode ( $subject ) ) );
	}
	public function tagopbygning() {

		$this->model_ibf->checkSession();
		// gather sess data
		$hoved = $this->session->userdata ( 'hoved' );
		$tilbyg = $this->session->userdata ( 'tilbyg' );
		// $hhvalm = $this->session->userdata ( 'valm' );
		// $hhkvist = $this->session->userdata ( 'kvist' );
		$antalVinklerVinkel = $this->session->userdata ( 'antalVinklerVinkel' );
		$antalVinkler = $this->session->userdata ( 'antalVinkler' );
		$antalTilbygninger = $this->session->userdata ( 'antalTilbygninger' );
		$antalKnaster = $this->session->userdata ( 'antalKnaster' );
		// ////
		$hb = $this->session->userdata ( 'res_hb' );
		$hl = $this->session->userdata ( 'res_hl' );
		$hv = $this->session->userdata ( 'res_hv' );
		$kvistantal = $this->session->userdata ( 'antalkviste' );
		$khb = $this->session->userdata ( 'res_khb' );
		$khl = $this->session->userdata ( 'res_khl' );
		$khv = $this->session->userdata ( 'res_khv' );
		$khvv = $this->session->userdata ( 'res_khvv' );
		$khvb = $this->session->userdata ( 'res_khvb' );
		$hl_t = ( int ) $this->session->userdata ( 'res_hl_t' );
		$hb_t = ( int ) $this->session->userdata ( 'res_hb_t' );
		$hv_t = ( int ) $this->session->userdata ( 'res_hv_t' );
		$hl_v = ( int ) $this->session->userdata ( 'res_hl_v' );
		$hb_v = ( int ) $this->session->userdata ( 'res_hb_v' );
		$hv_V = ( int ) $this->session->userdata ( 'res_hv_V' );
		$knhl = ( int ) $this->session->userdata ( 'res_knhl' );
		$knhb = ( int ) $this->session->userdata ( 'res_knhb' );
		$knra = ( int ) $this->session->userdata ( 'res_knra' );
		$knka = ( int ) $this->session->userdata ( 'res_knka' );
		$knhl2 = ( int ) $this->session->userdata ( 'res_knhl2' );
		$knhb2 = ( int ) $this->session->userdata ( 'res_knhb2' );
		$knra2 = ( int ) $this->session->userdata ( 'res_knra2' );
		$knka2 = ( int ) $this->session->userdata ( 'res_knka2' );
		$ra = ( int ) $this->session->userdata ( 'res_ra' );
		$ka = ( int ) $this->session->userdata ( 'res_ka' );
		$rat = ( int ) $this->session->userdata ( 'res_rat' );
		$kat = ( int ) $this->session->userdata ( 'res_kat' );
		$rav = ( int ) $this->session->userdata ( 'res_rav' );
		$kav = ( int ) $this->session->userdata ( 'res_kav' );


		
		// right table
		$mal_hovedhus = $this->session->userdata ( 'res_mal_hovedhus' );
		$mal_vinkel = $this->session->userdata ( 'res_mal_vinkel' );
		$mal_tilbygning = $this->session->userdata ( 'res_mal_tilbygning' );
		$mal_knast2 = $this->session->userdata ( 'res_mal_knast2' );
		// -----------
		$data ['grater_meter'] = $this->session->userdata ( 'grater_meter' );
		$data ['rygning_meter'] = $this->session->userdata ( 'rygning_meter' );
		$data ['klemliste'] = round ( $this->session->userdata ( 'fladen' ) * 1.05, 0 );


		// change ra and ka
		$metode = $this->session->userdata ( 'metode' );
		if ($metode == 2) {
			// dobbv
			if ($ka > 0) {
				$ka = $ka + 1;
			}
			if ($kat > 0) {
				$kat = $kat + 1;
			}
			if ($kav > 0) {
				$kav = $kav + 1;
			}
			if ($knka2 > 0) {
				$knka2 = $knka2 + 1;
			}
		}
		
		if ($metode == 3) {
			// vindsk...
			if ($ka > 0) {
				$ka = $ka + 2;
			}
			if ($knka2 > 0) {
				$knka2 = $knka2 + 2;
			}
			if ($kat > 0) {
				$kat = $kat + 1;
			}
			
			if ($kav > 0) {
				$kav = $kav + 1;
			}
		}
		// -------------
		
		// left table
		// set defaults
		$data ['label1'] = 'Hovedhus';
		$data ['label2'] = 'Længde';
		$data ['label3'] = 'Bredde';
		$data ['label4'] = 'Taghældning';
		$data ['label5'] = 'Antal kolonner';
		$data ['label6'] = 'Antal rækker';
		$data ['label7'] = 'Tilbygning';
		$data ['label8'] = 'Vinkel';
		$data ['label9'] = 'Knast';
		$data ['label17'] = 'Hovedhus';
		$data ['label19'] = 'Tilbygning';
		$data ['label18'] = 'Vinkel';
		$data ['label20'] = 'a:';
		$data ['label21'] = 'b:';
		$data ['label22'] = 'c:';
		
		$data ['text1'] = '2.5 cm';
		$data ['text2'] = '37.09 cm';
		$data ['text3'] = '34 cm';
		$data ['tilbyg_b'] = '34 cm';
		$data ['vinkel_b'] = '34 cm';
		// change based on tagtype
		if ($hoved == 7) {
			$data ['label1'] = "Hovedhus 1";
			$data ['label7'] = "Midterbygning";
			$data ['label8'] = "Hovedhus 2";
			$data ['label19'] = "Midt.";
			$data ['label18'] = "Hus 2";
		}
		
		if (($hoved == 2 && $antalVinklerVinkel == 2) || ($hoved == 5 && $antalVinkler == 2)) {
			$data ['label7'] = "Vinkel 2";
			$data ['label19'] = "Vinkel 2";
		}
		if ($hoved == 1 && $antalTilbygninger == 2) {
			$data ['label8'] = "Tilbygning 2";
			$data ['label18'] = "Tilbyg. 2";
		}
		if ($hoved == 1 && $antalKnaster == 2) {
			$data ['label8'] = "Knast 2";
		}
		// ////////////////
		
		// hovedhus
		if ($hl > 0) {
			$data ['lengde_hh'] = ( string ) ($hl / 10) . " " . 'cm' . ".";
			$data ['bredde_hh'] = ( string ) ($hb / 10) . " " . 'cm' . ".";
			$data ['tagh_hh'] = ( string ) ($hv) . " °";
			$data ['rek_hh'] = ( string ) ($ra);
			$data ['kol_hh'] = ( string ) ($ka);
		} else {
			$data ['lengde_hh'] = "-";
			$data ['bredde_hh'] = "-";
			$data ['tagh_hh'] = "-";
			$data ['rek_hh'] = "-";
			$data ['kol_hh'] = "-";
		}
		// Tilbygning
		if (($tilbyg == 2 || ($hoved == 2 && $antalVinklerVinkel == 2)) || $hoved == 7 || ($hoved == 5 && $antalVinkler == 2)) {
			$data ['lengde_tb'] = ( string ) ($hl_t / 10) . " " . 'cm' . ".";
			$data ['bredde_tb'] = ( string ) ($hb_t / 10) . " " . 'cm' . ".";
			$data ['tagh_tb'] = ( string ) ($hv_t) . " °";
			$data ['rek_tb'] = ( string ) ($rat); // (string)(rat_tilbyg)
			$data ['kol_tb'] = ( string ) ($kat); // (string)(kat_tilbyg)
		} else {
			$data ['lengde_tb'] = "-";
			$data ['bredde_tb'] = "-";
			$data ['tagh_tb'] = "-";
			$data ['rek_tb'] = "-";
			$data ['kol_tb'] = "-";
		}
		// Vinkel
		if ($hoved == 2 || $hoved == 7 || ($antalTilbygninger == 2 && $hoved == 1) || $hoved == 5) {
			$data ['lengde_v'] = ( string ) ($hl_v / 10) . " " . 'cm' . ".";
			$data ['bredde_v'] = ( string ) ($hb_v / 10) . " " . 'cm' . ".";
			$data ['tagh_v'] = ( string ) ($hv_V) . " °";
			
			$data ['rek_v'] = ( string ) ($rav);
			$data ['kol_v'] = ( string ) ($kav);
		} else {
			$data ['lengde_v'] = " - ";
			$data ['bredde_v'] = "-";
			$data ['tagh_v'] = "-";
			$data ['rek_v'] = "-";
			$data ['kol_v'] = "-";
		}
		// Knast
		if ($tilbyg == 3) {
			$data ['lengde_k'] = ( string ) ($knhl / 10) . " " . 'cm' . ".";
			$data ['bredde_k'] = ( string ) ($knhb / 10) . " " . 'cm' . ".";
			$data ['tagh_k'] = ( string ) ($hv) . " °";
			$data ['rek_k'] = ( string ) ($knra);
			$data ['kol_k'] = ( string ) ($knka);
			if ($hoved == 1 && $antalKnaster == 2) {
				$data ['lengde_v'] = ( string ) ($knhl2 / 10) . " " . 'cm' . ".";
				$data ['bredde_v'] = ( string ) ($knhb2 / 10) . " " . 'cm' . ".";
				$data ['tagh_v'] = ( string ) ($hv) . " °";
				
				$data ['rek_v'] = ( string ) ($knra2);
				$data ['kol_v'] = ( string ) ($knka2);
			}
		} else {
			$data ['lengde_k'] = "-";
			$data ['bredde_k'] = "-";
			$data ['tagh_k'] = "-";
			$data ['rek_k'] = "-";
			$data ['kol_k'] = "-";
		}
		
		// Legteafstand
		
		// ---------------------------------------------
		// Hovedhus
		// ---------------------------------------------
		$data ['text2'] = ( string ) ($mal_hovedhus) . (" " . 'cm');
		
		if ($hoved != 3) {
			if ($hv <= 27) {
				$data ['text1'] = "2,5 " . 'cm';
			}
			if ($hv > 27 && $hv <= 37) {
				$data ['text1'] = "2,0 " . 'cm';
			}
			if ($hv > 37) {
				$data ['text1'] = "1 " . 'cm';
			}
		} else {
			$data ['text1'] = "-";
		}
		
		// ---------------------------------------------
		// Tilbygning
		// ---------------------------------------------
		
		if ($tilbyg == 2 || ($hoved == 2 && $antalVinklerVinkel == 2) || $hoved == 7 || ($hoved == 5 && $antalVinkler == 2)) {
			$data ['tilbyg_c'] = ( string ) ($mal_tilbygning) . (" " . 'cm');
			if ($hv_t <= 27) {
				$data ['til_h'] = "2,5 " . 'cm';
			}
			if ($hv_t > 27 && $hv_t <= 37) { 
				$data ['til_h'] = "2,0 " . 'cm';
			}
			if ($hv_t > 37) {
				$data ['til_h'] = "1 " . 'cm';
			}
		} else {
			
			if ($tilbyg == 3) {
				$data ['label19'] = 'Knast '; // "Knast"
				$data ['tilbyg_c'] = ( string ) ($mal_tilbygning) . (" " . 'cm');
				$data ['til_h'] = "-";
			} else {
				$data ['til_h'] = "-";
				$data ['tilbyg_b'] = "-";
				$data ['tilbyg_c'] = "-";
			}
		}
		// ---------------------------------------------
		// Vinkel
		// ---------------------------------------------
		if ($hoved == 2 || $hoved == 7 || ($hoved == 1 && $antalTilbygninger == 2) || $hoved == 5) {
			
			$data ['vinkel_c'] = ( string ) ($mal_vinkel) . (" " . 'cm');
			if ($hv_V <= 27) {
				$data ['vinkel_v'] = "2,5 " . 'cm';
			}
			if ($hv_V > 27 && $hv_t <= 37) {
				$data ['vinkel_v'] = "2,0 " . 'cm';
			}
			if ($hv_V > 37) {
				$data ['vinkel_v'] = "1 " . 'cm';
			}
		} else {
			if ($tilbyg == 3 && $hoved == 1 && $antalKnaster == 2) {
				$data ['label18'] = "Knast 2";
				$data ['vinkel_c'] = ( string ) ($mal_knast2) . (" " . 'cm');
				$data ['vinkel_v'] = "-";
			} else {
				
				$data ['vinkel_v'] = "-";
				$data ['vinkel_b'] = "-";
				$data ['vinkel_c'] = "-";
			}
		}
		
		$this->load->view ( '/include/header', $data );
		$this->load->view ( 'view_tagopbygning', $data );
	}
	public function decode() {
		if ($this->input->post ( 'txt' )) {
			$txt = $this->input->post ( 'txt' );
			$search = array (
					'End If',
					'If',
					'Then',
					'<>',
					'Select case',
					'For',
					'Case',
					'Next',
					'GoTo',
					'True',
					'False',
					"'",
					"Else",
					'And',
					'Or',
					'(iCount)',
					'iCount',
					'End Select',
					'Sqr',
					'Fix',
					'Set abe1 = husDb.OpenRecordset("SELECT SUM(beregn.',
					') AS Expr1 From Beregn")',
					'CStr',
					'Int',
					'abe1.Close',
					'Vis_Flex',
					'abe1("Expr1")',
					'todec',
					'CLng',
					'CSng' 
			);
			$replace = array (
					'}',
					'if (',
					'){',
					'!==',
					'switch (',
					'for (',
					'case',
					'}',
					'goto',
					'true',
					'false',
					'//',
					'} else {',
					'&&',
					'||',
					'[$i]',
					'$i',
					'}',
					'sqrt',
					'Floor',
					'###',
					'',
					'(string)',
					'(int)',
					'',
					'$this->model_ibf->Vis_Flex(',
					'$beregn->',
					'$this->todec',
					'(float)',
					'(float)' 
			);
			$subject = $txt;
			$newtxt = str_replace ( $search, $replace, $subject );
			$insert = array (
					'txt' => $newtxt 
			);
			$this->db->insert ( 'decode', $insert );
		}
		$query = $this->db->get ( 'decode' );
		foreach ( $query->result () as $row ) {
			$dbtxt = $row->txt;
		}
		$data ['newtxt'] = $dbtxt;
		$this->load->view ( 'decode', $data );
	}
	public function CheckBack($field, &$data, $morebck = false) {
		if ($this->session->userdata ( $field ) !== False) {
			$data ['retning'] [$field] = html_entity_decode ( $this->session->userdata ( $field ) );
		} else {
			// if ($morebck == false) {
			$data ['retning'] ['back'] = 'false';
			// }
		}
	}
}


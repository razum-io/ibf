<script>
$( document ).ready(function() {
	retning = (<?php echo json_encode($retning)?>);
	load_kunde(retning);
	});
</script>


<!-- Variables -->
<?php
$errors = array (
		'name' => 'errors',
		'id' => 'errors',
		'size' => 40 
);
$kundenavn = array (
		'name' => 'kundenavn',
		'id' => 'kundenavn',
		'class' => 'form-control',
		'placeholder' => 'Kunde navn',
		'size' => 40 
);
$adresse = array (
		'name' => 'adresse',
		'id' => 'adresse',
		'class' => 'form-control',
		'placeholder' => 'Adresse',
		'size' => 40 
);
$by = array (
		'name' => 'by',
		'id' => 'by',
		'class' => 'form-control',
		'placeholder' => 'By',
		'size' => 40 
);
$telefon = array (
		'name' => 'telefon',
		'id' => 'telefon',
		'class' => 'form-control',
		'placeholder' => 'Telefon',
		'size' => 40 
);
$tilbudsnr = array (
		'name' => 'tilbudsnr',
		'id' => 'tilbudsnr',
		'class' => 'form-control',
		'placeholder' => 'Tilbudsnummer',
		'size' => 40 
);
$udfort = array (
		'name' => 'udfort',
		'id' => 'udfort',
		'class' => 'form-control',
		'placeholder' => 'Udført af',
		'size' => 40 
);
$date = date('m.d.Y h:i:s', time());
$beregningstidspunkt = array (
		'name' => 'beregningstidspunkt',
		'id' => 'beregningstidspunkt',
		'class' => 'form-control',
		'value'=>$date,
		'placeholder' => $date,
		'size' => 40
);
$buttonNext = array (
		'name' => 'next',
		'id' => 'next',
		'class' => 'form-control next',
		'type' => 'button',
		'onclick' => 'val_kunde()',
		'content' => 'Frem ->' 
);
$back = "'" . base_url ( '/home/index' ) . "'";
$buttonBack = array (
		'name' => 'back',
		'id' => 'back',
		'class' => 'form-control next',
		'type' => 'button',
		'onclick' => 'window.location.href=' . $back,
		'content' => '<-- Tilbage' 
);
?>
<!-- FORM -->
<h3>Informationer om kunden</h3>
<?php 
echo form_open ( 'getdata/kunde', array ('id' => 'kunde' ) );
echo form_label ( '', 'errors', $errors ) . '<br>';
echo form_input ( $kundenavn ) . '<br>';
echo form_input ( $adresse ) . '<br>';
echo form_input ( $by ) . '<br>';
echo form_input ( $telefon ) . '<br>';
echo form_input ( $tilbudsnr ) . '<br>';
echo form_input ( $udfort ) . '<br>';
echo form_input ( $beregningstidspunkt ) . '<br>';
?>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Forhandler</h3>
	</div>
	<ul class="list-group">
    <li class="list-group-item">
<!--     programma no molssoftsales.dat nolasa rindas  -->
<!--     un ieliek šeit, bet tas ir bezjēdzīgi. -->
    </li>
  </ul>
</div>
<?php
echo form_button ( $buttonNext );
echo form_button ( $buttonBack );
echo form_close ();
?>
<!-- End form -->
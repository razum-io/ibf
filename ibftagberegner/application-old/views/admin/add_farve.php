<div id="container">
<?php
$farve = array (
		'name' => 'newfarve',
		'class' => 'form-control',
		'placeholder' => 'Farve',
		'size' => 40 
);
$buttonNext = array (
		'name' => 'next',
		'id' => 'next',
		'class' => 'form-control next',
		'type' => 'button',
		'onclick' => 'frmfarve()',
		'content' => 'Frem ->' 
);
$options = array (
		'DS' => 'Dobbelt-S',
		'UK' => 'Vinge Økonomi',
		'VUP' => 'Vinge Økonomi plus'
);
?>

<?php
echo form_open ( 'admin/addFarveTbl', array (
		'id' => 'addFarveTbl' 
) );
?>
Select type
<?php echo form_dropdown('type', $options, 'DS'); ?>
<br>

Add Farve
<?php

echo form_input ( $farve ) . '<br>';
echo form_button ( $buttonNext );
echo form_close ();
?>
</div>
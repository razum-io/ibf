//define all variables
var antalVinklerVinkel;
var antalVinkler;
var antalTilbygninger;
var antalKnaster;
var hoved;
var tilbyg;
var valm;
var kvist;
var dynValmVinkel = [];
var dynValmTop = [];
var dynValmType = [];
var dynValmPlacering = [];
var globalAntalValgfrie = 0;
// nullstil dyn variabler
for ( var i = 1; i <= 6; i++) {
	dynValmVinkel[i] = 0;
	dynValmTop[i] = 0;
	dynValmType[i] = 0;
	dynValmPlacering[i] = 0;
}
var laengehus;

function genererIllustration(typeNrIllustration, filenavn, tegn2D, tegn3D,
		maalsaetning, tegnVinkler, form) {
	globalAntalValgfrie = 0;
	filenavn = '';
	typeNrIllustration = String(typeNrIllustration);
	// retrieve ilustration data from typeNrIllustration
	typeHusHoved = parseInt(typeNrIllustration.slice(0, 1));
	typeTilbygning = parseInt(typeNrIllustration.slice(1, 2));
	typeValm = parseInt(typeNrIllustration.slice(2, 3));
	typeKvist = parseInt(typeNrIllustration.slice(4, 5));
	antalVinklerVinkel = parseInt(antalVinklerVinkel);
	antalVinkler = parseInt(antalVinkler);
	antalTilbygninger = parseInt(antalTilbygninger);
	antalKnaster = parseInt(antalKnaster);
	var laengehus = new bygningsdel;
	if ((typeHusHoved == 1 && antalTilbygninger !== 2)
			|| (typeHusHoved == 2 && antalVinklerVinkel !== 2 && antalTilbygninger == 0)) {
		globalAntalValgfrie = 2;
		laengehus.bredde = 124;
		laengehus.hoejde = 54;
		laengehus.rygningsRetning = 0;
		laengehus.x = 15;
		laengehus.y = 15;
		laengehus.side2GavlType = typeValm;
		laengehus.side2GavlTypeValgfri = true;
		laengehus.side3GavlType = typeValm;
		laengehus.side3GavlTypeValgfri = true;
		for ( var i = 1; i <= 6; i++) {
			if (dynValmPlacering[i] == 2) {
				laengehus.side2GavlType = dynValmType[i];
			}
			if (dynValmPlacering[i] == 3) {
				laengehus.side3GavlType = dynValmType[i];
			}
			laengehus.side1Bogstav = 'a';
			laengehus.side2Bogstav = 'b';
		}

	}
	if ((typeHusHoved == 1 && antalTilbygninger == 2)) {
		globalAntalValgfrie = 2;
		laengehus.bredde = 54;
		laengehus.hoejde = 124;
		laengehus.rygningsRetning = 1;
		laengehus.x = 80;
		laengehus.y = 15;
		laengehus.side1GavlType = typeValm;
		laengehus.side1GavlTypeValgfri = true;
		laengehus.side4GavlType = typeValm;
		laengehus.side4GavlTypeValgfri = true;

		for ( var i = 1; i <= 6; i++) {
			if (dynValmPlacering[i] == 1) {
				laengehus.side1GavlType = dynValmType[i];
			}
			if (dynValmPlacering[i] == 4) {
				laengehus.side4GavlType = dynValmType[i];
			}
		}

		laengehus.side1Bogstav = 'b';
		laengehus.side2Bogstav = 'a';

	}

	if (typeHusHoved == 2 && antalVinklerVinkel !== 2
			&& antalTilbygninger !== 1) {
		// Definition af selve vinklen
		globalAntalValgfrie = 3;
		var tempBygning10 = new bygningsdel;
		tempBygning10.bredde = 44; // Skal altid vęre lige tal!
		tempBygning10.hoejde = 80;
		tempBygning10.rygningsRetning = 1; // Lodret
		tempBygning10.side4GavlType = typeValm;
		tempBygning10.side4GavlTypeValgfri = true;

		for ( var i = 1; i <= 6; i++) {
			if (dynValmPlacering[i] == 44) {
				tempBygning10.side4GavlType = dynValmType[i];
			}
			if (dynValmPlacering[i] == 3) {
				laengehus.side3GavlType = dynValmType[i];
			}
			tempBygning10.side3Bogstav = "d";
			tempBygning10.side4Bogstav = "e";
		}
		laengehus.side4Tilbygning = new tilbygning;
		laengehus.side4TilbygningAktiv = true;
		laengehus.side4Tilbygning.placering = 4; // Hųjre side
		laengehus.side4Tilbygning.bygning = tempBygning10;

	}

	if (typeHusHoved == 2
			&& (antalVinklerVinkel == 2 || (antalVinklerVinkel == 1 && antalTilbygninger == 1))) {
		// Lęngehus
		globalAntalValgfrie = 2;
		laengehus.bredde = 54;
		laengehus.hoejde = 116;
		laengehus.rygningsRetning = 1;
		laengehus.x = 80;
		laengehus.y = 20;
		laengehus.side1GavlType = typeValm;
		laengehus.side1GavlTypeValgfri = true;
		laengehus.side4GavlType = typeValm;
		laengehus.side4GavlTypeValgfri = true;

		for ( var i = 1; i <= 6; i++) {
			if (dynValmPlacering[i] == 1) {
				laengehus.side1GavlType = dynValmType[i];
			}
			if (dynValmPlacering[i] == 4) {
				laengehus.side4GavlType = dynValmType[i];
			}

		}
		laengehus.side1Bogstav = "b";
		laengehus.side2Bogstav = "a";

		globalAntalValgfrie = globalAntalValgfrie + 1;

		var tempBygning20 = new bygningsdel;
		tempBygning20.bredde = 50; // Skal altid vęre lige tal!
		tempBygning20.hoejde = 44;
		tempBygning20.rygningsRetning = 0; // Lodret
		tempBygning20.side2GavlType = typeValm;
		tempBygning20.side2GavlTypeValgfri = true;

		for ( var i = 1; i <= 6; i++) {
			if (dynValmPlacering[i] == 22) {
				tempBygning20.side2GavlType = dynValmType[i];
			}
		}
		tempBygning20.side1Bogstav = "g";
		tempBygning20.side2Bogstav = "h";
		laengehus.side2Tilbygning = new tilbygning;
		laengehus.side2TilbygningAktiv = true;
		if (antalTilbygninger == 1) {
			laengehus.side2Tilbygning.placering = 3;
		} else {
			laengehus.side2Tilbygning.placering = 4; // Til hųjre for midten
		}
		laengehus.side2Tilbygning.bygning = tempBygning20;
		// Tilbygning i hųjre side:
		globalAntalValgfrie = globalAntalValgfrie + 1;

		tempBygning20 = new bygningsdel;
		tempBygning20.bredde = 50; // Skal altid vęre lige tal!
		tempBygning20.hoejde = 44;
		tempBygning20.rygningsRetning = 0; // Lodret
		tempBygning20.side3GavlType = typeValm;
		tempBygning20.side3GavlTypeValgfri = true;

		for ( var i = 1; i <= 6; i++) {
			if (dynValmPlacering[i] == "33") {
				tempBygning20.side3GavlType = dynValmType[i];
			}
		}

		tempBygning20.side1Bogstav = "d";
		tempBygning20.side3Bogstav = "e";

		laengehus.side3Tilbygning = new tilbygning;
		laengehus.side3TilbygningAktiv = true;
		laengehus.side3Tilbygning.placering = 0;

		laengehus.side3Tilbygning.bygning = tempBygning20;
	}
	if (typeHusHoved == 5) {
		// 'Vinkelhus 45 grader
		globalAntalValgfrie = 2;
		laengehus.bredde = 84;
		laengehus.hoejde = 44;
		laengehus.rygningsRetning = 0; // Vandret '0 = Vandret / 1 = Lodret / 2
		// = Ingen rygning
		laengehus.x = 115;
		laengehus.y = 15;
		laengehus.side3GavlType = typeValm;
		laengehus.side3GavlTypeValgfri = true;

		for ( var i = 1; i <= 6; i++) {
			if (dynValmPlacering[i] == 3) {
				laengehus.side3GavlType = dynValmType[i];
			}
		}
		laengehus.side4Bogstav = "a";
		laengehus.side3Bogstav = "b";

		// 'Knękket!
		tempBygning10 = new bygningsdel;

		tempBygning10.bredde = 50; // 'Skal altid vęre lige tal!
		tempBygning10.hoejde = 44;
		tempBygning10.rygningsRetning = 1; // 'Lodret
		if (antalVinkler < 2) {
			tempBygning10.side4GavlType = typeValm;
			tempBygning10.side4GavlTypeValgfri = true;

			for ( var i = 1; i <= 6; i++) {
				if (dynValmPlacering[i] == 24) {
					tempBygning10.side4GavlType = dynValmType[i];
				}
			}
		}
		tempBygning10.side3Bogstav = "d";
		tempBygning10.side4Bogstav = "e";
		laengehus.side2Tilbygning = new tilbygning;
		laengehus.side2TilbygningAktiv = true;
		laengehus.side2Tilbygning.placering = 0; // Hųjre side
		laengehus.side2Tilbygning.bygning = tempBygning10;

		if (antalVinkler > 1) {
			// 'Der skal vęre to vinkler på bygningen!
			tempBygning10 = new bygningsdel;
			tempBygning10.bredde = 44; // Skal altid vęre lige tal!
			tempBygning10.hoejde = 84;
			tempBygning10.rygningsRetning = 1;
			tempBygning10.side4GavlType = typeValm;
			tempBygning10.side4GavlTypeValgfri = true;

			for ( var i = 1; i <= 6; i++) {
				if (dynValmPlacering[i] == 244) {
					tempBygning10.side4GavlType = dynValmType[i];
				}
			}
			tempBygning10.side3Bogstav = "f";
			tempBygning10.side4Bogstav = "g";

			laengehus.side2Tilbygning.bygning.side4Tilbygning = new tilbygning;
			laengehus.side2Tilbygning.bygning.side4TilbygningAktiv = true;
			laengehus.side2Tilbygning.bygning.side4Tilbygning.placering = 0;
			laengehus.side2Tilbygning.bygning.side4Tilbygning.bygning = tempBygning10;
		}
	}
	if (typeHusHoved == 6) {
		// 'Lęngehus
		laengehus.bredde = 64;
		laengehus.hoejde = 64;
		laengehus.rygningsRetning = 0; // 'Vandret '0 = Vandret / 1 = Lodret /
		// 2 = Ingen rygning
		laengehus.x = 15;
		laengehus.y = 15;
		laengehus.side2GavlType = typeValm;
		laengehus.side3GavlType = typeValm;
		laengehus.side1Bogstav = "a";
		laengehus.side2Bogstav = "b";
	}
	if (typeHusHoved == 7) {
		// H-Hus
		globalAntalValgfrie = 4;
		laengehus.bredde = 92;
		laengehus.hoejde = 44;
		laengehus.rygningsRetning = 0; // Vandret '0 = Vandret / 1 = Lodret / 2
		// =
		// Ingen rygning
		laengehus.x = 15;
		laengehus.y = 15;
		// Dim countIII As Integer
		laengehus.side2GavlType = typeValm;
		laengehus.side3GavlType = typeValm;

		for ( var i = 1; i <= 6; i++) {
			if (dynValmPlacering[i] == 2) {
				laengehus.side2GavlType = dynValmType[i];
			}
			if (dynValmPlacering[i] == 3) {
				laengehus.side3GavlType = dynValmType[i];
			}
		}

		laengehus.side2GavlTypeValgfri = true;
		laengehus.side3GavlTypeValgfri = true;
		laengehus.side1Bogstav = "a";
		laengehus.side2Bogstav = "b";

		// ----------------------------------
		// Sammenkoblingen
		// ----------------------------------

		tempBygning20 = new bygningsdel;
		tempBygning20.bredde = 34; // Skal altid vęre lige tal!
		tempBygning20.hoejde = 38;
		tempBygning20.rygningsRetning = 1; // Lodret
		tempBygning20.side4GavlType = 1;
		tempBygning20.side2Bogstav = "d";
		tempBygning20.side4Bogstav = "e";

		laengehus.side4Tilbygning = new tilbygning;
		laengehus.side4TilbygningAktiv = true;
		laengehus.side4Tilbygning.placering = 2; // Midten
		laengehus.side4Tilbygning.bygning = tempBygning20;

		// ----------------------------------
		// ----------------------------------
		// Den anden side
		// ----------------------------------

		tempBygning20 = new bygningsdel;
		tempBygning20.bredde = 92; // Skal altid vęre lige tal!
		tempBygning20.hoejde = 44;
		tempBygning20.rygningsRetning = 0; // Lodret
		tempBygning20.side2GavlType = typeValm;
		tempBygning20.side3GavlType = typeValm;
		tempBygning20.side2GavlTypeValgfri = true;
		tempBygning20.side3GavlTypeValgfri = true;

		for ( var i = 1; i <= 6; i++) {
			if (dynValmPlacering[i] == 442) {
				tempBygning20.side2GavlType = dynValmType[i];
			}
			if (dynValmPlacering[i] == 443) {
				tempBygning20.side3GavlType = dynValmType[i];
			}
		}
		tempBygning20.side2Bogstav = "h";
		tempBygning20.side4Bogstav = "g";

		laengehus.side4Tilbygning.bygning.side4Tilbygning = new tilbygning;
		laengehus.side4Tilbygning.bygning.side4TilbygningAktiv = true;
		laengehus.side4Tilbygning.bygning.side4Tilbygning.placering = 2;
		laengehus.side4Tilbygning.bygning.side4Tilbygning.bygning = tempBygning20;
	}

	if (typeTilbygning == 2) {
		// Almindelig tilbygning
		if (typeHusHoved == 1 && antalTilbygninger !== 2) {
			globalAntalValgfrie = globalAntalValgfrie + 1;
			tempBygning20 = new bygningsdel;
			tempBygning20.bredde = 44; // Skal altid vęre lige tal!
			tempBygning20.hoejde = 50;
			tempBygning20.rygningsRetning = 1; // Lodret
			tempBygning20.side4GavlType = typeValm;
			tempBygning20.side4GavlTypeValgfri = true;

			for ( var i = 1; i <= 6; i++) {
				if (dynValmPlacering[i] == 44) {
					tempBygning20.side4GavlType = dynValmType[i];
				}
			}

			tempBygning20.side2Bogstav = "d";
			tempBygning20.side4Bogstav = "e";

			laengehus.side4Tilbygning = new tilbygning;
			laengehus.side4TilbygningAktiv = true;
			laengehus.side4Tilbygning.placering = 3; // Til hųjre for midten
			laengehus.side4Tilbygning.bygning = tempBygning20;

		}

		if (typeHusHoved == 1 & antalTilbygninger == 2) {
			// Tilbygning i venstre side:
			globalAntalValgfrie = globalAntalValgfrie + 1;

			tempBygning20 = new bygningsdel;
			tempBygning20.bredde = 50; // Skal altid vęre lige tal!
			tempBygning20.hoejde = 44;
			tempBygning20.rygningsRetning = 0; // Lodret
			tempBygning20.side2GavlType = typeValm;
			tempBygning20.side2GavlTypeValgfri = true;

			for ( var i = 1; i <= 6; i++) {
				if (dynValmPlacering[i] == "22") {
					tempBygning20.side2GavlType = dynValmType[i];
				}
			}

			tempBygning20.side1Bogstav = "d";
			tempBygning20.side2Bogstav = "e";

			laengehus.side2Tilbygning = new tilbygning;
			laengehus.side2TilbygningAktiv = true;
			laengehus.side2Tilbygning.placering = 3; // Til hųjre for midten
			laengehus.side2Tilbygning.bygning = tempBygning20;

			// Tilbygning i hųjre side:
			globalAntalValgfrie = globalAntalValgfrie + 1;

			tempBygning20 = new bygningsdel;
			tempBygning20.bredde = 50; // Skal altid vęre lige tal!
			tempBygning20.hoejde = 44;
			tempBygning20.rygningsRetning = 0; // Lodret
			tempBygning20.side3GavlType = typeValm;
			tempBygning20.side3GavlTypeValgfri = true;

			for ( var i = 1; i <= 6; i++) {
				if (dynValmPlacering[i] == 33) {
					tempBygning20.side3GavlType = dynValmType[i];
				}
			}
			tempBygning20.side1Bogstav = "g";
			tempBygning20.side3Bogstav = "h";

			laengehus.side3Tilbygning = new tilbygning;
			laengehus.side3TilbygningAktiv = true;
			laengehus.side3Tilbygning.placering = 1;
			laengehus.side3Tilbygning.bygning = tempBygning20;
		}

		if (typeHusHoved == 6) {
			globalAntalValgfrie = globalAntalValgfrie + 1;
			tempBygning20 = new bygningsdel;

			tempBygning20.bredde = 44; // Skal altid vęre lige tal!
			tempBygning20.hoejde = 50;
			tempBygning20.rygningsRetning = 1;
			tempBygning20.side4GavlType = typeValm;
			tempBygning20.side4GavlTypeValgfri = true;

			for ( var i = 1; i <= 6; i++) {
				if (dynValmPlacering[i] == 44) {
					tempBygning20.side4GavlType = dynValmType[i];
				}
			}
			tempBygning20.side2Bogstav = "d";
			tempBygning20.side4Bogstav = "e";

			laengehus.side4Tilbygning = new tilbygning;
			laengehus.side4TilbygningAktiv = true;
			laengehus.side4Tilbygning.placering = 2;
			laengehus.side4Tilbygning.bygning = tempBygning20;
		}

		if (typeHusHoved == 2 && antalVinklerVinkel !== 1) {
			globalAntalValgfrie = globalAntalValgfrie + 1;
			tempBygning20 = new bygningsdel;
			tempBygning20.bredde = 50; // Skal altid vęre lige tal!
			tempBygning20.hoejde = 34;
			tempBygning20.rygningsRetning = 0; // Vandret
			tempBygning20.side2GavlType = typeValm;
			tempBygning20.side2GavlTypeValgfri = true;
			for ( var i = 1; i <= 6; i++) {
				if (dynValmPlacering[i] == 422) {
					tempBygning20.side2GavlType = dynValmType[i];
				}
			}
			tempBygning20.side1Bogstav = "g";
			tempBygning20.side2Bogstav = "h";
			laengehus.side4Tilbygning.bygning.side2Tilbygning = new tilbygning;
			laengehus.side4Tilbygning.bygning.side2TilbygningAktiv = true;
			laengehus.side4Tilbygning.bygning.side2Tilbygning.placering = 3;
			laengehus.side4Tilbygning.bygning.side2Tilbygning.bygning = tempBygning20;
		}
	}
	if (typeTilbygning == 3) {
		// Knast

		if (typeHusHoved == 1) {
			if (antalKnaster > 1) {
				laengehus.y = laengehus.y + 10;
				laengehus.side1KnastAktiv = true;
				laengehus.side1KnastSide1Bogstav = "f";
				laengehus.side1KnastSide3Bogstav = "g";
			}
			laengehus.side4KnastAktiv = true;
			laengehus.side4KnastSide3Bogstav = "e";
			laengehus.side4KnastSide4Bogstav = "d";
		}

		if (typeHusHoved == 2) {
			laengehus.side4KnastAktiv = true;
			laengehus.side4KnastSide3Bogstav = "h";
			laengehus.side4KnastSide4Bogstav = "g";
		}
	}

	// Kviste
	if (typeHusHoved == 7) {
		if (typeKvist == 2) {
			// Kvist med gavl
			laengehus.side4Tilbygning.bygning.side4Tilbygning.bygning.kvistAktiv = true;
			laengehus.side4Tilbygning.bygning.side4Tilbygning.bygning.kvistType = 1;
		}
		if (typeKvist == 3) {
			// Kvist med gavl
			laengehus.side4Tilbygning.bygning.side4Tilbygning.bygning.kvistAktiv = true;
			laengehus.side4Tilbygning.bygning.side4Tilbygning.bygning.kvistType = 2;
		}
		if (typeKvist == 4) {
			// Kvist med gavl
			laengehus.side4Tilbygning.bygning.side4Tilbygning.bygning.kvistAktiv = true;
			laengehus.side4Tilbygning.bygning.side4Tilbygning.bygning.kvistType = 3;
		}

	} else {

		if (typeKvist == 2) {
			// Kvist med gavl
			laengehus.kvistAktiv = true;
			laengehus.kvistType = 1;
		}
		if (typeKvist == 3) {
			// Kvist med gavl
			laengehus.kvistAktiv = true;
			laengehus.kvistType = 2;
		}
		if (typeKvist == 4) {
			// Kvist med gavl
			laengehus.kvistAktiv = true;
			laengehus.kvistType = 3;
		}
	}

	if (tegnVinkler == true) {

		// Vi påsętter vinkel bogstaver her!
		switch (typeHusHoved) {
		case 1:

			if (typeTilbygning == "2") {
				if (antalTilbygninger == "2") {
					laengehus.side1BogstavVinkel = "c";
					laengehus.side3Tilbygning.bygning.side3BogstavVinkel = "f";
					laengehus.side2Tilbygning.bygning.side2BogstavVinkel = "i";

				} else {
					laengehus.side2BogstavVinkel = "c";
					laengehus.side4Tilbygning.bygning.side4BogstavVinkel = "f";

				}
			} else {
				laengehus.side3BogstavVinkel = "c";
			}
			break;
		case 2:
			if (antalVinklerVinkel !== 2 && typeTilbygning !== 2) {
				laengehus.side2BogstavVinkel = "c";
				laengehus.side4Tilbygning.bygning.side4BogstavVinkel = "f";

			} else {
				laengehus.side1BogstavVinkel = "c";
				laengehus.side3Tilbygning.bygning.side3BogstavVinkel = "f";
				laengehus.side2Tilbygning.bygning.side2BogstavVinkel = "i";
			}
			break;
		case 3:
			break;
		case 4:
			break;
		case 5:
			laengehus.side2Tilbygning.bygning = {};
			if (antalVinkler == 2) {
				laengehus.side2Tilbygning.bygning.side4Tilbygning.bygning.side4BogstavVinkel = "c";
			} else {

				laengehus.side2Tilbygning.bygning.side4BogstavVinkel = "c";
			}
			break;
		case 6:
			laengehus.side2BogstavVinkel = "c";
			if (typeTilbygning == "2") {
				laengehus.side4Tilbygning.bygning.side4BogstavVinkel = "f";

			}
			break;
		case 7:
			laengehus.side2BogstavVinkel = "c";
			laengehus.side3BogstavVinkel = "f";
			laengehus.side4Tilbygning.bygning.side4Tilbygning.bygning.side2BogstavVinkel = "i";
			break;
		}
	}
	if (form !== 'none') {
		if (form == 'tagtype') {
			addVariable('tag', tag, form);
			strlaengehus = JSON.stringify(laengehus);
			addVariable('globalAntalValgfrie', globalAntalValgfrie, form);
			addVariable('laengehus', strlaengehus, form);
			addVariable('dynValmPlacering', JSON.stringify(dynValmPlacering),
					form);
			addVariable('dynValmType', JSON.stringify(dynValmType), form);
		} else if (form == 'valm') {
			strlaengehus = JSON.stringify(laengehus);
			addVariable('laengehus', strlaengehus, form);
			addVariable('dynValmType', JSON.stringify(dynValmType), form);
		} else {

			strlaengehus = JSON.stringify(laengehus);
			addVariable('laengehus', strlaengehus, form);
			addVariable('dynValmPlacering', JSON.stringify(dynValmPlacering),
					form);
			addVariable('dynValmType', JSON.stringify(dynValmType), form);
		}
	}

	return laengehus;

	// addVariable('',);
	// tegnHuset(tegn2D, tegn3D, tegnVinkler, maalsaetning, filenavn,
	// typeHusHoved, laengehus);
}
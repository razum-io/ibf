<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class Model_color_parts extends CI_Model {
	function get($data=""){
		if($data==""){
			$query = $this->db->get('color_parts');
		}else{
			$query = $this->db->get_where('color_parts', $data);
		}
		return $query->result(); 
		// selekte pec uzstaditajiem laukiem $data
		// atgriez datus masiva (iespejams vairakas rindas)
	}
	
	function insert($data){
		$this->db->insert("color_parts", $data);
		// nem datus no $data masiva, lieto CI insert funkciju
	}
	
	function update($data,$id){
		$this->db->update("color_parts",$data, "id = ".$id);
	}
	
	function delete($data){
		$this->db->delete("color_parts",$data);
	}
	
	function get_id($id){
		$data = array('id'=>$id);
		$query = $this->db->get_where('color_parts', $data);
		$result = $query->result(); 
		return get_object_vars($result[0]);
	}
	
	public function get_varer($data){
		if($data==""){
			$query = $this->db->get('varer');
		}else{
			$query = $this->db->get_where('varer', $data);
		}
		return $query->result(); 
	}
}
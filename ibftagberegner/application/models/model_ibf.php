<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class Model_ibf extends CI_Model {
	public $name;
	function checkPost($post, $m) {
		if (isset ( $_POST [$post] )) {
			if ($m) {
				$sum = str_replace ( ",", ".", $this->input->post ( $post ) );
				return $sum * 1000;
			} else {
				return $this->input->post ( $post );
			}
		} else {
			return 'nope';
		}
	}
	function getFarve() {
		$stenvalgtnr = $this->session->userdata ( 'stenvalgtnr' );
		if ($stenvalgtnr == 0) {
			$stenvalgt = 'Dobbelt';
			$type = 'dobbelt';		// new color table
		}
		if ($stenvalgtnr == 3) {
			$stenvalgt = 'VŲ';
			$type = 'vinge';		// new color table
		}
		if ($stenvalgtnr == 4) {  
			$stenvalgt = 'VŲP';
			$type = 'vinge_plus';	// new color table
		}
		$query = $this->db->get_where ( 'farvetabel', array (
				$stenvalgt => '1' 
		) );
		
		$query2 = $this->db->query("select id,name,".$type."_file as image from color where $type=1 and hidden=0 and deleted=0 order by num");

		return $query2->result ();
	}
	function husnummer($gototagmetode = 0, $mtd_done = 0, $mtdk_done = "00") {
		global $Navn, $stenlengde, $fuldstensbredde, $dekbredde, $byggereduktion, $maxLegte, $minLegte, $DekDobbv, $fuldstensb, $halvstendek, $DekVindskH, $DekVindskV, $RygDek, $FaconRygDek, $RygAfslutDek, $FaconRygAfslutDek, $ValmBegyndDek, $PultstenDek, $PultVindDekV, $PultVindDekH;
		
		$this->session->unset_userdata ( 'knast_rpt' );
		
		$tmp1 = $mtd_done;
		$tmp2 = $mtdk_done;
		global $halvsten, $hhtype, $hhkvist, $knast_met, $dynValmType, $dynValmPlacering, $globalAntalValgfrie, $antalVinkler, $hhvalm, $mtd_done, $mtdk_done, $antalVinklerVinkel, $hhtilb, $metode, $facon, $rygstart, $rygafslut;
		global $hb_v, $hv_V, $hv, $hl_v, $hb, $hv_t, $hl, $p2, $rek, $mal;
		global $ra, $ka, $rat, $kat, $rav, $kav, $mal_hovedhus, $mal_vinkel, $mal_tilbygning, $mal_knast2;
		
		$facon = $this->session->userdata ( 'facon' );
		if ($facon == '0' || $facon == 'False') {
			$facon = false;
		} else {
			$facon = true;
		}
		$p2 = 0.017453292;
		$mtd_done = $tmp1;
		$mtdk_done = $tmp2;
		$hhtype = $this->session->userdata ( 'hoved' );
		$hhtilb = $this->session->userdata ( 'tilbyg' );
		$hhvalm = $this->session->userdata ( 'valm' );
		$hhkvist = $this->session->userdata ( 'kvist' );
		$genberegningAfIndlaes = $this->session->userdata ( 'genberegningAfIndlaes' );
		$globalAntalValgfrie = intval ( $this->session->userdata ( 'globalAntalValgfrie' ) );
		$dynValmType = json_decode ( $this->session->userdata ( 'dynValmType' ), true );
		$dynValmPlacering = json_decode ( $this->session->userdata ( 'dynValmPlacering' ), true );
		$dynValmVinkel = json_decode ( $this->session->userdata ( 'dynValmVinkel' ), true );
		$dynValmTop = json_decode ( $this->session->userdata ( 'dynValmTop' ), true );
		$antalVinklerVinkel = $this->session->userdata ( 'antalVinklerVinkel' );
		$antalVinkler = $this->session->userdata ( 'antalVinkler' );
		$antalTilbygninger = $this->session->userdata ( 'antalTilbygninger' );
		$antalKnaster = $this->session->userdata ( 'antalKnaster' );
		$hb = $this->session->userdata ( 'hb' );
		$hl = $this->session->userdata ( 'hl' );
		$hv = $this->session->userdata ( 'hv' );
		$kvistantal = $this->session->userdata ( 'antalkviste' );
		$khb = $this->session->userdata ( 'khb' );
		$khl = $this->session->userdata ( 'khl' );
		$khv = $this->session->userdata ( 'khv' );
		$khvv = $this->session->userdata ( 'khvv' );
		$khvb = $this->session->userdata ( 'khvb' );
		$valm_v1 = $this->session->userdata ( 'valm_v1' );
		$valm_v3 = $this->session->userdata ( 'valm_v3' );
		$mal_hovedhus = $this->session->userdata ( '' );
		$mal_vinkel = $this->session->userdata ( '' );
		$mal_tilbygning = $this->session->userdata ( '' );
		
		$hl_t = $this->session->userdata ( 'hl_t' );
		$hb_t = $this->session->userdata ( 'hb_t' );
		$hv_t = $this->session->userdata ( 'hv_t' );
		$hl_v = $this->session->userdata ( 'hl_v' );
		$hb_v = $this->session->userdata ( 'hb_v' );
		$hv_V = $this->session->userdata ( 'hv_V' );
		$knhl = $this->session->userdata ( 'knhl' );
		$knhb = $this->session->userdata ( 'knhb' );
		$knra = $this->session->userdata ( 'knra' );
		$knka = $this->session->userdata ( 'knka' );
		$knhl2 = $this->session->userdata ( 'knhl2' );
		$knhb2 = $this->session->userdata ( 'knhb2' );
		$knra2 = $this->session->userdata ( 'knra2' );
		$knka2 = $this->session->userdata ( 'knka2' );
		// if tagmetode has been trigered, then after form submit it resumes code execution after call
		
		if ($gototagmetode == 1) {
			$spurgtOmTagmetode = true;
			goto afttagmetode;
		}
		$spec_met = $this->session->userdata ( 'spec_met' );
		if ($spec_met == 'kvist') {
			goto spec_metode;
		}
		$spurgtOmTagmetode = false;
		$this->hent_tilg ();
		// tomdatabase
		if ($genberegningAfIndlaes == 'false') {
			// Vi spÅ³rger kun hvis det ikke er en genberegning - Vinge Å²konomi Plus konvertering!
			$metode = 1;
			
			if ($globalAntalValgfrie > 0) {
				for($i = 1; $i <= $globalAntalValgfrie; $i ++) {
					if ($dynValmType [$i] == 1 || $dynValmType [$i] == 3) {
						$spurgtOmTagmetode = True;
						redirect ( 'home/tagmetode' );
						break;
					}
				}
			} else {
				if ($hhvalm == 1 or $hhvalm == 3) {
					redirect ( 'home/tagmetode' );
				}
			}
		}
		// after tagmetode has called
		afttagmetode:
		// if husnr == 14101 { beregnForskudt()}
		$knast_met = false;
		$metode = $this->session->userdata ( 'metode' );
		
		switch ($hhtype) {
			
			case 1 :
				$this->beregnHoved ( $hb, $hl, $hv, $mal_hovedhus, $ra, $ka, 1 );
				break;
			case 2 :
				$this->beregnHoved ( $hb, $hl, $hv, $mal_hovedhus, $ra, $ka, 1 );
				$this->beregn_vinkel ();
				if ($antalVinklerVinkel == 2) {
					// Der skal lige laves beregning af vinkel 2
					for($i = 1; $i <= $globalAntalValgfrie; $i ++) {
						if ($dynValmPlacering [$i] == "22") {
							$this->beregn_tb ( $hl_t, $hb_t, $hv_t, $hv, $dynValmType [$i], $hb, $rat, $kat );
						}
					}
				}
				break;
			case 3 :
				$this->ens_frit ();
				break;
			case 4 :
				$this->ens_veg ();
				break;
			case 5 :
				// Vinkelhus 45 grader
				$this->beregnHoved ( $hb, $hl, $hv, $mal_hovedhus, $ra, $ka, 3 ); // kald beregning af hovedhus
				
				$this->vinkel45_samling ( $hv, $hb, $hb_v );
				$this->beregnHoved ( $hb_v, $hl_v, $hv_V, $mal_vinkel, $rav, $kav, 4 ); // kald beregning af hovedhus
				
				if ($antalVinkler == 2) {
					$this->vinkel45_samling ( $hv_V, $hb_v, $hb_t );
					$this->beregnHoved ( $hb_t, $hl_t, $hv_t, $mal_tilbygning, $rat, $kat, 5 ); // kald beregning af hovedhus
				}
				break;
			case 6 :
				$this->beregnHoved ( $hb, $hl, $hv, $mal_hovedhus, $ra, $ka, 1 ); // Kald beregning af almindeligt hovedhus, selvom pyramidetag
				break; // Gammel metode: $this->beregnHoved //Kald beregning af almindeligt hovedhus, selvom pyramidetag
			case 7 :
				// Beregn hovedhus 1:
				$this->beregnHoved ( $hb, $hl, $hv, $mal_hovedhus, $ra, $ka, 1 ); // kald beregning af hovedhus 1
				
				$this->beregn_tb ( ($hl_t / 2), $hb_t, $hv_t, $hv, 0, $hb, $rat, $kat, 4 ); // Den ene halvdel sidder sammen med hovedhuset
				$gemKat_tilbyg = $kat; // _tilbyg
				
				$this->beregn_tb ( ($hl_t / 2), $hb_t, $hv_t, $hv_V, 0, $hb_v, $rav, $kav, 5 ); // Den anden halvdel sidder pĆ� det andet hovedhus
				                                                                                // kat_tilbyg = kat_tilbyg + gemKat_tilbyg
				$kat = $kat + $gemKat_tilbyg;
				
				// Beregn hovedhus 2:
				$this->beregnHoved ( $hb_v, $hl_v, $hv_V, $mal_vinkel, $rav, $kav, 2 ); // kald beregning af hovedhus 2
				$this->session->unset_userdata ( 'hv7_2tb_gone1' );
				$this->session->unset_userdata ( 'hv7_2tb_gone2' );
				break;
		}
		
		switch ($hhtilb) {
			case 2 :
				// $this->beregn_tb
				if ($hhtype == 2) {
					// Tilbygningen sidder fast pĆ�Ā� vinklen og ikke hovedhuset!
					for($i = 1; $i <= $globalAntalValgfrie; $i ++) {
						if ($dynValmPlacering [$i] == "22") {
							$this->beregn_tb ( $hl_t, $hb_t, $hv_t, $hv, ( int ) $dynValmType [$i], $hb, $rat, $kat );
							break;
						}
					}
				} else {
					
					if ($hhtype == 1 && $antalTilbygninger == 2) {
						
						for($i = 1; $i <= $globalAntalValgfrie; $i ++) {
							if ($dynValmPlacering [$i] == "22") {
								// Tilbygning 1
								$this->beregn_tb ( $hl_t, $hb_t, $hv_t, $hv, ( int ) $dynValmType [$i], $hb, $rat, $kat );
								$mal_gem = $mal_tilbygning;
							}
							if ($dynValmPlacering [$i] == "33") {
								// Tilbygning 2:
								$this->beregn_tb ( $hl_v, $hb_v, $hv_V, $hv, ( int ) $dynValmType [$i], $hb, $rav, $kav );
								$mal_vinkel = $mal_tilbygning;
								$mal_tilbygning = $mal_gem;
							}
						}
					} else {
						
						for($i = 1; $i <= $globalAntalValgfrie; $i ++) {
							if ($dynValmPlacering [$i] == "44") {
								$this->beregn_tb ( $hl_t, $hb_t, $hv_t, $hv, ( int ) $dynValmType [$i], $hb, $rat, $kat );
								break;
							}
						}
					}
				}
				break;
			case 3 :
				// Her skal vi tjekke om der er spurgt efter Tagmetode!
				
				if ($spurgtOmTagmetode == false) {
					redirect ( 'home/tagmetode' );
				}
				
				$this->beregnKnast ( $knra, $knka, $knhb, $knhl, $mal_tilbygning, 1 );
				
				if ($antalKnaster == 2 && $hhtype == 1) {
					$this->beregnKnast ( $knra2, $knka2, $knhb2, $knhl2, $mal_knast2, 2 );

				}
				break;
			case 4 :
				break;
		}
		switch ($hhtype) {
			// -----------------------------------
			// Lengehus
			// -----------------------------------
			case 1 :
				for($i = 1; $i <= $globalAntalValgfrie; $i ++) {
					if ($dynValmType [$i] == 2) {
						// Helvalm
						
						if (strlen ( $dynValmPlacering [$i] ) >= 2) {
							// Tilbygning
							if ($antalTilbygninger == 2) {
								if ($dynValmPlacering [$i] == "22") {
									// Tilbygning 1
									$this->helvalm_b ( $dynValmVinkel [$i], $hb_t, $hv_t, 1, $rat, false ); // kalder beregning af hus med helvalm pa tilbygning
								}
								if ($dynValmPlacering [$i] == "33") {
									// Tilbygning 2
									$this->helvalm_b ( $dynValmVinkel [$i], $hb_v, $hv_V, 1, $rav, false ); // kalder beregning af hus med helvalm pa tilbygning
								}
							} else {
								$this->helvalm_b ( $dynValmVinkel [$i], $hb_t, $hv_t, 1, $rat, false ); // kalder beregning af hus med helvalm pa tilbygning
							}
						} else {
							$this->helvalm_b ( $dynValmVinkel [$i], $hb, $hv, 1, $ra, false ); // kalder beregning af hus med helvalm
						}
					}
					if ($dynValmType [$i] == 3) {
						// Halvvalm
						if (strlen ( $dynValmPlacering [$i] ) > 2) {
							// Tilbygning
							$this->halvv_b ( $dynValmVinkel [$i], $dynValmTop [$i], $hv_t, 1, $mal_tilbygning );
						} else {
							$this->halvv_b ( $dynValmVinkel [$i], $dynValmTop [$i], $hv, 1, $mal_hovedhus );
						}
					}
				}
				break;
			// -----------------------------------
			// Vinkelhus
			// -----------------------------------
			case 2 :
				for($i = 1; $i <= $globalAntalValgfrie; $i ++) {
					if ($antalVinklerVinkel == 2 || $hhtilb == 2) {
						
						if ($dynValmType [$i] == 2) {
							// Helvalm
							
							// Vinkelhus med 2 vinkler!
							if (strlen ( $dynValmPlacering [$i] ) == 1) {
								// Vinkstrlen
								if ($dynValmPlacering [$i] == "1" || ($dynValmPlacering [$i] == "4" && $antalVinklerVinkel == 2)) {
									$this->helvalm_b ( $dynValmVinkel [$i], $hb, $hv, 1, $ra, true ); // kalder beregning af hus med helvalm
								} else {
									$this->helvalm_b ( $dynValmVinkel [$i], $hb, $hv, 1, $ra, false ); // kalder beregning af hus med helvalm
								}
							}
							
							if (strlen ( $dynValmPlacering [$i] ) == 2) {
								// Ä†ā€°n af vinklerne
								if ($dynValmPlacering [$i] == "22") {
									$this->helvalm_b ( $dynValmVinkel [$i], $hb_t, $hv_t, 1, $rat, false ); // kalder beregning af hus med helvalm
								} else {
									$this->helvalm_b ( $dynValmVinkel [$i], $hb_v, $hv_V, 1, $rav, false ); // kalder beregning af hus med helvalm
								}
							}
						}
						
						if ($dynValmType [$i] == 3) {
							// Halvvalm
							if (strlen ( $dynValmPlacering [$i] ) == 1) {
								// Vinklen
								$this->halvv_b ( $dynValmVinkel [$i], $dynValmTop [$i], $hv, 1, $mal_hovedhus );
							}
							
							if (strlen ( $dynValmPlacering [$i] ) == 2) {
								// Ä†ā€°n af vinklerne
								if ($dynValmPlacering [$i] == "22") {
									$this->halvv_b ( $dynValmVinkel [$i], $dynValmTop [$i], $hv_t, 1, $mal_tilbygning );
								} else {
									$this->halvv_b ( $dynValmVinkel [$i], $dynValmTop [$i], $hv_V, 1, $mal_vinkel );
								}
							}
						}
					} else {
						// Almindeligt vinkelhus
						if ($dynValmType [$i] == 2) {
							if (strlen ( $dynValmPlacering [$i] ) == 1) {
								// Vinklen
								if ($dynValmPlacering [$i] == "3") {
									$this->helvalm_b ( $dynValmVinkel [$i], $hb, $hv, 1, $ra, true ); // kalder beregning af hus med helvalm
								} else {
									$this->helvalm_b ( $dynValmVinkel [$i], $hb, $hv, 1, $ra, false ); // kalder beregning af hus med helvalm
								}
							}
							
							if (strlen ( $dynValmPlacering [$i] ) == 2) {
								// Vinklen
								$this->helvalm_b ( $dynValmVinkel [$i], $hb_v, $hv_V, 1, $rav, false ); // kalder beregning af hus med helvalm
							}
							if (strlen ( $dynValmPlacering [$i] ) == 3) {
								// Tilbygning
								$this->helvalm_b ( $dynValmVinkel [$i], $hb_t, $hv_t, 1, $rat, false ); // kalder beregning af hus med helvalm pa tilbygning
							}
						}
						
						if ($dynValmType [$i] == 3) {
							// Halvvalm
							if (strlen ( $dynValmPlacering [$i] ) == 1) {
								// Vinklen
								$this->halvv_b ( $dynValmVinkel [$i], $dynValmTop [$i], $hv, 1, $mal_hovedhus );
							}
							
							if (strlen ( $dynValmPlacering [$i] ) == 2) {
								// Vinklen
								$this->halvv_b ( $dynValmVinkel [$i], $dynValmTop [$i], $hv_V, 1, $mal_vinkel );
							}
							if (strlen ( $dynValmPlacering [$i] ) == 3) {
								// Tilbygning
								$this->halvv_b ( $dynValmVinkel [$i], $dynValmTop [$i], $hv_t, 1, $mal_tilbygning );
							}
						}
					}
				}
				break;
			case 5 :
				// -----------------------------------
				// Vinkelhus 45 grader
				// -----------------------------------
				for($i = 1; $i <= $globalAntalValgfrie; $i ++) {
					if ($dynValmType [$i] == 2) {
						// Helvalm
						
						if (strlen ( $dynValmPlacering [$i] ) == 1) {
							// Vinklen
							$this->helvalm_b ( $dynValmVinkel [$i], $hb, $hv, 1, $ra, false ); // kalder beregning af hus med helvalm
						}
						
						if (strlen ( $dynValmPlacering [$i] ) == 2) {
							$this->helvalm_b ( $dynValmVinkel [$i], $hb_v, $hv_V, 1, $rav, false ); // kalder beregning af hus med helvalm
						}
						if (strlen ( $dynValmPlacering [$i] ) == 3) {
							// Tilbygning
							$this->helvalm_b ( $dynValmVinkel [$i], $hb_t, $hv_t, 1, $rat, false ); // kalder beregning af hus med helvalm pa tilbygning
						}
					}
					
					if ($dynValmType [$i] == 3) {
						// Halvvalm
						if (strlen ( $dynValmPlacering [$i] ) == 1) {
							// Vinklen
							$this->halvv_b ( $dynValmVinkel [$i], $dynValmTop [$i], $hv, 1, $mal_hovedhus );
						}
						
						if (strlen ( $dynValmPlacering [$i] ) == 2) {
							// Vinklen
							$this->halvv_b ( $dynValmVinkel [$i], $dynValmTop [$i], $hv_V, 1, $mal_vinkel );
						}
						if (strlen ( $dynValmPlacering [$i] ) == 3) {
							// Tilbygning
							$this->halvv_b ( $dynValmVinkel [$i], $dynValmTop [$i], $hv_t, 1, $mal_tilbygning );
						}
					}
				}
				break;
			
			// -----------------------------------
			// Pyramidetag
			// -----------------------------------
			case 6 :
				// avoid executing second time
				if (!$this->session->userdata('repeat6')){
					// Her er det kun tilbygningen der er $dynamisk!
					$funktionsgennemlob = 2;
					
					$this->helvalm_b ( $valm_v1, $hb, $hv, $funktionsgennemlob, $ra, false ); // kalder beregning af hus med helvalm
					
					for($i = 1; $i <= $globalAntalValgfrie; $i ++) {
						if ($dynValmType [$i] == 2) {
							// Helvalm
							if (strlen ( $dynValmPlacering [$i] ) > 2) {
								$this->helvalm_b ( $dynValmVinkel [$i], $hb_t, $hv_t, 1, $rat, false ); // kalder beregning af hus med helvalm
							}
						}
						if ($dynValmType [$i] == 3) {
							// Halvvalm
							if (strlen ( $dynValmPlacering [$i] ) > 2) {
								$this->halvv_b ( $dynValmVinkel [$i], $dynValmTop [$i], $hv_t, 1, $mal_tilbygning );
							}
						}
					}
					$this->session->set_userdata('repeat6',1);
				}

			// -----------------------------------
			// H-Hus
			// -----------------------------------
			case 7 :
				// if ( hhtype = 7 ){
				// Beregninger pa $dynamiske valme
				// Dim $i As Integer
				
				for($i = 1; $i <= $globalAntalValgfrie; $i ++) {
					if ($dynValmType [$i] == 2) {
						// Helvalm
						
						if (strlen ( $dynValmPlacering [$i] ) > 2) {
							$this->helvalm_b ( $dynValmVinkel [$i], $hb_v, $hv_V, 1, $rav, false ); // kalder beregning af hus med helvalm
						} else {
							$this->helvalm_b ( $dynValmVinkel [$i], $hb, $hv, 1, $ra, false ); // kalder beregning af hus med helvalm
						}
					}
					if ($dynValmType [$i] == 3) {
						// Halvvalm
						if (strlen ( $dynValmPlacering [$i] ) > 2) {
							$this->halvv_b ( $dynValmVinkel [$i], $dynValmTop [$i], $hv_V, 1, $mal_vinkel );
						} else {
							$this->halvv_b ( $dynValmVinkel [$i], $dynValmTop [$i], $hv, 1, $mal_hovedhus );
						}
					}
				}
				break;
		}
		
		if ($hhkvist > 1) {
			$special_tagmetode = false;
			
			if ($hhvalm == 2 && $metode == 1) {
				redirect ( 'home/tagmetode/1' );
				spec_metode:
				$special_tagmetode = true;
			}
			for($l = 1; $l <= $kvistantal; $l ++) {
				$this->beregnKvist ( $khl, $khb, $khv, $khvv, $khvb ); // kalder beregning af kvist
			}
			if ($special_tagmetode == true) {
				$metode = 1; // Metode settes tilbage til 1
			}
		}
		// data for tagopbygning
		if (! $this->session->userdata ( 'res_hl' )) {
			$this->session->set_userdata ( array (
					'res_hb' => $hb,
					'res_hl' => $hl,
					'res_hv' => $hv 
			) );
		}
		if (! $this->session->userdata ( 'res_ra' )) {
			$this->session->set_userdata ( array (
					'res_ra' => $ra,
					'res_ka' => $ka 
			) );
		}
		$this->session->set_userdata ( array (
				'maxLegte' => $maxLegte,
				'mal' => $mal,
				// 'res_hb' => $hb,
				// 'res_hl' => $hl,
				// 'res_hv' => $hv,
				'res_khb' => $khb,
				'res_khl' => $khl,
				'res_khv' => $khv,
				'res_khvv' => $khvv,
				'res_khvb' => $khvb,
				'res_hl_t' => $hl_t,
				'res_hb_t' => $hb_t,
				'res_hv_t' => $hv_t,
				'res_hl_v' => $hl_v,
				'res_hb_v' => $hb_v,
				'res_hv_V' => $hv_V,
				// 'res_knhl' => $knhl,
				// 'res_knhb' => $knhb,
				'res_knra' => $knra,
				'res_knka' => $knka,
				// 'res_knhl2' => $knhl2,
				// 'res_knhb2' => $knhb2,
				'res_knra2' => $knra2,
				'res_knka2' => $knka2,
				// 'res_ra' => $ra,
				// 'res_ka' => $ka,
				'res_rat' => $rat,
				'res_kat' => $kat,
				'res_rav' => $rav,
				'res_kav' => $kav,
				'res_mal_hovedhus' => $mal_hovedhus,
				'res_mal_vinkel' => $mal_vinkel,
				'res_mal_tilbygning' => $mal_tilbygning,
				'res_mal_knast2' => $mal_knast2 
		) );
		
		redirect ( 'home/beregn' );
	}
	public function beregnKvist(&$lengde, &$bredde, &$vinkel, &$valmv, &$valmtop) {
		global $hv, $stenlengde, $mal, $mal_hovedhus, $hhkvist, $metode, $facon;
		global $Navn, $stenlengde, $fuldstensbredde, $dekbredde, $byggereduktion, $maxLegte, $minLegte, $DekDobbv, $fuldstensb, $halvstendek, $DekVindskH, $DekVindskV, $RygDek, $FaconRygDek, $RygAfslutDek, $FaconRygAfslutDek, $ValmBegyndDek, $PultstenDek, $PultVindDekV, $PultVindDekH;
		global $ra, $ka, $rat, $kat, $rav, $kav, $mal_hovedhus, $mal_vinkel, $mal_tilbygning, $mal_knast2;
		$tagunder = $this->session->userdata ( 'tagunder' );
		$this->Hentstendata ();
		if ($tagunder == "understrygning") {
			$maxLegte = ($maxLegte - 10);
		}
		$p2 = 1.745329E-2;
		$tagok = $this->session->userdata ( 'tag' );
		$_SESSION['dobbeltving'] = 0;
		$vindsk_v = 0;
		$vindsk_h = 0;
		$tbs = (($bredde / 2) * Tan ( $p2 * $vinkel )); // kvistens hĆ…Ā³jde
		$ths = sqrt ( $tbs * $tbs + ($bredde / 2) * ($bredde / 2) ); // kvistens "tagsĆ„Ā·de"
		$tbt = $tbs * (Tan ( $p2 * (90 - $hv) )); // "besparelsens" tagsĆ„Ā·de
		$tr = $this->todec ( sqrt ( $tbs * $tbs + $tbt * $tbt ) ); // rygningslengde ??????????????????????
		                                                           
		// -----------------------------
		                                                           // Udregning af besparet areal
		
		$tsukv = round ( $this->todec ( $lengde / Cos ( $p2 * $hv ) ) ); // Tagsiden under kvisten
		$kvmukv = (($tsukv / 1000) * ($bredde / 1000));
		
		$tsovkv = round ( $this->todec ( $tbs / Sin ( $p2 * $hv ) ) ); // Tagside under tagsten inderst pÄ†ļæ½ kvisten, imod hovedhuset
		$kvmovkv = ((($tsovkv / 1000) * ($bredde / 1000)) / 2);
		
		$besparede_kvm = $kvmukv + $kvmovkv;
		
		// -----------------------------
		
		// MsgBox (tr)
		$RED = $this->RED_hh ( $vinkel );
		$kat_kvist = $this->KolonnerM1 ( ($lengde + $tr) );
		$katt = $this->KolonnerM1 ( $tr );
		$kath = $this->KolonnerM1 ( $bredde );
		// rat = antalrekker_hh(ths, RED, stenlengde, maxLegte) //antal rekker pÄ†ļæ½ kvist
		$rat_kvist = $this->antalrekker_hh ( $ths, $RED, $stenlengde, $maxLegte ); // antal rekker pÄ†ļæ½ kvist
		                                                                           
		// rath1 = (Floor(tbt / (mal * 10)) + 1) + 1 //antal rekker pÄ†ļæ½ hus hvor kvist er
		                                                                           
		//
		if ($mal == 0) {
			$mal = $mal_hovedhus;
		}
		$rath = Floor ( ($lengde - $stenlengde) / ($mal * 10) ) + 1;
		//
		switch ($hhkvist) {
			case 2 :
				if ($metode == 3) {
					$kat_kvist = $kat_kvist - 1;
					$vindsk_v = $rat_kvist; // rat
					$vindsk_h = $rat_kvist; // rat
				}
				
				if ($metode == 2) {
					$kat_kvist = $kat_kvist - 1;
					$_SESSION['dobbeltving'] = $rat_kvist; // rat
				}
				break;
			case 3 :
				// Call helvalm_b(valmv, valmtop, vinkel, 1, rat, false)
				break;
			case 4 :
				if ($metode == 3) {
					$kat_kvist = $kat_kvist - 1;
					$vindsk_v = $rat_kvist; // rat
					$vindsk_h = $rat_kvist; // rat
				}
				if ($metode == 2) {
					$kat_kvist = $kat_kvist - 1;
					$_SESSION['dobbeltving'] = $rat_kvist; // rat
				}
				// Call halvv_b(valmv, valmtop, vinkel, 1, mal)
				break;
		}
		
		
		
		$ahes1 = (2 * $rat_kvist * $kat_kvist) - 2 * $rat_kvist * ($katt / 2); // antal sten pÄ†ļæ½ kvist
		$ahes2 = $rath * $kath; // (kath / 2) //antal sparede sten =12
		$ahes = $ahes1 - $ahes2; // antal hele sten
		                         // srl = $this->todec((2 * sqrt(tr ^ 2 + ths ^ 2)) / 1000) //skotrendelengde
		$srl = $this->todec ( (2 * sqrt ( $tr * $tr + $ths * $ths )) ); // skotrendelengde
		                                                                
		// ars = (Floor((($lengde + tr - RygAfslutDek)) / (Rygdek))) + 1 //Antal rygningssten
		
		$kvm = $this->todec ( 2 * ((($lengde / 1000 * $ths) / 1000) + (($ths / 1000 * ($tr / 2) / 1000))) ) - $besparede_kvm; // kvm tag
		$Klegte = $this->todec ( ((2 * ($rat_kvist * ($lengde + ($tr / 2)))) / 1000) - (Floor ( $tbt / ($mal * 10) + 1 ) * ($bredde / 1000)) );
		
		// FĆ…Ā³lgende kommer de beregnede data ind i databasen tagst.mdb
		
		// b_hh.Requery
		// Nye data kommes i databasen tagst.mdb - table "Beregn"
		
		$arr ["Type"] = 7;
		
		$arr ["normalsten"] = $ahes;
		// $arr["normalsten"] = 0 //I henhold til rettelse fra jens, flyttes sten fra taget over pÄ†ļæ½ kvisten, sÄ†ļæ½ vi regner ikke lengere med normalsten!
		
		$arr ["Dobbeltvinget"] = $_SESSION['dobbeltving'];
		$arr ["halvsten"] = 0;
		if ($mal >= 31 && $mal < 33) {
			$arr ["VindskH31"] = $vindsk_h;
			$arr ["VindskV31"] = $vindsk_v;
		}
		if ($mal >= 33 && $mal < 35) {
			$arr ["VindskH33"] = $vindsk_h;
			$arr ["VindskV33"] = $vindsk_v;
		}
		if ($mal >= 35 && $mal < 37) {
			$arr ["VindskH35"] = $vindsk_h;
			$arr ["VindskV35"] = $vindsk_v;
		}
		
		$arr ["Pultsten"] = 0;
		$arr ["PultvindskH"] = 0;
		$arr ["PultvindskV"] = 0;
		
		if ($facon == true) {
			$ars = (Floor ( (($lengde + $tr - $RygAfslutDek) / 1000) * 2.7 )); // Antal rygningssten
			
			$arr ["faconrygning"] = $ars;
			$arr ["faconrygafslut"] = 0;
			if (! ($hhkvist == 3 || $hhkvist == 4)) {
				$arr ["faconrygstart"] = 1;
			}
		} else {
			$ars = (Floor ( (($lengde + $tr - $RygAfslutDek) / 1000) * 3.3 )); // Antal rygningssten
			
			$arr ["rygafslut"] = 0;
			$arr ["rygning"] = $ars;
			if (! ($hhkvist == 3 || $hhkvist == 4)) {
				$arr ["rygstart"] = 1;
			}
		}
		$arr ["valmbegynder"] = 0;
		$arr ["valmkappe"] = 0;
		$arr ["kvm"] = $kvm;
		$arr ["legte"] = $Klegte;
		$arr ["legteafstand"] = $mal;
		$arr ["fuglegitter"] = ($lengde * 2);
		
		$arr ["alurullerRygninger"] = $tr;
		
		// $arr["alurullerRygninger"] = ($lengde + tr)
		switch ($metode) {
			case 1 :
				$arr ["Metode"] = "Afslutning med sternbredt";
				break;
			case 2 :
				$arr ["metode"] = "Afslutning med dobbeltvinget";
				break;
			case 3 :
				$arr ["metode"] = "Afslutning med vindskedesten";
				break;
		}
		$arr ["navn"] = $tagok + ".bmp";
		$arr ["hustype"] = $tagok;
		
		$arr ["skotrende"] = $srl;
		$this->adddatadb ( $arr, true );
		
		switch ($hhkvist) {
			case 2 :
				break;
				break;
			case 3 :
				$this->helvalm_b ( $valmv, $valmtop, $vinkel, 1, $rat_kvist, false );
				break;
			case 4 :
				$this->halvv_b ( $valmv, $valmtop, $vinkel, 1, $mal );
				break;
		}
	}
	public function KolonnerM1($lengde) {
		global $fuldstensbredde, $dekbredde, $byggereduktion;
		$KolonnerM1 = (( int ) ((($lengde - $fuldstensbredde) / $dekbredde) + 1));
		if (($lengde - (( int ) ($KolonnerM1 * $dekbredde) + $byggereduktion)) == $dekbredde) {
			$KolonnerM1 = $KolonnerM1 + 1;
		}
		return $KolonnerM1;
	}
	public function beregnKnast(&$berRaekker, &$berKolonner, &$berBredde, &$berLaengde, &$berMal, $knastnr) {
		global $Navn, $stenlengde, $fuldstensbredde, $dekbredde, $byggereduktion, $maxLegte, $minLegte, $DekDobbv, $fuldstensb, $halvstendek, $DekVindskH, $DekVindskV, $RygDek, $FaconRygDek, $RygAfslutDek, $FaconRygAfslutDek, $ValmBegyndDek, $PultstenDek, $PultVindDekV, $PultVindDekH;
		$this->Hentstendata ();
		global $hv, $hl, $ra, $ka, $p2, $mtdk_done, $halvsten;
		$metode = $this->session->userdata('metode');
		$halvsten = 0;
		$vlv_k = Floor ( $berLaengde / ((Cos ( $p2 * $hv ))) ); // vindskedelengde venstre
		$vlh_k = $vlv_k; // vindskedelengde hĆ…Ā³jre
		$RED = $this->RED_hh ( $hv ); // faktor for beregning af rekker - afhengig af sten og vinkel
		$ts_k = $this->tagside_hh ( ($berBredde * 2), $hv ); // tagside
		
		$berRaekker = $this->antalrekker_hh ( $ts_k, $RED, $stenlengde, $maxLegte ); // maxLegte) //
		$berKolonner = $this->AntalKolonner_hh ( $berLaengde ); // beregning af antal kolonner (afh
		$tagd_n = Floor ( ($berKolonner * $dekbredde) );
		$dekn_ialt = $tagd_n;
		
		$mang_dek = Floor ( ($berLaengde - $dekn_ialt) );
		
		$berMal = $this->todec ( ((($ts_k - 340 - $RED) / ($berRaekker - 1))) / 10 ); // legteafstand
		                                                                              // mal_tilbygning = berMal //(Man kan ikke bÄ†ļæ½de have en tilbygning og en knast pÄ†ļæ½ samme hus!!!)
		                                                                              // Her undgÄ†ļæ½r vi legteafstande der ikke er tilladte!
		if ($berMal < ($minLegte / 10)) {
			$berMal = ($minLegte / 10);
		} // Legteafstand tilbygning
		if ($berMal > ($maxLegte / 10)) {
			$berMal = ($maxLegte / 10);
		} // Legteafstand tilbygning
		  // berMal = mal_tilbygning
		
		$backup1 = $hl;
		$backup2 = $ra;
		$backup3 = $ka;
		$hl = $berLaengde;
		$ra = $berRaekker;
		$ka = $berKolonner;
		
		$knast_met = true;
		$tilfoej_halv_fjern_hel = $this->session->userdata ( 'tilfoej_halv_fjern_hel' );
		$this->session->set_userdata ( array (
				'knast_rpt' => '1' 
		) );
		global $tagd_n, $dekn_ialt, $mang_dek, $vindsk_h, $vindsk_v;
		$tagd_n = $this->session->userdata ( 'tagd_n' );
		
		// if ($antalKnaster == 2 && $mtcnt == 'k2') {
		
		// }
		switch ($metode) {
			case 1 : // Denne metode er midlertidigt disabled!!! = Metode 3 pÄ†ļæ½ oversigten
				$x = $this->metodest_hh1 ( $berLaengde, $berRaekker, $berKolonner, $knastnr ); // funktion til beregning af kolonner mm.
				$berLaengde = $x; // lengden kan blive forstĆ…Ā³rret/formindsket i "metodest_hh"
				$berKolonner = $this->AntalKolonner_hh ( $berLaengde );
				break;
			// OK til met. 1
			
			case 2 :
				
				$x = $this->metodest_hh2 ( $berLaengde, $berRaekker, $berKolonner, $knastnr ); // funktion til beregning af kolonner mm.
				$berLaengde = $x;
				$berKolonner = $this->AntalKolonner_hh ( $berLaengde );
				$berKolonner = $berKolonner - 1;
				if ($tilfoej_halv_fjern_hel == true) {
					$berKolonner = $berKolonner - 1;
				}
				break;
			case 3 :
				$x = $this->metodest_hh3 ( $berLaengde, $berRaekker, $berKolonner, $knastnr ); // funktion til beregning af kolonner mm.
				$berLaengde = $x;
				$berKolonner = $this->AntalKolonner_hh ( $berLaengde );
				if ($tilfoej_halv_fjern_hel == true) {
					$berKolonner = $berKolonner - 1;
				}
		}
		
		$sess = $this->session->all_userdata ();
		$halvsten = $this->session->userdata ( 'halvsten' );
		if ($knastnr == 1) {
			if (! $this->session->userdata ( 'res_knhl' )) {
				$this->session->set_userdata ( array (
						'res_knhl' => $berLaengde,
						'res_knhb' => $berBredde,
						'res_knra' => $berRaekker,
						'res_kaka' => $berKolonner 
				) );
			}
		}
		
		if ($knastnr == 2) {
			if (! $this->session->userdata ( 'res_knhl2' )) {
				$this->session->set_userdata ( array (
						'res_knhl2' => $berLaengde,
						'res_knhb2' => $berBredde,
						'res_knra2' => $berRaekker,
						'res_kaka2' => $berKolonner 
				) );
			}
		}
		$knast_met == false;
		
		$kn_ahes = $berKolonner * $berRaekker; // antal hele sten
		$am_kn = $this->mm_kn ( $berLaengde, $berBredde, $hv ); // tagflade / antal mÄ€Ā² tag
		$aml = $this->legte_hh ( $berRaekker, $berLaengde ); // Antal meter lĆ„ā„¢gte
		
		$arr ["Type"] = 9;
		
		if ($berMal < 33) {
			$arr ["VindskH31"] = $vindsk_h / 2;
			$arr ["VindskV31"] = $vindsk_v / 2;
		}
		if ($berMal >= 33 && $berMal < 35) {
			$arr ["VindskH33"] = $vindsk_h / 2;
			$arr ["VindskV33"] = $vindsk_v / 2;
		}
		if ($berMal >= 35) {
			$arr ["VindskH35"] = $vindsk_h / 2;
			$arr ["VindskV35"] = $vindsk_v / 2;
		}
		$arr ["normalsten"] = $kn_ahes;
		$arr ["halvsten"] = $halvsten;
		$arr ["Dobbeltvinget"] = $_SESSION['dobbeltving'] / 2;
		$arr ["Rygning"] = 0;
		$arr ["kvm"] = $am_kn;
		$arr ["legte"] = $aml;

		$this->adddatadb ( $arr, true, $knastnr );
	}
	public function mm_kn($lengde, $bredde, $V_hh) {
		$p2 = 1.745329E-2;
		$mm = ($bredde * $lengde) / ((Cos ( $p2 * $V_hh ))) / 1000; // antal mmÄ€Ā² tag
		$mm_kn = $this->todec ( $mm / 1000 ); // Antal mÄ€Ā² tag
		return $mm_kn;
	}
	public function halvv_b(&$valmv, &$valmbr, &$bygv, $antal, &$legteafstand) {
		global $Navn, $stenlengde, $fuldstensbredde, $dekbredde, $byggereduktion, $maxLegte, $minLegte, $DekDobbv, $fuldstensb, $halvstendek, $DekVindskH, $DekVindskV, $RygDek, $FaconRygDek, $RygAfslutDek, $FaconRygAfslutDek, $ValmBegyndDek, $PultstenDek, $PultVindDekV, $PultVindDekH;
		global $ra, $ka, $rat, $kat, $rav, $kav, $mal_hovedhus, $mal_vinkel, $mal_tilbygning, $mal_knast2;
		global $facon, $hhtype, $metode;
		$p2 = 1.745329E-2;
		$hh_hojde = ((($valmbr * 1000) / 2) * Tan ( $p2 * $bygv )); // valmens del af hovedhusets hojde
		$valmhojde = $hh_hojde / (Cos ( $p2 * (90 - $valmv) )); // Halvvalmens hojde (tagside)
		$valmensts = sqrt ( ($valmhojde * $valmhojde) + ((($valmbr * 1000) / 2) * (($valmbr * 1000) / 2)) ); // Halvvalmens tagside
		                                                                                                     // valmensts1 = sqrt((hh_hojde ^ 2) + (((valmbr * 1000) / 2) ^ 2)) //Halvvalmens tagside
		                                                                                                     
		// MsgBox ("Valmsts = " & Str(valmensts) & " / " & Str(valmensts1))
		
		$RED = $this->RED_hh ( $valmv );
		// halvvalmra = antalrekker_hh(valmhojde, RED, stenlengde, maxLegte) //Antal rekker pĆ�Ā� valm
		$halvvalmra = Floor ( (($valmensts - $RED) - $stenlengde) / $maxLegte ) + 3;
		
		// halvvalmra = antalrekker_hh(valmensts, RED, stenlengde, maxLegte) //Antal rekker pĆ�Ā� valm
		
		$valmlegte = $this->todec ( ($valmhojde - 340 - $RED) / (($halvvalmra - 1) * 10) ); // Legteafstand pĆ�Ā� valm (cm)
		                                                                                    
		// skal helst blive som hovedhusets hvis vinkel er ens
		                                                                                    // skal mĆ�Ā�ske foreslĆ�Ā� endring af valm_t1 hvis stenene skal ligge ens pĆ�Ā� hovedhus og valm
		
		$Antalstenpavalm = (Floor ( (((($valmbr * 1000) / 2) - $byggereduktion) / $dekbredde) * $halvvalmra )) + 1;
		$halv_rl = round ( $this->todec ( (($hh_hojde * (Tan ( $p2 * (90 - $valmv) )))) ) ); // sparet rygningslengde
		$Antalsparedesten = (( int ) (($halv_rl / $dekbredde) * $halvvalmra));
		$kvmbesparelse = $this->todec ( (($halv_rl / 1000) * ((sqrt ( ($hh_hojde * $hh_hojde) + (($valmbr * 1000) / 2) * (($valmbr * 1000) / 2) )) / 1000)) ); // Reduktion af hoved(gavl-)husets tagflade
		$kvmvalm = round ( $this->todec ( ($valmhojde / 1000) * (($valmbr / 2) / 1000) ), 2 ); // Valmens tagflade
		$kvmialt = $this->minusdec ( ($kvmvalm) - ($kvmbesparelse) );
		$Antalrygsten = 2 * (Floor ( (($valmensts - $ValmBegyndDek) / 1000) * 2.7 ) + 2); // Antal rygninger pĆ�Ā� valm (skal ganges med 2)
		if ($facon == false) {
			$AntalSp_rygsten = ( int ) (($halv_rl / 1000) * 2.7);
		} else {
			$AntalSp_rygsten = ( int ) (($halv_rl / 1000) * 3.3);
		}
		$Antalvalmstart = 2; // antal valmbegynder 
		$AntalValmAfslut = 1; // antal valmafslutning med 3 tilslutninger 
		switch ($metode) {
			case 1 :
				$_SESSION['dobbeltving'] = 0;
				$vhv = 0;
				$vhh = 0;
				break;
			case 2 :
				$_SESSION['dobbeltving'] = $halvvalmra - 2;
				$vhv = 0;
				$vhh = 0;
				break;
			case 3 :
				$_SESSION['dobbeltving'] = 0;
				$vhv = $halvvalmra - 2;
				$vhh = $halvvalmra - 2;
				break;
		}
		// Hvis hovedhus regnes der med 2 valme
		// Folgende kommer de beregnede data ind i databasen tagst.mdb - table "Beregnvalm"
		for($i = 1; $i <= $antal; $i ++) {
			$arr ["Type"] = 6;
			$arr ["kvm"] = $kvmialt;
			$arr ["normalsten"] = ( int ) ((- 1 * $Antalsparedesten) + $Antalstenpavalm);
			
			if ($facon == false) {
				$arr ["rygning"] = $Antalrygsten - $AntalSp_rygsten;
			} else {
				$arr ["rygning"] = $Antalrygsten;
				
				$arr ["faconrygning"] = (- 1 * $AntalSp_rygsten);
			}
			
			$arr ["valmbegynder"] = 2;
			$arr ["valmkappe"] = 1;
			
			// Antal sparede vindskedesten
			if ($legteafstand >= 31 && $legteafstand < 33) {
				$arr ["VindskH31"] = - 1 * $vhh;
				$arr ["VindskV31"] = - 1 * $vhv;
			}
			if ($legteafstand >= 33 && $legteafstand < 35) {
				$arr ["VindskH33"] = - 1 * $vhh;
				$arr ["VindskV33"] = - 1 * $vhv;
			}
			if ($legteafstand >= 35 && $legteafstand < 37) {
				$arr ["VindskH35"] = - 1 * $vhh;
				$arr ["VindskV35"] = - 1 * $vhv;
			}
			
			$arr ["dobbeltvinget"] = - 1 * $_SESSION['dobbeltving']; // Antal sparede dobbeltvingede sten
			$arr ["legteafstand"] = $valmlegte;
			$arr ["legte"] = $this->todec ( ((($valmbr / 2) * ($halvvalmra + 1)) / 1000) - (($halv_rl * ($halvvalmra + 1)) / 1000) );
			$arr ["alurullerRygninger"] = (- 1 * $halv_rl);
			
			// MsgBox (valmensts)
			
			$arr ["alurullerGrater"] = ($valmensts * 2);
			$arr ["fuglegitter"] = $valmbr * 1000;
			$this->adddatadb ( $arr, true );
		}
	}
	public function minusdec($tal) {
		$talstreng = ( string ) $tal;
		$a = strLen ( $talstreng );
		$b = $talstreng [0];
		;
		if ($b == "-") {
			$a = $a - 1;
			goto minus;
		}
		if ($b !== "-") {
			return $tal / 100;
		}
		minus:
		if ($a == 1) {
			$c = "-0,0" + substr ( $talstreng, 2, $a );
		}
		if ($a == 2) {
			$c = "-0," + substr ( $talstreng, 2, $a );
		}
		if ($a > 2) {
			$c = $tal;
		}
		return ( float ) ($c);
	}
	public function helvalm_b(&$valmv, &$bygbr, &$bygv, $antal, &$rek, $vinkk) {
		global $Navn, $stenlengde, $fuldstensbredde, $dekbredde, $byggereduktion, $maxLegte, $minLegte, $DekDobbv, $fuldstensb, $halvstendek, $DekVindskH, $DekVindskV, $RygDek, $FaconRygDek, $RygAfslutDek, $FaconRygAfslutDek, $ValmBegyndDek, $PultstenDek, $PultVindDekV, $PultVindDekH;
		global $ra, $ka, $rat, $kat, $rav, $kav, $mal_hovedhus, $mal_vinkel, $mal_tilbygning, $mal_knast2;
		global $facon, $hhtype;
		$p2 = 0.0174532925199;
		$hh_hojde = (($bygbr / 2) * Tan ( round ( $p2 * $bygv, 6 ) )); // hovedhusets hojde
		$helv_rl = round ( $this->todec ( (($hh_hojde * (Tan ( $p2 * (90 - $valmv) )))) ) ); // sparet rygningslengde
		                                                                                     
		// valmhojde1 = Floor(sqrt((hh_hojde ^ 2) + (helv_rl ^ 2)))
		$valmhojde = $hh_hojde / (Cos ( $p2 * (90 - $valmv) )); // Valmens hojde
		                                                        
		// MsgBox ("Valmhojde1= " & Str(valmhojde1) + " / Valmhojde = " + Str(valmhojde))
		$valmensts = sqrt ( ($valmhojde * $valmhojde) + (($bygbr / 2) * ($bygbr / 2)) ); // Valmens tagside
		$RED = $this->RED_hh ( $valmv );
		$bygts = $this->tagside_hh ( $bygbr, $bygv );
		$Antalstenpavalm = (Floor ( ((($bygbr / 2) - $byggereduktion) / $dekbredde) * $rek ) + 1);
		$Antalsparedesten = (Floor ( (($helv_rl - $byggereduktion) / $dekbredde) * $rek ));
		$valmlegte = $this->todec ( ($valmhojde - 340 - $RED) / (($rek - 1) * 10) ); // Legteafstand pa valm (cm)
		$kvmbesparelse = $this->todec ( ($helv_rl / 1000) * ($bygts / 1000) ); // Reduktion af hoved(gavl-)husets tagflade
		$kvmvalm = round ( $this->todec ( ($valmhojde / 1000) * (($bygbr / 2) / 1000) ), 2 ); // Valmens tagflade
		                                                                                      
		// Her skal beregnes sparet rygningslengde for at trekke antal sparede rygninger fra - se halvvalm
		                                                                                      // ---------------------
		                                                                                      // FACONSTEN!!!
		
		$Antalrygsten = Floor ( ($valmensts / 1000) * 2.7 ); // Antal rygninger pa valm (skal ganges med 2)
		                                                     
		// Antalrygsten = (valmensts - ValmBegyndDek) / Rygdek //Antal rygninger pa valm (skal ganges med 2)
		
		if ($vinkk == true) {
			$Antalvalmstart = 1;
		} else {
			$Antalvalmstart = 2; // antal valmbegynder
		}
		
		$AntalValmAfslut = 1; // antal valmafslutning med 3 tilslutninger
		                      
		// Folgende kommer de beregnede data ind i databasen tagst.mdb - table "Beregnvalm"
		for($a = 1; $a <= $antal; $a ++) {
			
			$arr ["Type"] = 3;
			
			$arr ["kvm"] = $this->todec ( $kvmvalm - $kvmbesparelse );
			$arr ["normalsten"] = Floor ( (- 1 * $Antalsparedesten) + $Antalstenpavalm );
			if ($facon == false) {
				$arr ["rygning"] = ($Antalrygsten * 2) + (- 1 * ($helv_rl / $RygDek));
			} else {
				$arr ["rygning"] = ($Antalrygsten * 2);
				
				$arr ["faconrygning"] = (- 1 * ($helv_rl / $FaconRygDek));
			}
			$arr ["valmbegynder"] = $Antalvalmstart;
			if ($hhtype !== '6' || ($hhtype == '6' && $antal == 1)) {
				
				// Den sidste mulighed er med, for at fa en valmkappe med til tilbygninger.
				
				$arr ["valmkappe"] = $AntalValmAfslut;
			}
			
			$arr ["legteafstand"] = $valmlegte;
			$arr ["fuglegitter"] = $bygbr;
			
			$arr ["alurullerRygninger"] = (- 1 * $helv_rl);
			if ($vinkk == true) {
				$arr ["alurullerGrater"] = 1 * (Floor ( sqrt ( ($valmhojde * $valmhojde) + (($bygbr / 2) * ($bygbr / 2)) ) ));
			} else {
				$arr ["alurullerGrater"] = 2 * (Floor ( sqrt ( ($valmhojde * $valmhojde) + (($bygbr / 2) * ($bygbr / 2)) ) ));
			}
			
			$this->adddatadb ( $arr, true );
		}
	}
	public function vinkel45_samling(&$vinkel, &$bygbrBygning1, &$bygbrBygning2) {
		global $facon, $bygv, $tagunder;
		global $ra, $ka, $rat, $kat, $rav, $kav, $mal_hovedhus, $mal_vinkel, $mal_tilbygning, $mal_knast2;
		if ($bygv == NULL) {
			$bygv = 0;
		}
		
		$p2 = 0.017453292;
		$tempA1 = round ( Tan ( $p2 * 25 ) * $bygbrBygning1, 2 );
		$tempA2 = round ( Tan ( $p2 * 20 ) * $bygbrBygning2, 2 );
		
		$byggeLaengde = ($tempA1 + $tempA2) / 2;
		$byggeBredde = ($bygbrBygning1 + $bygbrBygning2) / 2;
		
		$hh_hojde = (($byggeBredde / 2) * Tan ( $p2 * $bygv )); // hovedhusets hÄ†Ćøjde
		
		global $Navn, $stenlengde, $fuldstensbredde, $dekbredde, $byggereduktion, $maxLegte, $minLegte, $DekDobbv, $fuldstensb, $halvstendek, $DekVindskH, $DekVindskV, $RygDek, $FaconRygDek, $RygAfslutDek, $FaconRygAfslutDek, $ValmBegyndDek, $PultstenDek, $PultVindDekV, $PultVindDekH;
		$this->HentStenData ();
		
		if ($facon == true) {
			$Rygdek = $FaconRygDek;
			$RygAfslutDek = $FaconRygDek;
		}
		$tagunder = $this->session->userdata ( 'tagunder' );
		if ($tagunder == "understrygning") {
			$maxLegte = ($maxLegte - 10);
		} // OK!
		
		$husTagside = $this->tagside_hh ( $byggeBredde, $vinkel ); // tagside
		                                                           // //////////
		
		$RED = $this->RED_hh ( $vinkel ); // kald af function(red_hh) for faktor til beregning af rekker - afhengig af vinkel
		$husRaekker = $this->antalrekker_hh ( $husTagside, $RED, $stenlengde, $maxLegte ); // Antal rekker
		
		$mal = $this->todec ( ((($husTagside - 340 - $RED) / ($husRaekker - 1))) / 10 ); // legteafstand
		                                                                                 
		// mal_hovedhus = mal
		if ($mal < ($minLegte / 10)) {
			$mal = ($minLegte / 10);
		} // Legteafstand hovedhus
		if ($mal > ($maxLegte / 10)) {
			$mal = ($maxLegte / 10);
		} // Legteafstand hovedhus
		
		$husKolonner = $this->AntalKolonner_hh ( $byggeLaengde ); // Antalkolonner
		
		$am_hh = $this->mm_hh ( $byggeLaengde, $husTagside ); // Antal mÄ€Ā² tag
		
		$ahes = $this->hele_sten ( $husRaekker, $husKolonner ); // Antal hele sten
		                                                        
		// Valm sten!
		$valmensts = $husTagside / Cos ( $p2 * 25 );
		
		if ($facon == true) {
			$ars = Floor ( (($byggeLaengde) / 1000) * 3.33 ) + 1;
			$arsValm = Floor ( (($valmensts) / 1000) * 2.7 ) + 1;
		} else {
			$ars = Floor ( (($byggeLaengde) / 1000) * 2.7 ) + 1;
			$arsValm = Floor ( (($valmensts) / 1000) * 2.7 ) + 1;
		}
		
		$aml = $this->legte_hh ( $husRaekker, $byggeLaengde ); // Antal meter legte
		
		$Antalvalmstart = 1;
		$AntalValmAfslut = 1;
		
		$arr = array (
				"Type" => 30,
				
				"kvm" => $this->todec ( $am_hh ),
				"normalsten" => $ahes,
				
				"valmbegynder" => $Antalvalmstart,
				
				"valmkappe" => 0,
				"legteafstand" => $mal,
				"fuglegitter" => $byggeBredde * 2,
				
				"alurullerRygninger" => $valmensts,
				"alurullerGrater" => $valmensts,
				"skotrende" => $valmensts,
				
				"legte" => $aml,
				"legteafstand" => $mal,
				"AntalRekker" => $husRaekker,
				"AntalKolonner" => $husKolonner 
		);
		if ($facon == false) {
			$arr ["rygning"] = $ars + $arsValm;
		} else {
			$arr ["rygning"] = $arsValm;
			$arr ["faconrygning"] = $ars;
		}
		
		$this->adddatadb ( $arr, true );
	}
	public function ens_veg() {
		global $hv, $hl, $hb, $metode, $ka, $halve, $vindsk_h, $vindsk_v;
		global $Navn, $stenlengde, $fuldstensbredde, $dekbredde, $byggereduktion, $maxLegte, $minLegte, $DekDobbv, $fuldstensb, $halvstendek, $DekVindskH, $DekVindskV, $RygDek, $FaconRygDek, $RygAfslutDek, $FaconRygAfslutDek, $ValmBegyndDek, $PultstenDek, $PultVindDekV, $PultVindDekH;
		global $ra, $ka, $rat, $kat, $rav, $kav, $mal_hovedhus, $mal_vinkel, $mal_tilbygning, $mal_knast2;
		$this->HentStenData ();
		$tilfoej_halv_fjern_hel = $this->session->userdata ( 'tilfoej_halv_fjern_hel' );
		
		$tagunder = $this->session->userdata ( 'tagunder' );
		if ($tagunder == "understrygning") {
			$maxLegte = ($maxLegte - 10);
		}
		
		$ts = $this->tagside_hh ( (2 * $hb), $hv ); // tagside
		$vlv = $ts; // vindskedelengde venstre
		$vlh = $ts; // vindskedelengde hojre
		$RED = $this->RED_hh ( $hv ); // kald af function(red_hh) for faktor til beregning af rekker - afhengig af vinkel
		$ra = $this->antalrekker_hh ( $ts, $RED, $stenlengde, $maxLegte ); // Antal rekker
		$mal = $this->todec ( ((($ts - 340 - $RED) / ($ra - 1))) / 10 ); // legteafstand
		$ka = $this->AntalKolonner_hh ( $hl );
		// Antalkolonner
		switch ($metode) {
			case 1 :
				$x = $this->metodest_hh1 ( $hl, $ra, $ka ); // funktion til beregning af kolonner mm.
				$hl = $x; // lengden kan blive forstorret/formindsket i "metodest_hh"
				$ka = $this->AntalKolonner_hh ( $hl );
				break;
			case 2 :
				$x = $this->metodest_hh2 ( $hl, $ra, $ka ); // funktion til beregning af kolonner mm.
				$hl = $x;
				$ka = $this->AntalKolonner_hh ( $hl );
				if ($tilfoej_halv_fjern_hel == true) {
					$ka = $ka - 1;
				}
				break;
			case 3 :
				$x = $this->metodest_hh3 ( $hl, $ra, $ka ); // funktion til beregning af kolonner mm.
				$hl = $x;
				$ka = $this->AntalKolonner_hh ( $hl );
				if ($tilfoej_halv_fjern_hel == true) {
					$ka = $ka - 1;
				}
				break;
		}
		
		if ($halve == true) {
			$halvsten = $ra;
		} else {
			$halvsten = 0;
		}
		$aml = $this->legte_hh ( $ra, $hl );
		// Folgende kommer de beregnede data ind i databasen tagst.mdb
		$tagok = ( string ) $this->session->userdata ( 'tag' );
		$c = $tagok + ".bmp";
		$arr = array (
				"Type" => 5,
				"navn" => $Navn,
				"hustype" => $c,
				"Normalsten" => $ra * $ka,
				"Dobbeltvinget" => $_SESSION['dobbeltving'] / 2,
				"halvsten" => $halvsten,
				"faconrygning" => 0,
				"faconrygafslut" => 0,
				"faconrygstart" => 0,
				"rygning" => 0,
				"rygstart" => 0,
				"rygafslut" => 0,
				"valmbegynder" => 0,
				"valmkappe" => 0,
				"kvm" => ($hl * $ts / 1000000),
				"legte" => $aml,
				"legteafstand" => $mal,
				"AntalRekker" => $ra,
				"AntalKolonner" => $ka,
				"fuglegitter" => $hl 
		);
		$mal_hovedhus = $mal;
		if ($metode == 3) {
			if ($mal >= 31 && $mal < 33) {
				$arr ["VindskH31"] = ($vindsk_h / 2);
				$arr ["VindskV31"] = ($vindsk_v / 2);
			}
			if ($mal >= 33 && $mal < 35) {
				$arr ["VindskH33"] = ($vindsk_h / 2);
				$arr ["VindskV33"] = ($vindsk_v / 2);
			}
			if ($mal >= 35 && $mal < 37) {
				$arr ["VindskH35"] = ($vindsk_h / 2);
				$arr ["VindskV35"] = ($vindsk_v / 2);
			}
		}
		
		switch ($metode) {
			
			case 1 :
				$arr ["Metode"] = "Afslutning med sternbredt";
				break;
			case 2 :
				$arr ["metode"] = "Afslutning med dobbeltvinget";
				break;
			case 3 :
				$arr ["metode"] = "Afslutning med vindskedesten";
				break;
		}
		
		$this->adddatadb ( $arr, false );
	}
	public function ens_frit() {
		global $hl, $hv, $hb, $halve, $metode, $ka, $vindsk_h, $vindsk_v;
		global $Navn, $stenlengde, $fuldstensbredde, $dekbredde, $byggereduktion, $maxLegte, $minLegte, $DekDobbv, $fuldstensb, $halvstendek, $DekVindskH, $DekVindskV, $RygDek, $FaconRygDek, $RygAfslutDek, $FaconRygAfslutDek, $ValmBegyndDek, $PultstenDek, $PultVindDekV, $PultVindDekH;
		global $ra, $ka, $rat, $kat, $rav, $kav, $mal_hovedhus, $mal_vinkel, $mal_tilbygning, $mal_knast2;
		$this->HentStenData ();
		$tilfoej_halv_fjern_hel = $this->session->userdata ( 'tilfoej_halv_fjern_hel' );
		$tagunder = $this->session->userdata ( 'tagunder' );
		if ($tagunder == "understrygning") {
			$maxLegte = ($maxLegte - 10);
		}
		// ts = tagside_hh((hb), hv) //tagside
		
		// MsgBox (ts)
		$ts = $this->tagside_hh ( (2 * $hb), $hv ); // tagside
		                                            // MsgBox (ts)
		$vlv = $ts; // vindskedelengde venstre
		$vlh = $ts; // vindskedelengde hoj
		$RED = $this->RED_hh ( $hv ); // kald af function(red_hh) for faktor til beregning af rekker - afhengig af vinkel
		
		$ra = $this->antalrekker_hh ( $ts, $RED, $stenlengde, $maxLegte ); // Antal rekker
		$mal = $this->todec ( ((($ts - 340 - $RED) / ($ra - 1))) / 10 ); // legteafstand
		
		$ka = $this->AntalKolonner_hh ( $hl );
		
		// Antalkolonner
		switch ($metode) {
			case 1 :
				$x = $this->metodest_hh1 ( $hl, $ra, $ka ); // funktion til beregning af kolonner mm.
				$hl = $x; // lengden kan blive forstorret/formindsket i "metodest_hh"
				$ka = $this->AntalKolonner_hh ( $hl );
				break;
			case 2 :
				$x = $this->metodest_hh2 ( $hl, $ra, $ka ); // funktion til beregning af kolonner mm.
				$hl = $x; 
				$ka = $this->AntalKolonner_hh ( $hl );
				if ($tilfoej_halv_fjern_hel == true) {
					$ka = $ka - 1;
				}
				break;
			case 3 :
				$x = $this->metodest_hh3 ( $hl, $ra, $ka ); // funktion til beregning af kolonner mm.
				$hl = $x;
				$ka = $this->AntalKolonner_hh ( $hl );
				if ($tilfoej_halv_fjern_hel == true) {
					$ka = $ka - 1;
				}
				break;
		}
		$am_hh = $this->mm_hh ( $hl, $ts ); // Antal mĆ‚Ā² tag
		$ahes = ($ra - 1) * $ka; // Antal hele sten
		if ($halve == true) {
			$halvsten = $ra;
			$ahaps = 1;
		} else {
			$halvsten = 0;
			$ahaps = 0;
		}
		// aml = CSng(2 * (ra + 1) * (hl / 1000)) //Antal meter legte
		$aml = $this->legte_hh ( $ra, $hl ); // Antal meter legte
		                                     // Folgende kommer de beregnede data ind i databasen tagst.mdb
		$tagok = ( string ) $this->session->userdata ( 'tag' );
		$c = $tagok + ".bmp";
		
		// add variables to session
		$arr = array (
				"Type" => 4,
				"navn" => $Navn,
				"hustype" => $c,
				"Pultsten" => $ka,
				"halvpultsten" => $ahaps,
				
				// "VindskH31" => $raekker,
				// "VindskV31" => $raekker,
				"Normalsten" => $ahes,
				"Dobbeltvinget" => $_SESSION['dobbeltving'] / 2,
				"halvsten" => $halvsten,
				"faconrygning" => 0,
				"faconrygafslut" => 0,
				"faconrygstart" => 0,
				"rygning" => 0,
				"rygstart" => 0,
				"rygafslut" => 0,
				"valmbegynder" => 0,
				"valmkappe" => 0,
				"kvm" => ($hl * $ts / 1000000),
				"legte" => $aml,
				"legteafstand" => $mal,
				"AntalRekker" => $ra,
				"AntalKolonner" => $ka,
				"fuglegitter" => $hl 
		);
		$mal_hovedhus = $mal;
		if ($metode == 3) {
			if ($mal >= 31 && $mal < 33) {
				$arr ["VindskH31"] = ($vindsk_h / 2) - 1;
				$arr ["VindskV31"] = ($vindsk_v / 2) - 1;
			}
			if ($mal >= 33 && $mal < 35) {
				$arr ["VindskH33"] = ($vindsk_h / 2) - 1;
				$arr ["VindskV33"] = ($vindsk_v / 2) - 1;
			}
			if ($mal >= 35 && $mal < 37) {
				$arr ["VindskH35"] = ($vindsk_h / 2) - 1;
				$arr ["VindskV35"] = ($vindsk_v / 2) - 1;
			}
		} else {
			$arr ["VindskH31"] = 0;
			$arr ["VindskV31"] = 0;
		}
		
		switch ($metode) {
			
			case 1 :
				$arr ["Metode"] = "Afslutning med sternbredt";
				$arr ["PultvindskH"] = 0;
				$arr ["PultvindskV"] = 0;
				$arr ["Pultdobb"] = 0;
				break;
			case 2 :
				$arr ["metode"] = "Afslutning med dobbeltvinget";
				$arr ["PultvindskH"] = 0;
				$arr ["PultvindskV"] = 0;
				$arr ["Pultdobb"] = 1;
				break;
			case 3 :
				$arr ["metode"] = "Afslutning med vindskedesten";
				$arr ["PultvindskH"] = 1;
				$arr ["PultvindskV"] = 1;
				$arr ["Pultdobb"] = 0;
				break;
		}
		
		$this->adddatadb ( $arr, false );
	}
	public function beregn_tb($husLaengde, &$husBredde, &$husVinkel, &$husSammenkoblingVinkel, $valmensType, &$husSammenkoblingBredde, &$raekker, &$kolonner, $counthh7type = 0) {
		global $facon, $hv, $hhtype, $metode;
		global $Navn, $stenlengde, $fuldstensbredde, $dekbredde, $byggereduktion, $maxLegte, $minLegte, $DekDobbv, $fuldstensb, $halvstendek, $DekVindskH, $DekVindskV, $RygDek, $FaconRygDek, $RygAfslutDek, $FaconRygAfslutDek, $ValmBegyndDek, $PultstenDek, $PultVindDekV, $PultVindDekH;
		global $ra, $ka, $rat, $kat, $rav, $kav, $mal_hovedhus, $mal_vinkel, $mal_tilbygning, $mal_knast2;
		$tRygning = 0;
		$tagunder = $this->session->userdata ( 'tagunder' );
		$this->HentStenData ();
		if ($facon == true) {
			$Rygdek = $FaconRygDek;
			$RygAfslutDek = $FaconRygDek;
		}
		
		if ($tagunder == "understrygning") {
			$maxLegte = ($maxLegte - 10);
		}
		
		$ts_t = round ( $this->tagside_hh ( $husBredde, $husVinkel ), 3 ); // tagside
		
		$p2 = 0.017453292;
		$tr = round ( (Tan ( $p2 * $husVinkel ) * ($husBredde / 2)) * Tan ( $p2 * ($husSammenkoblingVinkel - $hv) ), 3 );
		
		$am_tb = $this->mm_tb ( $husLaengde, $ts_t, $tr ); // Antal mÄ€Ā² tag
		$vlv_t = Floor ( $husBredde / (2 * (Cos ( $p2 * $husVinkel ))) ); // vindskedelengde venstre
		$vlh_t = $vlv_t; // vindskedelengde hÄ†Ćøjre
		
		$RED = $this->RED_hh ( $husVinkel ); // faktor for beregning af rekker - afhengig af sten og vinkel
		                                     
		// rat
		$raekker = $this->antalrekker_hh ( $ts_t, $RED, $stenlengde, $maxLegte ); // antal rekker tagsten
		
		$rl = ( int ) ($tr + $husLaengde); // rygningslengde
		$katt = $this->AntalKolonner_hh ( $rl ) + 1; // Antal kolonner ved rygning pa tilbygning
		                                             // kat = AntalKolonner_hh(husLaengde) + 1 //Antal kolonner ved tagfod pa tilbygning
		$kolonner = $this->AntalKolonner_hh ( $husLaengde ) + 1; // Antal kolonner ved tagfod pa tilbygning
		                                                         
		// ahes1 = ((2 * rat * ((kat + katt) / 2))) //Antal hele sten pa tilbygning
		$ahes1 = ((2 * $raekker * (($kolonner + $katt) / 2))); // Antal hele sten pa tilbygning
		                                                       
		// ----------------------------
		$hoejde_t = sqrt ( ($ts_t * $ts_t) - (($husBredde * 0.5) * ($husBredde * 0.5)) );
		
		$ts_sammenkobling = round ( $this->tagside_hh ( $husSammenkoblingBredde, $husSammenkoblingVinkel ), 3 ); // tagside
		$hoejdeSammenkobling = sqrt ( ($ts_sammenkobling * $ts_sammenkobling) - (($husSammenkoblingBredde * 0.5) * ($husSammenkoblingBredde * 0.5)) );
		
		$ahes2_a = (sqrt ( ($hoejde_t * $hoejde_t) + ($tr * $tr) )) / $stenlengde;
		$ahes2_b = ($husBredde * 0.5) / $dekbredde;
		$ahes2 = $ahes2_a * $ahes2_b;
		
		// ahes2 = 0 //ahes2 = (ra * ka) / 2 //Antal sten, der skal fjernes pa hovedbygning
		$ahes = Floor ( $ahes1 - $ahes2 ) + 1; // Antal hele sten
		$alm_t = $this->legte_tb ( $ts_t, $husLaengde, $tr, $raekker ); // rat) //antal meter legte
		
		$mal_tilbygning = $this->todec ( ((($ts_t - 340 - $RED) / ($raekker - 1))) / 10 ); // legteafstand pa tilbygning
		                                                                                   
		// Her undgar vi forbudte legteafstande!
		if ($mal_tilbygning < ($minLegte / 10)) {
			$mal_tilbygning = ($minLegte / 10);
		} // Legteafstand tilbygning
		if ($mal_tilbygning > ($maxLegte / 10)) {
			$mal_tilbygning = ($maxLegte / 10);
		} // Legteafstand tilbygning
		
		$srl = ( int ) (2 * Floor ( sqrt ( ($tr * $tr) + ($ts_t * $ts_t) ) )); // skotrendelengde
		
		if ($facon == true) {
			$ars = Floor ( (($husLaengde + $tr) / 1000) * 3.33 ) + 1;
		} else {
			$ars = Floor ( (($husLaengde + $tr) / 1000) * 2.7 ) + 1;
		}
		
		if ($valmensType > 1 || $hhtype == '7') {
			// if ( valm > 100 || hoved = 70000 ){
			// ars = (Floor(hl_t / (Rygdek))) + 1 //Antal rygningssten
			$rygstart = 0;
			$rygafslut = 0;
		} else {
			// ars = (Floor((hl_t - (2 * RygAfslutDek)) / Rygdek)) + 1 //Antal rygningssten
			$rygstart = 1; // Stod til 1 fÄ†Ćør.....
			$rygafslut = 0;
		}
		
		// there is record for each session id
		
		$arr = array (
				"Type" => 8,
				"Normalsten" => $ahes,
				"halvsten" => Floor ( ($raekker * 2) / 3 ),
				"kvm" => $am_tb,
				"legte" => $alm_t,
				// "legteafstand" => $mal_vinkel,
				"AntalRekker" => $raekker,
				"AntalKolonner" => $kolonner,
				// "vinkelRygning" => $vinkelRygninger,
				"skotrende" => $srl,
				"fuglegitter" => (($husLaengde) * 2) - $husBredde,
				"alurullerRygninger" => $rl,
				"tRygning" => $tRygning 
		);
		
		if ($metode == 3 && $hhtype !== '7' && $valmensType !== 2) {
			if ($mal_tilbygning >= 31 && $mal_tilbygning < 33) {
				$arr ["VindskH31"] = $raekker;
				$arr ["VindskV31"] = $raekker;
			}
			if ($mal_tilbygning >= 33 && $mal_tilbygning < 35) {
				$arr ["VindskH33"] = $raekker;
				$arr ["VindskV33"] = $raekker;
			}
			if ($mal_tilbygning >= 35 && $mal_tilbygning < 37) {
				$arr ["VindskH35"] = $raekker;
				$arr ["VindskV35"] = $raekker;
			}
		}
		if ($metode == 2 && $hhtype !== '7') {
			$arr ["Dobbeltvinget"] = $raekker;
		} else {
			$arr ["Dobbeltvinget"] = 0;
		}
		
		if ($facon == true) {
			$arr ["faconrygning"] = $ars;
			$arr ["faconrygafslut"] = $rygstart;
			$arr ["faconrygstart"] = $rygstart;
		} else {
			$arr ["rygning"] = $ars;
			$arr ["rygstart"] = $rygstart;
			$arr ["rygafslut"] = $rygafslut;
		}
		if ($counthh7type == 4 || $counthh7type == 5) {
			if (! $this->session->userdata ( 'hv7_2tb_gone1' )) {
				$this->session->set_userdata ( array (
						'hv7_2tb_gone1' => '1' 
				) );
				$this->adddatadb ( $arr, true );
			} else if (! $this->session->userdata ( 'hv7_2tb_gone2' )) {
				$this->session->set_userdata ( array (
						'hv7_2tb_gone2' => '1' 
				) );
				$this->adddatadb ( $arr, true );
			}
		} else {
			$this->adddatadb ( $arr, true );
		}
	}
	public function Hentstendata() {
		$stenvalgtnr = $this->session->userdata ( 'stenvalgtnr' );
		$this->db->select ( 'Navn, stenlengde, fuldstensbredde, dekbredde, byggereduktion, maxLegte, minLegte,DekDobbv, fuldstensb, halvstendek, DekVindskH, DekVindskV, 	RygDek, FaconRygDek, RygAfslutDek, FaconRygAfslutDek, ValmBegyndDek, PultstenDek, PultVindDekV, PultVindDekH' );
		$query = $this->db->get_where ( 'Stentype', array (
				'nr' => $stenvalgtnr 
		) );
		$db = $query->result_array ();
		// puts all gathered database data in variables
		// declares vars
		global $Navn, $stenlengde, $fuldstensbredde, $dekbredde, $byggereduktion, $maxLegte, $minLegte, $DekDobbv, $fuldstensb, $halvstendek, $DekVindskH, $DekVindskV, $RygDek, $FaconRygDek, $RygAfslutDek, $FaconRygAfslutDek, $ValmBegyndDek, $PultstenDek, $PultVindDekV, $PultVindDekH;
		foreach ( $db [0] as $var => $value ) {
			${$var} = $value;
		}
	}
	public function beregn_vinkel() {
		global $hb, $hb_v, $hv_V, $hv, $hl_v, $hv_t, $globalAntalValgfrie, $halvsten_vinkel, $dynValmPlacering, $dynValmType, $antalVinklerVinkel, $hhtilb, $metode;
		global $rav, $kav, $facon;
		global $Navn, $stenlengde, $fuldstensbredde, $dekbredde, $byggereduktion, $maxLegte, $minLegte, $DekDobbv, $fuldstensb, $halvstendek, $DekVindskH, $DekVindskV, $RygDek, $FaconRygDek, $RygAfslutDek, $FaconRygAfslutDek, $ValmBegyndDek, $PultstenDek, $PultVindDekV, $PultVindDekH;
		global $ra, $ka, $rat, $kat, $rav, $kav, $mal_hovedhus, $mal_vinkel, $mal_tilbygning, $mal_knast2;
		$this->Hentstendata ();
		
		$vinkelRygninger = 0;
		$stenvalgtnr = $this->session->userdata ( 'stenvalgtnr' );
		$stenvalgt = $stenvalgtnr;
		
		$tagunder = $this->session->userdata ( 'tagunder' );
		if ($facon == true) {
			$Rygdek = $FaconRygDek;
			$RygAfslutDek = $FaconRygDek;
		}
		if ($tagunder == "understrygning") {
			$maxLegte = ($maxLegte - 10);
		}
		
		// ----------------------------------------------------------------
		
		$ts_v = round ( $this->tagside_hh ( $hb_v, $hv_V ), 2 ); // tagside
		
		$p2 = 0.017453292;
		$tr = round ( (Tan ( $p2 * $hv_V ) * ($hb_v / 2)) * Tan ( $p2 * (90 - $hv) ), 2 );
		// //CSng((Sin(p2 * hv_V) * ts_v) * Tan(90 - hv)) // for (sog!
		
		// //////CSng((ts_v / (Tan(p2 * (90 - hv))))) //forskel mellem tagfod og rygning
		
		$am_v = $this->mm_tb ( $hl_v, $ts_v, $tr ); // Antal mÄ€Ā² tag
		
		$vlv_v = Floor ( $hb_v / (2 * (Cos ( $p2 * $hv_V ))) ); // vindskedelengde venstre
		$vlh_v = $vlv_v; // vindskedelengde hojre
		$RED = $this->RED_hh ( $hv_V ); // faktor for beregning af rekker - afhengig af sten og vinkel
		$rav = $this->antalrekker_hh ( $ts_v, $RED, $stenlengde, $maxLegte ); // antal rekker tagsten
		
		$rl = ( int ) ($tr + $hl_v); // rygningslengde
		
		$katv = $this->AntalKolonner_hh ( $rl ) + 1; // Antal kolonner ved rygning pa tilbygning
		$kav = $this->AntalKolonner_hh ( $hl_v ) + 1; // Antal kolonner ved tagfod pa tilbygning
		
		$ahes1 = ((2 * $rav * ($kav + $katv) / 2)); // Antal hele sten pa vinklen
		
		$hoejde_v = sqrt ( ($ts_v * $ts_v) - (($hb_v * 0.5) * ($hb_v * 0.5)) );
		$ahes2_a = (sqrt ( ($hoejde_v * $hoejde_v) + ($tr * $tr) )) / $stenlengde;
		$ahes2_b = ($hb_v * 0.5) / $dekbredde;
		$ahes2 = $ahes2_a * $ahes2_b;
		
		// ahes2 = (rav * kav) / 2 //Antal sten, der skal fjernes pa hovedbygning
		$ahes = Floor ( $ahes1 - $ahes2 ) + 1; // Antal hele sten
		$alm_v = $this->legte_tb ( $ts_v, $hl_v, $tr, $rav ); // antal meter legte
		                                                      
		// TILBYGNING?
		$mal_vinkel = $this->todec ( ((($ts_v - 340 - $RED) / ($rav - 1))) / 10 ); // legteafstand pa tilbygning // Her undgar vi forbudte legteafstande!
		if ($mal_vinkel < ($minLegte / 10)) {
			$mal_vinkel = ($minLegte / 10);
		} // Legteafstand vinkel
		if ($mal_vinkel > ($maxLegte / 10)) {
			$mal_vinkel = ($maxLegte / 10);
		}
		for($i = 1; $i <= $globalAntalValgfrie; $i ++) {
			if (($dynValmPlacering [$i] == 3 && $antalVinklerVinkel == 1) || ($dynValmPlacering [$i] == 1 && $antalVinklerVinkel == 1 && $hhtilb == 2) || ($dynValmPlacering [$i] == 1 && $antalVinklerVinkel == 2)) {
				if ($dynValmType [$i] == 1) {
					// Gavl
					$srl = ( int ) (2 * Floor ( sqrt ( ($tr * $tr) + ($ts_v * $ts_v) ) )); // skotrendelengde
					if (($stenvalgt == 0 || $stenvalgt == 3 || $stenvalgt == 4)) {
						$halvsten_vinkel = Floor ( ($rav * 2) / 3 );
					}
				} else {
					// Helvalm eller Halvvalm
					// Her kan vi tjekke for den nye "Vinkelrygning"
					if ($dynValmType [$i] == 2 && $hb_v == $hb && $hv_V == $hv) {
						$vinkelRygninger = $vinkelRygninger + 1;
					}
					
					$srl = ( int ) (Floor ( sqrt ( ($tr * $tr) + ($ts_v * $ts_v) ) )); // skotrendelengde
					if (($stenvalgt == 0 || $stenvalgt == 3 || $stenvalgt == 4)) {
						$halvsten_vinkel = Floor ( $rav / 3 );
					}
				}
				
				if ($facon == true) {
					if ($dynValmType [$i] == 2) {
						$ars = Floor ( (($hl_v + $tr - $ts_v) / 1000) * 3.33 ) + 1;
					} else {
						$ars = Floor ( (($hl_v + $tr) / 1000) * 3.33 ) + 1;
					}
				} else {
					if ($dynValmType [$i] == 2) {
						$ars = Floor ( (($hl_v + $tr - $ts_v) / 1000) * 2.7 ) + 1;
					} else {
						$ars = Floor ( (($hl_v + $tr) / 1000) * 2.7 ) + 1;
					}
				}
			}
		}
		
		if ($antalVinklerVinkel == 2) {
			// Sammenkobling med vinkel 2 beregnes, da den beregnes som en tilbygning!
			for($i = 1; $i <= $globalAntalValgfrie; $i ++) {
				if ($dynValmPlacering [$i] == "44") {
					if ($dynValmType [$i] == 1) {
						// Gavl
						$srl = $srl + ( int ) (2 * Floor ( sqrt ( ($tr * $tr) + ($ts_v * $ts_v) ) )); // skotrendelengde
						if ((stenvalgt == 0 || stenvalgt == 3 || stenvalgt == 4)) {
							$halvsten_vinkel = $halvsten_vinkel + Floor ( ($rav * 2) / 3 );
						}
					} else {
						// Helvalm eller Halvvalm
						
						// Her kan vi tjekke for den nye "Vinkelrygning"
						if ($dynValmType [$i] == 2 && $hb_v == hb_t && $hv_V == $hv_t) {
							
							$vinkelRygninger = $vinkelRygninger + 1;
						}
						
						$srl = $srl + ( int ) (Floor ( sqrt ( ($tr * $tr) + ($ts_v * $ts_v) ) )); // skotrendelengde
						if (($stenvalgt == 0 || $stenvalgt == 3 || $stenvalgt == 4)) {
							$halvsten_vinkel = $halvsten_vinkel + Floor ( $rav / 3 );
						}
					}
					
					break;
				}
			}
		}
		
		for($i = 1; $i <= $globalAntalValgfrie; $i ++) {
			if ($antalVinklerVinkel == 2) {
				if ($dynValmPlacering [$i] == "33") {
					if ($dynValmType [$i] == 1) {
						$rygstart = 1;
						$rygafslut = 0;
					}
					$gemValmType = $dynValmType [$i];
					break;
				}
			} else {
				if ($dynValmPlacering [$i] == "44" || ($dynValmPlacering [$i] == "33" && $hhtilb == 2)) {
					if ($dynValmType [$i] == 1) {
						$rygstart = 1;
						$rygafslut = 0;
					}
					$gemValmType = $dynValmType [$i];
					break;
				}
			}
		}
		
		// set gathered final data in db
		// there is record for each session id
		if (! isset ( $_SESSION['dobbeltving'] )) {
			$_SESSION['dobbeltving'] = 0;
		}
		if (! isset ( $rygafslut )) {
			$rygafslut = 0;
		}
		if (! isset ( $rygstart )) {
			$rygstart = 0;
		}
		$arr = array (
				"Type" => 2,
				"Normalsten" => $ahes,
				"halvsten" => $halvsten_vinkel,
				"kvm" => $am_v,
				"legte" => $alm_v,
				"legteafstand" => $mal_vinkel,
				"AntalRekker" => $rav,
				"AntalKolonner" => $kav,
				"vinkelRygning" => $vinkelRygninger,
				"skotrende" => $srl,
				"fuglegitter" => ($hl_v * 2) - $hb_v,
				"alurullerRygninger" => $rl 
		);
		
		if ($metode == 3 && ($gemValmType == 1 || $gemValmType == 3)) {
			if ($mal_vinkel >= 31 && $mal_vinkel < 33) {
				$arr ["VindskH31"] = $rav;
				$arr ["VindskV31"] = $rav;
			}
			if ($mal_vinkel >= 33 && $mal_vinkel < 35) {
				$arr ["VindskH33"] = $rav;
				$arr ["VindskV33"] = $rav;
			}
			if ($mal_vinkel >= 35 && $mal_vinkel < 37) {
				$arr ["VindskH35"] = $rav;
				$arr ["VindskV35"] = $rav;
			}
		}
		if ($metode == 2 && ($gemValmType == 1 || $gemValmType == 3)) {
			$arr ["Dobbeltvinget"] = $rav;
		} else {
			$arr ["Dobbeltvinget"] = 0;
		}
		
		if ($facon == true) {
			$arr ["faconrygning"] = $ars;
			$arr ["faconrygafslut"] = $rygstart;
			$arr ["faconrygstart"] = $rygstart;
		} else {
			$arr ["rygning"] = $ars;
			$arr ["rygstart"] = $rygstart;
			$arr ["rygafslut"] = $rygafslut;
		}
		
		$this->adddatadb ( $arr, false );
	}
	public function mm_tb($lengde, $tagside, $tagrest) {
		$mm = ((2 * ($lengde) * (( int ) ($tagside))));
		$mm2 = (( int ) ($tagside) * ($tagrest / 2)); // antal mmĀ² tag
		$mm = $mm + $mm2;
		return $this->todec ( (( int ) ($mm) / 1000000) ); // Antal mĀ² tag
	}
	public function legte_tb($tagside, $lengde, $tagrest, $rek) {
		return Floor ( 2 * ($rek + 1) * ($lengde / 1000) ) + 1;
	}
	public function beregnHoved(&$husBredde, &$husLaengde, &$husVinkel, &$mal, &$husRaekker, &$husKolonner, $kaldNummer) {
		global $facon, $hhtype, $ka, $metode, $dynValmPlacering, $dynValmType, $globalAntalValgfrie, $antalVinkler, $hhvalm, $stenvalgtnr, $halve, $knast_met, $mtd_done, $mtdk_done, $halvsten;
		global $ra, $mal_hovedhus;
		global $ra, $ka, $rat, $kat, $rav, $kav, $mal_hovedhus, $mal_vinkel, $mal_tilbygning, $mal_knast2;
		$metode = $this->session->userdata ( 'metode' );
		$tilfoej_halv_fjern_hel = $this->session->userdata ( 'tilfoej_halv_fjern_hel' );
		$halve = $this->session->userdata ( 'halve' );
		// $hustagside = $ts;
		$pyramidesten = 0;
		$vindsk_h = 0;
		$vindsk_v = 0;
		global $Navn, $stenlengde, $fuldstensbredde, $dekbredde, $byggereduktion, $maxLegte, $minLegte, $DekDobbv, $fuldstensb, $halvstendek, $DekVindskH, $DekVindskV, $RygDek, $FaconRygDek, $RygAfslutDek, $FaconRygAfslutDek, $ValmBegyndDek, $PultstenDek, $PultVindDekV, $PultVindDekH;
		$this->Hentstendata ();
		$stenvalgtnr = $this->session->userdata ( 'stenvalgtnr' );
		
		$tagunder = $this->session->userdata ( 'tagunder' );
		
		if ($facon) {
			$Rygdek = $FaconRygDek;
			$RygAfslutDek = $FaconRygDek;
		}
		
		if ($tagunder == "understrygning") {
			$maxLegte = ($maxLegte - 10); // OK!
		}
		
		$husTagside = $this->tagside_hh ( $husBredde, $husVinkel ); // tagside
		$RED = $this->RED_hh ( $husVinkel ); // kald af function(red_hh) for faktor til beregning af rÄ™kker - afhÄ™ngig af vinkel
		$husRaekker = $this->antalrekker_hh ( $husTagside, $RED, $stenlengde, $maxLegte ); // Antal rÄ™kker
		$ra = $husRaekker;
		$mal = $this->todec ( ((($husTagside - 340 - $RED) / ($husRaekker - 1))) / 10 ); // lÄ™gteafstand // mal_hovedhus = mal
		$mal_hovedhus = $mal;
		if ($mal < ($minLegte / 10)) {
			$mal = ($minLegte / 10);
		} // LÄ™gteafstand hovedhus
		if ($mal > ($maxLegte / 10)) {
			$mal = ($maxLegte / 10);
		} // LÄ™gteafstand hovedhus
		
		$husKolonner = $this->AntalKolonner_hh ( $husLaengde ); // Antalkolonner
		
		$ka = $husKolonner;
		
		$gavl1Id = 1;
		$gavl2Id = 2;
		
		$foersteFundet = False;
		if ($globalAntalValgfrie > 0) {
			
			if ($kaldNummer == 1) {
				for($i = 1; $i <= $globalAntalValgfrie; $i ++) {
					if (strlen ( $dynValmPlacering [$i] ) == 1 && $foersteFundet == false) {
						$gavl1Id = $i;
						$foersteFundet = true;
					} else {
						if (strlen ( $dynValmPlacering [$i] ) == 1) {
							$gavl2Id = $i;
						}
					}
				}
			}
			if ($dynValmType [$gavl1Id] == 2 && $dynValmType [$gavl2Id] == 2) {
				goto jump1;
			}
			if ($kaldNummer == 2) {
				if ($dynValmType [3] == 2 && $dynValmType [4] == 2) {
					goto jump1;
				}
			}
			if ($kaldNummer == 3) {
				// Vinkelhus 45 grader
				for($i = 1; $i <= $globalAntalValgfrie; $i ++) {
					if ($dynValmPlacering [$i] == "3") {
						if ($dynValmType [$i] == 2) {
							// Vi kan springe over!
							goto jump1;
						} else {
							break;
						}
					}
				}
			}
			if ($kaldNummer == 4) {
				// Vinkelhus 45 grader
				// Her er der ingen ide i at forĆ„ā€ Ä�ā‚¬Ā¦Ć„ā‚¬Ä€Ā³ge eller reducere, da der bare skĆ„ā€ Ä�ā‚¬Ė›Ć„ļæ½Ä�ā‚¬Ė›Ä€Ā¢res af seten alligevel.
				if ($antalVinkler == 2) {
					goto jump1;
				} else {
					for($i = 1; $i <= $globalAntalValgfrie; $i ++) {
						if ($dynValmPlacering [$i] == "24") {
							if ($dynValmType [$i] == 2) {
								// Vi kan springe over!
								goto jump1;
							} else {
								break;
							}
						}
					}
				}
			}
			if ($kaldNummer == 5) {
				// Vinkelhus 45 grader
				// Her er der ingen ide i at forĆ„ā€ Ä�ā‚¬Ā¦Ć„ā‚¬Ä€Ā³ge eller reducere, da der bare skĆ„ā€ Ä�ā‚¬Ė›Ć„ļæ½Ä�ā‚¬Ė›Ä€Ā¢res af seten alligevel.
				for($i = 1; $i <= $globalAntalValgfrie; $i ++) {
					if ($dynValmPlacering [$i] == "244") {
						if ($dynValmType [$i] == 2) {
							// Vi kan springe over!
							goto jump1;
						} else {
							break;
						}
					}
				}
			}
		} else {
			if ($hhvalm == 2 || $hhvalm == 4) {
				goto jump1;
			}
		}
		// variables that will be received in metodest functions
		global $vindsk_v, $vindsk_h, $halvsten;
		// --
		if ($hhtype == 7 && $kaldNummer == 2) {
			$knast_nummer = 3;
			$this->session->set_userdata ( array (
					'knast_rpt' => '1' 
			) );
		} else {
			$knast_nummer = 0;
		}
		switch ($metode) {
			case 2 :
				$x = $this->metodest_hh2 ( $husLaengde, $husRaekker, $husKolonner, $knast_nummer ); // funktion til beregning af kolonner mm.
				$husLaengde = $x;
				$husKolonner = $this->AntalKolonner_hh ( $husLaengde );
				
				if ($tilfoej_halv_fjern_hel == true) {
					$husKolonner = $husKolonner - 1;
				}
				break;
			case 3 :
				$x = $this->metodest_hh3 ( $husLaengde, $husRaekker, $husKolonner, $knast_nummer ); // funktion til beregning af kolonner mm.
				$husLaengde = $x;
				
				$husKolonner = $this->AntalKolonner_hh ( $husLaengde );
				
				if ($tilfoej_halv_fjern_hel == true) {
					$husKolonner = $husKolonner - 1;
				}
				break;
		}
		jump1:
		if (! isset ( $_SESSION['dobbeltving'] )) {
			$_SESSION['dobbeltving'] = 0;
		}
		if (! isset ( $rygafslut )) {
			$rygafslut = 0;
		}
		if (! isset ( $rygstart )) {
			$rygstart = 0;
		}
		$am_hh = $this->mm_hh ( $husLaengde, $husTagside ); // Antal mÄ€Ā² tag
		$ahes = $this->hele_sten ( $husRaekker, $husKolonner ); // Antal hele sten
		if ($facon == true) {
			$ars = Floor ( (($husLaengde) / 1000) * 3.33 ) + 1;
		} else {
			$ars = Floor ( (($husLaengde) / 1000) * 2.7 ) + 1;
		}
		
		switch ($hhtype) {
			case 6 :
				$pyramidesten = 1;
				break;
			case 5 :
				// Vinkelhus 45 grader
				if ($kaldNummer == 3) {
					$rygafslut = 0;
					$rygstart = 0;
					for($i = 1; $i <= $globalAntalValgfrie; $i ++) {
						if ($dynValmPlacering [$i] == "3") {
							if ($dynValmType [$i] == 1) {
								// Der skal vere en gavl for der skal bruges en "starter"
								$rygstart = 1;
								break;
							}
						}
					}
					
					// rygstart = 1
					if ($metode == 2 || $metode == 3) {
						for($i = 1; $i <= $globalAntalValgfrie; $i ++) {
							if ($dynValmPlacering [$i] == "3") {
								if ($dynValmType [$i] == 1 || $dynValmType [$i] == 3) {
									if ($metode == 2) {
										$_SESSION['dobbeltving'] = $_SESSION['dobbeltving'] / 2;
									}
									if ($metode == 3) {
										$vindsk_h = $vindsk_h / 2;
										$vindsk_v = $vindsk_v / 2;
									}
								} else {
									$vindsk_h = 0;
									$vindsk_v = 0;
									$_SESSION['dobbeltving'] = 0;
								}
								break;
							}
						}
					}
				}
				if ($kaldNummer == 4) {
					if ($antalVinkler == 2) {
						$rygafslut = 0;
						$rygstart = 0;
						$vindsk_h = 0;
						$vindsk_v = 0;
						$_SESSION['dobbeltving'] = 0;
					} else {
						$rygafslut = 0;
						$rygstart = 0;
						$fundetGavlen = false;
						for($i = 1; $i <= $globalAntalValgfrie; $i ++) {
							if ($dynValmPlacering [$i] == "3") {
								if ($dynValmType [$i] == 1) {
									// Der skal vere en gavl for der skal bruges en "starter"
									$rygstart = $rygstart + 1;
									$fundetGavlen = true;
								}
							}
							
							if ($dynValmPlacering [$i] == "24") {
								if ($dynValmType [$i] == 1) {
									// Der skal vere en gavl for der skal bruges en "starter"
									if ($rygstart > 0) {
										$rygstart = 0;
										$rygslut = 1;
									} else {
										$rygstart = 1;
									}
								}
							}
						}
						if ($fundetGavlen == true) {
							if ($rygstart > 1) {
								$rygstart = 0;
								$rygafslut = 1;
							} else {
								$rygstart = 0;
							}
						}
						
						// $rygstart = 1
						if ($metode == 2 || $metode == 3) {
							for($i = 1; $i <= $globalAntalValgfrie; $i ++) {
								if ($dynValmPlacering [$i] == "24") {
									if ($dynValmType [$i] == 1 || $dynValmType [$i] == 3) {
										if ($metode == 2) {
											$_SESSION['dobbeltving'] = $_SESSION['dobbeltving'] / 2;
										}
										if ($metode == 3) {
											$vindsk_h = $vindsk_h / 2;
											$vindsk_v = $vindsk_v / 2;
										}
									} else {
										$vindsk_h = 0;
										$vindsk_v = 0;
										$_SESSION['dobbeltving'] = 0;
									}
									break;
								}
							}
						}
					}
				}
				if ($kaldNummer == 5) {
					$rygafslut = 0;
					$rygstart = 0; // 1
					$fundetGavlen = false;
					for($i = 1; $i <= $globalAntalValgfrie; $i ++) {
						if ($dynValmPlacering [$i] == "3") {
							if ($dynValmType [$i] == 1) {
								// Der skal vere en gavl for der skal bruges en "starter"
								$fundetGavlen = true;
								$rygstart = $rygstart + 1;
							}
						}
						
						if ($dynValmPlacering [$i] == "244") {
							if ($dynValmType [$i] == 1) {
								// Der skal vere en gavl for der skal bruges en "starter"
								$rygstart = $rygstart + 1;
							}
						}
					}
					
					if ($fundetGavlen == true) {
						if ($rygstart > 1) {
							$rygstart = 0;
							$rygafslut = 1;
						} else {
							$rygstart = 0;
						}
					}
					
					if ($metode == 2 || $metode == 3) {
						for($i = 1; $i <= $globalAntalValgfrie; $i ++) {
							if ($dynValmPlacering [$i] == "244") {
								if ($dynValmType [$i] == 1 || $dynValmType [$i] == 3) {
									if ($metode == 2) {
										$_SESSION['dobbeltving'] = $_SESSION['dobbeltving'] / 2;
									}
									if ($metode == 3) {
										$vindsk_h = $vindsk_h / 2;
										$vindsk_v = $vindsk_v / 2;
									}
								} else {
									$vindsk_h = 0;
									$vindsk_v = 0;
									$_SESSION['dobbeltving'] = 0;
								}
								break;
							}
						}
					}
				}
				
				break;
			default :
				if ($kaldNummer == 1) {
					if ($globalAntalValgfrie > 0) {
						$rygstart = 0;
						$rygafslut = 0;
						if (($dynValmType [$gavl1Id] >= 2 && $dynValmType [$gavl2Id] == 1) || ($dynValmType [$gavl1Id] == 1 && $dynValmType [$gavl2Id] >= 2)) {
							// Hvis der er valm i den ene og gavl i den anden, benyttes ALTID en rygnings starter
							$rygafslut = 0;
							$rygstart = 1;
						}
						if ($dynValmType [$gavl1Id] == 1 && $dynValmType [$gavl2Id] == 1) {
							$rygafslut = 1;
							$rygstart = 1;
						}
						
						// Korrektion for forskellige gavl typer
						if ((($dynValmType [$gavl1Id] == 1 || $dynValmType [$gavl2Id] == 3) && $dynValmType [$gavl2Id] == 2) || (($dynValmType [$gavl2Id] == 1 || $dynValmType [$gavl2Id] == 3) && $dynValmType [$gavl1Id] == 2)) {
							// Den ene side er gavl eller halvvalm og den anden helvalm
							if ($metode == 2) {
								$_SESSION['dobbeltving'] = $_SESSION['dobbeltving'] / 2;
							}
							if ($metode == 3) {
								$vindsk_h = $vindsk_h / 2;
								$vindsk_v = $vindsk_v / 2;
							}
						}
						// ---------------------------------------
					}
				}
				if ($kaldNummer == 2) {
					$rygstart = 0;
					$rygafslut = 0;
					
					if (($dynValmType [3] >= 2 && $dynValmType [4] == 1) || ($dynValmType [3] == 1 && $dynValmType [4] >= 2)) {
						// Hvis der er valm i den ene og gavl i den anden, benyttes ALTID en rygnings starter
						$rygafslut = 0;
						$rygstart = 1;
					}
					if ($dynValmType [3] == 1 && $dynValmType [4] == 1) {
						$rygafslut = 1;
						$rygstart = 1;
					}
					// Korrektion for forskellige gavl typer
					if ((($dynValmType [3] == 1 || $dynValmType [3] == 3) && $dynValmType [4] == 2) || (($dynValmType [4] == 1 || $dynValmType [4] == 3) && $dynValmType [3] == 2)) {
						// Den ene side er gavl eller halvvalm og den anden helvalm 
						if ($metode == 2) {
							$_SESSION['dobbeltving'] = $_SESSION['dobbeltving'] / 2;
						}
						if ($metode == 3) {
							$vindsk_h = $vindsk_h / 2;
							$vindsk_v = $vindsk_v / 2;
						}
					}
					// ---------------------------------------
				}
		}
		
		// set gathered final data in db
		// there is record for each session id
		$aml = $this->legte_hh ( $husRaekker, $husLaengde ); // Antal meter lĆ¦gte
		$tagok = $this->session->userdata ( 'tag' );
		$c = $tagok . '.bmp';
		$arr = array (
				"Type" => 1,
				"Normalsten" => $ahes,
				
				"Dobbeltvinget" => $_SESSION['dobbeltving'],
				"halvsten" => $halvsten,
				
				"Pyramidesten" => $pyramidesten,
				"Pultsten" => 0,
				"PultvindskH" => 0,
				"PultvindskV" => 0,
				"Valmbegynder" => 0,
				"Valmkappe" => 0,
				"kvm" => $am_hh,
				"legte" => $aml,
				"legteafstand" => $mal,
				"AntalRekker" => $husRaekker,
				"AntalKolonner" => $husKolonner,
				
				"fuglegitter" => ($husLaengde * 2),
				"alurullerRygninger" => $husLaengde,
				
				"navn" => $Navn,
				"hustype" => $c 
		);
		if ($mal >= 31 && $mal < 33) {
			$arr ["VindskH31"] = $vindsk_h;
			$arr ["VindskV31"] = $vindsk_v;
		}
		if ($mal >= 33 && $mal < 35) {
			$arr ["VindskH33"] = $vindsk_h;
			$arr ["VindskV33"] = $vindsk_v;
		}
		if ($mal >= 35 && $mal < 37) {
			$arr ["VindskH35"] = $vindsk_h;
			$arr ["VindskV35"] = $vindsk_v;
		}
		switch ($metode) {
			case 1 :
				$arr ["Metode"] = "Afslutning med sternbredt";
				break;
			case 2 :
				$arr ["metode"] = "Afslutning med dobbeltvinget";
				break;
			case 3 :
				$arr ["metode"] = "Afslutning med vindskedesten";
				break;
		}
		if ($hhtype == '5') {
			$rygstart = $rygstart + $rygafslut;
			$rygafslut = 0;
		}
		
		if ($facon == true) {
			$arr ["faconrygning"] = $ars;
			$arr ["faconrygafslut"] = $rygafslut;
			$arr ["faconrygstart"] = $rygstart;
		} else {
			$arr ["rygning"] = $ars;
			$arr ["rygstart"] = $rygstart;
			$arr ["rygafslut"] = $rygafslut;
		}
		
		//echo $facon;
		//die();
		
		if ($hhtype == '5') {
			$this->adddatadb ( $arr, true );
		} else {
			if ($kaldNummer == 2) {
				$this->adddatadb ( $arr, true );
			}
			$this->adddatadb ( $arr, false );
		}
		
		if (! $this->session->userdata ( 'res_hl' )) {
			$this->session->set_userdata ( array (
					'res_hl' => $husLaengde,
					'res_hb' => $husBredde,
					'res_hv' => $husVinkel,
					'res_ra' => $husRaekker,
					'res_ka' => $husKolonner 
			) );
		}
	}
	function adddatadb($valuearr, $rpt, $knastnr = 0) {
		// ////knast nr/////
		// 1.knast
		// 2. knast
		// 3 beregnhoved
		// 4,5 beregn_tb
		$sessid = $this->session->userdata ( 'sessuser_id' );
		if ($rpt) {
			$skip_update = 0;
			$cond = array (
					"Type" => '9',
					"sessid" => $sessid 
			);
			$query = $this->db->get_where ( 'beregn', $cond );
			$rowcount = $query->num_rows ();
			if ($rowcount == 1 && $knastnr == 1) {
				$skip_update = 1;
			}
			if ($rowcount == 2 && $knastnr == 2) {
				$skip_update = 1;
			}
			
			//
			if (! $skip_update) {
				if ($this->session->userdata ( 'tmpnum' )) {
					$tmpnum = ( int ) $this->session->userdata ( 'tmpnum' );
					$tmpnum ++;
					$this->session->set_userdata ( array (
							'tmpnum' => $tmpnum 
					) );
				} else {
					$tmpnum = 1;
					$this->session->set_userdata ( array (
							'tmpnum' => $tmpnum 
					) );
				}
				$this->db->insert ( 'beregn', array (
						'sessid' => $sessid,
						'tmpnum' => $tmpnum 
				) );
				
				$this->db->where ( 'sessid', $sessid );
				$this->db->where ( 'tmpnum', $tmpnum );
				$this->db->update ( 'beregn', $valuearr );
			}
		} else {
			// check if has already been set
			$cond = array (
					"Type" => $valuearr ["Type"],
					"sessid" => $sessid 
			);
			$query = $this->db->get_where ( 'beregn', $cond );
			if (! $query->num_rows () > 0 || $knastnr == 3) {
				if ($this->session->userdata ( 'tmpnum' )) {
					$tmpnum = ( int ) $this->session->userdata ( 'tmpnum' );
					$tmpnum ++;
					$this->session->set_userdata ( array (
							'tmpnum' => $tmpnum 
					) );
				} else {
					$tmpnum = 1;
					$this->session->set_userdata ( array (
							'tmpnum' => $tmpnum 
					) );
				}
				$this->db->insert ( 'beregn', array (
						'sessid' => $sessid,
						'tmpnum' => $tmpnum 
				) );
				
				$this->db->where ( 'sessid', $sessid );
				$this->db->where ( 'tmpnum', $tmpnum );
				$this->db->update ( 'beregn', $valuearr );
			}
		}
	}
	function legte_hh($ral, $hll) {
		return (Floor ( 2 * ($ral + 1) * ($hll / 1000) ) + 1); // Antal meter lĆ¦gte
	}
	function mm_hh($lengde, $tagside) {
		return $this->todec ( (2 * ($lengde / 1000000) * ( int ) ($tagside)) );
	}
	function hele_sten($raF, $kaF) {
		return 2 * ($raF * $kaF);
	}
	function metodest_hh2(&$stl, &$rek, &$kol, $knastnr = 0) {
		global $ka, $knast_met, $dekbredde, $DekDobbv, $hhtype, $resmet, $halvstendek, $halve, $stenvalgtnr, $vindsk_v, $vindsk_h, $halvsten;
		global $mtd_done, $mtdk_done, $kaldNummer;
		$vindsk_v = 0;
		$vindsk_h = 0;
		$halvsten = 0;
		$_SESSION['dobbeltving'] = 0;
		
		$tagd_n = Floor ( $kol * $dekbredde );
		$dekn_ialt = Floor ( $tagd_n + $DekDobbv );
		$mang_dek = Floor ( $stl - $dekn_ialt );
		$lengde = $stl;
		$knast_rpt = $this->session->userdata ( 'knast_rpt' );
		
		$mtd = 0;
		if ($kaldNummer == 2) {
			$mtd = 1;
			$mtcnt = 'k2';
		} else {
			if ($knast_rpt == '1') {
				if ($mtdk_done [1] != '1' && $knastnr == 2) {
					$mtd = 1;
					$mtcnt = 'k2';
				}
				if ($mtdk_done [0] != '1') {
					$mtd = 1;
					$mtcnt = 'k1';
				}
			}
			
			if ($mtd_done != 1) {
				$mtd = 1;
				$mtcnt = '1';
			}
		}
		if ($mtd == 1) {
			if ($hhtype == '5') {
				// Ved vinkelhus 45 grader skal der ikke velges noget!
				$halve = false;
			} else {
				if ($mang_dek !== 0) {
					
					$tmpsess = array (
							'lengde' => $lengde,
							'dekdobbv' => $DekDobbv,
							'tagd_n' => $tagd_n,
							'dekn_ialt' => $dekn_ialt,
							'ka' => $ka,
							'knast_met' => $knast_met,
							'location' => '1',
							'mang_dek' => $mang_dek,
							'dekbredde' => $dekbredde,
							'halvstendek' => $halvstendek,
							'stenvalgt' => $stenvalgtnr,
							'mtcnt' => $mtcnt,
							'MET_MET' => $mtd,
							'mtd_done' => $mtd_done,
							'mtdk_done' => $mtdk_done,
							'knast_nr' => $knastnr 
					);
					$this->tmpSessVars ( $tmpsess );
					redirect ( 'home/met2q' );
				} else {
					goto m2;
				}
			}
		} else {
			if ($knastnr == 1) {
				$tilfoej_halv_fjern_hel = $this->session->userdata ( '1_tilfoej_halv_fjern_hel' );
				$halve = ( bool ) $this->session->userdata ( '1_halve' );
				$resmet = $this->session->userdata ( '1_resmet' );
			} else if ($knastnr == 2) {
				$tilfoej_halv_fjern_hel = $this->session->userdata ( '2_tilfoej_halv_fjern_hel' );
				$halve = ( bool ) $this->session->userdata ( '2_halve' );
				$resmet = $this->session->userdata ( '2_resmet' );
			} else if ($knastnr == 3) {
				$tilfoej_halv_fjern_hel = $this->session->userdata ( '3_tilfoej_halv_fjern_hel' );
				$halve = ( bool ) $this->session->userdata ( '3_halve' );
				$resmet = $this->session->userdata ( '3_resmet' );
			} else {
				
				$tilfoej_halv_fjern_hel = $this->session->userdata ( 'tilfoej_halv_fjern_hel' );
				$halve = ( bool ) $this->session->userdata ( 'halve' );
				$resmet = $this->session->userdata ( 'resmet' );
			}
		}
		$stl = $stl + $resmet;
		if ($halve == true) {
			$halvsten = Floor ( (2 * $rek) );
		}
		$this->session->set_userdata ( array (
				'halvsten' => $halvsten 
		) );
		m2:
		
		$vindsk_v = 0;
		$vindsk_h = 0;
		$_SESSION['dobbeltving'] = Floor ( (2 * $rek) );
		return $stl;
	}
	function metodest_hh3($stl, $rek, $kol, $knastnr = 0) {
		// knast_nr = 3 -> 2nd from h-hus
		global $ka, $knast_met, $dekbredde, $DekDobbv, $DekVindskH, $DekVindskV, $hhtype, $resmet, $halvstendek, $halve, $stenvalgtnr, $vindsk_v, $vindsk_h, $halvsten;
		global $mtd_done, $mtdk_done, $kaldNummer;
		global $tagd_n, $dekn_ialt, $mang_dek;
		$vindsk_v = 0;
		$vindsk_h = 0;
		$halvsten = 0;
		$_SESSION['dobbeltving'] = 0;
		
		$tagd_n = Floor ( $kol * $dekbredde );
		$dekn_ialt = Floor ( $tagd_n + $DekVindskH + $DekVindskV );
		$mang_dek = Floor ( $stl - $dekn_ialt );
		$lengde = $stl;
		$mtd = 0;
		
		$knast_rpt = $this->session->userdata ( 'knast_rpt' );
		
		if ($kaldNummer == 2) {
			$mtd = 1;
			$mtcnt = 'k2';
		} else {
			if ($knast_rpt == '1') {
				if ($mtdk_done [1] != '1' && $knastnr == 2) {
					$mtd = 1;
					$mtcnt = 'k2';
				}
				if ($mtdk_done [0] != '1') {
					$mtd = 1;
					$mtcnt = 'k1';
				}
			}
			
			if ($mtd_done != 1) {
				$mtd = 1;
				$mtcnt = '1';
			}
		}
		
		if ($mtd == 1) {
			if ($hhtype == '5') {
				// Ved vinkelhus 45 grader skal der ikke velges noget!
				$halve = false;
			} else {
				if ($mang_dek != 0) {
					$tmpsess = array (
							'lengde' => $lengde,
							'DekVindskH' => $DekVindskH,
							'DekVindskV' => $DekVindskV,
							'tagd_n' => $tagd_n,
							'dekn_ialt' => $dekn_ialt,
							'ka' => $ka,
							'knast_met' => $knast_met,
							'location' => '1',
							'mang_dek' => $mang_dek,
							'dekbredde' => $dekbredde,
							'halvstendek' => $halvstendek,
							'stenvalgt' => $stenvalgtnr,
							'mtcnt' => $mtcnt,
							'MET_MET' => $mtd,
							'mtd_done' => $mtd_done,
							'mtdk_done' => $mtdk_done,
							'knast_nr' => $knastnr 
					);
					
					$this->tmpSessVars ( $tmpsess );
					$link = 'home/met3q';
					
					redirect ( $link );
				} else {
					goto m2;
				}
			}
		} else {
			if ($knastnr == 1) {
				$tilfoej_halv_fjern_hel = $this->session->userdata ( '1_tilfoej_halv_fjern_hel' );
				$halve = ( bool ) $this->session->userdata ( '1_halve' );
				$resmet = $this->session->userdata ( '1_resmet' );
			} else if ($knastnr == 2) {
				$tilfoej_halv_fjern_hel = $this->session->userdata ( '2_tilfoej_halv_fjern_hel' );
				$halve = ( bool ) $this->session->userdata ( '2_halve' );
				$resmet = $this->session->userdata ( '2_resmet' );
			} else if ($knastnr == 3) {
				$tilfoej_halv_fjern_hel = $this->session->userdata ( '3_tilfoej_halv_fjern_hel' );
				$halve = ( bool ) $this->session->userdata ( '3_halve' );
				$resmet = $this->session->userdata ( '3_resmet' );
			} else {
				
				$tilfoej_halv_fjern_hel = $this->session->userdata ( 'tilfoej_halv_fjern_hel' );
				$halve = ( bool ) $this->session->userdata ( 'halve' );
				$resmet = $this->session->userdata ( 'resmet' );
			}
		}
		$stl = $stl + $resmet;
		if ($halve == true) {
			$halvsten = Floor ( (2 * $rek) );
		} else {
			$halvsten = 0;
		}
		$this->session->set_userdata ( array (
				'halvsten' => $halvsten 
		) );
		m2:
		
		$vindsk_v = 2 * $rek;
		$vindsk_h = 2 * $rek;
		return $stl;
	}
	function tmpSessVars($tmpsess) {
		foreach ( $tmpsess as $var => $value ) {
			$setdata ['tmp_' . $var] = $value;
		}
		$this->session->set_userdata ( $setdata );
	}
	function tagside_hh($bredde, $vinkel) {
		$pi2 = 0.017453292;
		return $bredde / (2 * round ( (cos ( $pi2 * $vinkel )), 3 ));
	}
	function RED_hh($vinkel) {
		if ($vinkel <= 25) {
			return 25;
		}
		if ($vinkel > 25 && $vinkel <= 35) {
			return 20;
		}
		if ($vinkel > 35) {
			return 10;
		}
	}
	function antalrekker_hh($tagside, $RED, $Stenl, $legteaf) {
		global $minLegte;
		$antalrekker_hh = Floor ( (($tagside - $RED) - $Stenl) / $legteaf ) + 1 + 1;
		If (($antalrekker_hh - 1) !== 0) {
			$aib = ($this->todec ( ((($tagside - 340 - $RED) / ($antalrekker_hh - 1))) ));
		} else {
			$aib = ($this->todec ( ((($tagside - 340 - $RED) / 1)) ));
		}
		If ($antalrekker_hh <= 0) {
			$antalrekker_hh = 1;
		}
		If ($antalrekker_hh <= 2) {
			return $antalrekker_hh;
		}
		$nrplus = 0;
		$nummerett = 0;
		$nrminus = 0;
		While ( $aib > $legteaf || $aib < $minLegte ) {
			
			If ($aib > $legteaf) {
				$antalrekker_hh = $antalrekker_hh + 1;
				$nummerett = $antalrekker_hh;
				$nrplus = $nrplus + 1;
			}
			
			If ($aib < $minLegte) {
				$antalrekker_hh = $antalrekker_hh - 1;
				$nrminus = $nrminus + 1;
			}
			
			$aib = ($this->todec ( ((($tagside - 340 - $RED) / ($antalrekker_hh - 1))) )); // Ny lÄ™gteafstand
			If ($nrplus > 4 && $nrminus > 4) {
				$antalrekker_hh = $nummerett;
				GoTo lastline;
			}
		}
		lastline:
		return $antalrekker_hh;
	}
	function todec($tal) {
		// $talstreng = (string) $tal;
		// $a = strlen ( talstreng );
		// For($b = 1; $b <= $a; $b ++) {
		// $c = substr( $talstreng, $b, $b+1 );
		// If (Asc ( $c) == 44) {
		// GoTo komma;
		// }
		// }
		// If (b == a) {
		// $talstreng2 = talstreng;
		// GoTo lastline;
		// }
		// komma:
		// If ($a - $b = 1) {
		// $b = $b + 1;
		// GoTo b1;
		// }
		// $b = $b + 2;
		
		// b1:
		// $talstreng2 = Mid ( $talstreng, 1, $b );
		// lastline:
		// $todec = CSng ( $talstreng2 );
		return round ( $tal, 5 );
	}
	function AntalKolonner_hh($lengde) {
		
		global $metode, $DekDobbv, $dekbredde, $DekVindskH, $DekVindskV, $byggereduktion;
		if (! $metode) {
			$metode = $this->session->userdata ( 'metode' );
		}
		if (! $metode) {
			$metode = 1;
		}
		switch ($metode) {
			case 1 :
				$AntalKolonner_hh = (floor( ((($lengde) / $dekbredde) + 1)));
				
				// Oprindelig: AntalKolonner_hh = ((int)(((lĆ¦ngde - fuldstensbredde) / dĆ¦kbredde) + 1))
				if (($lengde - (floor( ($AntalKolonner_hh * $dekbredde) + $byggereduktion))) == $dekbredde) {
					$AntalKolonner_hh = $AntalKolonner_hh + 1;
				}
				break;
			case 2 :
				$AntalKolonner_hh = floor( (($lengde - $DekDobbv) / $dekbredde));
				break;
			case 3 :
				$AntalKolonner_hh = floor( (($lengde - $DekVindskV - $DekVindskH) / $dekbredde));
				break;
		}
		if ($AntalKolonner_hh <= 0) {
			$AntalKolonner_hh = 1;
		}
		if ($AntalKolonner_hh<42) {
		}
		return $AntalKolonner_hh;
	}
	function hent_tilg() {
		$stenvalgtnr = $this->session->userdata ( 'stenvalgtnr' );
		$farvehandle = $this->session->userdata ( 'farvevalg' );
		$farveid = $this->session->userdata ( 'farveid' );
		$tagunder = $this->session->userdata ( 'tagunder' );
		switch ($stenvalgtnr) {
			case 0 :
				$stee = "DS";
				break;
			case 3 :
				$stee = "UK";
				break;
			case 4 :
				$stee = "VUP";
				break;
		}
		$this->db->select ( 'IBF, Tun, Navn,Enh,Rkf,Standart,Tilbehor,Rt,vu,ut' );
		
		$cond = array (
				$stee => '1',
				'farveid' => $farveid 
		);
		
		$query = $this->db->get_where ( 'vare', $cond );
		
		$db = $query->result ();
// 		echo '<pre>';
// 		print_r($this->db->last_query());
// 		echo '<br>';
// 		foreach ( $db as $obj ) {
// 			echo $obj->IBF.'<br>';
// 		}
// 		echo '</pre>';
// 		exit;
		$res = array ();
		foreach ( $db as $obj ) {
			if (($tagunder == "understrygning" && $obj->vu == '0') || ($tagunder == "undertag" && $obj->ut == '0')) {
				unset ( $obj );
			} else {
				$obj->TUN = $obj->Tun;
				unset ( $obj->Tun );
				$obj->Enhed = $obj->Enh;
				unset ( $obj->Enh );
				$obj->Rekkefulge = $obj->Rkf;
				unset ( $obj->Rkf );
				$obj->Rettes = $obj->Rt;
				unset ( $obj->Rt );
				$obj->Antal = '';
				unset ( $obj->ut );
				unset ( $obj->vu );
			}
			if (isset ( $obj )) {
				array_push ( $res, $obj );
			}
		}
		
		$json = json_encode ( $res );
		$this->session->set_userdata ( array (
				'tilgengelig' => $json 
		) );
	}
	public function Vis_flex($stennummer, $stentypee, $antal) {
		global $stenvalgt, $beregn, $farvevalg, $tilgTable_stand;
		if (strlen ( $stennummer ) > 3) {
			$varen = $stennummer;
		} else {
			$this->db->select ( $stentypee );
			
			$query = $this->db->get_where ( 'definition_stentype', array (
					'stentype' => $stenvalgt 
			) );
			$result = $query->result ();
			$result = $result[0];
			
			if (strlen ( $result->$stentypee ) == 4) {
				$varen = $result->$stentypee;
			} else {
				$varen = $result->$stentypee . $stennummer . ( string ) $farvevalg;
			}
		}
		if ($varen == "9576") {
			
			if ($farvevalg == 50) {
				$varen = "9577";
			}
		}
		
		// IBF ALU GRATRULLER - D.S.:
		// --------------------------
		if ($varen == "9566") {
			if ($farvevalg == 50) {
				$varen = "9567";
			}
		}
		
		foreach ( $tilgTable_stand as $obj ) {
			
			if (substr ( ( string ) ($obj->IBF), 0, strlen ( ( string ) ($varen) ) ) == ( string ) ($varen)) {
				if ($obj->Standart == "1") {
					$obj->Antal = $obj->Antal . $antal;
					
					// Speciel sortering af Vindskedesten
					switch ($stennummer) {
						case "12" :
							$obj->Rekkefulge = 32;
							break;
						case "13" :
							$obj->Rekkefulge = 33;
							break;
						case "14" :
							$obj->Rekkefulge = 34;
							break;
						case "15" :
							$obj->Rekkefulge = 35;
							break;
						case "16" :
							$obj->Rekkefulge = 36;
							break;
						case "17" :
							$obj->Rekkefulge = 37;
							break;
					}
					
					break;
				}
			}
		}
	}
	function deleteberegn() {
		$sessid = $this->session->userdata ( 'sessuser_id' );
		$this->db->where ( 'sessid', $sessid );
		$this->db->delete ( 'beregn' );
	}
	function checkSession(){
		if ($this->session->userdata ('stenvalgtnr' ) !== False){
			return true;
		} else {
			redirect ( 'home' );
		}
	}
}

		<div class="row">
			<div class="col-md-8 col-md-offset-1">
				<div class="panel panel-primary">
					<div class="panel-heading"><?php echo $title?></div>
					<div class="panel-body">

						<?php echo form_open_multipart('color/'.$action);?>
							
							<div class="form-group row">
								<div class="col-sm-3">
									<label class="control-label" for="nr">Name:</label>
								</div>
								<div class="col-sm-7">
									<input type="text" name="name" class="form-control" id="name" placeholder=""
										required
									value="<?php echo $name?>">
								</div>
							</div>
							<div class="form-group row">
								<div class="col-sm-3">
									<label class="control-label" for="nr">Order num:</label>
								</div>
								<div class="col-sm-1">
									<input type="text" name="num" class="form-control" id="num" placeholder=""
										required
									value="<?php echo $num?>">
								</div>
							</div>							
							<div class="form-group row">
								<div class="col-sm-3">
									<label class="control-label">Dobbelt-S Image</label>
								</div>
								<div class="col-sm-1">
									<input type="checkbox" name="dobbelt" class="form-control" placeholder="" <?php if($dobbelt==1) echo 'checked'; ?>/>
								</div>
								<div class="col-sm-7">
									<input type="file" name="dobbelt_file" class="form-control" placeholder=""
									value="<?php echo $dobbelt_file?>">
								</div>
							</div>
							<div class="form-group row">
								<div class="col-sm-3">
									<label class="control-label">Vinge Okonomi Image</label>
								</div>
								<div class="col-sm-1">
									<input type="checkbox" name="vinge" class="form-control" placeholder="" <?php if($vinge==1) echo 'checked'; ?>/>
								</div>
								<div class="col-sm-7">
									<input type="file" name="vinge_file" class="form-control" placeholder=""
										
									value="<?php echo $vinge_file?>">
								</div>
							</div>
							<div class="form-group row">
								<div class="col-sm-3">
									<label class="control-label">Vinge Okonomi plus Image</label>
								</div>
								<div class="col-sm-1">
									<input type="checkbox" name="vinge_plus" class="form-control" placeholder="" <?php if($vinge_plus==1) echo 'checked'; ?>/>
								</div>
								<div class="col-sm-7">
									<input type="file" name="vinge_plus_file" class="form-control" placeholder=""
										
									value="<?php echo $vinge_plus_file?>">
								</div>
							</div>

							<div class="col-sm-offset-3 col-sm-8">
								<div class="pull-right"><button type="button" class="btn btn-primary" onclick="window.location.href='<?php echo base_url()?>color'">Back</button>
								<button type="submit" class="btn btn-success">Save</button></div>
							</div>


						</form>

					</div>

				</div>

			</div>
		</div>
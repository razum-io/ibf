<style>
#per {
	margin-top: 40px;
}

#imgcont3d {
	margin-top: 150px;
	margin-bottom: 100px;
}
</style>
<script>
$( document ).ready(function() {
	hvhus3();
	dynValmType = <?php echo $dynValmType; ?>;
	baseurl = <?php echo '"'.base_url('assets/illustration').'"'; ?>;
	antalVinklerVinkel = <?php echo '"'.$antalvinklervinkel.'"'; ?>;
	antalVinkler = <?php echo '"'.$antalvinkler.'"'; ?>;
	antalTilbygninger = <?php echo '"'.$antaltilbygninger.'"'; ?>;
	antalKnaster = <?php echo '"'.$antalknaster.'"'; ?>;
	tag = <?php echo '"'.$tag.'"'; ?>;
	drawillustration('hvhus3');

	retning = (<?php echo json_encode($retning)?>);
	load_hvhus3(retning);
});
</script>
<?php
$buttonNext = array (
		'name' => 'next',
		'id' => 'next',
		'class' => 'form-control next',
		'onclick' => 'val_hvhus3()',
		'type' => 'button',
		'content' => 'Frem ->' 
);
$back = "'" . base_url ( 'home/vinkel' ) . "'";
$buttonBack = array (
		'name' => 'back',
		'id' => 'back',
		'class' => 'form-control next',
		'type' => 'button',
		'onclick' => 'window.location.href=' . $back,
		'content' => '<-- Tilbage' 
);
$under_capt = array (
		'undertag' => 'Undertag',
		'understrygning' => 'Understrygning' 
);
$valg_af_capt = array (
		'False' => 'Alm. rygningssten',
		'True' => 'Faconrygningssten' 
);

?>
<?php

echo form_open ( 'getdata/hvhus3', array (
		'id' => 'hvhus3' 
) );
?>
<div class="container">
	<div style="margin-top:50px;"></div>
	<div class=row>
		<div class=col-md-8>
			<div class=row>
				<div class=col-md-12>
					<div class="row input-group">
						<div class="textfieldtext col-md-7">
						a:  Længde på hus incl. udhæng:
						</div>
						<div class="col-md-3 ">
						<?php
						$data = array (
								'name' => 'hvhus3_l',
								'id' => 'hvhus3_l',
								'class' => 'textfield textfieldwithspan num' 
						);
						echo form_input ( $data );
						?>
						<span class="textfieldspan">m</span> 
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class=col-md-8>
			<div class=row>
				<div class=col-md-12>
					<div class="row input-group">
						<div class="textfieldtext col-md-7">
						b:  Bredde på hus incl. udhæng:
						</div>
						<div class="col-md-3 ">
						<?php
						$data = array (
								'name' => 'hvhus3_b',
								'id' => 'hvhus3_b',
								'class' => 'textfield textfieldwithspan num' 
						);
						echo form_input ( $data );
						?>
						<span class="textfieldspan">m</span> 
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class=col-md-8>
			<div class=row>
				<div class=col-md-12>
					<div class="row input-group">
						<div class="textfieldtext col-md-7">
						c:  Taghældning:
						</div>
						<div class="col-md-3 ">
						<?php
						$data = array (
								'name' => 'hvhus3_v',
								'id' => 'hvhus3_v',
								'class' => 'textfield textfieldwithspan num' 
						);
						echo form_input ( $data );
						?>
						<span class="textfieldspan">m</span> 
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="col-md-12" id="imgcont" style="margin-top:-100px">	</div>
			<div class="col-md-12" id="imgcont3d">	</div>
		</div>
	</div>

		<div class=col-md-12>
			<div class="row">
				<div class="col-md-6">
					<h4>Undertag/Understrygning</h4>
					<?php
					foreach ( $under_capt as $value => $capt ) :
					$radio_under = array (
							'name' => 'under',
							'value' => $value,
							'id' => $value 
					);
					?>
							<div class=col-md-12>

				  <?php echo form_radio($radio_under)?><label for="<?php echo $radio_under['id']?>"></label><span class=radiotext><?php echo $capt?></span>
				  
						</div>	
				<?php endforeach; ?>
				</div>
				<div class="col-md-6" style="display:none">
					<h4>Valg af rygningssten</h4>
					<?php  

					foreach ( $valg_af_capt as $value => $capt ) :
						$radio_valg_af = array (
								'name' => 'valg_af',
								'value' => $value,
								'id' => $value 
						);
						?>
						<div class=col-md-12>		

					  <?php echo form_radio($radio_valg_af)?><label for="<?php echo $radio_valg_af['id']?>"></label><span class=radiotext><?php echo $capt?></span>
					  </div>
								
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	<div class=row style="margin-bottom:200px">
		<div class="col-md-12">
			<div style="float:right">
			<img src="<?php echo base_url('assets')?>/img/left.png" onclick="window.location.href=<?php echo $back?>" style="cursor:pointer;">
			<img src="<?php echo base_url('assets')?>/img/right.png" onclick = 'val_hvhus3()' style="cursor:pointer;">
			</div>
		</div>
	<div>
</div>
<!--
<div class="row">
	<div class="col-md-6">
		<ul class="list-group">
			<li class="list-group-item">


				<div class="input-group">
		a:  Længde på hus incl. udhæng:
		<?php
		$data = array (
				'name' => 'hvhus3_l',
				'id' => 'hvhus3_l',
				'class' => 'form-control incolright num' 
		);
		echo form_input ( $data );
		?>
		<span class="input-group-addon incolrightadd">m</span>
				</div>
			</li>

			<li class="list-group-item">


				<div class="input-group">
		b:  Bredde på hus incl. udhæng:
		<?php
		$data = array (
				'name' => 'hvhus3_b',
				'id' => 'hvhus3_b',
				'class' => 'form-control incolright num' 
		);
		echo form_input ( $data );
		?>
		<span class="input-group-addon incolrightadd">m</span>
				</div>
			</li>

			<li class="list-group-item">


				<div class="input-group">
		c:  Taghældning:
		<?php
		$data = array (
				'name' => 'hvhus3_v',
				'id' => 'hvhus3_v',
				'class' => 'form-control incolright num' 
		);
		echo form_input ( $data );
		?>
		<span class="input-group-addon incolrightadd">°</span>
				</div>
			</li>
		</ul>
	</div>
	<div class="col-md-6" id="imgcont">	</div>
	<div class="col-md-6" id="imgcont3d">	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<!-- 	Undertag/Understrygning 
		<h4>Undertag/Understrygning</h4>
		<ul class="list-group">
			
<?php

foreach ( $under_capt as $value => $capt ) :
	$radio_under = array (
			'name' => 'under',
			'value' => $value,
			'id' => $value 
	);
	?>
			<li class="list-group-item">

  <?php echo form_radio($radio_under).$capt?>
  
  			</li>
<?php endforeach; ?>
		</ul>

	</div>
</div>

<?php
echo form_button ( $buttonNext );
echo form_button ( $buttonBack );
echo form_close ();

?>-->
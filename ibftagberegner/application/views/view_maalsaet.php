<style>
#per {
	min-height: 170px;
}
</style>
<script>
$( document ).ready(function() {
	tag = <?php echo $tag;?>;
	globalValmTypeMaalsaet = <?php echo $globalValmTypeMaalsaet;?>;
	antalVinklerVinkel = <?php echo $antalvinklervinkel;?>;
	laengehus = '<?php echo $laengehus;?>';
	antalTilbygninger = <?php echo $antaltilbygninger;?>;
	dynValmType = <?php echo $dynValmType;?>;
	dynValmPlacering = <?php echo $dynValmPlacering;?>;
	antalVinkler = <?php echo '"'.$antalvinkler.'"'; ?>;
	antalTilbygninger = <?php echo '"'.$antaltilbygninger.'"'; ?>;
	antalKnaster = <?php echo '"'.$antalknaster.'"'; ?>;
	maalsaet(tag,JSON.parse(laengehus),globalValmTypeMaalsaet,antalVinklerVinkel,antalTilbygninger,dynValmType,dynValmPlacering);

	baseurl = <?php echo '"'.base_url('assets/illustration').'"'; ?>;

	tag = <?php echo '"'.$tag.'"'; ?>;
	drawillustration('maalsaet','valm'+globalValmTypeMaalsaet); 

});
</script>
<?php
$buttonNext = array (
		'name' => 'next',
		'id' => 'next',
		'onclick' => 'val_maalsaet()',
		'class' => 'form-control next',
		'type' => 'button',
		'content' => 'Frem ->' 
);
$backlnk = "'" . $back . "'";
$buttonBack = array (
		'name' => 'back',
		'id' => 'back',
		'class' => 'form-control next',
		'type' => 'button',
		'onclick' => 'window.location.href=' . $backlnk,
		'content' => '<-- Tilbage' 
);
?>



<?php

echo form_open ( 'getdata/maalsaet', array (
		'id' => 'maalsaet' 
) );
?>
<div class="container">
	<div class=row>
		<div class="col-md-8">
				
		<?php
		for($i = 1; $i <= 6; $i ++) :

			?>	
		<h4  class="col-md-12" id="framecapt<?php echo $i?>">
			Valm <span id="framenum<?php echo $i;?>"><?php echo $i?></span>
		</h4>
		<div class=col-md-12 id="<?php echo 'framevalm'.$i?>">
		<div class=row>
		<div class=col-md-8   id="valmvinkel<?php echo $i?>">
			<div class="row input-group">
				<div class="textfieldtext col-md-8">
				a:Vinkel på valm:
				</div>
				<div class="col-md-4">
				<?php
				$data = array (
					'name' => 'txtValmVinkel[' . $i . ']',
					'id' => 'txtValmVinkel' . $i,
					'class' => 'textfield textfieldwithspan num' 
			);
				echo form_input ( $data );
				?>
				<span class="textfieldspan">°</span> 
				</div>
			</div>
		</div>
			
		<div class=col-md-8   id="valmtop<?php echo $i?>">
			<div class="row input-group" >
				<div class="textfieldtext col-md-8">
				b: Bredde - valmtop:
				</div>
				<div class="col-md-4 ">
				<?php
				$data = array (
					'name' => 'txtValmTop[' . $i . ']',
					'id' => 'txtValmTop' . $i,
					'class' => 'textfield textfieldwithspan  num' 
			);
				echo form_input ( $data );
				?>
				<span class="textfieldspan">m</span> 
				</div>
			</div>
		</div>
		</div>
		</div>
		<?php endfor;?>
		</div>
		
		<div class="col-md-4">
			<?php if ($globalValmTypeMaalsaet == 2):?>
			<img
				src="<?php echo base_url('assets/css/images/maalsaet/typesB.png');?>">
			<?php else :?>
			<img
				src="<?php echo base_url('assets/css/images/maalsaet/typesA.png');?>">
			<?php endif;?>
		</div>
		<div class="col-md-4" id="per"></div>
	</div>
	<div class="col-md-12" style="margin-bottom:200px">
		<div style="float:right">
		<img src="<?php echo base_url('assets')?>/img/left.png" onclick="window.location.href='<?php echo $back?>'" style="cursor:pointer;">
		<img src="<?php echo base_url('assets')?>/img/right.png" onclick = 'val_maalsaet()' style="cursor:pointer;">
		</div>
	</div>
</div>




<!--
<div class="row">
	<div class="col-md-6">
		
		
		<?php
		for($i = 1; $i <= 6; $i ++) :

			?>	
			<h4 id="framecapt<?php echo $i?>">
			Valm <span id="framenum<?php echo $i;?>"><?php echo $i?></span>
		</h4>

		<ul class="list-group" id="<?php echo 'framevalm'.$i?>">
			<li class="list-group-item" id="valmvinkel<?php echo $i?>">
				<div class="input-group">
		a:Vinkel på valm:
		<?php
			$data = array (
					'name' => 'txtValmVinkel[' . $i . ']',
					'id' => 'txtValmVinkel' . $i,
					'class' => 'form-control incolright num' 
			);
			echo form_input ( $data );
			?>
		<span class="input-group-addon incolrightadd">°</span>
				</div>
			</li>



			<li class="list-group-item" id="valmtop<?php echo $i?>">


				<div class="input-group">
		b: Bredde - valmtop:
		<?php
			$data = array (
					'name' => 'txtValmTop[' . $i . ']',
					'id' => 'txtValmTop' . $i,
					'class' => 'form-control incolright num' 
			);
			echo form_input ( $data );
			?>
		<span class="input-group-addon incolrightadd">m</span>
				</div>
			</li>
		</ul>
		<?php endfor;?>
	</div>
	<div class="col-md-2">
		<?php if ($globalValmTypeMaalsaet == 2):?>
		<img
			src="<?php echo base_url('assets/css/images/maalsaet/typesB.png');?>">
		<?php else :?>
		<img
			src="<?php echo base_url('assets/css/images/maalsaet/typesA.png');?>">
		<?php endif;?>
	</div>
	<div class="col-md-4" id="per"></div>
</div>
<div class="row">
	<div class="col-md-12">
<?php
echo form_button ( $buttonNext );
echo form_button ( $buttonBack );
echo form_close ();

?>
</div>
</div>-->

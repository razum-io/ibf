<style>
#imgcont {
	width:200px;
	height:120px;
	display: inline-block;
}

#imgcont3d {
	//float:right;
	width:200px;
	height:120px;
	display: inline-block;
}
table{
		width:100%;
}
.rowTitle{
	font-weight:bold;
	float:left;
	--margin-top:7px;
	padding-right:10px;
	font-size: 1.7em;
}

.textfieldtext{
	overflow:hidden;
	display:block;
	min-height:35px;
}
.content{
	font-size:1.5em
}
</style>
<script>
$("#navia").append('<li><a href="<?php echo base_url('home/index/beregn');?>">Skift stentype</a></li>');
//$("#navia").append('<li><a href="<?php echo base_url('home/tagopbygning');?>">Tagopbygning</a></li>');
$("#navia").append('<li><a href="<?php echo base_url('home/beregn/1');?>" target=_blank>Print</a></li>');

$( document ).ready(function() {
	dynValmType = <?php echo $dynValmType; ?>;
	baseurl = <?php echo '"'.base_url('assets/illustration').'"'; ?>;
	antalVinklerVinkel = <?php echo '"'.$antalvinklervinkel.'"'; ?>;
	antalVinkler = <?php echo '"'.$antalvinkler.'"'; ?>;
	antalTilbygninger = <?php echo '"'.$antaltilbygninger.'"'; ?>;
	antalKnaster = <?php echo '"'.$antalknaster.'"'; ?>;
	tag = <?php echo '"'.$tag.'"'; ?>;
	drawillustration('beregn');
	if (hoved == 5){
		$('#imgcont').css('margin-bottom','100px'); 
		$('#imgcont3d').css('margin-bottom','100px'); 
	}
});
function changeantal(elem){
	 var antal = prompt("", elem.innerHTML);
	 if (antal != null) {
		 elem.innerHTML = antal;	    
		 }
	 
}
</script>
<?php

function dot($x) {
	return str_replace(',','.',$x);
}

function comma($x) {
	return str_replace('.',',',$x);
}

function comma_split($x) {
	$x = dot($x);
	$a = explode(' ',$x);
	if (count($a)<2)
		return $x;
		
	if (is_numeric($a[0])) {
		$a[0] = comma(round($a[0],2));
	}
	
	return $a[0] . ' '. $a[1];
}

$buttonNext = array (
		'name' => 'next',
		'id' => 'next',
		'class' => 'form-control next',
		'type' => 'submit',
		'content' => 'Frem ->' 
);
$backlbl = "'" . $back . "'";
$buttonBack = array (
		'name' => 'back',
		'id' => 'back',
		'class' => 'form-control next',
		'type' => 'button',
		'onclick' => 'window.location.href=' . $backlbl,
		'content' => '<-- Tilbage' 
);
?>
<?php

echo form_open ( 'getdata/tagmetode', array (
		'id' => 'beregn' 
) );
?>

<div class=container>
	<div class="row">
		<div class="col-md-6">
			<h3 style="font-weight:bold;">Kunde:</h3>
			<br>
			<div class=row>
					<div class="col-md-12"><span class="rowTitle">Navn: </span><span class="textfieldtext"><?php echo $Navn;?></span></div>
				
					<div class="col-md-12"><span class="rowTitle">Adresse: </span><span class="textfieldtext"><?php echo $adresse;?></span></div>
				
					<div class="col-md-12"><span class="rowTitle">By: </span><span class="textfieldtext"><?php echo $by;?></span></div>
				
					<div class="col-md-12"><span class="rowTitle">Telefon: </span><span class="textfieldtext"><?php echo $telefon;?></span></div>
				
					<div class="col-md-12"><span class="rowTitle">Tilbudsnummer: </span><span class="textfieldtext"><?php echo $tilbudsnummer;?></span></div>
				
					
			</div>
			<div class=row>
				<br>			
				<div class="col-md-12"><span class="rowTitle">Beregningstidspunkt: </span><span style="font-size:1.7em"><?php echo $beregningstidspunkt;?></span></div>
			</div>	
			
		</div>
		<div class="col-md-6">
			<h3>Tagtype:</h3>
			<br>
			<table  style="font-size:1.7em;" >
				<tr>
					<th>Hustype:</th>
					<td>
						<?php 
						if ($hoved==1) echo 'Længehus';
						if ($hoved==2) echo 'Vinkelhus';
						if ($hoved==3) echo 'Tag med ensidig hældning (frit)';
						if ($hoved==4) echo 'Tag med ensidig hældning (ved mur)';
						if ($hoved==5) echo 'Vinkelhus 45°';
						if ($hoved==6) echo 'Pyramidetag';
						if ($hoved==7) echo 'H - Hus';
						?>
					</td>
				</tr>
				<tr>
					<th>Stentype:</th>
					<?php if ($stenvalgtnr==0) { ?>
						<td>Dobbelt-S</td>
					<?php } ?>
					<?php if ($stenvalgtnr==3) { ?>
						<td>Vinge Økonomi</td>
					<?php } ?>
					<?php if ($stenvalgtnr==4) { ?>
						<td>Vinge Økonomi plus</td>
					<?php } ?>
				</tr>
				<tr>
					<th>Farve:</th>
					<td><?php echo $color_name?></td>
				</tr>			
				
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<h3>Tagopbygning</h3>
			<table style="font-size:1em;" class="beregn_table">
				<tr>
					<th></th>
					<th><?php echo $tagopbygning['label1'];?></th>
					<th><?php echo $tagopbygning['label7'];?></th>
					<th><?php echo $tagopbygning['label8'];?></th>
					<th><?php echo $tagopbygning['label9'];?></th>
					<?php if ($antalkviste) { ?>
					<th>Kvist</th>
					<?php } ?>
				</tr>
				<tr>
					<td class="first"><?php echo $tagopbygning['label2'];?></td>
					<td><?php echo comma($tagopbygning['lengde_hh']);?></td>
					<td><?php echo comma($tagopbygning['lengde_tb']);?></td>
					<td><?php echo comma($tagopbygning['lengde_v']);?></td>
					<td><?php echo comma($tagopbygning['lengde_k']);?></td>
					<?php if ($antalkviste) { ?>
						<td><?php echo $khl/10?> cm</td>
					<?php } ?>
				</tr>
				<tr>
					<td class="first"><?php echo $tagopbygning['label3'];?></td>
					<td><?php echo comma($tagopbygning['bredde_hh']);?></td>
					<td><?php echo comma($tagopbygning['bredde_tb']);?></td>
					<td><?php echo comma($tagopbygning['bredde_v']);?></td>
					<td><?php echo comma($tagopbygning['bredde_k']);?></td>
					<?php if ($antalkviste) { ?>
						<td><?php echo $khb/10?> cm</td>
					<?php } ?>
				</tr>
				<tr>
					<td class="first"><?php echo $tagopbygning['label4'];?></td>
					<td><?php echo comma($tagopbygning['tagh_hh']);?></td>
					<td><?php echo comma($tagopbygning['tagh_tb']);?></td>
					<td><?php echo comma($tagopbygning['tagh_v']);?></td>
					<td><?php echo comma($tagopbygning['tagh_k']);?></td>
					<?php if ($antalkviste) { ?>
						<td><?php echo $khv?> °(<?php echo $antalkviste?> st.)</td>
					<?php } ?>
				</tr>
				<tr>
					<td class="first"><?php echo $tagopbygning['label5'];?></td>
					<td><?php echo comma($tagopbygning['kol_hh']);?></td>
					<td><?php echo comma($tagopbygning['kol_tb']);?></td>
					<td><?php echo comma($tagopbygning['kol_v']);?></td>
					<td><?php echo comma($tagopbygning['kol_k']);?></td>
					<?php if ($antalkviste) { ?>
					
					<?php } ?>
				</tr>
				<tr>
					<td class="first"><?php echo $tagopbygning['label6'];?></td>
					<td><?php echo comma($tagopbygning['rek_hh']);?></td>
					<td><?php echo comma($tagopbygning['rek_tb']);?></td>
					<td><?php echo comma($tagopbygning['rek_v']);?></td>
					<td><?php echo comma($tagopbygning['rek_k']);?></td>
					<?php if ($antalkviste) { ?>
					
					<?php } ?>
				</tr>
			</table>
			<p >*Antal kolonner er inkl. vindskedesten, dog ekskl. evt. 1/2 kolonner.</p>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<h3>Taget : </h3>
			<br>
			<table style="font-size:1.7em;">
				<tr>			
					<th>Tagflade:</th>
					<td><?php echo comma(round($frmflade,2));?><?php if (round($frmflade,3)!==0):?>
					m<sup>2</sup>
					<?php endif;?> </td>
				</tr>
				<tr>
					<th>Meter Lægte:</th>
					<td><?php echo comma(round($frmlegte,2));?> m</td>
				</tr>
				<tr>
					<th>Meter Tagrende:</th>
					<td><?php echo comma(round($frmtagrende,2));?> m</td>
				</tr>
				<tr>
					<th>Rygning :</th>
					<td><?php echo comma(round($tagopbygning['rygning_meter'],2));?> m</td>

				</tr>
				<tr>
					<th>Grater :</th>
					<td><?php echo comma(round($tagopbygning['grater_meter'],2));?> m</td>
				</tr>
				<tr>
					<th>Klemliste :</th>
					<td><?php echo comma(round($tagopbygning['klemliste'],2));?> m</td>
				</tr>
				
			</table>	
			
		</div>
		<?php if ($HeValmVinkel[1] || $HaValmVinkel[1]) { ?>
		<div class="col-sm-6">
			<h3>Valm: </h3>
			<br>
			<table style="font-size:1.7em;">
				<tr>			
					<th>Valm</th>
					<th>Vinkel</th>
					<th>Bredde</th>
				</tr>
				<?php 
				if ($HeValmVinkel[1])
				for ($valm=1;$valm<=count($HeValmVinkel);$valm++)
				{
					?><tr>
					<td>Helvalm <?php echo $valm?></td>
					<td><?php echo $HeValmVinkel[$valm]?> °</td>
					<td></td>
					</tr><?php
				}
				
				if ($HaValmVinkel[1])
				for ($valm=1;$valm<=count($HaValmVinkel);$valm++)
				{
					?><tr>
					<td>Halvvalm <?php echo $valm?></td>
					<td><?php echo $HaValmVinkel[$valm]?> °</td>
					<td><?php echo comma($HaValmTop[$valm])?> m</td>
					</tr><?php
				}
				?>		
				</table>
		</div>	
		<?php } ?>
	</div>
	<div class="row" style="margin-top:50px">
		<div class="col-sm-6">
			<div id=imgcont style="height:200px"></div>
			<div id=imgcont3d style="height:200px"></div>
		</div>
	</div>
	
	<div class="row">
		<h3>Lægteafstand :</h3>
		<div class="col-sm-6">
			<img style="width:140px; height:auto;" src="<?php echo base_url('assets/css/images/legteafstand.bmp')?>">
		</div>
		<div class="col-sm-6">
				<table style="font-size:1.7em;">
					<tr>
						<th></th>
						<th><?php echo $tagopbygning['label17'];?></th>
						<th><?php echo $tagopbygning['label19'];?></th>
						<th><?php echo $tagopbygning['label18'];?></th>
					</tr>
					<tr>
						<th><?php echo $tagopbygning['label20'];?></th>
						<td><?php echo comma_split($tagopbygning['text1']);?></td>
						<td><?php echo comma_split($tagopbygning['til_h']);?></td>
						<td><?php echo comma_split($tagopbygning['vinkel_v']);?></td>
					</tr>
					<tr>
						<th><?php echo $tagopbygning['label22'];?></th>
						<td><?php echo comma_split($tagopbygning['text2']);?></td>
						<td><?php echo comma_split($tagopbygning['tilbyg_c']);?></td>
						<td><?php echo comma_split($tagopbygning['vinkel_c']);?></td>
					</tr>
					<tr>
						<th><?php echo $tagopbygning['label21'];?></th>
						<td><?php echo comma_split($tagopbygning['text3']);?></td>
						<td><?php echo comma_split($tagopbygning['tilbyg_b']);?></td>
						<td><?php echo comma_split($tagopbygning['vinkel_b']);?></td>
					</tr>
				</table>
		</div>
	</div>
	<div class=row>
		<!--
		<div class="col-sm-6">	
			<h4>Afslutning med vindskedesten :<h4>
			<img style="width:200px;" src="<?php echo base_url('assets/img/bilde1.png')?>"> 
		</div>-->
		<!--<img style="width:100px; height:auto;" src="/ibf/assets/css/images/legteafstand.bmp">-->
				
				
		<div class="col-sm-6">	
			<h4>Breddemodul :<h4>
			<?php if ($stenvalgtnr==0 && $metode==3) { ?>
			<img src="<?php echo base_url('assets/img/bred1.jpg')?>" width=400> 
			<?php } ?>
			<?php if ($stenvalgtnr==0 && $metode==2) { ?>
			<img src="<?php echo base_url('assets/img/bred2.jpg')?>" width=400> 
			<?php } ?>
			<?php if ($stenvalgtnr!=0 && $metode==3) { ?>
			<img src="<?php echo base_url('assets/img/bred3.jpg')?>" width=400> 
			<?php } ?>
			<?php if ($stenvalgtnr!=0 && $metode==2) { ?>
			<img src="<?php echo base_url('assets/img/bred4.jpg')?>" width=400> 
			<?php } ?>
		</div>
		
	</div>
	<div class=row>
		<h3>Nedenstående mængder er beregnet på baggrund af modtagne informationer.
		<br>Anførte mængder er vejledende uden ansvar for IBF
		<br><span style="font-weight:bold;">Inkl. 3% håndteringssvind</span><h3>
	</div>
	<div class="row">
		<div class="col-md-12">


			<table style="font-size:1.7em;" class="table-hover">
				<tr>
					<th>Varenr:</th>
					<th>DB nr:</th>
					<th>Varebeskrivelse:</th>
					<th>Mængde:</th>
					<th>Enhed:</th>
				</tr>
				 <?php
					$search = array (
							'Ų',
							'Ę' 
					);
					$replace = array (
							'Ø',
							'Æ' 
					);
					foreach ( $tilgTable_stand as $obj ) :
						if (( int ) $obj->Antal > 0) :
							?>
							<tr>
					<td><?php echo $obj->IBF?></td>
					<td><?php echo $obj->TUN?></td>
					<td><?php echo str_replace($search, $replace,$obj->Navn);?></td>
					<td onclick="changeantal(this)"><?php echo $obj->Antal?></td>
					<td><?php echo $obj->Enhed?></td>

				</tr>
						
						
						
						
						
						
						
						
						
						
						
						
						<?php
	endif;
					endforeach
					;
					
					?>
				 

			</table>
		</div>

	</div>
	<div class=row style="margin-bottom:200px">
		<div class="col-md-12">
			<div style="float:right">
			<img src="<?php echo base_url('assets')?>/img/left.png" onclick="window.location.href='<?php echo $back?>'" style="cursor:pointer;">
			</div>
		</div>
	<div>
</div>

<!--
<div>
	<img style="width:60px;" src="<?php echo base_url('/assets/css/images/logo.png')?>">	
	<h3 style="float:right;margin-right:50px; font-size:1.5em;">IBF Tagsten - en sikker dansk tagløsning<h3>
	
</div>
<div class="row">
<!-------------------------------------------
	<div class="col-md-8">
			<h3>Tagopbygning</h3>
			<table style="font-size:1em;" class="table">
				<tr>
					<th></th>
					<th><?php echo $tagopbygning['label1'];?></th>
					<th><?php echo $tagopbygning['label7'];?></th>
					<th><?php echo $tagopbygning['label8'];?></th>
					<th><?php echo $tagopbygning['label9'];?></th>
				</tr>
				<tr>
					<th><?php echo $tagopbygning['label2'];?></th>
					<td><?php echo comma($tagopbygning['lengde_hh']);?></td>
					<td><?php echo comma($tagopbygning['lengde_tb']);?></td>
					<td><?php echo comma($tagopbygning['lengde_v']);?></td>
					<td><?php echo comma($tagopbygning['lengde_k']);?></td>
				</tr>
				<tr>
					<th><?php echo $tagopbygning['label3'];?></th>
					<td><?php echo comma($tagopbygning['bredde_hh']);?></td>
					<td><?php echo comma($tagopbygning['bredde_tb']);?></td>
					<td><?php echo comma($tagopbygning['bredde_v']);?></td>
					<td><?php echo comma($tagopbygning['bredde_k']);?></td>
				</tr>
				<tr>
					<th><?php echo $tagopbygning['label4'];?></th>
					<td><?php echo comma($tagopbygning['tagh_hh']);?></td>
					<td><?php echo comma($tagopbygning['tagh_tb']);?></td>
					<td><?php echo comma($tagopbygning['tagh_v']);?></td>
					<td><?php echo comma($tagopbygning['tagh_k']);?></td>
				</tr>
				<tr>
					<th><?php echo $tagopbygning['label5'];?></th>
					<td><?php echo comma($tagopbygning['kol_hh']);?></td>
					<td><?php echo comma($tagopbygning['kol_tb']);?></td>
					<td><?php echo comma($tagopbygning['kol_v']);?></td>
					<td><?php echo comma($tagopbygning['kol_k']);?></td>
				</tr>
				<tr>
					<th><?php echo $tagopbygning['label6'];?></th>
					<td><?php echo comma($tagopbygning['rek_hh']);?></td>
					<td><?php echo comma($tagopbygning['rek_tb']);?></td>
					<td><?php echo comma($tagopbygning['rek_v']);?></td>
					<td><?php echo comma($tagopbygning['rek_k']);?></td>
				</tr>
			</table>
			
	</div>
	
	<?php if ($antalkviste) { ?>
	<div class="col-sm-4">
		<h3>Kvist: </h3>
		<table style="font-size:1em;" class="table">
			<tr>
				<td>Antal kviste: </th>
				<td><?php echo $antalkviste?> stk.</th>
			</tr>
			<tr>
				<td>Kvistens længde:</th>
				<td><?php echo $khl/10?> cm</th>
			</tr>
			<tr>
				<td>Kvistens bredde:</th>
				<td><?php echo $khb/10?> cm</th>
			</tr>
			<tr>
				<td>Kvistens vinkel:</th>
				<td><?php echo $khv?> °</th>
			</tr>
			<?php if (isset($khvv)) { ?>
				<?php if (($khvv)) { ?>
				<tr>
					<td>Kvistvalmens vinkel:</th>
					<td><?php echo $khvv?> °</th>
				</tr>
				<?php } ?>
			<?php } ?>
			<?php if (isset($khvb)) { ?>
				<?php if (($khvb)) { ?>
				<tr>
					<td>Kvistvalmens bredde:</th>
					<td><?php echo $khvb*100?> cm</th>
				</tr>
				<?php } ?>
			<?php } ?>
		</table>
	</div>
	<?php } ?>
</div>
<p >*Antal kolonner er inkl. vindskedesten, dog ekskl. evt. 1/2 kolonner.</p>

<div class="row">
	<div class="col-sm-8">
		<h3>Taget : </h3>
		<table style="font-size:1em;" class="table">
			<tr>			
				<th>Tagflade:</th>
				<td><?php echo comma(round($frmflade,2));?><?php if (round($frmflade,3)!==0):?>
				m<sup>2</sup>
				<?php endif;?> </td>
			</tr>
			<tr>
				<th>Meter Lægte:</th>
				<td><?php echo comma(round($frmlegte,2));?> m</td>
			</tr>
			<tr>
				<th>Meter Tagrende:</th>
				<td><?php echo comma(round($frmtagrende,2));?> m</td>
			</tr>
			<tr>
				<th>Rygning :</th>
				<td><?php echo comma(round($tagopbygning['rygning_meter'],2));?> m</td>

			</tr>
			<tr>
				<th>Grater :</th>
				<td><?php echo comma(round($tagopbygning['grater_meter'],2));?> m</td>
			</tr>
			<tr>
				<th>Klemliste :</th>
				<td><?php echo comma(round($tagopbygning['klemliste'],2));?> m</td>
			</tr>
			
		</table>	
		
	</div>
	<?php if ($HeValmVinkel[1] || $HaValmVinkel[1]) { ?>
	<div class="col-sm-4">
		<h3>Valm: </h3>
		<table style="font-size:1em;" class="table">
			<tr>			
				<th>Valm</th>
				<th>Vinkel</th>
				<th>Bredde</th>
			</tr>
			<?php 
			if ($HeValmVinkel[1])
			for ($valm=1;$valm<=count($HeValmVinkel);$valm++)
			{
				?><tr>
				<td>Helvalm <?php echo $valm?></td>
				<td><?php echo $HeValmVinkel[$valm]?> °</td>
				<td></td>
				</tr><?php
			}
			
			if ($HaValmVinkel[1])
			for ($valm=1;$valm<=count($HaValmVinkel);$valm++)
			{
				?><tr>
				<td>Halvvalm <?php echo $valm?></td>
				<td><?php echo $HaValmVinkel[$valm]?> °</td>
				<td><?php echo comma($HaValmTop[$valm])?> m</td>
				</tr><?php
			}
			?>		
			</table>
	</div>	
	<?php } ?>
</div>


<div class="row">
	<div class="col-sm-6">
		<h3>Kunde</h3>
		<table style="font-size:1em;" class="table">
			<tr>
				<th>Navn :</th>
				<td><?php echo $Navn;?></td>
			</tr>
			<tr>
				<th>Adresse :</th>
				<td><?php echo $adresse;?></td>
			</tr>
			<tr>
				<th>By :</th>
				<td><?php echo $by;?></td>
			</tr>
			<tr>
				<th>Telefon :</th>
				<td><?php echo $telefon;?></td>
			</tr>
			<tr>
				<th>Tilbudsnummer :</th>
				<td><?php echo $tilbudsnummer;?></td>
			</tr>
			<tr>
				<th>Beregningstidspunkt :</th>
				<td><?php echo $beregningstidspunkt;?></td>
			</tr>
			
		</table>
	</div>
	<!--<div class="col-sm-6">
		<h3>Forhandler:</h3>
		<table  class="table">
			<tr>			
				<th>Tegflade:</th>
				<td><?php echo round($frmflade,3);?><?php if (round($frmflade,3)!==0):?>
				m<sup>2</sup>
				<?php endif;?> </td>
			</tr>
			<tr>
				<th>Meter Lægte:</th>
				<td><?php echo round($frmlegte,3);?> m</td>
			</tr>
			<tr>
				<th>Meter Tagrende:</th>
				<td><?php echo round($frmtagrende,3);?> m</td>
			</tr>
		</table>
	</div>--
	<div class="col-sm-6">
		<h3>Tagtype:</h3>
		<table class="table" style="font-size:1em;" >
			<tr>
				<th>Hustype:</th>
				<td>
					<?php 
					if ($hoved==1) echo 'Længehus';
					if ($hoved==2) echo 'Vinkelhus';
					if ($hoved==3) echo 'Tag med ensidig hældning (frit)';
					if ($hoved==4) echo 'Tag med ensidig hældning (ved mur)';
					if ($hoved==5) echo 'Vinkelhus 45°';
					if ($hoved==6) echo 'Pyramidetag';
					if ($hoved==7) echo 'H - Hus';
					?>
				</td>
			</tr>
			<tr>
				<th>Stentype:</th>
				<?php if ($stenvalgtnr==0) { ?>
					<td>Dobbelt-S</td>
				<?php } ?>
				<?php if ($stenvalgtnr==3) { ?>
					<td>Vinge Økonomi</td>
				<?php } ?>
				<?php if ($stenvalgtnr==4) { ?>
					<td>Vinge Økonomi plus</td>
				<?php } ?>
			</tr>
			<tr>
				<th>Farve:</th>
				<td><?php echo $color_name?></td>
			</tr>			
			
		</table>
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<div id=imgcont style="height:200px"></div>
		<div id=imgcont3d style="height:200px"></div>
	</div>
</div>
<BR><BR>
<!--<div class="row">		
	<div class="col-sm-6">
		<h3>Hovedhus:</h3>
		<table class="table">
			<?php 
			$session = $this->session->all_userdata();
			foreach($session as $key=>$val) { 
				if (strpos($key, 'input_data') === 0) {
					$label_key = str_replace('input_data','input_tag',$key);
					?>
					<tr>
						<th><?php echo $session[$label_key]?>:</th>
						<td><?php echo $val?></td>
					</tr>
					<?php
				}			
			}
			?>

		</table>
	</div>	
	
<div class="col-sm-6">
		<h3>Kvist:</h3>
		<table class="table">
			<tr>
				<th>Antal kviste :</th>
				<td>1 stk.</td>
			</tr>
			<tr>
				<th>Kvistens Langde :</th>
				<td>240 cm</td>
			</tr>
			<tr>
				<th>Kvistens bredde :</th>
				<td>470.4 cm</td>
			</tr>	
			<tr>
				<th>Kvistens vinkle :</th>
				<td>470.4 cm</td>
			</tr>			
		</table>
	</div>

</div>	--

	
<br><br>

<h3>Lægteafstand :</h3>
<div class="row">
	<div class="col-sm-4">
		<img style="width:120px; height:auto;" src="<?php echo base_url('assets/css/images/legteafstand.bmp')?>">
	</div>
	<div class="col-sm-4">
			<table class="table">
				<tr>
					<th></th>
					<th><?php echo $tagopbygning['label17'];?></th>
					<th><?php echo $tagopbygning['label19'];?></th>
					<th><?php echo $tagopbygning['label18'];?></th>
				</tr>
				<tr>
					<th><?php echo $tagopbygning['label20'];?></th>
					<td><?php echo comma_split($tagopbygning['text1']);?></td>
					<td><?php echo comma_split($tagopbygning['til_h']);?></td>
					<td><?php echo comma_split($tagopbygning['vinkel_v']);?></td>
				</tr>
				<tr>
					<th><?php echo $tagopbygning['label22'];?></th>
					<td><?php echo comma_split($tagopbygning['text2']);?></td>
					<td><?php echo comma_split($tagopbygning['tilbyg_c']);?></td>
					<td><?php echo comma_split($tagopbygning['vinkel_c']);?></td>
				</tr>
				<tr>
					<th><?php echo $tagopbygning['label21'];?></th>
					<td><?php echo comma_split($tagopbygning['text3']);?></td>
					<td><?php echo comma_split($tagopbygning['tilbyg_b']);?></td>
					<td><?php echo comma_split($tagopbygning['vinkel_b']);?></td>
				</tr>
			</table>
	</div>
</div>
<div class=row>
	<div class="col-sm-4">	
		<h4>Afslutning med vindskedesten :<h4>
		<!--<img style="width:100px; height:auto;" src="/ibf/assets/css/images/legteafstand.bmp">--
		<img style="width:200px;" src="<?php echo base_url('assets/img/bilde1.png')?>"> 
	</div>
			
			
	<div class="col-sm-4">	
		<h4>Breddemodul :<h4>
		<?php if ($stenvalgtnr==0 && $metode==3) { ?>
		<img src="<?php echo base_url('assets/img/bred1.jpg')?>" width=400> 
		<?php } ?>
		<?php if ($stenvalgtnr==0 && $metode==2) { ?>
		<img src="<?php echo base_url('assets/img/bred2.jpg')?>" width=400> 
		<?php } ?>
		<?php if ($stenvalgtnr!=0 && $metode==3) { ?>
		<img src="<?php echo base_url('assets/img/bred3.jpg')?>" width=400> 
		<?php } ?>
		<?php if ($stenvalgtnr!=0 && $metode==2) { ?>
		<img src="<?php echo base_url('assets/img/bred4.jpg')?>" width=400> 
		<?php } ?>
	</div>
	
</div>



		
		
	<h3>Nedenstående mængder er beregnet på baggrund af modtagne informationer.
	<br>Anførte mængder er vejledende uden ansvar for IBF
	<br><span style="font-weight:bold;">Inkl. 3% håndteringssvind</span><h3>


<!-------------------------------------------
<div class="row">
	<div class="col-md-12">


		<table class="table table-hover">
			<tr>
				<th>Varenr:</th>
				<th>Tunnr:</th>
				<th>Varebeskrivelse:</th>
				<th>Mængde:</th>
				<th>Enhed:</th>
			</tr>
			 <?php
				$search = array (
						'Ų',
						'Ę' 
				);
				$replace = array (
						'Ø',
						'Æ' 
				);
				foreach ( $tilgTable_stand as $obj ) :
					if (( int ) $obj->Antal > 0) :
						?>
						<tr>
				<td><?php echo $obj->IBF?></td>
				<td><?php echo $obj->TUN?></td>
				<td><?php echo str_replace($search, $replace,$obj->Navn);?></td>
				<td onclick="changeantal(this)"><?php echo $obj->Antal?></td>
				<td><?php echo $obj->Enhed?></td>

			</tr>
					
					
					
					
					
					
					
					
					
					
					
					
					<?php
					endif;
				endforeach
				;
				
				?>
			 

		</table>
	</div>

</div>


<?php
echo form_button ( $buttonBack );
echo form_close ();

?>-->

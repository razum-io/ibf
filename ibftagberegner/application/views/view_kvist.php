<script>
$( document ).ready(function() {
	kvist = <?php echo $kvist;?>;
	baseurl = <?php echo '"'.base_url('assets/illustration').'"'; ?>;
	kvistfrm(kvist,baseurl);
});
</script>
<?php
$buttonNext = array (
		'name' => 'next',
		'id' => 'next',
		'class' => 'form-control next',
		'type' => 'submit',
		'content' => 'Frem ->' 
);
$backlbl = "'" . $back . "'";
$buttonBack = array (
		'name' => 'back',
		'id' => 'back',
		'class' => 'form-control next',
		'type' => 'button',
		'onclick' => 'window.location.href=' . $backlbl,
		'content' => '<-- Tilbage         ' 
);
$kvist_capt = array (
		'l' => 'a: Kvistens længde:', 
		'b' => 'b: Kvistens bredde:',
		'v' => 'c: Kvistens vinkel:',
		'v_v' => 'd: Kvistvalmens vinkel:',
		'v_b' => 'e: Kvistvalmens bredde:' 
);
?>




<div class=container>
<div style="margin-top:50px;"></div>
<?php

echo form_open ( 'getdata/kvist', array (
		'id' => 'maalsaet' 
) );
?>
	<div class="row">
	<?php foreach ( $kvist_capt as $value => $capt ) : ?>	
		<?php
					
		if ($value == 'v' || $value == 'v_v') {
						$lett = '°';
					} else {
						$lett = 'm';
					}
					?>
		<div class=col-md-8  id="list<?php echo $value?>">
			<div class="row input-group">
				<div class="textfieldtext col-md-7">
				<?php echo $capt; ?>
				</div>
				<div class="col-md-offset-2 col-md-3 ">
				<?php
				$data = array (
					'name' => 'kvist' . $value,
					'class' => 'textfield textfieldwithspan ' 
				);
				echo form_input ( $data );
				?>
				<span class="textfieldspan"><?php echo $lett?></span> 
				</div>
			</div>
		</div>
		
		<?php endforeach;?>
		<div class="col-md-12">
		<img id="kvistpic" width="150" height="150">
	</div>
	</div>
	<div class=row style="margin-bottom:200px">
		<div class="col-md-12">
			<div style="float:right">
			<img src="<?php echo base_url('assets')?>/img/left.png" onclick="window.location.href='<?php echo $back?>'" style="cursor:pointer;">
			<img src="<?php echo base_url('assets')?>/img/right.png" onclick = "document.getElementById('maalsaet').submit();" style="cursor:pointer;">
			</div>
		</div>
	<div>
</div><!--


<div class="row">
	<div class="col-md-6">

		<ul class="list-group">
		<?php foreach ( $kvist_capt as $value => $capt ) : ?>	
		<?php
			
if ($value == 'v' || $value == 'v_v') {
				$lett = '°';
			} else {
				$lett = 'm';
			}
			?>
			<li class="list-group-item" id="list<?php echo $value?>">
				<div class="input-group">
					<?php
			echo $capt;
			$data = array (
					'name' => 'kvist' . $value,
					'class' => 'form-control incolright' 
			);
			echo form_input ( $data );
			?>
					<span class="input-group-addon incolrightadd"><?php echo $lett?></span>
				</div>
			</li>

		<?php endforeach;?>
		</ul>
	</div>
	<div class="col-md-6">
		<img id="kvistpic" width="150" height="150">
	</div>
</div>
-->

<?php
// echo form_button ( $buttonNext );
// echo form_button ( $buttonBack );
// echo form_close ();

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>IBF Tagsten - <?php (isset($title))?  print_r($title) : false;?></title>

<link href="<?php echo base_url('assets/css/bootstrap.css');?>"
	rel="stylesheet" type="text/css">
<link href="<?php echo base_url('assets/css/home.css');?>"
	rel="stylesheet" type="text/css">
<link href="<?php echo base_url('assets/css/custom.css');?>"
	rel="stylesheet" type="text/css">
<link href="<?php echo base_url('assets/css/jquery.selectbox.css');?>"
	rel="stylesheet" type="text/css">

<script src="<?php echo base_url('assets/js/jquery.js');?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.js');?>"></script>
<script src="<?php echo base_url('assets/js/forms.js');?>"></script>
<script src="<?php echo base_url('assets/js/ilustration-script.js')?>"></script>
<script src="<?php echo base_url('assets/js/ilustration-form.js')?>"></script>
<script src="<?php echo base_url('assets/js/ilustration-draw.js')?>"></script>
<script src="<?php echo base_url('assets/js/class.js')?>"></script>
<script src="<?php echo base_url('assets/js/formval.js')?>"></script>
<script src="<?php echo base_url('assets/js/floodfill.js')?>"></script>
<script src="<?php echo base_url('assets/js/jqueryvalidator.js')?>"></script>

<script>
	var base_url = '<?php echo base_url()?>';
</script>

<?php 
	$num = $this->session->userdata('stenvalgtnr'); 
	if (!$num) 
		$num = '0';
	
	$f = $this->uri->segment(2);
	if (!$f)
		$f = 'index';
		
	// the last image is default
	$largeimage = 'large'.$num.'b.jpg';

	
	if ($f=='index'||$f=='kunde' || $f=='farvevalg' ) {
		$largeimage = 'large'.$num.'.jpg';

	}
	if ($f=='tagtype' || $f=='valm' || $f=='vinkel') {
		$largeimage = 'large'.$num.'a.jpg';

	}
	if ($f=='beregn') {
		$largeimage = '';
	}
?>

</head>
<body onload="autoscroll();resize();" onresize="resize();">
<div class="top" id="desktop_header">
	<div class="container">
		<div class="site-header-inner col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="row col-xs-10 col-sm-10 col-md-4 col-lg-4">
			<img class="img-responsive pull-left" style="max-width:100px; margin-left:25%;" src="<?php echo base_url('assets/img/logo.png');?>" alt="logo">
			</div>
			<div class="row col-xs-8 col-sm-8 col-md-8 col-lg-8">
				<h4 style="padding:0px;margin:0px 0px 10px 14px;">IBF Tagberegningsprogram</h4>
				<div class="navbar navbar-default nav" style="margin-bottom:0px">
					<ul class="nav navbar-nav navbar-left">
						<li><a href="<?php echo base_url('home/nuberegning');?>" class=mm style="color:#000000;font-weight:700;padding:15px;line-height:100%;">Ny beregning</a></li>
						<!--<li><a href="#" class=mm style="color:#000000;padding:15px;line-height:100%;">Hjælp</a></li>
						<li><a href="#" class=mm style="color:#000000;padding:15px;line-height:100%;">Om programmet</a></li>-->
						<?php if ($f=='beregn') {?>
							<li><a href="<?php echo base_url('home/index/beregn');?>" class=mm style="color:#000000;font-weight:700;padding:15px;line-height:100%;">Skift stentype</a></li>
							<li><a href="<?php echo base_url('home/beregn/1');?>" class=mm style="color:#000000;font-weight:700;padding:15px;line-height:100%;">Print</a></li>
						<?php } ?>
					</ul>
				</div>
			</div>
		</div><!-- site-header-inner -->

	</div><!-- aizver konteineri -->

</div><!-- aizver top -->

<div class="top" id="mobile_header">
	<div class="container">
		<div class="site-header-inner col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="row col-md-2 col-lg-2">
				<img class="img-responsive pull-left" style="max-width:50px; " src="<?php echo base_url('assets/img/logo.png');?>" alt="logo">

				<div class="row col-xs-2 col-sm-8 col-md-8 col-lg-8">
					<h4 style="font-size:14pt;margin:0px">IBF Tagberegningsprogram</h4>
				</div>
			</div>
			<div class="row" style="margin-top:15px;">

					<div class="col-sm-4"><a href="<?php echo base_url('home/nuberegning');?>" class=mm style="color:#000000;font-size:14pt;font-weight:700;line-height:100%;">Ny beregning</a></div>
					<?php if ($f=='beregn') {?>
						<div class="col-sm-4"><a href="<?php echo base_url('home/index/beregn');?>" class=mm style="color:#000000;font-size:14pt;font-weight:700;line-height:100%;">Skift stentype</a></div>
						<div class="col-sm-4"><a href="<?php echo base_url('home/beregn/1');?>" class=mm style="color:#000000;font-size:14pt;font-weight:700;line-height:100%;">Print</a></div>
					<?php } ?>

			</div>


		</div><!-- site-header-inner -->

	</div><!-- aizver konteineri -->

</div><!-- aizver top -->

<div class="container">
	<div class="row">

		<?php 
		//no image 5 page
		if ($largeimage!=''){?>
		<img src="<?php echo base_url('assets')?>/img/<?php echo $largeimage?>" id=large_image class="col-sm-12 col-xs-12 " >
		<!--cache images-->
		<?php }?>
	</div>
		<?php if ($largeimage!=''){?>
			<?php if ($f=='index'){?>
				<div class="row">
					<?php if ($num==0) { ?>
						<img onclick='buttonPress("0")' class=type_sm_button id=type_sm_button0 src="<?php echo base_url('assets')?>/img/dark-box.png" style="position:relative;left:50px;top:-40px;cursor:pointer;" >
					<?php } else { ?>
						<img onclick='buttonPress("0")' class=type_sm_button id=type_sm_button0 src="<?php echo base_url('assets')?>/img/light-box.png" style="position:relative;left:50px;top:-40px;cursor:pointer;" >
					<?php } ?>
					<?php if ($num==3) { ?>
						<img onclick='buttonPress("3")' class=type_sm_button id=type_sm_button3 src="<?php echo base_url('assets')?>/img/dark-box.png" style="position:relative;left:60px;top:-40px;cursor:pointer;">
					<?php } else { ?>
						<img onclick='buttonPress("3")' class=type_sm_button id=type_sm_button3 src="<?php echo base_url('assets')?>/img/light-box.png" style="position:relative;left:60px;top:-40px;cursor:pointer;">
					<?php } ?>
					<?php if ($num==4) { ?>
						<img onclick='buttonPress("4")' class=type_sm_button id=type_sm_button4 src="<?php echo base_url('assets')?>/img/dark-box.png" style="position:relative;left:70px;top:-40px;cursor:pointer;">
					<?php } else { ?>
						<img onclick='buttonPress("4")' class=type_sm_button id=type_sm_button4 src="<?php echo base_url('assets')?>/img/light-box.png" style="position:relative;left:70px;top:-40px;cursor:pointer;">
					<?php } ?>
				</div>
			<?php } else { ?>
				<div class="row">
					<?php if ($num==0) { ?>
						<img src="<?php echo base_url('assets')?>/img/dark-box.png" style="position:relative;left:50px;top:-40px;">
					<?php } else { ?>
						<img src="<?php echo base_url('assets')?>/img/light-box.png" style="position:relative;left:50px;top:-40px;">
					<?php } ?>
					<?php if ($num==3) { ?>
						<img src="<?php echo base_url('assets')?>/img/dark-box.png" style="position:relative;left:60px;top:-40px;">
					<?php } else { ?>
						<img src="<?php echo base_url('assets')?>/img/light-box.png" style="position:relative;left:60px;top:-40px;">
					<?php } ?>
					<?php if ($num==4) { ?>
						<img src="<?php echo base_url('assets')?>/img/dark-box.png" style="position:relative;left:70px;top:-40px;">
					<?php } else { ?>
						<img src="<?php echo base_url('assets')?>/img/light-box.png" style="position:relative;left:70px;top:-40px;">
					<?php } ?>
				</div>
			<?php } ?>
		<?php } ?>
	<?php if ($f=='index') { ?>
	<div class="row" style="margin-top:15px; margin-bottom:15px;">
		<div class="col-sm-4"><a id=type_button0 class="roof_type" onclick='buttonPress("0")'>Dobbelt-S</a></div>
		<div class="col-sm-4"><a id=type_button3 class="roof_type" onclick='buttonPress("3")'>Vinge Økonomi</a></div>
		<div class="col-sm-4"><a id=type_button4 class="roof_type" onclick='buttonPress("4")'>Vinge Økonomi "Plus"</a></div>
	</div>
	<?php } ?>
	
	<?php if ($f=='kunde' || $f=='farvevalg'|| $f=='tagtype' ) { ?>
	<div class="row" style="margin-top:15px; margin-bottom:15px;">
		<div class="col-sm-4"><a style="<?php if ($num!=0){?> display:none;<?php }?>" id=type_button0 class="roof_type2" >Dobbelt-S</a></div>
		<div class="col-sm-4"><a style="<?php if ($num!=3){?> display:none;<?php }?>" id=type_button3 class="roof_type2" >Vinge Økonomi</a></div>
		<div class="col-sm-4"><a style="<?php if ($num!=4){?> display:none;<?php }?>" id=type_button4 class="roof_type2" >Vinge Økonomi "Plus"</a></div>
	</div>
	<?php } ?>

</div>

<script>
var f = '<?php echo $f ?>';
function autoscroll(){
	if (f!='beregn' && f!='')
	$('html, body').animate({ scrollTop: 500 }, 1000);
}

function resize(){
	if ($(window).width()<930) {
		$("#desktop_header").hide();
		$("#mobile_header").show();
	} else {
		$("#desktop_header").show();
		$("#mobile_header").hide();
	}
}
</script>

<img src="<?php echo base_url('assets')?>/img/large0.jpg" style="opacity: 0.1;" width=1 height=1>
<img src="<?php echo base_url('assets')?>/img/large3.jpg" style="opacity: 0.1;" width=1 height=1>
<img src="<?php echo base_url('assets')?>/img/large4.jpg" style="opacity: 0.1;" width=1 height=1>
<img src="<?php echo base_url('assets')?>/img/large0a.jpg" style="opacity: 0.1;" width=1 height=1>
<img src="<?php echo base_url('assets')?>/img/large3a.jpg" style="opacity: 0.1;" width=1 height=1>
<img src="<?php echo base_url('assets')?>/img/large4a.jpg" style="opacity: 0.1;" width=1 height=1>
<img src="<?php echo base_url('assets')?>/img/large0b.jpg" style="opacity: 0.1;" width=1 height=1>
<img src="<?php echo base_url('assets')?>/img/large3b.jpg" style="opacity: 0.1;" width=1 height=1>
<img src="<?php echo base_url('assets')?>/img/large4b.jpg" style="opacity: 0.1;" width=1 height=1>

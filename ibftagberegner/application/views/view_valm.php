<style>
#per{
	margin-top:40px;
	min-height: 270px;
}
#imgcont3d{
	padding-top: 150px;
}
</style>
<script src="<?php echo base_url('/assets/js/valm_num.js')?>"></script>
<script>
$( document ).ready(function() {
tag = <?php echo '"'.$tag.'"'; ?>;
hoved = <?php echo '"'.$hoved.'"'; ?>;
valm = <?php echo '"'.$valm.'"'; ?>;
antalVinklerVinkel = <?php echo '"'.$antalvinklervinkel.'"'; ?>;
antalVinkler = <?php echo '"'.$antalVinkler.'"'; ?>;
antalKnaster = <?php echo '"'.$antalKnaster.'"'; ?>;
antaltilbygninger = <?php echo '"'.$antaltilbygninger.'"'; ?>;
antalTilbygninger = <?php echo '"'.$antaltilbygninger.'"'; ?>;
laengehus = <?php echo $laengehus;?>;
valmfrm(tag,hoved,valm,parseInt(antalVinklerVinkel),parseInt(antaltilbygninger),laengehus);
retning = (<?php echo json_encode($retning)?>);
baseurl = <?php echo '"'.base_url('assets/illustration').'"'; ?>;
load_valm();
if (hoved==2)
	load_valm_num(hoved,antalVinklerVinkel,antaltilbygninger);
else
	load_valm_num(hoved,antalVinkler,antaltilbygninger);
if (hoved == 5){
	$('#imgcont3d').css('padding-top','30px'); 
}
});
</script>
<?php
$buttonNext = array (
		'name' => 'next',
		'id' => 'next',
		'class' => 'form-control next',
		'type' => 'submit',
		'content' => 'Frem ->' 
);
$back = "'" . base_url ( 'home/tagtype' ) . "'";
$buttonBack = array (
		'name' => 'back',
		'id' => 'back',
		'class' => 'form-control next',
		'type' => 'button',
		'onclick' => 'window.location.href=' . $back,
		'content' => '<-- Tilbage' 
);
$valmleft_capt = array (
		'1' => 'Gavl',
		'2' => 'Helvalm',
		'3' => 'Halvvalm' 
);
?>


<div class="container">
	<div class="col-md-12">
		<h4>Tryk på den/de markerede gavle for at ændre opbygning.</h4>
	</div>
</div>

<div class="container">
	<div class="row">
		<?php

		echo form_open ( 'getdata/valm', array (
				'id' => 'valm' 
		) );
		?>
		<?php 
		for ($i=1;$i<=6;$i++):?>
			<div class="row col-md-2" style="margin-bottom:20px" id="valmblock<?php echo $i;?>">
			<table class=col-md-12>
			<?php
			$j = 1;
			foreach ( $valmleft_capt as $num => $capt ) :
				$radio_left = array (
						'name' => 'valm' . $i,
						'id' => 'valm' . $num.$i,
						'value' => $num,
						'onclick' => 'valmchange(' . $i . ',' . $num . ')' 
				);
				?>
					<tr>
						<td>
							<span style="font-size:20px"><?php if ($j==1) echo $i?></span>
						</td>
						<td>
							<?php echo form_radio($radio_left)?><label for="<?php echo $radio_left['id']?>"></label><span class=radiotext><?php echo $capt?></span>
						</td>
					</tr>
				<?php
				$j++;
			endforeach;
			?>
			</table>
			</ul>
		</div>
	<?php endfor;?>
		
	</div>
	<div class="row">
		<div class="col-md-4" id="per">
			<style>
				.valm_num{font-size:14px;display:none;position:absolute;z-index:1000;}
			</style>
			<span id=valm_num1 class=valm_num>1</span>
			<span id=valm_num2 class=valm_num>2</span>
			<span id=valm_num3 class=valm_num>3</span>
			<span id=valm_num4 class=valm_num>4</span>
			<span id=valm_num5 class=valm_num>5</span>
			<span id=valm_num6 class=valm_num>6</span>

			<span id=valm_num3d1 class=valm_num>1</span>
			<span id=valm_num3d2 class=valm_num>2</span>
			<span id=valm_num3d3 class=valm_num>3</span>
			<span id=valm_num3d4 class=valm_num>4</span>
			<span id=valm_num3d5 class=valm_num>5</span>
			<span id=valm_num3d6 class=valm_num>6</span>
		</div>
	</div>
	<div class=row style="margin-bottom:200px">
		<div class="col-md-12">
			<div style="float:right">
			<img src="<?php echo base_url('assets')?>/img/left.png" onclick="window.location.href=<?php echo $back?>" style="cursor:pointer;">
			<img src="<?php echo base_url('assets')?>/img/right.png" onclick = " document.getElementById('valm').submit();" style="cursor:pointer;">
			</div>
		</div>
	<div>  
</div>














<!--

<div class="row">
	<div class="col-md-12">
		<h4>Tryk på den/de markerede gavle for at ændre opbygning.</h4>
	</div>
</div>
<div class="row">
	<div class="col-md-8">
		<div class="row">
	<?php for ($i=1;$i<=6;$i++):?>
	<div class="col-md-6" id="valmblock<?php echo $i;?>">
				<ul>
		<?php
		
		foreach ( $valmleft_capt as $num => $capt ) :
			$radio_left = array (
					'name' => 'valm' . $i,
					'id' => 'valm' . $num,
					'value' => $num,
					'onclick' => 'valmchange(' . $i . ',' . $num . ')' 
			);
			?>
<li class="list-group-item">
<?php echo form_radio($radio_left).$capt;?>
</li>
<?php
		endforeach
		;
		?>
	</ul>
			</div>
	<?php endfor;?>
</div>
	</div>
	<div class="col-md-4" id="per">


	</div>

</div>
<?php
//echo form_button ( $buttonNext );
//echo form_button ( $buttonBack );
//echo form_close ();

?>-->

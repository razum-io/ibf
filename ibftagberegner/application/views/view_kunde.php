<script>
$( document ).ready(function() {
	retning = (<?php echo json_encode($retning)?>);
	load_kunde(retning);
	});
</script>


<!-- Variables -->
<?php
$errors = array (
		'name' => 'errors',
		'id' => 'errors',
		'size' => 40,
		'class' => ''
);
$kundenavn = array (
		'name' => 'kundenavn',
		'id' => 'kundenavn',
		'class' => 'textfield',
		'placeholder' => 'Kunde navn',
		'size' => 40 
);
$adresse = array (
		'name' => 'adresse',
		'id' => 'adresse',
		'class' => 'textfield',
		'placeholder' => 'Adresse',
		'size' => 40 
);
$by = array (
		'name' => 'by',
		'id' => 'by',
		'class' => 'textfield',
		'placeholder' => 'By',
		'size' => 40 
);
$telefon = array (
		'name' => 'telefon',
		'id' => 'telefon',
		'class' => 'textfield',
		'placeholder' => 'Telefon',
		'size' => 40 
);
$tilbudsnr = array (
		'name' => 'tilbudsnr',
		'id' => 'tilbudsnr',
		'class' => 'textfield',
		'placeholder' => 'Tilbudsnummer',
		'size' => 40 
);
$udfort = array (
		'name' => 'udfort',
		'id' => 'udfort',
		'class' => 'textfield',
		'placeholder' => 'Udført af',
		'size' => 40 
);
date_default_timezone_set('Europe/Copenhagen');
$date = date('d.m.y. H:i', time());

$beregningstidspunkt = array (
		'name' => 'beregningstidspunkt',
		'id' => 'beregningstidspunkt',
		'class' => 'textfield',
		'value'=>$date,
		'placeholder' => $date,
		'size' => 40
);
$buttonNext = array (
		'name' => 'next',
		'id' => 'next',
		'class' => 'buttonnext',
		'type' => 'button',
		'onclick' => 'val_kunde()'

);
$back = "'" . base_url ( '/home/index' ) . "'";
$buttonBack = array (
		'name' => 'back',
		'id' => 'back',
		'class' => 'buttonback',
		'type' => 'button',
		'onclick' => 'window.location.href=' . $back 
);
?>
<!-- FORM -->
<div class="container">
	<div class=row>
		<h3 class="col-md-5">Informationer om kunden</h3>
	</div>

	<?php echo form_open ( 'getdata/kunde', array ('id' => 'kunde' ) );?>
	<div class=row>
		<div class=col-md-5>
			<?php echo form_label ( '', 'errors', $errors ) . '<br>';?>
		</div>
	</div>
	<div class=row>
		<div class=col-md-5>
			<?php echo form_input ( $kundenavn ) . '<br>';?>
		</div>
	</div>
	<div class=row>
		<div class=col-md-5>
			<?php echo form_input ( $adresse ) . '<br>';?>
		</div>
	</div>
	<div class=row>
		<div class=col-md-5>
			<?php echo form_input ( $by ) . '<br>';?>
		</div>
	</div>
	<div class=row>
		<div class=col-md-5>
			<?php echo form_input ( $telefon ) . '<br>';?>
		</div>
	</div>
	<div class=row>
		<div class=col-md-5>
			<?php echo form_input ( $tilbudsnr ) . '<br>';?>
		</div>
	</div>
	<div class=row>
		<div class=col-md-5>
			<?php echo form_input ( $udfort ) . '<br>';?>
		</div>
	</div>
	<div class=row>
		<div class=col-md-5>
			<?php echo form_input ( $beregningstidspunkt ) . '<br>';?>
		</div>
	</div>

	<div class=row>
		<div class="col-md-5">
			<div style="float:right">
			<img src="<?php echo base_url('assets')?>/img/left.png" onclick="window.location.href=<?php echo $back?>" style="cursor:pointer;">
			<img src="<?php echo base_url('assets')?>/img/right.png" onclick = 'val_kunde();' style="cursor:pointer;">
			</div>
		</div>
	<div>
	
	<?php echo form_close ();?>
</div>
<div style="margin-bottom:200px">

</div>
<!-- End form -->
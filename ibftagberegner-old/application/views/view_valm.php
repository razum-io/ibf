<style>
#per{
	margin-top:40px;
	min-height: 270px;
}
#imgcont3d{
	padding-top: 150px;
}
</style>
<script>
$( document ).ready(function() {
tag = <?php echo '"'.$tag.'"'; ?>;
hoved = <?php echo '"'.$hoved.'"'; ?>;
valm = <?php echo '"'.$valm.'"'; ?>;
antalVinklerVinkel = <?php echo '"'.$antalvinklervinkel.'"'; ?>;
antalVinkler = <?php echo '"'.$antalVinkler.'"'; ?>;
antalKnaster = <?php echo '"'.$antalKnaster.'"'; ?>;
antaltilbygninger = <?php echo '"'.$antaltilbygninger.'"'; ?>;
antalTilbygninger = <?php echo '"'.$antaltilbygninger.'"'; ?>;
laengehus = <?php echo $laengehus;?>;
valmfrm(tag,hoved,valm,parseInt(antalVinklerVinkel),parseInt(antaltilbygninger),laengehus);
retning = (<?php echo json_encode($retning)?>);
baseurl = <?php echo '"'.base_url('assets/illustration').'"'; ?>;
load_valm();
if (hoved == 5){
	$('#imgcont3d').css('padding-top','30px'); 
}
});
</script>
<?php
$buttonNext = array (
		'name' => 'next',
		'id' => 'next',
		'class' => 'form-control next',
		'type' => 'submit',
		'content' => 'Frem ->' 
);
$back = "'" . base_url ( 'home/tagtype' ) . "'";
$buttonBack = array (
		'name' => 'back',
		'id' => 'back',
		'class' => 'form-control next',
		'type' => 'button',
		'onclick' => 'window.location.href=' . $back,
		'content' => '<-- Tilbage' 
);
$valmleft_capt = array (
		'1' => 'Gavl',
		'2' => 'Helvalm',
		'3' => 'Halvvalm' 
);
?>
<?php

echo form_open ( 'getdata/valm', array (
		'id' => 'valm' 
) );
?>
<div class="row">
	<div class="col-md-12">
		<h4>Tryk på den/de markerede gavle for at ændre opbygning.</h4>
	</div>
</div>
<div class="row">
	<div class="col-md-8">
		<div class="row">
	<?php for ($i=1;$i<=6;$i++):?>
	<div class="col-md-6" id="valmblock<?php echo $i;?>">
				<ul>
		<?php
		
		foreach ( $valmleft_capt as $num => $capt ) :
			$radio_left = array (
					'name' => 'valm' . $i,
					'id' => 'valm' . $num,
					'value' => $num,
					'onclick' => 'valmchange(' . $i . ',' . $num . ')' 
			);
			?>
<li class="list-group-item">
<?php echo form_radio($radio_left).$capt;?>
</li>
<?php
		endforeach
		;
		?>
	</ul>
			</div>
	<?php endfor;?>
</div>
	</div>
	<div class="col-md-4" id="per">


	</div>

</div>
<?php
echo form_button ( $buttonNext );
echo form_button ( $buttonBack );
echo form_close ();

?>

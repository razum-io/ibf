<style>
#per {
	margin-top: 40px;
}

#imgcont3d {
	padding-top: 150px;
}
</style>
<script>
$( document ).ready(function() {
	hoved = <?php echo '"'.$hoved.'"'; ?>;
	stenvalgt = <?php echo '"'.$stenvalgt.'"'; ?>;
	console.log("sten: "+stenvalgt);
	tilbyg = <?php echo '"'.$tilbyg.'"'; ?>;
	antalvinklervinkel = <?php echo '"'.$antalvinklervinkel.'"'; ?>;
	antaltilbygninger = <?php echo '"'.$antaltilbygninger.'"'; ?>;
	antalknaster = <?php echo '"'.$antalknaster.'"'; ?>;
	hvhus2(parseInt(hoved),parseInt(stenvalgt),parseInt(tilbyg),parseInt(antalvinklervinkel),parseInt(antaltilbygninger),parseInt(antalknaster ));

	dynValmType = <?php echo $dynValmType; ?>;
	baseurl = <?php echo '"'.base_url('assets/illustration').'"'; ?>;
	antalVinklerVinkel = <?php echo '"'.$antalvinklervinkel.'"'; ?>;
	antalVinkler = <?php echo '"'.$antalvinkler.'"'; ?>;
	antalTilbygninger = <?php echo '"'.$antaltilbygninger.'"'; ?>;
	antalKnaster = <?php echo '"'.$antalknaster.'"'; ?>;
	tag = <?php echo '"'.$tag.'"'; ?>;
	drawillustration('hvhus2');
	retning = (<?php echo json_encode($retning)?>);
	load_hvhus2(retning);
});
</script>

<?php
$buttonNext = array (
		'name' => 'next',
		'id' => 'next',
		'class' => 'form-control next',
		'onclick' => 'val_hvhus2()',
		'type' => 'button',
		'content' => 'Frem ->' 
);
$back = "'" . base_url ( 'home/vinkel' ) . "'";
$buttonBack = array (
		'name' => 'back',
		'id' => 'back',
		'class' => 'form-control next',
		'type' => 'button',
		'onclick' => 'window.location.href=' . $back,
		'content' => '<-- Tilbage' 
);
$under_capt = array (
		'undertag' => 'Undertag',
		'understrygning' => 'Understrygning' 
);
$valg_af_capt = array (
		'False' => 'Alm. rygningssten',
		'True' => 'Faconrygningssten' 
);
?>
<?php

echo form_open ( 'getdata/hvhus2', array (
		'id' => 'hvhus2' 
) );
?>
<div class="row">
	<div class="col-md-6">
		<h4 id="label1">Data for hovedhus</h4>
		<ul class="list-group" id="list1">

			<li class="list-group-item" id="l">


				<div class="input-group">
		a:  Længde på hus incl. udhæng:
		<?php
		$data = array (
				'name' => 'hvhus2_l',
				'id' => 'hvhus2_l',
				'class' => 'form-control incolright num' 
		);
		echo form_input ( $data );
		?>
		<span class="input-group-addon incolrightadd">m</span>
				</div>
			</li>

			<li class="list-group-item" id="b">


				<div class="input-group">
		b:  Bredde på hus incl. udhæng:
		<?php
		$data = array (
				'name' => 'hvhus2_b',
				'id' => 'hvhus2_b',
				'class' => 'form-control incolright num' 
		);
		echo form_input ( $data );
		?>
		<span class="input-group-addon incolrightadd">m</span>
				</div>
			</li>

			<li class="list-group-item" id="v">


				<div class="input-group">
		c:  Taghældning:
		<?php
		$data = array (
				'name' => 'hvhus2_v',
				'id' => 'hvhus2_v',
				'class' => 'form-control incolright num' 
		);
		echo form_input ( $data );
		?>
		<span class="input-group-addon incolrightadd">°</span>
				</div>
			</li>
		</ul>
		<h4 id="label2">Data for vinkel</h4>
		<ul class="list-group" id="list2">

			<li class="list-group-item" id="l2">


				<div class="input-group">
					<span id="labeld">d: Længde på hus incl. udhæng:</span>
		<?php
		$data = array (
				'name' => 'hvhus2_l2',
				'id' => 'hvhus2_l2',
				'class' => 'form-control incolright num' 
		);
		echo form_input ( $data );
		?>
		<span class="input-group-addon incolrightadd">m</span>
				</div>
			</li>

			<li class="list-group-item" id="b2">


				<div class="input-group">
					<span id="labele">e: Bredde på hus incl. udhæng:</span>
		<?php
		$data = array (
				'name' => 'hvhus2_b2',
				'id' => 'hvhus2_b2',
				'class' => 'form-control incolright num' 
		);
		echo form_input ( $data );
		?>
		<span class="input-group-addon incolrightadd">m</span>
				</div>
			</li>

			<li class="list-group-item" id="v2">


				<div class="input-group">
		f:  Taghældning:
		<?php
		$data = array (
				'name' => 'hvhus2_v2',
				'id' => 'hvhus2_v2',
				'class' => 'form-control incolright num' 
		);
		echo form_input ( $data );
		?>
		<span class="input-group-addon incolrightadd">°</span>
				</div>
			</li>
		</ul>
		<h4 id="label3">Data for tilbygning</h4>
		<ul class="list-group" id="list3">

			<li class="list-group-item" id="l3">


				<div class="input-group">
					<span id="labelg">g: Længde på hus incl. udhæng:</span>
		<?php
		$data = array (
				'name' => 'hvhus2_l3',
				'id' => 'hvhus2_l3',
				'class' => 'form-control incolright num' 
		);
		echo form_input ( $data );
		?>
		<span class="input-group-addon incolrightadd">m</span>
				</div>
			</li>

			<li class="list-group-item" id="b3">


				<div class="input-group">
					<span id="labelh">h: Bredde på hus incl. udhæng:</span>
		<?php
		$data = array (
				'name' => 'hvhus2_b3',
				'id' => 'hvhus2_b3',
				'class' => 'form-control incolright num' 
		);
		echo form_input ( $data );
		?>
		<span class="input-group-addon incolrightadd">m</span>
				</div>
			</li>

			<li class="list-group-item" id="v3">


				<div class="input-group">
		i:  Taghældning:
		<?php
		$data = array (
				'name' => 'hvhus2_v3',
				'id' => 'hvhus2_v3',
				'class' => 'form-control incolright num' 
		);
		echo form_input ( $data );
		?>
		<span class="input-group-addon incolrightadd">°</span>
				</div>
			</li>
		</ul>
	</div>
	<div class="col-md-6" id="per"></div>
</div>
<div class="row">
	<div class="col-md-6">
		<!-- 	Undertag/Understrygning -->
		<h4>Undertag/Understrygning</h4>
		<ul class="list-group">
			
<?php

foreach ( $under_capt as $value => $capt ) :
	$radio_under = array (
			'name' => 'under',
			'value' => $value,
			'id' => $value 
	);
	?>
			<li class="list-group-item">

  <?php echo form_radio($radio_under).$capt?>
  
  			</li>
<?php endforeach; ?>
		</ul>
		<!-- 		Valg af rygningssten -->
	</div>
	<div class="col-md-6" id="valgaf">
		<h4>Valg af rygningssten</h4>
		<ul class="list-group">
			
<?php

foreach ( $valg_af_capt as $value => $capt ) :
	$radio_valg_af = array (
			'name' => 'valg_af',
			'value' => $value,
			'id' => $value 
	);
	?>
			<li class="list-group-item">

  <?php echo form_radio($radio_valg_af).$capt?>
  
  			</li>
<?php endforeach; ?>
		</ul>
	</div>
</div>

<?php
echo form_button ( $buttonNext );
echo form_button ( $buttonBack );
echo form_close ();

?>
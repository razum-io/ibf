function tilbygning(tmp) {
	this.placering;
	this.bygning = new bygningsdel(tmp);
}
function bygningsdel(tmp) {
	this.x = 0;
	this.y = 0;
	this.hoejde = 0;
	this.bredde = 0;
	this.rygningsRetning = 0;

	this.kvistAktiv = 0;
	this.kvistType = 0;

	this.side1KnastAktiv = 0;
	this.side1KnastSide1Bogstav = 0;
	this.side1KnastSide2Bogstav = 0;
	this.side1KnastSide3Bogstav = 0;

	this.side4KnastAktiv = 0;
	this.side4KnastSide2Bogstav = 0;
	this.side4KnastSide3Bogstav = 0;
	this.side4KnastSide4Bogstav = 0;

	this.side1Bogstav = 0;
	this.side2Bogstav = 0;
	this.side3Bogstav = 0;
	this.side4Bogstav = 0;

	this.side1BogstavVinkel = 0; // Variabler til målsętning af taghęldningen
	this.side2BogstavVinkel = 0;
	this.side3BogstavVinkel = 0;
	this.side4BogstavVinkel = 0;

	this.side1TilbygningAktiv = false;
	this.side2TilbygningAktiv = false;
	this.side3TilbygningAktiv = false;
	this.side4TilbygningAktiv = false;

	if (tmp == undefined){
		tmp = 0;
	}
	tmp = tmp +1;
	if (tmp == 2) {
	
	} else {
		this.side1Tilbygning = new tilbygning(tmp);
		this.side2Tilbygning = new tilbygning(tmp);
		this.side3Tilbygning = new tilbygning(tmp);
		this.side4Tilbygning = new tilbygning(tmp);
	}
	// Defineres dynamisk i runtime-mode
	this.side1GavlTypeValgfri = false;
	this.side1GavlType = 0;
	this.side1GavlTypeValgfriX = 0;
	this.side1GavlTypeValgfriY = 0;
	this.side1GavlTypeValgfriBredde = 0;
	this.side1GavlTypeValgfriHoejde = 0;

	this.side2GavlTypeValgfri = false;
	this.side2GavlType = 0;
	this.side2GavlTypeValgfriX = 0;
	this.side2GavlTypeValgfriY = 0;
	this.side2GavlTypeValgfriBredde = 0;
	this.side2GavlTypeValgfriHoejde = 0;

	this.side3GavlTypeValgfri = false;
	this.side3GavlType = 0;
	this.side3GavlTypeValgfriX = 0;
	this.side3GavlTypeValgfriY = 0;
	this.side3GavlTypeValgfriBredde = 0;
	this.side3GavlTypeValgfriHoejde = 0;

	this.side4GavlTypeValgfri = false;
	this.side4GavlType = 0;
	this.side4GavlTypeValgfriX = 0;
	this.side4GavlTypeValgfriY = 0;
	this.side4GavlTypeValgfriBredde = 0;
	this.side4GavlTypeValgfriHoejde = 0;
}

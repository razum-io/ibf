function drawillustration(frm, tpe) {

	tag = tag.toString();
	hoved = parseInt(tag.slice(0, 1));
	tilb = parseInt(tag.slice(1, 2));
	valm = parseInt(tag.slice(2, 3));
	kvist = parseInt(tag.slice(4, 5));
	form = frm;
	type = tpe;
	
	if($("#per").length != 0) {
		$('#imgcont').remove();
		$("#per").append("<div id='imgcont'></div>");
		$('#imgcont3d').remove();
		$("#per").append("<div id='imgcont3d'></div>");
	}
	// remove and create div

	// calculate dynvalmtype;
	antalValgfrie = 0;
	if (form == "tagtype") {
		if (hoved == 2) {
			if (antalVinklerVinkel !== 2 && antalTilbygninger !== 1) {
				dynValmPlacering[2] = '3';
				dynValmType[2] = 2;
			} else {
				dynValmPlacering[1] = '1';
				dynValmType[1] = 2;
				if (antalTilbygninger !== 1) {
					dynValmPlacering[2] = '4';
					dynValmType[2] = 2;
				}
			}
		}
		laengehus = genererIllustration(tag, '', false, true, false, false,
				'none');
		laengehus = genererIllustration(tag, '', true, false, false, false,
				'none');
		danFelter(laengehus, "", valm);
	}

	

	showvalm2d();
	if ( form !== "maalsaet") {
		showvalm3d();
	}
	

}
function shownums(DVT, x1, y1, x2, y2, x3, y3, x4, y4) {
	switch (DVT) {
	case '0100':
		img('1.png', x2, y2);
		break;
	case '1000':
		img('1.png', x1, y1);
		break;
	case '1100':
		img('1.png', x1, y1);
		img('2.png', x2, y2);
		break;
	case '0010':
		img('1.png', x3, y3);
		break;
	case '1010':
		img('1.png', x1, y1);
		img('2.png', x3, y3);
		break;
	case '0110':
		img('1.png', x2, y2);
		img('2.png', x3, y3);
		break;
	case '1110':
		img('1.png', x1, y1);
		img('2.png', x2, y2);
		img('3.png', x3, y3);
		break;
	case '0001':
		img('1.png', x4, y4);
		break;
	case '0101':
		img('1.png', x2, y2);
		img('2.png', x4, y4);
		break;
	case '1001':
		img('1.png', x1, y1);
		img('2.png', x4, y4);
		break;
	case '1101':
		img('1.png', x1, y1);
		img('2.png', x2, y2);
		img('3.png', x4, y4);
		break;
	case '0011':
		img('1.png', x3, y3);
		img('2.png', x4, y4);
		break;
	case '1011':
		img('1.png', x1, y1);
		img('2.png', x3, y3);
		img('3.png', x4, y4);
		break;
	case '0111':
		img('1.png', x2, y2);
		img('2.png', x3, y3);
		img('3.png', x4, y4);
		break;
	case '1111':
		img('1.png', x1, y1);
		img('2.png', x2, y2);
		img('3.png', x3, y3);
		img('4.png', x4, y4);
		break;
	}
}
function showvalm2d() {
	if (form == 'hvhus1' || form == 'hvhus2' || form == 'hvhus3'
			|| form == 'vinkel_45' || form == 'h_hus') {
		izm = true;
	} else {
		izm = false;
	}
	if (type == 'valm2') {
		valm = 2;
	} else if (type == 'valm3') {
		valm = 3;
	}

	DVT = '';
	(dynValmType[1] == valm) ? DVT = '1' : DVT = '0';
	(dynValmType[2] == valm) ? DVT += '1' : DVT += '0';
	(dynValmType[3] == valm) ? DVT += '1' : DVT += '0';
	(dynValmType[4] == valm) ? DVT += '1' : DVT += '0';
	switch (hoved) {
	case 1:
		if (antalTilbygninger == 1 || tilb == 1) {
			img('11131111bck.png'); // back
			(dynValmType[1] == 1) ? img('11111111b.png') : false;
			(dynValmType[2] == 1) ? img('11111111c.png') : false;

			(dynValmType[1] == 2) ? img('11211111a.png') : false;
			(dynValmType[2] == 2) ? img('11211111b.png') : false;

			(dynValmType[1] == 3) ? img('11141111a.png') : false;
			(dynValmType[2] == 3) ? img('11141111b.png') : false;
			if (tilb == 2) {
				(dynValmType[3] == 1) ? img('12111111a.png') : false;
				(dynValmType[3] == 2) ? img('12211111a.png') : false;
				(dynValmType[3] == 3) ? img('12311111a.png') : false;
				(izm) ? img('izm/2a.png', -15, -30) : false;
			}
			showkvist('11121111a.png', '11131111a.png', '11141111c.png');
			// valm

			if (type == 'valm2' || type == 'valm3') {
				shownums(DVT, 32, 27, 130, 27, 100, 95);
			}
			(izm) ? img('izm/1a.png', -15, -30) : false;

		}
		if (antalTilbygninger == 2) {
			img('12211121a.png'); // back
			(dynValmType[1] == 1) ? img('12111121a.png') : false;
			(dynValmType[2] == 1) ? img('12111121d.png') : false;
			(dynValmType[3] == 1) ? img('12111121b.png') : false;
			(dynValmType[4] == 1) ? img('12111121c.png') : false;

			(dynValmType[1] == 2) ? img('12211121d.png') : false;
			(dynValmType[2] == 2) ? img('12211121b.png') : false;
			(dynValmType[3] == 2) ? img('12211121c.png') : false;
			(dynValmType[4] == 2) ? img('12211121e.png') : false;

			(dynValmType[1] == 3) ? img('12311121a.png') : false;
			(dynValmType[2] == 3) ? img('12311121d.png') : false;
			(dynValmType[3] == 3) ? img('12311121b.png') : false;
			(dynValmType[4] == 3) ? img('12311121c.png') : false;
			showkvist('12121121a.png', '12231121a.png', '12341121a.png');
			if (type == 'valm2' || type == 'valm3') {
				shownums(DVT, 100, 10, 30, 80, 170, 44, 100, 115);
			}
			(izm) ? img('izm/3a.png', -15, -30) : false;
		}
		if (tilb == 3) {
			img('13111112a.png'); // bck
			img('13111112c.png'); // first knast bottom
			if (antalKnaster == 2) {
				img('13111112b.png'); // secund knast top
				(izm) ? img('izm/5a.png', -15, -30) : false;
			}
			(dynValmType[1] == 1) ? img('13111112d.png') : false;
			(dynValmType[2] == 1) ? img('13111112e.png') : false;

			(dynValmType[1] == 2) ? img('13211112a.png') : false;
			(dynValmType[2] == 2) ? img('13211112b.png') : false;

			(dynValmType[1] == 3) ? img('13311112a.png') : false;
			(dynValmType[2] == 3) ? img('13311112b.png') : false;
			showkvist('11121111a.png', '11131111a.png', '11141111c.png', 0, 10);
			if (type == 'valm2' || type == 'valm3') {
				shownums(DVT, 16, 37, 126, 37);
			}
			(izm && antalKnaster !== '2') ? img('izm/4a.png', -26, -19) : false;
		}
		break;
	case 2:
		if (antalVinklerVinkel == 2) {
			img('21122111A.png'); // bck
			(dynValmType[1] == 1) ? img('21112111L.png') : false;
			(dynValmType[2] == 1) ? img('21122111C.png') : false;
			(dynValmType[3] == 1) ? img('21122111D.png') : false;
			(dynValmType[4] == 1) ? img('21112111K.png') : false;

			// 1 and 4 already in back
			(dynValmType[2] == 2) ? img('21232111B.png') : false;
			(dynValmType[3] == 2) ? img('21232111C.png') : false;

			(dynValmType[1] == 3) ? img('21312111K.png') : false;
			(dynValmType[2] == 3) ? img('21342111B.png') : false;
			(dynValmType[3] == 3) ? img('21342111C.png') : false;
			(dynValmType[4] == 3) ? img('21312111L.png') : false;
			showkvist('12121121a.png', '12231121a.png', '12341121a.png', 10, 0);
			if (type == 'valm2' || type == 'valm3') {
				shownums(DVT, 94, 10, 25, 92, 165, 22, 94, 105);
			}
			(izm) ? img('izm/6a.png', -15, -30) : false;
		} else {
			if (tilb == 1 || tilb == 3) {
				// img('21111111a.png'); // bck

				(dynValmType[1] == 1) ? img('21111111b.png') : false; // bck
				(dynValmType[2] == 1) ? img('21111111ax.png') : false; // bck
				(dynValmType[3] == 1) ? img('21111111c.png') : false; // bck

				(dynValmType[1] == 2) ? img('21211111a.png') : false;
				(dynValmType[2] == 2) ? img('21111111bxx.png') : false;
				(dynValmType[3] == 2) ? img('21211111b.png') : false;

				(dynValmType[1] == 3) ? img('21311111a.png') : false;
				(dynValmType[2] == 3) ? img('21111111cxx.png') : false;
				(dynValmType[3] == 3) ? img('21311111b.png') : false;
				if (tilb == 3) {
					img('23111111a.png');
					(izm) ? img('izm/8a.png', -25, -32) : false;
				}
				showkvist('21121111a.png', '21231111a.png', '21341111a.png',
						10, 0);
				if (type == 'valm2' || type == 'valm3') {
					shownums(DVT, 23, 26, 130, 25, 118, 123);
				}
				(izm && tilb !== 3) ? img('izm/7a.png', -15, -30) : false;
			} else { // tilb == 2
				img('22111111a.png'); // bck
				(dynValmType[1] == 1) ? img('22111111axy.png') : false;
				(dynValmType[1] == 3) ? img('22111111first.png') : false;
				
//				(dynValmType[1] == 2) ? img('22211111b.png') : false;
				(dynValmType[2] == 2) ? img('22211111b.png') : false;
				(dynValmType[3] == 2) ? img('22211111a.png') : false;
				(dynValmType[4] == 2) ? img('22211111c.png') : false;

//				(dynValmType[1] == 3) ? img('22311111a.png') : false;
				(dynValmType[2] == 3) ? img('22311111a.png') : false;
				(dynValmType[3] == 3) ? img('22311111b.png') : false;
				(dynValmType[4] == 3) ? img('22311111c.png') : false;
				if (type == 'valm2' || type == 'valm3') {
					shownums(DVT, 100, 13, 31, 75, 169, 26, 100, 108);
				}
				(izm) ? img('izm/9a.png', -15, -30) : false;
			}
		}
		break;
	case 3:
		(izm)? img('imgMaalsaes3_1.bmp') : img('img3.bmp');
		break;
	case 4:
		(izm)? img('imgMaalsaes4_1.bmp') : img('img4.bmp');
		break;
	case 5:
		if (antalVinkler == 1) {
			img('51111111bck.png');// bck51111111a
			(dynValmType[1] == 1) ? img('51111111a.png') : false;
			(dynValmType[2] == 1) ? img('51111211b.png') : false;

			(dynValmType[1] == 2) ? img('51211111a.png') : false;
			(dynValmType[2] == 2) ? img('51211211a.png') : false;

			(dynValmType[1] == 3) ? img('51311111a.png') : false;
			(dynValmType[2] == 3) ? img('51311211a.png') : false;

			if (type == 'valm2' || type == 'valm3') {
				shownums(DVT, 58, 61, 178, 27);
			}
			(izm) ? img('izm/10a.png', -15, -30) : false;
		} else {
			img('51111211a.png');// bck
			(dynValmType[2] == 2) ? img('51211211b.png') : false;
			(dynValmType[1] == 2) ? img('51211211a.png') : false;

			(dynValmType[2] == 3) ? img('51311211b.png') : false;
			(dynValmType[1] == 3) ? img('51311211a.png') : false;
			if (type == 'valm2' || type == 'valm3') {
				shownums(DVT, 45, 139, 178, 27);
			}
			(izm) ? img('izm/11a.png', -15, -30) : false;
		}

		break;
	case 6:
		img('61211111a.png');
		if (tilb == 2) {
			img('62211111.png');
			(dynValmType[1] == 1) ? img('6211a.png') : false;
			(dynValmType[1] == 3) ? img('6231a.png') : false;
			if (type == 'valm2' || type == 'valm3') {
				shownums(DVT, 49, 104);
			}
			(izm) ? img('izm/13a.png', -15, -30) : false;
		}
		(izm) ? img('izm/12a.png', -15, -30) : false;
		showkvist('61221111a.png', '61231111a.png');
		break;
	case 7:
		img('71111111a.png'); // back
		// no need for dynValmType[i] == 1, becouse back already has it

		(dynValmType[1] == 2) ? img('71211111b.png') : false;
		(dynValmType[2] == 2) ? img('71211111d.png') : false;
		(dynValmType[3] == 2) ? img('71211111c.png') : false;
		(dynValmType[4] == 2) ? img('71211111e.png') : false;

		(dynValmType[1] == 3) ? img('71311111b.png') : false;
		(dynValmType[2] == 3) ? img('71311111d.png') : false;
		(dynValmType[3] == 3) ? img('71311111c.png') : false;
		(dynValmType[4] == 3) ? img('71311111e.png') : false;
		showkvist('71121111a.png', '71231111a.png', '71341111a.png');
		(izm) ? img('izm/14a.png', -15, -30) : false;
		if (type == 'valm2' || type == 'valm3') {
			shownums(DVT, 28, 24, 105, 24, 28, 105, 105, 105);
		}
		break;
	}
}
function showvalm3d() {

	switch (hoved) {
	case 1:
		if (antalTilbygninger == 1 || tilb == 1) {
			img3d('11131111L.png'); // back
			(dynValmType[1] == 1) ? img3d('11111111L.png') : false;
			(dynValmType[2] == 1) ? img3d('11111111M.png') : false;

			(dynValmType[1] == 2) ? img3d('11131111M.png') : false;
			(dynValmType[2] == 2) ? img3d('11131111N.png') : false;

			(dynValmType[1] == 3) ? img3d('11141111K.png') : false;
			(dynValmType[2] == 3) ? img3d('11141111L.png') : false;
			if (tilb == 2) {
				(dynValmType[3] == 1) ? img3d('12111111k.png') : false;
				(dynValmType[3] == 2) ? img3d('12211111K.png') : false;
				(dynValmType[3] == 3) ? img3d('12311111k.png') : false;
				(izm) ? angle(25, 206,174,206) : false;
			}
			showkvist3d('11121111k.png', '11131111.png', '11141111M.png');
			(izm && tilb !== 2) ? angle(143, 141) : false;
		}
		if (antalTilbygninger == 2) {
			img3d('12211121k.png'); // back
			(dynValmType[1] == 1) ? img3d('12111121k.png') : false;
			(dynValmType[2] == 1) ? img3d('12111121N.png') : false;
			(dynValmType[3] == 1) ? img3d('12111121L.png') : false;
			(dynValmType[4] == 1) ? img3d('12111121M.png') : false;

			(dynValmType[1] == 2) ? img3d('12211121P.png') : false;
			(dynValmType[2] == 2) ? img3d('12211121L.png') : false;
			(dynValmType[3] == 2) ? img3d('12211121N.png') : false;
			(dynValmType[4] == 2) ? img3d('12211121M.png') : false;

			(dynValmType[1] == 3) ? img3d('12311121K.png') : false;
			(dynValmType[2] == 3) ? img3d('12311121N.png') : false;
			(dynValmType[3] == 3) ? img3d('12311121L.png') : false;
			(dynValmType[4] == 3) ? img3d('12311121M.png') : false;
			showkvist3d('12121121k.png', '12231121k.png', '12341121k.png');
			(izm) ? angle(34,170,174,146,25,240) : false;
		}
		if (tilb == 3) {
			if (antalKnaster == 2) {
				img3d('13211112N.png'); // secund knast top
			}
			img3d('13211112K.png'); // bck
			(dynValmType[1] == 1) ? img3d('13111112K.png') : false;
			(dynValmType[2] == 1) ? img3d('13111112L.png') : false;

			(dynValmType[1] == 2) ? img3d('13211112P.png') : false;
			(dynValmType[2] == 2) ? img3d('13211112L.png') : false;

			(dynValmType[1] == 3) ? img3d('13311112K.png') : false;
			(dynValmType[2] == 3) ? img3d('13311112L.png') : false;
			img3d('13211112M.png'); // first knast bottom
			showkvist3d('11121111k.png', '11131111.png', '11141111M.png', 10,
					145);
			(izm) ? angle(136, 136) : false;
		}
		break;
	case 2:
		if (antalVinklerVinkel == 2) {
			img3d('21122111K.png'); // back
			// (dynValmType[1] == 1) ? img3d('21122111N.png') : false;
			(dynValmType[2] == 1) ? img3d('21122111M.png') : false;
			(dynValmType[3] == 1) ? img3d('21122111N.png') : false;
			// (dynValmType[4] == 1) ? img3d('12111121M.png') : false;
			//
			// (dynValmType[1] == 2) ? img3d('21232111M.png') : false;
			(dynValmType[2] == 2) ? img3d('21232111L.png') : false;
			(dynValmType[3] == 2) ? img3d('21232111M.png') : false;
			// (dynValmType[4] == 2) ? img3d('12211121M.png') : false;
			//
			// (dynValmType[1] == 3) ? img3d('21342111L.png') : false;
			(dynValmType[2] == 3) ? img3d('21342111L.png') : false;
			(dynValmType[3] == 3) ? img3d('21342111M.png') : false;
			// (dynValmType[4] == 3) ? img3d('12311121M.png') : false;
			showkvist3d('12121121k.png', '12231121k.png', '12341121k.png', -3);
			(izm) ? angle(14,170,154,146,25,240) : false;
		} else {
			if (tilb == 1 || tilb == 3) {

				img3d('11131111L.png'); // bck
				img3d('11111111M.png'); // dynValmType[2] = 2, always like this.
				(dynValmType[1] == 1) ? img3d('11111111L.png') : false;
				(dynValmType[3] == 1) ? img3d('12111111k.png', 42, 145) : false;

				(dynValmType[1] == 2) ? img3d('11131111M.png') : false;
				(dynValmType[3] == 2) ? img3d('12211111K.png', 40, 145) : false;

				(dynValmType[1] == 3) ? img3d('11141111K.png') : false;
				(dynValmType[3] == 3) ? img3d('12311111k.png', 42, 145) : false;
				if (tilb == 3) {
					img3d('13211112M.png', 22, 156); // first knast bottom
				}

				showkvist3d('11121111k.png', '11131111.png', '11141111M.png',
						20, 150);
				(izm) ? angle(20, 206,200,200) : false;
				
			} else { // tilb == 2
				img3d('12211121k.png'); // back
				(dynValmType[1] == 1) ? img3d('12111121k.png') : false;
				(dynValmType[2] == 1) ? img3d('12111121N.png') : false;
				(dynValmType[3] == 1) ? img3d('12111121L.png') : false;
				(dynValmType[4] == 1) ? img3d('12111121M.png') : false;

				(dynValmType[1] == 2) ? img3d('12211121P.png') : false;
				(dynValmType[2] == 2) ? img3d('12211121L.png') : false;
				(dynValmType[3] == 2) ? img3d('12211121N.png') : false;
				(dynValmType[4] == 2) ? img3d('12211121M.png') : false;

				(dynValmType[1] == 3) ? img3d('12311121K.png') : false;
				(dynValmType[2] == 3) ? img3d('12311121N.png') : false;
				(dynValmType[3] == 3) ? img3d('12311121L.png') : false;
				(dynValmType[4] == 3) ? img3d('12311121M.png') : false;
				showkvist3d('12121121k.png', '12231121k.png', '12341121k.png');
				(izm) ? angle(34,170,174,146,25,240) : false;
			}
		}
		break;
	case 3:
		// img3d('img3d3.bmp');
		break;
	case 4:
		// img3d('img3d4.bmp');
		break;
	case 5:
		if (antalVinkler == 1) {
			(dynValmType[1] == 1) ? img3d('51111111K.png') : false;
			(dynValmType[2] == 1) ? img3d('51111211L.png') : false;

			(dynValmType[1] == 2) ? img3d('51211111k.png') : false;
			(dynValmType[2] == 2) ? img3d('51211211k.png') : false;

			(dynValmType[1] == 3) ? img3d('51311111k.png') : false;
			(dynValmType[2] == 3) ? img3d('51311211K.png') : false;
			(izm) ? angle(124,290) : false;
		} else {
			(dynValmType[1] == 1) ? img3d('51111211k.png') : false;
			(dynValmType[2] == 1) ? img3d('51111211L.png') : false;

			(dynValmType[1] == 2) ? img3d('51211211L.png') : false;
			(dynValmType[2] == 2) ? img3d('51211211k.png') : false;

			(dynValmType[1] == 3) ? img3d('51311211L.png') : false;
			(dynValmType[2] == 3) ? img3d('51311211K.png') : false;
			(izm) ? angle(204,304) : false;
		}

		break;
	case 6:
		img3d('61211111k.png');
		if (tilb == 2) {
			img3d('62211111k.png');
			(dynValmType[1] == 1) ? img3d('6211K.png', 15, 191) : false;
			(dynValmType[1] == 3) ? img3d('6231K.png', 15, 191) : false;
			(izm) ? angle(14,194,145,200) : false;
		}
		showkvist3d('61221111K.png', '61231111K.png');
		(izm) ? angle(14,194) : false;
		break;
	case 7:
		img3d('71211111M.png'); // back
		// no need for dynValmType[i] == 2, becouse back already has it
		(dynValmType[1] == 2) ? img3d('71211111k.png') : false;
		(dynValmType[3] == 2) ? img3d('71211111L.png') : false;

		(dynValmType[1] == 1) ? img3d('71111111k.png') : false;
		(dynValmType[2] == 1) ? img3d('71111111L.png') : false;
		(dynValmType[3] == 1) ? img3d('71111111N.png') : false;
		(dynValmType[4] == 1) ? img3d('71111111M.png') : false;

		(dynValmType[1] == 3) ? img3d('71311111K.png') : false;
		(dynValmType[2] == 3) ? img3d('71311111L.png') : false;
		(dynValmType[3] == 3) ? img3d('71311111M.png') : false;
		(dynValmType[4] == 3) ? img3d('71311111N.png') : false;
		showkvist3d('11121111k.png', '11131111.png', '11141111M.png', 60, 160);
		(izm) ? angle(22,198,124,156,75,217) : false;
		break;
	}
}
function showkvist(kv2, kv3, kv4, x, y) {
	switch (kvist) {
	case 1:
		break;
	case 2:
		img(kv2, x, y);
		break;
	case 3:
		img(kv3, x, y);
		break;
	case 4:
		img(kv4, x, y);
		break;
	}
}
function showkvist3d(kv2, kv3, kv4, x, y) {
	switch (kvist) {
	case 1:
		break;
	case 2:
		img3d(kv2, x, y);
		break;
	case 3:
		img3d(kv3, x, y);
		break;
	case 4:
		img3d(kv4, x, y);
		break;
	}
}
function angle(x1, y1, x2, y2, x3, y3) {
	img3d('c.png',x1,y1);
	if (x2 !== undefined) {
		img3d('f.png',x2,y2);
	}
	if (x3 !== undefined) {
		img3d('i.png',x3,y3);
	}
}
function img(url, x, y) {
	var img = new Image(); // width, height values are optional params
	img.src = baseurl + '/' + url;
	if (y !== undefined) {
		img.style.top = y + "px";
	}
	if (x !== undefined) {
		img.style.left = x + "px";
	}
	img.setAttribute("class", "illustration");
	$("#imgcont").append(img);
}
function img3d(url, x, y) {
	var img = new Image(); // width, height values are optional params
	img.src = baseurl + '/' + url;
	if (y !== undefined) {
		img.style.top = y + "px";
	}
	if (x !== undefined) {
		img.style.left = x + "px";
	}
	img.setAttribute("class", "illustration");
	$("#imgcont3d").append(img);
}
function delimg() {
	$('#imgcont').remove();
	$("#per").append("<div id='imgcont'></div>");
}
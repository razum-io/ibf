<script src="/sites/all/themes/ibf/js/ms_scripts.js"></script>

<?php
	$email = "";
	$my_message = "";
	$show_message = "show";
	if(isset($_POST['cancel']))
	{
	    $email = $_POST['email'];
		$my_message = custom_mailchimp_unsubscribe($email);
		unset($_POST['cancel']);
		unset($_POST['email']);
	}
	else if (isset($_POST['subscribe']))
	{
		$email = $_POST['email'];
		unset($_POST['email']);
		unset($_POST['subscribe']);


		$my_message = custom_mailchimp_subscribe($email);
	}

	if (strpos($my_message, "Tak") > -1 || strpos($my_message, "nu afmeldt") > -1)
			$show_message .= "status";
?>
<?php
  // We hide the comments and links now so that we can render them later.
  hide($content['comments']);
  hide($content['links']);
  hide($content['field_subtitle_headline']);
  hide($content['field_related_subjects']);
  hide($content['field_related_illustration']);
  hide($content['field_productimage']);
  hide($content['field_teasertext']);
  hide($content['field_movieiframe']);
  hide($content['field_bannerimage']);
  hide($content['language']);
?>

<?php if ($teaser): ?>

	<?php if (($node->field_passive['und'][0]['value']) == '0') : ?>
		<a href="<?php print url('node/'.$node->nid); ?>">
	<?php endif; ?>
		<div class="product">
			<?php if (isset($node->field_productimage) && count($node->field_productimage)> 0) : ?>
				<?php print render($content['field_productimage'][0]); ?>
			<?php else: // intet produkt billede/galleribillede ?>
					<?php print render($content['field_teaserimage']); ?>
			<?php endif; ?>
			<h4><?php print render($content['field_subtitle_headline']); ?></h4>
			<h3><?php print $title; ?></h3>
		</div>
	<?php if (($node->field_passive['und'][0]['value']) == '0') : ?>
		</a>
	<?php endif; ?>

<?php else: // Teaser slut ?>

<div id="content-main" class="node-<?php print $node->nid; ?> <?php print $classes; ?>"<?php print $attributes; ?>>

<div class="headerbox">
	<?php if (isset($node->field_subtitle_headline) && count($node->field_subtitle_headline)> 0) : ?>
		<h2><?php print render($content['field_subtitle_headline']); ?></h2>
	<?php endif; ?>
	<?php print render($title_prefix); ?>
		<h1 class="title" id="page-title"><?php print $title; ?></h1>
	<?php print render($title_suffix); ?>
	<?php if (isset($node->field_date)): ?>
		<h3><?php print render($content['field_date']); ?></h3>
	<?php endif; ?>
</div>
  <div class="content clearfix"<?php print $content_attributes; ?>>
	<div class="nyheder-content"><?php print render($content['body']); ?></div>
    <?php print render($content); ?>
    <div class="clear"></div>
    <?php
		$viewName = 'nyhedsbreve';
		$display_id = 'page';
		$myArgs = array();

		$view = views_get_view($viewName);
		$view->set_display($display_id);
		$view->set_arguments($myArgs);
		$view->execute();
		$news_count = count($view->result);

		if ($news_count > 0)
		{
			print views_embed_view($viewName, $display_id);
		}

    	$viewName = 'nyheder';
		$display_id = 'page';
		$myArgs = array();

		$view = views_get_view($viewName);
		$view->set_display($display_id);
		$view->set_arguments($myArgs);
		$view->execute();
		$news_count = count($view->result);

		if ($news_count > 0)
		{
			print views_embed_view($viewName, $display_id);
		}

		$viewName = 'nyheder';
		$display_id = 'page_1';
		$myArgs = array();

		$view = views_get_view($viewName);
		$view->set_display($display_id);
		$view->set_arguments($myArgs);
		$view->execute();
		$news_count = count($view->result);

		if ($news_count > 0)
		{
			print views_embed_view($viewName, $display_id);
		}
    ?>
  </div>

</div>
<div class="newsletter-mailchimp">
	<div>IBF NYHEDSBREV</div>
	<div>Udfyld og tilmeld dig vores nyhedsbrev</div>
	<div class="message<?php print $show_message; ?>"><?php print $my_message; ?></div>
	<form name="mailchimp" method="post" class="clearfix">
		<input class="email" type="text" name="email" value="E-mail" onfocus="if (this.value=='E-mail') this.value=''" onblur="if (this.value=='') this.value='E-mail'">
		<div>
			<input class="cancel" type="submit" name="cancel" value="AFMELD" />
			<input class="subscribe" type="submit" name="subscribe" value="TILMELD" />
		</div>
	</form>
</div>
<div class="clear"></div>
<?php endif; // teaser if slut ?>

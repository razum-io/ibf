<?php

/**
 * @file
 * Bartik's theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type, i.e., "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined, e.g. $node->body becomes $body. When needing to access
 * a field's raw values, developers/themers are strongly encouraged to use these
 * variables. Otherwise they will have to explicitly specify the desired field
 * language, e.g. $node->body['en'], thus overriding any language negotiation
 * rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */

//  print_r($node);
?>

<?php
  // We hide the comments and links now so that we can render them later.
  hide($content['comments']);
  hide($content['links']);
  hide($content['field_subtitle_headline']);
  hide($content['field_related_boxes']);
  hide($content['field_related_subjects']);
  hide($content['field_related_illustration']);
  hide($content['field_productimage']);
  hide($content['field_teasertext']);
  hide($content['field_bannerimage']);
  hide($content['field_teaserimage_sml']);
  hide($content['field_colorimage']);
  hide($content['field_brochure_url']);
  hide($content['language']);
  hide($content['body']);


?>
<?php if ($view_mode == "another_teaser"): ?>

	<?php if (isset($node->field_teaserimage_med) && count($node->field_teaserimage_med)> 0) : ?>
		<div class="product sidebox">
			<?php print render($content['field_teaserimage_med']); ?>
		</div>
	<? endif; ?>

<?php else: // another_teaser slut ?>

<?php if ($view_mode == "mega_teaser"): ?>

	<?php if (isset($node->field_teaserimage_sml)): ?>
	<a href="<?php print url('node/'.$node->nid); ?>">
	<div class="product">
		<?php print render($content['field_teaserimage_sml']); ?>
		<h3><?php print $title; ?></h3>
	</div>
	</a>
	<?php endif; ?>

<?php else: // mega_teaser slut ?>

<?php if ($teaser): ?>

	<?php if (isset($node->field_productimage)): ?>
	<a href="<?php print url('node/'.$node->nid); ?>">
	<div class="product">
		<?php print render($content['field_productimage'][0]); ?>
		<h3><?php print $title; ?></h3>
	</div>
	</a>
	<?php endif; ?>

<?php else: // Teaser slut ?>

<div id="content-main" class="node-<?php print $node->nid; ?> <?php print $classes; ?>"<?php print $attributes; ?>>

  <div class="content clearfix"<?php print $content_attributes; ?>>

	<div id="slideshow">
		<div id="imageslideshow"><?php print render($content['field_productimage']); ?></div>
	</div>

	<div class="headerbox">
		<?php print render($title_prefix); ?>
			<h1 class="content-title">
				<?php print $title; ?>
			</h1>
		<?php print render($title_suffix); ?>
	</div>

	<?php print render($content['body']); ?>
    <?php print render($content); ?>

  <script type="text/javascript" src="<?php print(base_path() . path_to_theme()); ?>/js/calc.js?v3"></script>
  <script type="text/javascript" src="<?php print(base_path() . path_to_theme()); ?>/js/patterns.js?v6"></script>

  </div>
    <?php
        $tiles = array();
        if (!empty($node->tun_data)) {
            $data = $node->tun_data;

            // unpack tun data
            $list = array();
            foreach ($data as $key => $value) {
                $list = array_merge($list, $value['data']);
            }

            // group by beregningsgruppe

            $groups = array();
            foreach ($list as $key => $item) {
              if ($item->beregninggruppe != '') {
                  if ($item->image != '') {
                    $groups[$item->beregninggruppe] = $item;
                  }
              }
            }




            $default = new stdClass();
            $stone_template = '<div class="tile tabindex="{cnt}" checked"><label for="pavetiletype-{id}">{image}<input class="form-radio stonetype" type="radio" name="pavetiletype" id="pavetiletype-{id}" value=\'{value}\' ></label></div>';

            $cnt = 2;
            foreach ($groups as $key => $item) {
                if ($item->beregninggruppe) {
                  $cnt ++;
                  $default->width = $item->bredde;
                  $default->height = $item->laengde;
                  $default->group = $item->beregninggruppe;
                  $default->m2 = $item->antalstk;
                  $default->name = $item->navn;
                  $default->sand = $item->fugesand;
                  $default->tun = $item->tun;

                  $rpl = array(
                    '{image}' => theme('image', array('path' => $item->image, 'width' => 60)),
                    '{id}' => $item->beregninggruppe,
                    '{value}' => json_encode($default),
                    '{cnt}' => $cnt,


                  );

                  $tiles[] = strtr($stone_template,$rpl);
                }
            }






        }
    ?>
<?php if (count($tiles) > 0) : ?>
<form method="GET" id="pavecalc" action="#">
  <div class="header">
    <h2><?php print t('Paving calculations'); ?></h2>
    <div class="fieldset">
		<div class="form-item form-length-box">
			<input type="text" tabindex="1" class="form-text form-number form-length" value="" name="length" id="pavecalclength">
			<label for="pavecalclength"><?php print t('Length'); ?></label>
		</div>
      <span class="multiply"></span>
		<div class="form-item form-width-box">
			<label for="pavecalcwidth"><?php print t('Width'); ?></label>
			<input type="text" tabindex="2" class="form-text form-number form-width" value="" name="width" id="pavecalcwidth">
		</div>
      <span class="equals"></span>
      <div class="form-result" id="calc-area"><strong></strong> <?php print t('m<sup>2</sup>'); ?></div>
    </div>
  </div>

  <div class="pavetiletype" style="display: none;">
    <h3><?php print t('Tile type'); ?></h3>
    <?php print(join('', $tiles)); ?>

  </div>

  <div class="pavepatterntype" style="display: none;">
    <h3><?php print(t('Laying Pattern')); ?></h3>
    <div id="patterns">
    </div>
    <div class="clear"></div>


    <div class="tabel-box" id="pattern-simple" >
      <h3 class=""><?php print t('Blokforbandt mønster'); ?></h3>
      <table class="horiz-lines">
        <tr>
          <td colspan="2" class="js-stone">-</td>
          <td rowspan="3" class="spacer"></td>
          <td></td>
        </tr>
        <tr>
          <td><?php print t('Usage approx. per m2'); ?>:</td>
          <td class="js-usage">- <?php print t('pcs.'); ?></td>
          <td class="amount"><?php print t('approx.'); ?> <strong class="js-amount">- <?php print t('pcs.'); ?></strong></td>
        </tr>
        <tr class="note">
          <td><?php print t('NB! The measurements indicated do not fit the selected pattern.'); ?></td>
          <td></td>
          <td></td>
        </tr>
      </table>
      <input class="form-radio" type="radio" name="calc-kombitype" id="calc-kombitype-k0" value="k0" checked="checked" style="display: none;">
    </div>

    <div class="tabel-box" id="pattern-complex">
      <h3 class="js-title"></h3>
      <div class="tabel-box-kombi" id="area_exact">
        <h4><?php print t('Entered'); ?>: <span class="dimension"></span> <em>m</em></h4>
        <div class="tableround">
			<table class="horiz-lines kombinationstyle">
			  <tr>
				<td></td>
			  </tr>
			  <tr>
				<td><?php print t('Usage approx. per m2'); ?>: <?php print t('stk.'); ?></td>
			  </tr>
			  <tr class="k2">
				<td></td>
			  </tr>
			  <tr>
				<td><?php print t('Usage approx. per m2'); ?>: <?php print t('stk.'); ?></td>
			  </tr>
			</table>
		</div>
		<div class="note"><label for="calc-kombitype-k1"><?php print t('Use exact dimensions'); ?> <input class="form-radio" type="radio" name="calc-kombitype" id="calc-kombitype-k1" value="k1"></label></div>
	</div>

      <div class="tabel-box-kombi tabel-box-kombi-less" id="area_less">
        <h4><?php print t('Smaller'); ?></h4>
        <div class="tableround">
			<table class="horiz-lines kombinationstyle">
			  <tr>
				<td><strong class="dimension"> - </strong> m</td>
			  </tr>
			  <tr>
				<td class="amount"><?php print t('approx.'); ?> <strong>- <?php print t('pcs.'); ?></strong></td>
			  </tr>
			  <tr class="k2">
				<td>&nbsp;</td>
			  </tr>
			  <tr>
				<td class="amount"><?php print t('approx.'); ?> <strong>- <?php print t('pcs.'); ?></strong></td>
			  </tr>
			</table>
		</div>
		<div class="note"><label for="calc-kombitype-k2"><?php print t('Smaller than entered'); ?> <input class="form-radio" type="radio" name="calc-kombitype" id="calc-kombitype-k2" value="k2" checked="checked"></label></div>
	</div>

      <div class="tabel-box-kombi tabel-box-kombi-more" id="area_more">
        <h4><?php print t('Greater'); ?></h4>
        <div class="tableround">
  			<table class="horiz-lines kombinationstyle">
  			  <tr>
  				<td><strong class="dimension">- x -</strong> m</td>
  			  </tr>
  			  <tr>
  				<td class="amount"><?php print t('approx.'); ?> <strong>- <?php print t('pcs.'); ?></strong></td>
  			  </tr>
  			  <tr class="k2">
  				<td>&nbsp;</td>
  			  </tr>
  			  <tr>
  				<td class="amount"><?php print t('approx.'); ?> <strong>- <?php print t('pcs.'); ?></strong></td>
  			  </tr>
  			</table>
        </div>
			  <div class="note"><label for="calc-kombitype-k3"><?php print t('Greater than entered'); ?> <input class="form-radio" type="radio" name="calc-kombitype" id="calc-kombitype-k3" value="k3"></label></div>
      </div>
      <div class="clear"></div>

      <div class="alert" id="measurementsalert"><p><strong>OBS!</strong> <?php print t('The measurements indicated do not fit the selected pattern.'); ?></p></div>

    </div>


  </div>

  <div class="paveextras"  style="display: none;">
    <h3><?php print t('Add-on Options'); ?></h3>

    <div class="tabel-box">
      <div class="tile calc-extras checked">
        <label for="calc-extras-e1">
          <img alt="Sand" src="<?php print(base_path() . path_to_theme()); ?>/grafik/sand.jpg" width="120" height="120">
          <input class="form-checkbox" type="checkbox" name="calc-extrastype" id="calc-extrastype-e1" value="e1" checked="checked">
        </label>
      </div>
      <h3><?php print t('Crack sand'); ?></h3>
      <table class="horiz-lines">
        <tr>
          <td><?php print t('Weight Approx. per m2'); ?>:</td>
          <td><span id="weightpr2m">-</span> kg</td>
          <td rowspan="3" class="spacer"></td>
          <td class="amount">ca. <strong><span id="weighttotal">-</span> kg</strong></td>
        </tr>
        <tr>
          <td><?php print t('Usage approx. per m2'); ?>:</td>
          <td class="amountprkvm"><span id="bagsprm2">-</span> stk.</td>
          <td class="amount">ca. <strong><span id="bagstotal">-</span> stk</strong></td>
        </tr>
        <tr class="note">
          <td><?php print t('Crack sand is calculated for 5mm intervals.'); ?></td>
          <td></td>
          <td></td>
        </tr>
      </table>
      <div class="clear"></div>
    </div>
  </div>
<div class="node-contact" style="display: none">
  <div class="resultbar">
    <h2><?php print t('Result'); ?></h2>
    <div class="retailers">
    <div class="current-retailer"><?php print t('dealer list...'); ?></div>
    <ul class="retailer-list" style="display: none;">
    <?php
        $view = views_get_view_result('retailers', 'default');
        foreach ($view as $key => $value) {
          print('<li><a href="javascript:;" onclick="setCurrentRetailer(' . $value->nid . ',this);return false;">' . $value->node_title .  '</a></li>');
        }
    ?>
    </ul>
    </div>

    <a href="javascript:openprint();" class="button black"><?php print t('Print'); ?></a>
  </div></div>
</form>

<?php endif; ?>
    <?php
    // tun magic data table from hell
    if (!empty($node->tun_data)) {
      // toggle bar
      ?>
      <div class="product-specification">
        <h2 class="togglebar"><?php echo t("Specification"); ?></h2>
        <div class="data-tables">
          <?php
          // data tables
          foreach ($node->tun_data as $group) {
            $rows = count($group['data']);
            //print_r($group);
            ?><h3><?php echo $group['title']; ?></h3><?php
            ?>
            <table>
              <thead>
                <tr>
                  <th class="image"></th>
                  <?php

                    ?><th class=""><?php echo t("Width/Length"); ?></th><?php
                    ?><th><?php echo t("Thickness"); ?></th><?php
                    /* ?><th class=""><?php echo t("Measure"); ?></th><?php */
                    ?><th><?php echo t("Colour"); ?></th><?php
                    ?><th class=""><?php echo t("Tun number"); ?></th><?php
                    ?><th><?php echo t("Number ca. units/m2"); ?></th><?php

                  /*
                  if (!empty($group['data'][0]->bredde) && !empty($group['data'][0]->laengde)) {
                    ?><th class=""><?php echo t("Width/Length"); ?></th><?php
                  }
                  if (!empty($group['data'][0]->tykkelse)) {
                    ?><th><?php echo t("Thickness"); ?></th><?php
                  }
                  if (!empty($group['data'][0]->maal)) {
                    ?><th class=""><?php echo t("Measure"); ?></th><?php
                  }
                  if (!empty($group['data'][0]->farve)) {
                    ?><th><?php echo t("Colour"); ?></th><?php
                  }
                  ?>
                  <th class=""><?php echo t("Tun number"); ?></th>
                  <?php
                  if (!empty($group['data'][0]->antalstk)) {
                    ?><th><?php echo t("Number ca. units/m2"); ?></th><?php
                  }
                  if (!empty($group['data'][0]->vaegt)) {
                    ?><th><?php echo t("Weight ca. kg/unit"); ?></th><?php
                  }
                   *
                   */
                  ?>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td rowspan="<?php echo ($rows+1); ?>" class="images">
                    <?php
                    foreach ($group['data'] as $item) {
                      if (!empty($item->image)) {
                        echo theme('image', array('path' => $item->image, 'width' => 60));
                      }
                    }
                    ?>
                    &nbsp;
                  </td>
                </tr>
                <?php
                foreach ($group['data'] as $item) {
                  ?>
                  <tr>
                    <?php
                    if (!empty($group['data'][0]->bredde) && !empty($group['data'][0]->laengde)) {
                      ?><td><?php echo $item->bredde; ?> x <?php echo $item->laengde; ?></td><?php
                    }
                    else {
                      ?><td>-</td><?php
                    }

                    if (!empty($group['data'][0]->tykkelse)) {
                      ?><td><?php echo $item->tykkelse; ?></td><?php
                    }
                    else {
                      ?><td>-</td><?php
                    }

                    /*if (!empty($group['data'][0]->maal)) {
                      ?><td><?php echo $item->maal; ?></td><?php
                    }
                    else {
                      ?><td>-</td><?php
                    } */

                    if (!empty($group['data'][0]->farve)) {
                      ?><td><?php echo t($item->farve); ?></td><?php
                    }
                    else {
                      ?><td>-</td><?php
                    }
                    ?>
                    <td><?php echo $item->tun; ?></td>
                    <?php
                    if (!empty($group['data'][0]->antalstk)) {
                      ?><td><?php echo $item->antalstk; ?></td><?php
                    }
                    else {
                      ?><td>-</td><?php
                    }

                    /*
                    if (!empty($group['data'][0]->vaegt)) {
                      ?><td><?php echo $item->vaegt; ?></td><?php
                    } */
                    ?>
                  </tr>
                  <?php
                }
                ?>
              </tbody>
            </table>
            <?php
          }
          ?>
        </div>
      </div>
      <?php

    } // tun data
    ?>

</div>

<div id="related">
  <?php if (isset($node->field_bannerimage)): ?>
	<div class="field_bannerimage">
		<?php print render($content['field_bannerimage']); ?>
	</div>
  <?php endif; ?>

  <?php if (isset($node->field_colorimage)): ?>
	<div class="field_colorimage">
		<?php print render($content['field_colorimage']); ?>
	</div>
  <?php endif; ?>
  <?php if (isset($node->field_brochure_url) && count($node->field_brochure_url)> 0): ?>
	<h3><?php echo t("Brochurer"); ?></h3>
	<div class="field_brochure_url">
		<?php print render($content['field_brochure_url']); ?>
		<div class="clear"></div>
	</div>
  <?php endif; ?>

  <?php if (isset($node->field_related_boxes)): ?>
	<div class="field_related_boxes">
		<?php print render($content['field_related_boxes']); ?>
	</div>
  <?php endif; ?>

	  <?php if (isset($node->field_related_subjects) && count($node->field_related_subjects)> 0) : ?>
		<div id="related-subjects">
			<h2><?php echo t("Relaterede emner"); ?></h2>
			<?php print render($content['field_related_subjects']); ?>
			<div class="clear"></div>
		</div>
	  <?php endif; ?>

  <div class="clear"></div>
</div> <!-- /.section, /#sidebar-second -->

<div class="clear"></div>
<?php endif; // teaser if slut ?>
<?php endif; // mega_teaser if slut ?>
<?php endif; // anotherteaser if slut ?>

<?php
	$newsletter_url = "";
	if (!empty($node->field_pdf_url[LANGUAGE_NONE][0]['value']))
	{
		$newsletter_url = $node->field_pdf_url[LANGUAGE_NONE][0]['value'];
	}

	render($content['field_pdf_url']);
?>

<a href="<?php print $newsletter_url; ?>">
<div class="news-item">
	<div class="date"><?php print render($content['field_date']); ?></div>
	<h3><?php print render($title); ?></h3>
	<?php print render($content); ?>
</div>
</a>
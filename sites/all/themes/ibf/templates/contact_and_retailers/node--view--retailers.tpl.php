<?php
/**
* Denne skabelon bruges til at printe de enkelte forhandlere og afdelinger
**/

  if ($teaser) {
    // get coordinates
    $geo = array(
      'lat' => null,
      'lon' => null
    );
    if (!empty($node->field_geo[LANGUAGE_NONE][0]) || !empty($node->field_auto_geo[LANGUAGE_NONE][0])) {
      if (!empty($node->field_geo[LANGUAGE_NONE][0])) {
        $geo['lat'] = $node->field_geo[LANGUAGE_NONE][0]['lat'];
        $geo['lon'] = $node->field_geo[LANGUAGE_NONE][0]['lon'];
      }
      else if (!empty($node->field_auto_geo[LANGUAGE_NONE][0])) {
        $geo['lat'] = $node->field_auto_geo[LANGUAGE_NONE][0]['lat'];
        $geo['lon'] = $node->field_auto_geo[LANGUAGE_NONE][0]['lon'];
      }
    }
  ?>
  <div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?> data-lat="<?php echo $geo['lat']; ?>" data-lon="<?php echo $geo['lon']; ?>">

	<div class="vcard">
    <?php print render($title_prefix); ?>
    <?php if (!$page): ?>
      <h2 class="title fn org"><?php print $title; ?></h2>
    <?php endif; ?>
    <?php print render($title_suffix); ?>

    <div class="content"<?php print $content_attributes; ?>>
      <?php
        // We hide the comments and links now so that we can render them later.
        hide($content['comments']);
        hide($content['links']);
        hide($content['field_geo']);
        hide($content['field_auto_geo']);
        hide($content['field_email']);
        hide($content['field_adr']);
        hide($content['field_zipcode']);
        hide($content['field_city']);
        hide($content['field_phone']);
        hide($content['field_adresse_placering']);
        hide($content['body']);
        print render($content);

        $gmap_q = $title;
        //$gmap_q .= ", " . $node->field_address[LANGUAGE_NONE][0]['country'] . "-" . $node->field_address[LANGUAGE_NONE][0]['postal_code'] . " " . $node->field_address[LANGUAGE_NONE][0]['thoroughfare'];
        //$gmap_q .= ", " . $node->field_address[LANGUAGE_NONE][0]['locality'];

        $city = !empty($node->field_city[LANGUAGE_NONE][0]['value']) ? $node->field_city[LANGUAGE_NONE][0]['value'] : "";
        $address = !empty($node->field_adr[LANGUAGE_NONE][0]['value']) ? $node->field_adr[LANGUAGE_NONE][0]['value'] : "";
        $zipcode = !empty($node->field_zipcode[LANGUAGE_NONE][0]['value']) ? $node->field_zipcode[LANGUAGE_NONE][0]['value'] : "";

        $gmap_q .= ", " . $zipcode . " " . $address;
        $gmap_q .= ", " . $city;

      ?>

      <div class="adr">
        <div class="street-address"><?php echo render($content['field_adr']); ?></div>
        <span class="postal-code"><?php echo render($content['field_zipcode']); ?></span> <span class="locality"><?php echo render($content['field_city']); ?></span>
      </div>
	<div class="tel"><span class="type"><?php echo t("Phone:")?></span> <a href="tel:<?php echo render($content['field_phone']); ?>"><?php echo render($content['field_phone']); ?></a></div>
	<div class="email"><span class="type"><?php echo t("Email:")?></span> <a href="mailto:<?php echo render($content['field_email']); ?>"><?php echo render($content['field_email']); ?></a></div>
	<div class="otherinfo"><?php echo render($content['body']); ?></div>
	<div class="show-on-map geo button black"><a href="http://maps.google.com/maps/?f=q&source=s_q&ie=UTF8&ll=<?php echo $geo['lat']; ?>,<?php echo $geo['lon']; ?>&spn=0.055174,0.138531&z=13&q=<?php echo urlencode($gmap_q);?>&hnear=<?php echo urlencode($gmap_q);?>"><?php echo t("Show on the map"); ?></a></div>
</div>


    </div>
  </div>
  <?php
}
else {

}
?>

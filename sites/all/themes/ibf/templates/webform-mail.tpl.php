<?php

/**
 * @file
 * Customize the e-mails sent by Webform after successful submission.
 *
 * This file may be renamed "webform-mail-[nid].tpl.php" to target a
 * specific webform e-mail on your site. Or you can leave it
 * "webform-mail.tpl.php" to affect all webform e-mails on your site.
 *
 * Available variables:
 * - $node: The node object for this webform.
 * - $submission: The webform submission.
 * - $email: The entire e-mail configuration settings.
 * - $user: The current user submitting the form.
 * - $ip_address: The IP address of the user submitting the form.
 *
 * The $email['email'] variable can be used to send different e-mails to different users
 * when using the "default" e-mail template.
 */
?>

<?php
    
    $col_left = 175;
    $col_right = 475;
    $col_tot = $col_left + $col_right;

	//Note: I use hardcoded styling because they are more reliable in email clients. You dont have to use this.
	//If you don't use this, omit the $options variable below and in the l() call.
    global $base_url;

    if($submission){
        print ($email['html'] ? '<table width="'.$col_tot.'" style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">' : '');
        print ($email['html'] ? '<tbody>' : '');
        
        //Title
        print ($email['html'] ? '<tr>' : '');
        print ($email['html'] ? '<th valign="top" align="left" colspan=2 style="font-size:16px;">' : '');
        print t('IBF formular</br>&nbsp;');
        print ($email['html'] ? '</th>' : '');
        print ($email['html'] ? '</tr>' : '');
    
        //Date        
        print ($email['html'] ? '<tr>' : '');
        print ($email['html'] ? '<th  valign="top" align="left" width="'.$col_left.'">' : '');
        print t('Emailen er sendt').':';
        print ($email['html'] ? '</th>' : '');
        
        print ($email['html'] ? '<td valign="top" align="left" width="'.$col_right.'">' : '');
        print t('!date', array('!date' => date('Y/m/d, H:i', $submission->submitted)));
        print ($email['html'] ? '</td>' : '');
        print ($email['html'] ? '</tr>' : '');

        foreach($node->webform['components'] as $eid => $entry){
            if($entry['type']=='fieldset'){
                print ($email['html'] ? '<tr>' : '');
                print ($email['html'] ? '<th colspan=2 valign="top" align="left" style="font-size:14px;">&nbsp;<br/>' : '');
                print $entry['name'];
                print ($email['html'] ? '</th>' : '');
                
                print ($email['html'] ? '</tr>' : '');
            }
            else if ($entry['type']=='select'){
                if($submission->data[$eid]['value'][0]){
                    print ($email['html'] ? '<tr>' : '');
                    print ($email['html'] ? '<th valign="top" align="left" width="'.$col_left.'">' : '');
                    
                    print $entry['name'].':';
                    print ($email['html'] ? '</th>' : '');
                    
                    print ($email['html'] ? '<td valign="top" align="left" width="'.$col_right.'">' : '');
                    
                    foreach($submission->data[$eid]['value'] as $vid => $value){ 
                        $curnode = node_load($value);
                        if ($curnode == false) {
	                        print($value . "<br>");                        
                        }
                        else {
	                        print($curnode->title . "<br>");
                        }
                        
                    } 
                    print ($email['html'] ? '</td>' : '');
                    print ($email['html'] ? '</tr>' : '');
                }
            }
            else{
                if($submission->data[$eid]['value'][0]){
                    print ($email['html'] ? '<tr>' : '');
                    print ($email['html'] ? '<th valign="top" align="left" width="'.$col_left.'">' : '');
                    
                    print $entry['name'].':';
                    print ($email['html'] ? '</th>' : '');
                    
                    print ($email['html'] ? '<td valign="top" align="left" width="'.$col_right.'">' : '');
                    foreach($submission->data[$eid]['value'] as $vid => $value){ 
                        print nl2br($value);
                    } 
                    print ($email['html'] ? '</td>' : '');
                    print ($email['html'] ? '</tr>' : '');
                }
            }
        }

        $options = array(
            'attributes' => array(
                'style' => 'font-family:Arial, Helvetica, sans-serif; font-size:12px;',
            ),
        );
        
//        print ($email['html'] ? '<tr>' : '');
//        print ($email['html'] ? '<th colspan=2 valign="top" align="left" style="font-size:14px;">&nbsp;<br/>' : '');
//        print l('Se formularen online',url('node/'. $node->nid .'/submission/'. $submission->sid, array('absolute' => TRUE)), $options);
//        print ($email['html'] ? '</th>' : '');
//        print ($email['html'] ? '</tr>' : '');
        
//        print ($email['html'] ? '<tr>' : '');
//        print ($email['html'] ? '<th valign="top" align="left" colspan=2 style="font-size:14px;">&nbsp;<br/>' : '');
//        print l('www.ibf.dk',$base_url.base_path(), $options);
//        print ($email['html'] ? '</th>' : '');
//        print ($email['html'] ? '</tr>' : '');
        
        print ($email['html'] ? '</tbody>' : '');
        print ($email['html'] ? '</table>' : '');
    }
?>

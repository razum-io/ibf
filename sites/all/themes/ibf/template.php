<?php

/**
 * Add body classes if certain regions have content.
 */
function ibf_preprocess_html(&$variables, $hook) {
	
  // add jquery validate
  drupal_add_js("http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js", "external");
	
	// Classes for body element. Allows advanced theming based on context
	// (home page, node of certain type, etc.)
	if (!$variables['is_front']) {
	// Add unique class for each page.
	$path = drupal_get_path_alias($_GET['q']);
	// Add unique class for each website section.
		list($section, ) = explode('/', $path, 2);
		if (arg(0) == 'node') {
			if (arg(1) == 'add') {
				$section = 'node-add';
			}
			elseif (is_numeric(arg(1)) && (arg(2) == 'edit' || arg(2) == 'delete')) {
				$section = 'node-' . arg(2);
			}
		}
		$variables['classes_array'][] = drupal_html_class('section-' . $section);
    
    
    $path_array = explode("/", $path);
    if (count($path_array) > 1 && $section ==  $path_array[0]) {
      $variables['classes_array'][] = drupal_html_class('section-' . $section . '-' . $path_array[1]);
    }
    
    
	}
	if (theme_get_setting('ibf_wireframes')) {
		$variables['classes_array'][] = 'with-wireframes'; // Optionally add the wireframes style.
	}

	// Add page and section body classes
	global $base_path;
	list(,$path) = explode($base_path, $_SERVER['REQUEST_URI'], 2);
	list($path,) = explode('?', $path, 2);
	$path = rtrim($path, '/');
	// Construct the id name from the path, replacing slashes with dashes.
	$body_id = str_replace('/', '-', $path);
	// Construct the class name from the first part of the path only.
	list($body_class,) = explode('/', $path, 2);
	// $body_class = $body_class . ' not-front';
	$body_id = 'page-'. $body_id;
	$body_class = 'section-'. $body_class;
	$vars['classes_array'][] = ' ' . $body_id . ' ' . $body_class;

	if (!empty($variables['page']['featured'])) {
		$variables['classes_array'][] = 'featured';
	}
	
	// Add conditional stylesheets for IE
	drupal_add_css(path_to_theme() . '/css/ie.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'lte IE 7', '!IE' => FALSE), 'preprocess' => FALSE));
	drupal_add_css(path_to_theme() . '/css/ie6.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'IE 6', '!IE' => FALSE), 'preprocess' => FALSE));
}

/**
 * Override or insert variables into the page template for HTML output.
 */
function ibf_process_html(&$variables) {
  // Hook into color.module.
  if (module_exists('color')) {
    _color_html_alter($variables);
  }
}

/**
 * Override or insert variables into the page template.
 */
function ibf_process_page(&$variables) {
  // Hook into color.module.
  if (module_exists('color')) {
    _color_page_alter($variables);
  }
  // Always print the site name and slogan, but if they are toggled off, we'll
  // just hide them visually.
  $variables['hide_site_name']   = theme_get_setting('toggle_name') ? FALSE : TRUE;
  $variables['hide_site_slogan'] = theme_get_setting('toggle_slogan') ? FALSE : TRUE;
  if ($variables['hide_site_name']) {
    // If toggle_name is FALSE, the site_name will be empty, so we rebuild it.
    $variables['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
  }
  if ($variables['hide_site_slogan']) {
    // If toggle_site_slogan is FALSE, the site_slogan will be empty, so we rebuild it.
    $variables['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
  }
  // Since the title and the shortcut link are both block level elements,
  // positioning them next to each other is much simpler with a wrapper div.
  if (!empty($variables['title_suffix']['add_or_remove_shortcut']) && $variables['title']) {
    // Add a wrapper div using the title_prefix and title_suffix render elements.
    $variables['title_prefix']['shortcut_wrapper'] = array(
      '#markup' => '<div class="shortcut-wrapper clearfix">',
      '#weight' => 100,
    );
    $variables['title_suffix']['shortcut_wrapper'] = array(
      '#markup' => '</div>',
      '#weight' => -99,
    );
    // Make sure the shortcut link is the first item in title_suffix.
    $variables['title_suffix']['add_or_remove_shortcut']['#weight'] = -100;
  }
  
  
  
}

function ibf_process_block(&$variables) {
  
  //print_r($variables['theme_hook_suggestions']);
   if ($variables['block']->module == 'nodeblock') {
    $variables['elements']['#block']->subject = NULL;
  }
}

/**
 * Implements hook_preprocess_maintenance_page().
 */
function ibf_preprocess_maintenance_page(&$variables) {
  if (!$variables['db_is_active']) {
    unset($variables['site_name']);
  }
  drupal_add_css(drupal_get_path('theme', 'bartik') . '/css/maintenance-page.css');
}

/**
 * Override or insert variables into the maintenance page template.
 */
function ibf_process_maintenance_page(&$variables) {
  // Always print the site name and slogan, but if they are toggled off, we'll
  // just hide them visually.
  $variables['hide_site_name']   = theme_get_setting('toggle_name') ? FALSE : TRUE;
  $variables['hide_site_slogan'] = theme_get_setting('toggle_slogan') ? FALSE : TRUE;
  if ($variables['hide_site_name']) {
    // If toggle_name is FALSE, the site_name will be empty, so we rebuild it.
    $variables['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
  }
  if ($variables['hide_site_slogan']) {
    // If toggle_site_slogan is FALSE, the site_slogan will be empty, so we rebuild it.
    $variables['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
  }
}

/**
 * Override or insert variables into the node template.
 */
function ibf_preprocess_node(&$variables) {
  if ($variables['view_mode'] == 'full' && node_is_page($variables['node'])) {
    $variables['classes_array'][] = 'node-full';
  }
  
    if (arg(0) == 'node') {
    $current_node = node_load((int)arg(1));
    if (!empty($current_node) && $current_node->type == 'top_oversigtsside' && $variables['teaser'] == TRUE) {
      if (in_array($variables['node']->type, array('produkt', 'produkt_oversigt'))) {
        $variables['theme_hook_suggestions'][] = 'node__top_oversigtsside_teaser';
      }
    }
  }
  
  //print_r($variables['theme_hook_suggestions']);
  
}

/**
 * Override or insert variables into the block template.
 */
function ibf_preprocess_block(&$variables) {
  // In the header region visually hide block titles.
  if ($variables['block']->region == 'header') {
    $variables['title_attributes_array']['class'][] = 'element-invisible';
  }
}

/**
 * Implements theme_menu_tree().
 */
function ibf_menu_tree($variables) {
  return '<ul class="menu clearfix">' . $variables['tree'] . '</ul>';
}

/**
 * Implements theme_field__field_type().
 */
function ibf_field__taxonomy_term_reference($variables) {
  $output = '';

  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<h3 class="field-label">' . $variables['label'] . ': </h3>';
  }

  // Render the items.
  $output .= ($variables['element']['#label_display'] == 'inline') ? '<ul class="links inline">' : '<ul class="links">';
  foreach ($variables['items'] as $delta => $item) {
    $output .= '<li class="taxonomy-term-reference-' . $delta . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</li>';
  }
  $output .= '</ul>';

  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . (!in_array('clearfix', $variables['classes_array']) ? ' clearfix' : '') . '">' . $output . '</div>';

  return $output;
}

function ibf_preprocess_views_view($variables) {
  // watchdog('debug',print_r($variables,true));
}

/**
 * Calculate current grouping level.
 *
 * This works like a patch for views contrib module, considering that
 * $grouping_level is always 0 in the views-view-grouping.tpl.php template.
 *
 * @see my_module_preprocess_views_view_grouping()
 *
 * @param array $rows
 *   View rows.
 * @param int $level (optional)
 *   Current group level.
 *
 * @return int
 *   Group level.
 */
 
function _my_module_get_views_grouping_level($rows, $level = 0) {
  $first_row = reset($rows);
  if (is_array($first_row) && isset($first_row['group']) ) { //&& !empty($first_row['rows'])) {
    $level++;
    return _my_module_get_views_grouping_level($first_row['rows'], $level);
  }
  return $level;
}
/**
 * Implements hook_preprocess_views_view_grouping().
 *
 * Correct the grouping level.
 */

function ibf_preprocess_views_view_grouping(&$vars) {
 //var_dump($vars);
 //$grouping = $vars['grouping_level'];//which is 0 for both grouping.
 
  //$vars['content'] = $vars['view']->style_plugin->render_grouping_sets($vars['rows'], $vars['grouping_level']+1);
 
  $grouping_level = _my_module_get_views_grouping_level($vars['rows']);
  $vars['grouping_level'] = $grouping_level;
}

function ibf_preprocess_views_view_table(&$vars) {
//  if(in_array($vars['view']->name,array('contactpersonsdepartment'))) {
//    foreach ($vars['rows'] as $k => $row ) {
//      print "<div class=\"afdeling\">" ;
//    }
//  }
}

function ibf_preprocess_field(&$variables, $hook) {
  /* print_r($variables['theme_hook_suggestions']); */
}

function ibf_theme(){
    return array(
        'user_login_block' => array(
            'template' => 'templates/user-login',
            'render element' => 'form',
        ), 
    ); 
}

function ibf_preprocess_user_login(&$variables) {
  $variables['intro_text'] = t('This is my awesome login form');
  $variables['rendered'] = drupal_render_children($variables['form']);
}

//function ibf_webform_mail_headers($variables) {
//	$headers = array(
//		'Content-Type' => 'text/html; charset=UTF-8; format=flowed; delsp=yes',
//		'X-Mailer' => 'Drupal Webform (PHP/'. phpversion() .')',
//	);
//	return $headers;
//}

//function ibf_webform_mail_headers_713($form_values, $node, $sid, $cid) {
//  $headers = array(
//    'Content-Type'  => 'text/html; charset=UTF-8; format=flowed; delsp=yes',
//    'X-Mailer'      => 'Drupal Webform (PHP/'. phpversion() .')',
//  );
//  return $headers;
//}

//function ibf_webform_mail_message_713($form_values, $node, $sid, $cid) {
//  return _ibf_callback('webform-mail-713', array('form_values' => $form_values, 'node' => $node, 'sid' => $sid, 'cid' => $cid));
//}

// this can be made betterm but i dont know how :-s
var megadropdownisopen = true;

(function ($) {
  $(document).ready(function(){

// Prislister
    $('body.page-prislister.i18n-da .view-id-pricelists h1').after('<div class="newprices2013"><a href="/da/prislister-2015">Bestilling af prislister 2015</a></div>');

  $('a[href^="http://"]')
    .attr({
    target: "_blank"
    });
  $('a[href$=".pdf"]')
    .attr({
    target: "_blank",
    title: "Download PDF"
  });

  // InFieldLabels
  $('.infieldlabels label').inFieldLabels();
  $('#block-mailchimp-lists-1 label').inFieldLabels();
  $('#block-webform-client-block-579 label').inFieldLabels();
  $('#gmap-controls-find-by-zip label').inFieldLabels();

  //Phone number format
  $(".vcard .tel a").text(function(i, text) {
    return text.replace(/(\d{2})(\d{2})(\d{2})(\d{2})/, "$1 $2 $3 $4");
  });

  //innner adding
  $("#block-mailchimp-lists-1,#block-webform-client-block-579,#block-block-5").wrapInner('<div class="inner" />');
  $(".product-specification td.images img").wrap('<div class="inner" />');

  //Just to be sure
  $('#content-wrapper').addClass('jsactive');

  //Product page
  $('#pavecalc .pavetiletype .tile:last,#pavecalc .pavepatterntype .tile:last').after('<div class="clear" />');
  $("#pavecalc .tile").removeClass('checked');
  $("#pavecalc .tile input:radio[checked=true]").parent().parent().addClass('checked');
  $("#pavecalc .tile input:radio").click(function() {
    $("#pavecalc .tile input:radio[checked=false]").parent().parent().removeClass('checked');
    $("#pavecalc .tile input:radio[checked=true]").parent().parent().addClass('checked');
  });
  $("#pavecalc .tabel-box-kombi input:radio[checked=true]").parent().parent().parent().addClass('checked');
  $("#pavecalc .tabel-box-kombi input:radio").click(function() {
    $("#pavecalc .tabel-box-kombi input:radio[checked=false]").parent().parent().parent().removeClass('checked');
    $("#pavecalc .tabel-box-kombi input:radio[checked=true]").parent().parent().parent().addClass('checked');
  });
//  $("#area_exact").click(function() {
//    $("#measurementsalert.alert").hide();
//  });
//  $("#area_less").click(function() {
//    $("#measurementsalert.alert").show();
//  });
//  $("#area_more").click(function() {
//    $("#measurementsalert.alert").show();
//  });


  $('#pavecalc table.horiz-lines tbody tr:last-child').addClass('last');
  $('#content-main.wideview #content-product-list :nth-child(3n)').addClass('last');
  $('#main-content .belaegning-list ul :nth-child(3n)').addClass('last');

  //Mega menu
    $('#block-views-mega-drop-down-block .view-content .views-row .megadrop').hide();
    $('#block-views-mega-drop-down-block div.title').append('<div class="shadowfixer"></div>');
    $('#block-views-mega-drop-down-block .view-content .views-row div.title').mouseenter(function(){
      $('#block-views-mega-drop-down-block .view-content .views-row div.title').removeClass('active')
      $(this).addClass('active');
      $('#block-views-mega-drop-down-block .view-content .views-row .megadrop').hide();
      $(this).siblings('.megadrop').show();
      return false;
    });

    $('#block-views-mega-drop-down-block .view-content .views-row div.title').hover(function() {megadropdownisopen=true});
    $('#block-views-mega-drop-down-block .view-content .views-row div.title').mouseover(function() {megadropdownisopen=true});
    $('#block-views-mega-drop-down-block .view-content .views-row .megadrop').hover(function() {megadropdownisopen=true});
    $('#block-views-mega-drop-down-block .view-content .views-row .megadrop').mouseover(function() {megadropdownisopen=true});
    $('#block-views-mega-drop-down-block .view-content .views-row div.title').mouseout(function() {megadropdownisopen=false});
    $('#block-views-mega-drop-down-block .view-content .views-row .megadrop').mouseout(function() {megadropdownisopen=false});

    window.setInterval(function() {if (megadropdownisopen== false) {
      $('#block-views-mega-drop-down-block .view-content .views-row div.title').removeClass('active')
      $('#block-views-mega-drop-down-block .view-content .views-row .megadrop').hide();}
    }, 200);

    $('#block-views-mega-drop-down-block .megadrop .collums ul li').wrapInner('<span />');

    $('<span></span>').appendTo('#nav-secondary .menu-level-2 li a');

  // produkt Slideshow
    var currentPosition = 0;
    var slideWidth = 620;
    var slides = $('.slide');
    var numberOfSlides = slides.length;

    // Remove scrollbar in JS
    $('#imageslideshow').css('overflow', 'hidden');

    // Wrap all .slides with #slideInner div
    slides
    .wrapAll('<div id="slideInner"></div>')
    // Float left to display horizontally, readjust .slides width
    .css({
      'float' : 'left',
      'width' : slideWidth
    });

    // Set #slideInner width equal to total width of all slides
    $('#slideInner').css('width', slideWidth * numberOfSlides);

    // Insert controls in the DOM
    $('#slideshow')
    .prepend('<span class="control" id="leftControl">Clicking moves left</span>')
    .append('<span class="control" id="rightControl">Clicking moves right</span>');

    // Hide left arrow control on first load
    manageControls(currentPosition);

    // Create event listeners for .controls clicks
    $('.control')
    .bind('click', function(){
    // Determine new position
    currentPosition = ($(this).attr('id')=='rightControl') ? currentPosition+1 : currentPosition-1;

    // Hide / show controls
    manageControls(currentPosition);
    // Move slideInner using margin-left
    $('#slideInner').animate({
      'marginLeft' : slideWidth*(-currentPosition)
    });
    });

    // manageControls: Hides and Shows controls depending on currentPosition
    function manageControls(position){
    // Hide left arrow if position is first slide
    if(position==0){ $('#leftControl').hide() } else{ $('#leftControl').show() }
    // Hide right arrow if position is last slide
    if(position==numberOfSlides-1){ $('#rightControl').hide() } else{ $('#rightControl').show() }
    }

    // Fancy box start
    $(".fancybox a").fancybox();

    // brochure
    if ($(".view-id-takso").length > 0) {
      $(".view-id-takso h2.togglebar").each(function() {
        $(this).parents("div.views-table:first").find(".contentbody").hide();
      });
//      $(".view-id-takso .views-table:first h2.togglebar").addClass("active");
//      $(".view-id-takso .views-table:first .contentbody").show();

      $(".view-id-takso .views-table h2.togglebar").click(function() {

      $(".view-id-takso .views-table .contentbody").hide();
      $(".view-id-takso .views-table h2.togglebar").removeClass("active");

      $(this).parents("div.views-table:first").find(".contentbody").slideDown();
      $(this).addClass("active");

      });

    }

    // brochure cart
    if ($("#webform-client-form-378").length > 0 || $("#webform-client-form-713").length > 0 || $("#webform-client-form-579").length > 0) {

      // borchure add to cart
      var cart = $(".brochure-cart");
      $(".view a.order").live("click", function() {
        var views_row = $(this).parents(".views-row:first");
        var nid = views_row.find("input.nid").val();
        var pdf_url = views_row.find(".pdf a").attr("href");
        var title = views_row.find("h3").text();

        var html;
        $.post('/ajax/cart/add', {title: title, nid: nid, pdf_url: pdf_url}, function(json) {
          if (json.cart != undefined && json.cart.length > 0) {
            $.each(json.cart, function() {
              if (cart.find(".item-"+this.nid).length == 0) {
                html = "<div class=\"item item-"+this.nid+"\"><input type=\"hidden\" name=\"submitted[cart]["+this.nid+"]\" value=\""+this.nid+"\" class=\"nid\" /><a href=\""+this.pdf_url+"\" class=\"download\" target=\"_blank\">"+this.title+" (Pdf)</a> <a href=\"javascript:;\" class=\"remove\">x</a></div>";
                cart.append(html);
              }
            });
          }
        }, 'json');
      });

      $("#webform-component-cart").empty();
      //var some_html = '<div class="form-item form-type-checkbox form-item-submitted-test-test"><input type="hidden" id="edit-submitted-test-1" name="submitted[cart][60]" value="60" class="form-checkbox"><label class="option" for="edit-submitted-test-1">TEST 2</label></div>';
      //$("#webform-component-cart").append(some_html);
      $(".brochure-cart .item .remove").live("click", function() {
        var nid = $(this).parents(".item:first").find("input.nid").val();
        var item = $(this);

        $.post('/ajax/cart/remove', {nid: nid}, function(json) {
          if (json.error != undefined && json.error == false) {
            item.parents(".item:first").remove();
          }
        }, 'json');

      });

      $(".webform-client-form").submit(function() {

        if ($(".brochure-cart .item").length == 0) {
          if ($("#brochure-cart").length > 0) {
            return false;
          }
        }

        // js validation here maybe
        //var email = $(".webform-client-form input.email").val();
        //if (validateEmail(email) == false) return false;
      });

      $(".webform-client-form").validate({
        errorElement: "span",
        rules: {
          "submitted[e_mail]": {
            required: true,
            email: true
          }
        },
        messages: {
          "submitted[e_mail]": {
            "required": Drupal.t("E-mail feltet er obligatorisk"),
            "email": Drupal.t("Indtast venligst en gyldig E-mail.")
          }
        }
      });
    }
    // (erhverv / privat)
    if ($("body.page-pdf").length > 0) {
      //alert("test");

    }

    // brochure cart

    // product spec
    if ($(".product-specification").length > 0) {
      $(".product-specification .data-tables").hide();
      $(".product-specification .togglebar").click(function() {
        if ($(".product-specification .data-tables").is(":visible")) {
          $(".product-specification .togglebar").removeClass("active");
          $(".product-specification .data-tables").hide();
        }
        else {
          $(".product-specification .togglebar").addClass("active");
          $(".product-specification .data-tables").show();
        }
      });

    }

    // some jquery form validations

    if ($("#mailchimp-lists-user-subscribe-form-1").length > 0) {
      $("#mailchimp-lists-user-subscribe-form-1").validate({
        errorElement: "span",
        rules: {
          "mailchimp_lists[mailchimp_1][mergevars][EMAIL]": {
            required: true,
            email: true
          }
        },
        messages: {
          "mailchimp_lists[mailchimp_1][mergevars][EMAIL]": {
            "required": Drupal.t("E-mail feltet er obligatorisk."),
            "email": Drupal.t("Indtast venligst en gyldig E-mail.")
          }
        }
      });
    }


    function validateEmail(email) {
      var re = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))/
      return re.test(email);
    }

// (erhverv / privat)

    if ($("body.page-pdf.i18n-da").length > 0) {
      var cart = $(".brochure-cart");
      var customer_type = 'private';
      setCustomerType(customer_type);

      var private_html = "<div class='private-packages'>";
      private_html += "<h2>Bestil brochure pakke</h2><p>Nedenfor kan du bestille en pakke med forskellige inspirerende brochurer, som bredt dækker IBF's sortiment indenfor henholdsvis Fliser og belægningssten eller Tagsten. Vælg en af nedenstående, og udfyld bestillingsformularen i højre side - så sender vi pakken til dig.</p>";
      private_html += "<div class='package'><input type='hidden' class='nid' value='1341' /><input type='hidden' class='title' value='Belægningspakke' /><a href='javascript:;' class='belaegningspakke'>Bestil belægningspakke</a></div>";
      private_html += "<div class='package'><input type='hidden' class='nid' value='1342' /><input type='hidden' class='title' value='Tagstenspakke' /><a href='javascript:;' class='tagstenspakke'>Bestil Tagstenspakke</a></div>";
      private_html += "</div>";

      var commercial_html = "<div class='commercial-packages'>";
      commercial_html += "<a href='/da/prislister/'>Prislister</a>";
      commercial_html += "</div>";

      $(".view-takso .view-header").append(private_html + commercial_html);
      $(".view-takso .view-header .private-packages").hide();
      $(".view-takso .view-header .commercial-packages").hide();
      // a bit of redundancy
      $(".private-packages a").live("click", function() {
        var pack = $(this).parents(".package:first");
        var nid = pack.find("input.nid").val();
        var pdf_url = '#';
        var title = pack.find("input.title").val();
        var html;

        $.post('/ajax/cart/add', {title: title, nid: nid, pdf_url: pdf_url}, function(json) {
          if (json.cart != undefined && json.cart.length > 0) {
            $.each(json.cart, function() {
              if (cart.find(".item-"+this.nid).length == 0) {
                html = "<div class=\"item item-"+this.nid+"\"><input type=\"hidden\" name=\"submitted[cart]["+this.nid+"]\" value=\""+this.nid+"\" class=\"nid\" /><a href=\""+this.pdf_url+"\" class=\"download\" target=\"_blank\">"+this.title+" (Pdf)</a> <a href=\"javascript:;\" class=\"remove\">x</a></div>";
                cart.append(html);
              }
            });
          }
        }, 'json');

      });

      var fancy_html = "<div class='customer-chooser'>";
      fancy_html += "<p>For at kunne bestille brochurer fra IBF, skal du vælge \"Professionel\" eller \"Privat\"</p>";
      fancy_html += "<a href='javascript:;' class='commercial'>Professionel</a>";
      fancy_html += "<a href='javascript:;' class='private'>Privat</a>";
      fancy_html += "</div>";

      $(".customer-chooser a").live("click", customerChoiceHandler);

      $.fancybox({
        'content': fancy_html,
        'closeBtn': false,
        'modal': true
      });

    }

    function customerChoiceHandler(e) {

      if ($(this).attr("class") == 'commercial') setCustomerType('commercial');
      if ($(this).attr("class") == 'private') setCustomerType('private');
      $.fancybox.close();

    }

    function setCustomerType(customer_type) {

      if (customer_type == 'private') {
        $(".view-takso .pdflisting li.last").hide();
        $(".view-takso .view-header .private-packages").show();
      }
      else if (customer_type == 'commercial') {
        var cart = $(".brochure-cart");

        $(".view-takso .pdflisting li.last").show();
        $(".view-takso .view-header .private-packages").hide();
        $(".view-takso .view-header .commercial-packages").show();
        if (cart.find(".item-1342").length > 0 || cart.find(".item-1341").length > 0) {
          cart.find(".item-1341").remove();
          cart.find(".item-1342").remove();
          $.post('/ajax/cart/remove', {nid: [1341, 1342]});
        }
      }
    }

  /* =============================================================
     Belægning oversigtsside
     ============================================================= */
    var $tiles=$('<div class="tiles" />')// .css({height:$(document).height()})
    var $shadow=$('<div class="shadow" />')// .css({height:$(document).height()})
    var $car=$('<div class="car" />')// .css({height:$(document).height()})
    var $skater=$('<div class="skater" />')// .css({height:$(document).height()})
    var $ball=$('<div class="ball" />')// .css({height:$(document).height()})
        $('#container-wrapper').prepend($tiles);
        $('#container-wrapper').prepend($shadow);
        $('#container-wrapper').prepend($car);
        $('#container-wrapper').prepend($skater);
        $('#container-wrapper').prepend($ball);

  }); // document ready end

})(jQuery);









// // this can be made betterm but i dont know how :-s
// var megadropdownisopen = true;

// (function ($) {
// 	$(document).ready(function(){

// // Prislister
//    	$('body.page-prislister.i18n-da .view-id-pricelists h1').after('<div class="newprices2014"><a href="/da/prislister-2014">Bestilling af prislister 2014</a></div>');

// 	$('a[href^="http://"]')
// 		.attr({
// 		target: "_blank"
// 		});
// 	$('a[href$=".pdf"]')
// 		.attr({
// 		target: "_blank",
// 		title: "Download PDF"
// 	});

// 	// InFieldLabels
// 	$('.infieldlabels label').inFieldLabels();
// 	$('#block-mailchimp-lists-1 label').inFieldLabels();
// 	$('#block-webform-client-block-579 label').inFieldLabels();
// 	$('#gmap-controls-find-by-zip label').inFieldLabels();

// 	//Phone number format
// 	$(".vcard .tel a").text(function(i, text) {
// 		return text.replace(/(\d{2})(\d{2})(\d{2})(\d{2})/, "$1 $2 $3 $4");
// 	});

// 	//innner adding
// 	$("#block-mailchimp-lists-1,#block-webform-client-block-579,#block-block-5").wrapInner('<div class="inner" />');
// 	$(".product-specification td.images img").wrap('<div class="inner" />');

// 	//Just to be sure
// 	$('#content-wrapper').addClass('jsactive');

// 	//Product page
// 	$('#pavecalc .pavetiletype .tile:last,#pavecalc .pavepatterntype .tile:last').after('<div class="clear" />');
// 	$("#pavecalc .tile").removeClass('checked');
// 	$("#pavecalc .tile input:radio[checked=true]").parent().parent().addClass('checked');
// 	$("#pavecalc .tile input:radio").click(function() {
// 		$("#pavecalc .tile input:radio[checked=false]").parent().parent().removeClass('checked');
// 		$("#pavecalc .tile input:radio[checked=true]").parent().parent().addClass('checked');
// 	});
// 	$("#pavecalc .tabel-box-kombi input:radio[checked=true]").parent().parent().parent().addClass('checked');
// 	$("#pavecalc .tabel-box-kombi input:radio").click(function() {
// 		$("#pavecalc .tabel-box-kombi input:radio[checked=false]").parent().parent().parent().removeClass('checked');
// 		$("#pavecalc .tabel-box-kombi input:radio[checked=true]").parent().parent().parent().addClass('checked');
// 	});
// //	$("#area_exact").click(function() {
// //		$("#measurementsalert.alert").hide();
// //	});
// //	$("#area_less").click(function() {
// //		$("#measurementsalert.alert").show();
// //	});
// //	$("#area_more").click(function() {
// //		$("#measurementsalert.alert").show();
// //	});


// 	$('#pavecalc table.horiz-lines tbody tr:last-child').addClass('last');
// 	$('#content-main.wideview #content-product-list :nth-child(3n)').addClass('last');
// 	$('#main-content .belaegning-list ul :nth-child(3n)').addClass('last');

// 	//Mega menu
//   	$('#block-views-mega-drop-down-block .view-content .views-row .megadrop').hide();
//   	$('#block-views-mega-drop-down-block div.title').append('<div class="shadowfixer"></div>');
//   	$('#block-views-mega-drop-down-block .view-content .views-row div.title').mouseenter(function(){
//   		$('#block-views-mega-drop-down-block .view-content .views-row div.title').removeClass('active')
//   		$(this).addClass('active');
//   		$('#block-views-mega-drop-down-block .view-content .views-row .megadrop').hide();
//   		$(this).siblings('.megadrop').show();
//   		return false;
//   	});

//   	$('#block-views-mega-drop-down-block .view-content .views-row div.title').hover(function() {megadropdownisopen=true});
//   	$('#block-views-mega-drop-down-block .view-content .views-row div.title').mouseover(function() {megadropdownisopen=true});
//   	$('#block-views-mega-drop-down-block .view-content .views-row .megadrop').hover(function() {megadropdownisopen=true});
//   	$('#block-views-mega-drop-down-block .view-content .views-row .megadrop').mouseover(function() {megadropdownisopen=true});
//   	$('#block-views-mega-drop-down-block .view-content .views-row div.title').mouseout(function() {megadropdownisopen=false});
//   	$('#block-views-mega-drop-down-block .view-content .views-row .megadrop').mouseout(function() {megadropdownisopen=false});

//   	window.setInterval(function() {if (megadropdownisopen== false) {
//   		$('#block-views-mega-drop-down-block .view-content .views-row div.title').removeClass('active')
//   		$('#block-views-mega-drop-down-block .view-content .views-row .megadrop').hide();}
//   	}, 200);

//   	$('#block-views-mega-drop-down-block .megadrop .collums ul li').wrapInner('<span />');

//   	$('<span></span>').appendTo('#nav-secondary .menu-level-2 li a');

//   // produkt Slideshow
//   	var currentPosition = 0;
//   	var slideWidth = 620;
//   	var slides = $('.slide');
//   	var numberOfSlides = slides.length;

//   	// Remove scrollbar in JS
//   	$('#imageslideshow').css('overflow', 'hidden');

//   	// Wrap all .slides with #slideInner div
//   	slides
//   	.wrapAll('<div id="slideInner"></div>')
//   	// Float left to display horizontally, readjust .slides width
//   	.css({
//   	  'float' : 'left',
//   	  'width' : slideWidth
//   	});

//   	// Set #slideInner width equal to total width of all slides
//   	$('#slideInner').css('width', slideWidth * numberOfSlides);

//   	// Insert controls in the DOM
//   	$('#slideshow')
//   	.prepend('<span class="control" id="leftControl">Clicking moves left</span>')
//   	.append('<span class="control" id="rightControl">Clicking moves right</span>');

//   	// Hide left arrow control on first load
//   	manageControls(currentPosition);

//   	// Create event listeners for .controls clicks
//   	$('.control')
//   	.bind('click', function(){
//   	// Determine new position
//   	currentPosition = ($(this).attr('id')=='rightControl') ? currentPosition+1 : currentPosition-1;

//   	// Hide / show controls
//   	manageControls(currentPosition);
//   	// Move slideInner using margin-left
//   	$('#slideInner').animate({
//   	  'marginLeft' : slideWidth*(-currentPosition)
//   	});
//   	});

//   	// manageControls: Hides and Shows controls depending on currentPosition
//   	function manageControls(position){
//   	// Hide left arrow if position is first slide
//   	if(position==0){ $('#leftControl').hide() } else{ $('#leftControl').show() }
//   	// Hide right arrow if position is last slide
//   	if(position==numberOfSlides-1){ $('#rightControl').hide() } else{ $('#rightControl').show() }
//   	}

//     // Fancy box start
//   	$(".fancybox a").fancybox();

//     // brochure
//     if ($(".view-id-takso").length > 0) {
//       $(".view-id-takso h2.togglebar").each(function() {
//         $(this).parents("div.views-table:first").find(".contentbody").hide();
//       });
// //      $(".view-id-takso .views-table:first h2.togglebar").addClass("active");
// //      $(".view-id-takso .views-table:first .contentbody").show();

//       $(".view-id-takso .views-table h2.togglebar").click(function() {

// 			$(".view-id-takso .views-table .contentbody").hide();
// 			$(".view-id-takso .views-table h2.togglebar").removeClass("active");

// 			$(this).parents("div.views-table:first").find(".contentbody").slideDown();
// 			$(this).addClass("active");

//       });

//     }

//     // brochure cart
//     if ($("#webform-client-form-378").length > 0 || $("#webform-client-form-713").length > 0 || $("#webform-client-form-579").length > 0) {

//       // borchure add to cart
//       var cart = $(".brochure-cart");
//       $(".view a.order").live("click", function() {
//         var views_row = $(this).parents(".views-row:first");
//         var nid = views_row.find("input.nid").val();
//         var pdf_url = views_row.find(".pdf a").attr("href");
//         var title = views_row.find("h3").text();

//         var html;
//         $.post('/ajax/cart/add', {title: title, nid: nid, pdf_url: pdf_url}, function(json) {
//           if (json.cart != undefined && json.cart.length > 0) {
//             $.each(json.cart, function() {
//               if (cart.find(".item-"+this.nid).length == 0) {
//                 html = "<div class=\"item item-"+this.nid+"\"><input type=\"hidden\" name=\"submitted[cart]["+this.nid+"]\" value=\""+this.nid+"\" class=\"nid\" /><a href=\""+this.pdf_url+"\" class=\"download\" target=\"_blank\">"+this.title+" (Pdf)</a> <a href=\"javascript:;\" class=\"remove\">x</a></div>";
//                 cart.append(html);
//               }
//             });
//           }
//         }, 'json');
//       });

//       $("#webform-component-cart").empty();
//       //var some_html = '<div class="form-item form-type-checkbox form-item-submitted-test-test"><input type="hidden" id="edit-submitted-test-1" name="submitted[cart][60]" value="60" class="form-checkbox"><label class="option" for="edit-submitted-test-1">TEST 2</label></div>';
//       //$("#webform-component-cart").append(some_html);
//       $(".brochure-cart .item .remove").live("click", function() {
//         var nid = $(this).parents(".item:first").find("input.nid").val();
//         var item = $(this);

//         $.post('/ajax/cart/remove', {nid: nid}, function(json) {
//           if (json.error != undefined && json.error == false) {
//             item.parents(".item:first").remove();
//           }
//         }, 'json');

//       });

//       $(".webform-client-form").submit(function() {

//         if ($(".brochure-cart .item").length == 0) {
//           if ($("#brochure-cart").length > 0) {
//             return false;
//           }
//         }

//         // js validation here maybe
//         //var email = $(".webform-client-form input.email").val();
//         //if (validateEmail(email) == false) return false;
//       });

//       $(".webform-client-form").validate({
//       	errorElement: "span",
//       	rules: {
//       		"submitted[e_mail]": {
//       			required: true,
//       			email: true
//       		}
//       	},
//       	messages: {
//       		"submitted[e_mail]": {
//       			"required": Drupal.t("E-mail feltet er obligatorisk"),
//       			"email": Drupal.t("Indtast venligst en gyldig E-mail.")
//       		}
//       	}
//       });
//     }
//     // (erhverv / privat)
//     if ($("body.page-pdf").length > 0) {
//       //alert("test");

//     }

//     // brochure cart

//     // product spec
//     if ($(".product-specification").length > 0) {
//       $(".product-specification .data-tables").hide();
//       $(".product-specification .togglebar").click(function() {
//         if ($(".product-specification .data-tables").is(":visible")) {
//           $(".product-specification .togglebar").removeClass("active");
//           $(".product-specification .data-tables").hide();
//         }
//         else {
//           $(".product-specification .togglebar").addClass("active");
//           $(".product-specification .data-tables").show();
//         }
//       });

//     }

//     // some jquery form validations

//     if ($("#mailchimp-lists-user-subscribe-form-1").length > 0) {
//     	$("#mailchimp-lists-user-subscribe-form-1").validate({
//     		errorElement: "span",
//       	rules: {
//       		"mailchimp_lists[mailchimp_1][mergevars][EMAIL]": {
//       			required: true,
//       			email: true
//       		}
//       	},
//       	messages: {
//       		"mailchimp_lists[mailchimp_1][mergevars][EMAIL]": {
//       			"required": Drupal.t("E-mail feltet er obligatorisk."),
//       			"email": Drupal.t("Indtast venligst en gyldig E-mail.")
//       		}
//       	}
//       });
//     }


//     function validateEmail(email) {
//       var re = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))/
//       return re.test(email);
//     }

// // (erhverv / privat)

//     if ($("body.page-pdf.i18n-da").length > 0) {
//       var cart = $(".brochure-cart");
//       var customer_type = 'private';
//       setCustomerType(customer_type);

//       var private_html = "<div class='private-packages'>";
//       private_html += "<h2>Bestil brochure pakke</h2><p>Nedenfor kan du bestille en pakke med forskellige inspirerende brochurer, som bredt dækker IBF's sortiment indenfor henholdsvis Fliser og belægningssten eller Tagsten. Vælg en af nedenstående, og udfyld bestillingsformularen i højre side - så sender vi pakken til dig.</p>";
//       private_html += "<div class='package'><input type='hidden' class='nid' value='1341' /><input type='hidden' class='title' value='Belægningspakke' /><a href='javascript:;' class='belaegningspakke'>Bestil belægningspakke</a></div>";
//       private_html += "<div class='package'><input type='hidden' class='nid' value='1342' /><input type='hidden' class='title' value='Tagstenspakke' /><a href='javascript:;' class='tagstenspakke'>Bestil Tagstenspakke</a></div>";
//       private_html += "</div>";

//       var commercial_html = "<div class='commercial-packages'>";
//       commercial_html += "<a href='/da/prislister/'>Prislister</a>";
//       commercial_html += "</div>";

//       $(".view-takso .view-header").append(private_html + commercial_html);
//       $(".view-takso .view-header .private-packages").hide();
//       $(".view-takso .view-header .commercial-packages").hide();
//       // a bit of redundancy
//       $(".private-packages a").live("click", function() {
//         var pack = $(this).parents(".package:first");
//         var nid = pack.find("input.nid").val();
//         var pdf_url = '#';
//         var title = pack.find("input.title").val();
//         var html;

//         $.post('/ajax/cart/add', {title: title, nid: nid, pdf_url: pdf_url}, function(json) {
//           if (json.cart != undefined && json.cart.length > 0) {
//             $.each(json.cart, function() {
//               if (cart.find(".item-"+this.nid).length == 0) {
//                 html = "<div class=\"item item-"+this.nid+"\"><input type=\"hidden\" name=\"submitted[cart]["+this.nid+"]\" value=\""+this.nid+"\" class=\"nid\" /><a href=\""+this.pdf_url+"\" class=\"download\" target=\"_blank\">"+this.title+" (Pdf)</a> <a href=\"javascript:;\" class=\"remove\">x</a></div>";
//                 cart.append(html);
//               }
//             });
//           }
//         }, 'json');

//       });

//       var fancy_html = "<div class='customer-chooser'>";
//       fancy_html += "<p>For at kunne bestille brochurer fra IBF, skal du vælge \"Professionel\" eller \"Privat\"</p>";
//       fancy_html += "<a href='javascript:;' class='commercial'>Professionel</a>";
//       fancy_html += "<a href='javascript:;' class='private'>Privat</a>";
//       fancy_html += "</div>";

//       $(".customer-chooser a").live("click", customerChoiceHandler);

//       $.fancybox({
//         'content': fancy_html,
//         'closeBtn': false,
//         'modal': true
//       });

//     }

//     function customerChoiceHandler(e) {

//       if ($(this).attr("class") == 'commercial') setCustomerType('commercial');
//       if ($(this).attr("class") == 'private') setCustomerType('private');
//       $.fancybox.close();

//     }

//     function setCustomerType(customer_type) {

//       if (customer_type == 'private') {
//         $(".view-takso .pdflisting li.last").hide();
//         $(".view-takso .view-header .private-packages").show();
//       }
//       else if (customer_type == 'commercial') {
//         var cart = $(".brochure-cart");

//         $(".view-takso .pdflisting li.last").show();
//         $(".view-takso .view-header .private-packages").hide();
//         $(".view-takso .view-header .commercial-packages").show();
//         if (cart.find(".item-1342").length > 0 || cart.find(".item-1341").length > 0) {
//           cart.find(".item-1341").remove();
//           cart.find(".item-1342").remove();
//           $.post('/ajax/cart/remove', {nid: [1341, 1342]});
//         }
//       }
//     }



//   }); // document ready end

// })(jQuery);

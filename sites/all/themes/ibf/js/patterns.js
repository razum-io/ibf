var globalpatlib = new Array();

globalpatlib["repeat"] = {
	id: "repeat",
	// "flise_kvadrat","flise_kvadrat_bånd","flise_kvadrat_kopsten"
	groups: ["sten_holme_normal","sten_holme_halv","sten_holme_makro","sten_holme_kvad_stor","sten_holme_kvad_lille","sten_holme_center_lille","sten_holme_center_stor","sten_holme_center_50","sten_holme_center_51","sten_holme_center_52","sten_holme_center_53","sten_beton_normal","sten_nybro_normal","sten_nybro_halv","sten_nybro_makro","flise_brosten","sten_holland_normal","sten_kloster_normal_3","flise_modul_30","flise_modul_40","flise_modul_50","flise_sq_30","flise_sq_30_dialonal","flise_sq_50","flise_sq_50_dialonal","flise_sq_kopsten","flise_trend_40","flise_trend_40_60","flise_struktur_30"],
	main: {
		stones: [
			{tun: 'Sten', count: 1},
		],
		height: 100,
		width: 100,
		title: "Forbandt"
	},
	
	bottom: { stones:[], width:0, height:0},
	right: 	{ stones:[], width:0, height:0},
	left: 	{ stones:[], width:0, height:0},
	top: 	{ stones:[], width:0, height:0},
	corner: { stones:[], width:0, height:0}
}	






	
	globalpatlib["skitse4"] = {
		id:"skitse4",
		sand: 3.1,
		groups: ["flise_modul_30","flise_brosten"],
		main: {
			stones: [
				{tun: "30x30", count: 4, id:"flise_modul_30"},
				{tun: "10x10", count: 13, id:"flise_brosten"}
			],
			height: 70,
			width: 70,
			title: 'Fliser Modul 30/Betonbrosten kombination 1'
		},
		
		bottom: {
			stones: [
				{tun: "10x10", count: 1, id:"flise_brosten"}
			],
			height: 10,
			width: 10
		},

		right: {
			stones: [
				{tun: "10x10", count: 1}
			],
			height: 10,
			width: 10
		},
		left: { stones:[], width:0, height:0},
		top: { stones:[],width:0, height:0},
		corner: {
			stones: [
				{tun: "10x10", count: 1}
			],
			count:1,
			width:0,
			height:0
		}
	}
	


     // My Calculations

     globalpatlib["skitse5"] = {
     	id:"skitse5",
     	sand: 2.81,
     	groups: ["flise_modul_30","flise_brosten"],
		main: {
			stones: [
				{tun: "30x30", count: 9, id:"flise_modul_30"},
				{tun: "10x10", count: 19, id:"flise_brosten"}
			],
			height: 100,
			width: 100,
			title: 'Fliser Modul 30/Betonbrosten kombination 2'
		},

		bottom: {
			stones: [
				{tun: "10x10", count: 1, id:"flise_brosten"}
			],
			height: 10,
			width: 10
		},

		right: {
			stones: [
				{tun: "10x10", count: 1}
			],
			height: 10,
			width: 10
		},
		left: { stones:[], width:0, height:0},
		top: { stones:[],width:0, height:0},
		corner: {
			stones: [
				{tun: "10x10", count: 1}
			],
			count:1,
			width:0,
			height:0
		}
	}

    globalpatlib["skitse6"] = {
    	id:"skitse6",
     	sand: 3.23,
    	groups: ["flise_modul_40","flise_brosten"],
		main: {
			stones: [
				{tun: "40x40", count: 1, id:"flise_modul_40"},
				{tun: "10x10", count: 9, id:"flise_brosten"}
			],
			height: 50,
			width: 50,
			title: 'Fliser Modul 40/Betonbrosten kombination 1'
		},

		bottom: {
			stones: [
				{tun: "10x10", count: 1}
			],
			height: 10,
			width: 10
		},

		right: {
			stones: [
				{tun: "10x10", count: 1}
			],
			height: 10,
			width: 10
		},
		left: { stones:[], width:0, height:0},
		top: { stones:[],width:0, height:0},
		corner: {
			stones: [
				{tun: "10x10", count: 1}
			],
			count:1,
			width:0,
			height:0
		}
	}

    globalpatlib["skitse7"] = {
    	id:"skitse7",
    	groups: ["flise_modul_40","flise_brosten"],
    	sand: 2.49,
		main: {
			stones: [
				{tun: "40x40", count: 4, id:"flise_modul_40"},
				{tun: "10x10", count: 17, id:"flise_brosten"}
			],
			height: 90,
			width: 90,
			title: 'Fliser Modul 40/Betonbrosten kombination 2'
		},

		bottom: {
			stones: [
				{tun: "10x10", count: 1}
			],
			height: 10,
			width: 10
		},

		right: {
			stones: [
				{tun: "10x10", count: 1}
			],
			height: 10,
			width: 10
		},
		left: { stones:[], width:0, height:0},
		top: { stones:[],width:0, height:0},
		corner: {
			stones: [
				{tun: "10x10", count: 1}
			],
			count:1,
			width:0,
			height:0
		}
	}

    globalpatlib["skitse8"] = {
    	id:"skitse8",
    	sand: 2.74,
    	groups: ["flise_modul_50","flise_brosten"],
		main: {
			stones: [
				{tun: "50x50", count: 1, id:"flise_modul_50"},
				{tun: "10x10", count: 11, id:"flise_brosten"}
			],
			height: 60,
			width: 60,
			title: 'Fliser Modul 50/Betonbrosten kombination 1'
		},

		bottom: {
			stones: [
				{tun: "10x10", count: 1}
			],
			height: 10,
			width: 10
		},

		right: {
			stones: [
				{tun: "10x10", count: 1}
			],
			height: 10,
			width: 10
		},
		left: { stones:[], width:0, height:0},
		top: { stones:[],width:0, height:0},
		corner: {
			stones: [
				{tun: "10x10", count: 1}
			],
			count:1,
			width:0,
			height:0
		}
	}

    globalpatlib["skitse9"] = {
    	id:"skitse9",
    	groups: ["flise_modul_50","flise_brosten"],
    	sand: 2.06,
		main: {
			stones: [
				{tun: "50x50", count: 4, id:"flise_modul_50"},
				{tun: "10x10", count: 21, id:"flise_brosten"}
			],
			height: 110,
			width: 110,
			title: 'Fliser Modul 50/Betonbrosten kombination 2'
		},

		bottom: {
			stones: [
				{tun: "10x10", count: 1}
			],
			height: 10,
			width: 10
		},

		right: {
			stones: [
				{tun: "10x10", count: 1}
			],
			height: 10,
			width: 10
		},
		left: { stones:[], width:0, height:0},
		top: { stones:[],width:0, height:0},
		corner: {
			stones: [
				{tun: "10x10", count: 1}
			],
			count:1,
			width:0,
			height:0
		}
	}

    globalpatlib["skitse10"] = {
    	id:"skitse10",
    	groups: ["flise_sq_50","flise_sq_kopsten"],
    	sand:1.26,
		main: {
			stones: [
				{tun: "50x50", count: 1, id:"flise_sq_50"},
				{tun: "5x5", count: 1, id:"flise_sq_kopsten"}
			],
			height: 50,
			width: 50,
			title: 'Kombination'
		},

		bottom: {
			stones: [
				{tun: "5x5", count: 1}
			],
			height: 10,
			width: 50
		},

		right: {
			stones: [
				{tun: "5x5", count: 1}
			],
			height: 50,
			width: 10
		},
		left: { stones:[], width:0, height:0},
		top: { stones:[],width:0, height:0},
		corner: {
			stones: [
				{tun: "5x5", count: 1}
			],
			count:1,
			width:0,
			height:0
		}
	}

    globalpatlib["skitse11"] = {
    	id:"skitse11",
    	groups: ["flise_sq_30","flise_sq_kopsten"],
    	sand: 2.09,
		main: {
			stones: [
				{tun: "30x30", count: 1, id:"flise_sq_30"},
				{tun: "5x5", count: 1, id:"flise_sq_kopsten"}
			],
			height: 30,
			width: 30,
			title: 'Standard mønster'
		},

		bottom: {
			stones: [
				{tun: "5x5", count: 1}
			],
			height: 6,
			width: 30
		},

		right: {
			stones: [
				{tun: "5x5", count: 1}
			],
			height: 30,
			width: 6
		},
		left: { stones:[], width:0, height:0},
		top: { stones:[],width:0, height:0},
		corner: {
			stones: [
				{tun: "5x5", count: 1}
			],
			count:1,
			width:0,
			height:0
		}
	}




	globalpatlib["skitse15"] = {
		id: "skitse15",
		sand: 2.98,
		groups: ["flise_kvadrat","flise_kvadrat_bånd","flise_kvadrat_kopsten"],
		main: {
			stones: [
				{tun: 'Normalflise', count: 1, id:'flise_kvadrat'},
				{tun: 'Bånd', count: 2, id:'flise_kvadrat_bånd'},
				{tun: 'Kop sten', count: 1, id:'flise_kvadrat_kopsten'}
			],
			height: 40,
			width: 40,
			title: "Kvadrat	- Standardmønster"
		},

		bottom: {
			stones: [
				{tun: 'Normalflise', count: 1},
				{tun: 'Bånd', count: 1}
			],
			height: 30,
			width: 40
		},

		right: {
			stones: [
				{tun: 'Normalflise', count: 1},
				{tun: 'Bånd', count: 1}
			],
			height: 40,
			width: 30
		},
		left: { stones:[], width:0, height:0},
		top: { stones:[],width:0, height:0},
		corner: {
			stones: [{tun: 'Normalflise', count: 1}],
			count:1,
			width:0,
			height:0
		}
	}	


    globalpatlib["skitse16"] = {
    	id:"skitse16",
    	sand: 2.88,
    	groups: ["flise_struktur_30","flise_struktur_30_60","flise_struktur_60"],
		main: {
			stones: [
	            {tun: "30x30", count: 1.6, id: "flise_struktur_30"},
				{tun: "30x60", count: 1.6, id: "flise_struktur_30_60"},
                  {tun: "60x60", count: 1.6, id: "flise_struktur_60"}
			],
			height: 100,
			width: 100,
			title: 'Forslag 1 - Vilkårligt forband'
		},

		bottom: { stones: [], height: 0, width: 0 },
		right: { stones: [], height: 0, width: 0 },
		left: { stones:[], width:0, height:0},
		top: { stones:[],width:0, height:0},
		corner: { stones: [], width:0, height:0 }
    }

    globalpatlib["skitse17"] = {
    	id:"skitse17",
    	groups: ["flise_struktur_30","flise_struktur_30_60","flise_struktur_60"],
		main: {
			stones: [
	            {tun: "30x30", count: 2.22, id: "flise_struktur_30"},
				{tun: "60x60", count: 2.22, id: "flise_struktur_60"}
			],
			height: 100,
			width: 100,
			title: 'Forslag 2 - Vilkårligt forband'
		},

		bottom: { stones: [], height: 0, width: 0},
		right: { stones: [], height: 0, width: 0},
		left: { stones:[], width:0, height:0},
		top: { stones:[],width:0, height:0},
		corner: { stones: [], width:0, height:0}
	}

    globalpatlib["skitse18"] = {
    	id:"skitse18",
    	groups: ["flise_struktur_30_60"],
		main: {
			stones: [
				{tun: "30x60", count: 2, id: "flise_struktur_30_60"}
			],
			height: 60,
			width: 60,
			title: 'Forslag 3'
		},

		bottom: { stones: [], height: 0, width: 0 },
		right: { stones: [], height: 0, width: 0 },
		left: { stones:[], width:0, height:0},
		top: { stones:[],width:0, height:0},
		corner: { stones: [], width:0, height:0 }
	}

    globalpatlib["skitse19"] = {
    	id:"skitse19",
    	groups: ['flise_struktur_30_60','flise_struktur_30'],
		main: {
			stones: [
	            {tun: "30x60", count: 5.55, id:"flise_struktur_30_60"},
				{tun: "30x30", count: 11.1, id:"flise_struktur_30"}
			],
			height: 100,
			width: 100,
			title: 'Forslag 4'
		},

		bottom: { stones: [], height: 0, width: 0 },
		right: { stones: [], height: 0, width: 0 },
		left: { stones:[], width:0, height:0},
		top: { stones:[],width:0, height:0},
		corner: { stones: [], width:0, height:0 }
	}

    globalpatlib["skitse20"] = {
    	id:"skitse20",
    	groups: ['flise_struktur_t1','flise_struktur_t2','flise_struktur_t3','flise_struktur_t4','flise_struktur_60','flise_struktur_t6','flise_struktur_t7'],
    	sand: 2.9,
		main: {
			stones: [
	            {tun: "flise_struktur_t1", count: 1},
				{tun: "flise_struktur_t2", count: 8},
                {tun: "flise_struktur_t3", count: 12},
				{tun: "flise_struktur_t4", count: 4},
                {tun: "flise_struktur_t5", count: 4},
				{tun: "flise_struktur_t6", count: 4},
                {tun: "flise_struktur_t7", count: 4}
			],
			height: 900,
			width: 900,
			title: 'Cirkel 3x3 meter'
		},

		bottom: { stones: [], height: 0, width: 0 },
		right: { stones: [], height: 0, width: 0 },
		left: { stones:[], width:0, height:0},
		top: { stones:[],width:0, height:0},
		corner: { stones: [], width:0, height:0}
	}

    globalpatlib["skitse22"] = {
    	id:"skitse22",
    	groups: ['flise_trend_40_60'],
    	sand: 0.85,
		main: {
			stones: [
				{tun: "40x60", count: 6, id:"flise_trend_40_60"},
			],
			height: 120,
			width: 120,
			title: 'Blokspecial'
		},

		bottom: { stones: [], height: 0, width: 0 },
		right: { stones: [], height: 0, width: 0 },
		left: { stones:[], width:0, height:0},
		top: { stones:[],width:0, height:0},
		corner: { stones: [], width:0, height:0}
	}

    globalpatlib["skitse23"] = {
    	groups:['flise_trend_40','flise_trend_40_60'],
    	id:"skitse23",
    	sand: 1,
		main: {
			stones: [
				{tun: "40x40", count: 6, id:"flise_trend_40"},
                {tun: "40x60", count: 2, id:"flise_trend_40_60"}
			],
			height: 80,
			width: 180,
			title: 'Halvforbandt Kantflise'
		},

		bottom: {stones: [], height: 0, width: 0 },

		right: {
			stones: [],
			height: 0,
			width: 0
		},
		left: { stones:[], width:0, height:0},
		top: { stones:[],width:0, height:0},
		corner: {
			stones: [],
			width:0,
			height:0
		}
	}

    globalpatlib["skitse24"] = {
    	groups:['flise_trend_40','flise_trend_40_60'],
    	id:"skitse24",
    	sand: 0.8,
		main: {
			stones: [
                {tun: "40x60", count: 1, id:"flise_trend_40_60"}
			],
			height: 40,
			width: 60,
			title: 'Triediedelforbandt Kantflise'
		},

		bottom: { stones: [], height: 0, width: 0},
		right:  { stones: [], height: 0, width: 0},
		left:   { 
			stones:[ {tun: "40x40", count: 1, id:"flise_trend_40"}], 
			width:40, 
			height:40
		},
		top:    { stones:[],width:0, height:0},
		corner: { stones: [], width:0, height:0 }
	}

    globalpatlib["skitse30"] = {
    	id:"skitse30",
    	groups: ['sten_holme_normal','sten_holme_halv'],
    	sand: 4.56,
		main: {
			stones: [
				{tun: 'Normalsten', count: 1, id:"sten_holme_normal"}
			],
			height: 14,
			width: 21,
			title: "Halvforbandt"
		},

		bottom: {
			stones: [],
			height: 0,
			width: 0
		},

		right: {
			stones: [
			 {tun:'Halve', count: 1, id:"sten_holme_halv"}
			],
			height: 14,
			width: 10.5
		},
		left: { stones:[], width:0, height:0},
		top: { stones:[],width:0, height:0},
		corner: {
			stones: [],
			count:0,
			width:0,
			height:0
		}
	}
	

globalpatlib["skitse31"] = {
	id: "skitse31",
	groups: ["sten_holme_kvad_stor","sten_holme_makro","sten_holme_kvad_lille","sten_holme_normal"],
	sand: 4.56,
	main: {
		stones: [
			{tun: 'Kvadrat stor', count: 5.4, id:"sten_holme_kvad_stor"},
			{tun: 'Makro', count: 5.4, id:"sten_holme_makro"},
			{tun: 'Kvadrat lille', count: 10.8, id:"sten_holme_kvad_lille"},
			{tun: 'Normalsten', count: 5.4, id:"sten_holme_normal"}
		],
		height: 100,
		width: 100,
		title: "Holmegaardsten®	Vilkårligt"
	},
	
	bottom: { stones:[], width:0, height:0},
	right: 	{ stones:[], width:0, height:0},
	left: 	{ stones:[], width:0, height:0},
	top: 	{ stones:[], width:0, height:0},
	corner: { stones:[], width:0, height:0}
}	
	

    globalpatlib["skitse32"] = {
    	id:"skitse32",
    	groups: ['sten_holme_normal'],
    	sand: 4.56,
		main: {
			stones: [
				{tun: "14x21", count: 34, id:"sten_holme_normal"}
			],
			height: 100,
			width: 100,
			title: 'Vinkelforbandt'
		},

		bottom: { stones:[], width:0, height:0},
		right: 	{ stones:[], width:0, height:0},
		left: 	{ stones:[], width:0, height:0},
		top: 	{ stones:[], width:0, height:0},
		corner: { stones:[], width:0, height:0}
	}

    globalpatlib["skitse35"] = {
    	id:"skitse35",
    	groups: ['sten_holme_kvad_stor','sten_holme_kvad_lille'],
    	sand: 3.4,
		main: {
			stones: [
				{tun: "28x28", count: 9, id:"sten_holme_kvad_stor"},
				{tun: "14x14", count: 13, id:"sten_holme_kvad_lille"}
			],
			height: 98,
			width: 98,
			title: 'Kvadrat stor - kombi'
		},

		bottom: {
		    stones: [{tun: "14x14", count: 1, id:"sten_holme_kvad_lille"}],
			height: 14,
			width: 14
        },

		right: {
			stones: [{tun: "14x14", count: 1}],
			height: 14,
			width: 14
		},
		left: { stones:[], width:0, height:0},
		top: { stones:[],width:0, height:0},
		corner: {
			stones: [{tun: "14x14", count: 1}],
			count:1,
			width:0,
			height:0
		}
	}
	




	globalpatlib["skitse37"] = {
		id:"skitse37",
		groups: ['sten_classico_2_normal', 'sten_classico_3_halv'],
		sand: 5.81,
		main: {
			stones: [
				{tun: '1/1 normalsten nr. 2', count: 1, id:"sten_classico_2_normal"}
			],
			height: 11.8,
			width: 11.8,
			title: "Classico - nr.2 halvforbandt"
		},
		
		bottom: {
			stones: [],
			height: 0,
			width: 0
		},

		right: {
			stones: [
			 {tun:'1/2 normalsten nr. 3', count: 1, id:"sten_classico_3_halv"}, 
			],
			height: 11.8,
			width: 6
		},
		left: { stones:[], width:0, height:0},
		top: { stones:[],width:0, height:0},
		corner: {
			stones: [],
			count:0,
			width:0,
			height:0
		}

		
	}
		

    globalpatlib["skitse38"] = {
    	id:"skitse38",
		groups: ['sten_classico_1_normal', 'sten_classico_1_halv'],
		sand: 4.9,
    	
		main: {
			stones: [
				{tun: '1,5 normalsten nr. 1', count: 1, id:"sten_classico_1_normal"}
			],
			height: 11.8,
			width: 17.5,
			title: "Classico - nr.1 tredjedel forbandt"
		},

		bottom: {
			stones: [],
			height: 0,
			width: 0
		},

		right: {
			stones: [
			 {tun:'1/2 normalsten nr. 3', count: 1, id:"sten_classico_1_halv"}
			],
			height: 11.8,
			width: 6
		},
		left: { stones:[], width:0, height:0},
		top: { stones:[],width:0, height:0},
		corner: {
			stones: [],
			count:0,
			width:0,
			height:0
		}
	}


    globalpatlib["skitse40"] = {
    	id:"skitse40",
		groups: ['sten_nybro_makro', 'sten_nybro_normal'],
		sand: 3.1,

		main: {
			stones: [
				{tun: 'Nybrosten makro', count: 1, id:"sten_nybro_makro"}
			],
			height: 21.5,
			width: 29,
			title: "Halvforbandt"
		},

		bottom: {
			stones: [],
			height: 0,
			width: 0
		},

		right: {
			stones: [
			 {tun:'Nybrosten normal', count: 1, id:"sten_nybro_normal"},
			],
			height: 21.5,
			width: 14.5
		},
		left: { stones:[], width:0, height:0},
		top: { stones:[],width:0, height:0},
		corner: {
			stones: [],
			count:0,
			width:0,
			height:0
		}
	}

    globalpatlib["skitse41"] = {
    	id:"skitse41",
		groups: ['sten_nybro_normal', 'sten_nybro_halv'],
		sand: 3.45,
    	
		main: {
			stones: [
				{tun: 'Nybrosten', count: 1, id:"sten_nybro_normal"}
			],
			height: 14.5,
			width: 21.5,
			title: "Halvforbandt"
		},

		bottom: { stones: [], height: 0, width: 0 },

		right: {
			stones: [
			 {tun:'Halve', count: 1, id:"sten_nybro_halv"}
			],
			height: 14.5,
			width: 10.75
		},
		left: { stones:[], width:0, height:0},
		top: { stones:[],width:0, height:0},
		corner: { stones: [], count:0, width:0, height:0 }
	}

    globalpatlib["skitse42"] = {
    	id:"skitse42",
		groups: ['sten_bonde_normal', 'sten_bonde_halv'],
		sand: 3.26,

		main: {
			stones: [
				{tun: 'Bondesten normal', count: 1, id:"sten_bonde_normal"}
			],
			height: 14,
			width: 21,
			title: "Halvforbandt"
		},

		bottom: {
			stones: [],
			height: 0,
			width: 0
		},

		right: {
			stones: [
			 {tun:'Bondesten halv', count: 1, id:"sten_bonde_halv"},
			],
			height: 14,
			width: 10.5
		},
		left: { stones:[], width:0, height:0},
		top: { stones:[],width:0, height:0},
		corner: {
			stones: [],
			count:0,
			width:0,
			height:0
		}
	}
	
    globalpatlib["skitse42r"] = {
    	id:"skitse42r",
		groups: ['sten_bonde_normal', 'sten_bonde_halv'],
		sand: 3.26,

		main: {
			stones: [
				{tun: 'Bondesten normal', count: 1, id:"sten_bonde_normal"}
			],
			height: 21,
			width: 14,
			title: "Halvforbandt"
		},

		bottom: {
			stones: [],
			height: 0,
			width: 0
		},

		right: {
			stones: [
			 {tun:'Bondesten halv', count: 1, id:"sten_bonde_halv"},
			],
			height: 10.5,
			width: 14
		},
		left: { stones:[], width:0, height:0},
		top: { stones:[],width:0, height:0},
		corner: {
			stones: [],
			count:0,
			width:0,
			height:0
		}
	}
	
	

    globalpatlib["skitse43"] = {
    	id:"skitse43",
		groups: ['sten_classico_2_normal', 'sten_classico_1_halv'],
		groups: [],
		sand: 4.9,
    	
		main: {
			stones: [
				{tun: 'Classico 2 normal', count: 1, id:"sten_classico_2_normal"}
			],
			height: 14.5,
			width: 21.5,
			title: "Halvforbandt"
		},

		bottom: { stones: [], height: 0, width: 0 },

		right: {
			stones: [
			 {tun:'Classico 1 halv', count: 1, id:"sten_classico_1_halv"}
			],
			height: 14.5,
			width: 10.75
		},
		left: { stones:[], width:0, height:0},
		top: { stones:[],width:0, height:0},
		corner: { stones: [], count:0, width:0, height:0 }
	}	
	


    globalpatlib["skitse44"] = {
    	id:"skitse44",
		groups: ['sten_byg_normal', 'sten_byg_halv'],
		sand: 4.9,

		main: {
			stones: [
				{tun: 'Bygholm normalsten', count: 1, id:"sten_byg_normal"}
			],
			height: 11.3,
			width: 22.6,
			title: "Halvforbandt"
		},

		bottom: {
			stones: [],
			height: 0,
			width: 0
		},

		right: {
			stones: [],
			height: 0,
			width: 0
		},
		left: { stones:[], width:0, height:0},
		top: { stones:[],width:0, height:0},
		corner: {
			stones: [],
			count:0,
			width:0,
			height:0
		}
	}
	
    globalpatlib["skitse45"] = {
    	id:"skitse45",
		groups: ['sten_classico_3_halv','sten_classico_2_normal'],
		groups: [],
		sand: 4.9,
    	
		main: {
			stones: [
				{tun: 'Classico 2 normal', count: 1, id:"sten_classico_2_normal"}
			],
			height: 14.5,
			width: 21.5,
			title: "Halvforbandt"
		},

		bottom: { stones: [], height: 0, width: 0 },

		right: {
			stones: [
			 {tun:'Classico 3 halv', count: 1, id:"sten_classico_3_halv"}
			],
			height: 14.5,
			width: 10.75
		},
		left: { stones:[], width:0, height:0},
		top: { stones:[],width:0, height:0},
		corner: { stones: [], count:0, width:0, height:0 }
	}	
	
	


    globalpatlib["skitse46"] = {
    	id:"skitse46",
		groups: ['sten_holland_normal','sten_holland_halv'],
		sand: 4.9,
    	
		main: {
			stones: [
				{tun: 'Hollandersten normal', count: 1, id:"sten_holland_normal"}
			],
			height: 10,
			width: 20,
			title: "Halvforbandt"
		},

		bottom: { stones: [], height: 0, width: 0 },

		right: {
			stones: [
			 {tun:'Hollandersten halv', count: 1, id:"sten_holland_halv"}
			],
			height: 10,
			width: 10
		},
		left: { stones:[], width:0, height:0},
		top: { stones:[],width:0, height:0},
		corner: { stones: [], count:0, width:0, height:0 }
	}	


    globalpatlib["skitse47"] = {
    	id:"skitse47",
		groups: ['sten_kloster_normal','sten_kloster_halv'],
		sand: 3.26,
    	
		main: {
			stones: [
				{tun: 'SF klostersten normal', count: 1, id:"sten_kloster_normal"}
			],
			height: 7,
			width: 21,
			title: "Halvforbandt"
		},

		bottom: { stones: [], height: 0, width: 0 },

		right: {
			stones: [
			 {tun:'SF klostersten halv', count: 1, id:"sten_kloster_halv"}
			],
			height: 7,
			width: 10.5
		},
		left: { stones:[], width:0, height:0},
		top: { stones:[],width:0, height:0},
		corner: { stones: [], count:0, width:0, height:0 }
	}	


    globalpatlib["skitse47r"] = {
    	id:"skitse47r",
		groups: ['sten_kloster_normal','sten_kloster_halv'],
		sand: 3.26,
    	
		main: {
			stones: [
				{tun: 'SF klostersten normal', count: 1, id:"sten_kloster_normal"}
			],
			height: 21,
			width: 7,
			title: "Halvforbandt"
		},

		bottom: { stones: [], height: 0, width: 0 },

		right: {
			stones: [
			 {tun:'SF klostersten halv', count: 1, id:"sten_kloster_halv"}
			],
			height: 10.5,
			width: 7
		},
		left: { stones:[], width:0, height:0},
		top: { stones:[],width:0, height:0},
		corner: { stones: [], count:0, width:0, height:0 }
	}	

    globalpatlib["skitse48"] = {
    	id:"skitse48",
		groups: ['sten_kloster_normal_2','sten_kloster_halv_2'],
		sand: 4.9,
    	
		main: {
			stones: [
				{tun: 'SF klostersten normal 2', count: 1, id:"sten_kloster_normal_2"}
			],
			height: 14,
			width: 21,
			title: "Halvforbandt"
		},

		bottom: { stones: [], height: 0, width: 0 },

		right: {
			stones: [
			 {tun:'SF klostersten halv 2', count: 1, id:"sten_kloster_halv_2"}
			],
			height: 14,
			width: 10.5
		},
		left: { stones:[], width:0, height:0},
		top: { stones:[],width:0, height:0},
		corner: { stones: [], count:0, width:0, height:0 }
	}	


    globalpatlib["skitse48r"] = {
    	id:"skitse48r",
		groups: ['sten_kloster_normal_2','sten_kloster_halv_2'],
		sand: 4.9,
    	
		main: {
			stones: [
				{tun: 'SF klostersten normal 2', count: 1, id:"sten_kloster_normal_2"}
			],
			height: 21,
			width: 14,
			title: "Halvforbandt"
		},

		bottom: { stones: [], height: 0, width: 0 },

		right: {
			stones: [
			 {tun:'SF klostersten halv 2', count: 1, id:"sten_kloster_halv_2"}
			],
			height: 10.5,
			width: 14
		},
		left: { stones:[], width:0, height:0},
		top: { stones:[],width:0, height:0},
		corner: { stones: [], count:0, width:0, height:0 }
	}	


    globalpatlib["skitse49"] = {
    	id:"skitse49",
		groups: ["flise_krystal","flise_krystal_normal","flise_krystal_halv"],
		sand: 4.9,
    	
		main: {
			stones: [
				{tun: 'SF klostersten normal 2', count: 1, id:"sten_kloster_normal_2"}
			],
			height: 21,
			width: 14,
			title: "Halvforbandt"
		},

		bottom: { stones: [], height: 0, width: 0 },

		right: {
			stones: [
			 {tun:'SF klostersten halv 2', count: 1, id:"sten_kloster_halv_2"}
			],
			height: 10.5,
			width: 14
		},
		left: { stones:[], width:0, height:0},
		top: { stones:[],width:0, height:0},
		corner: { stones: [], count:0, width:0, height:0 }
	}	


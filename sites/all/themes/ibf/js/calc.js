
var global_cur_retail = 0;
function openprint() {
	var dim = getSize();
	pattern_id = jQuery('input.pavetype:checked').val();
	stone_json = jQuery('input.stonetype:checked').val();
	pattern = globalpatlib[pattern_id];
	stone = eval('(' + stone_json + ')');
	
	// console.log(pattern_id);
	// fill out size from DB, count = 1.
	if (pattern_id == "repeat") {
		pattern.main.stones[0].tun = stone.name;
		pattern.main.stones[0].id = stone.group;
		pattern.main.width = stone.width;
		pattern.main.height = stone.height; 
	}
	
	// console.log(pattern_id);	
	var pat= [];
		pat['k0'] = calc(pattern, dim.height_in_cm, dim.width_in_cm,0);
		pat['k1'] = pat['k0'];
		pat['k2'] = calc(pattern, dim.height_in_cm, dim.width_in_cm,-1);
		pat['k3'] = calc(pattern, dim.height_in_cm, dim.width_in_cm,1);
		
		// console.log(pat);		
		
		for(var e in pat) {
		 	pat[e].curstone = stone;
		}
		
		

	// find current selectio
		pat_selected = jQuery('input[name="calc-kombitype"]:checked').val();
		curpat = pat[pat_selected];
		// console.log("result");
		// console.log(curpat);
		
	var grpcnt = new Array();	
		for(var e in curpat.stones) {
			grpcnt[curpat.grpmap[e]] = curpat.stones[e];
		}
	
		// define querystring params;
	var param = new Array();
		param['f']=global_cur_retail;
		param['pattern'] = pattern_id;
		param['width']   = curpat.main.width;
		param['height']  = curpat.main.height;

		if (jQuery("#calc-extrastype-e1:checked").val()) {
			param['sm2'] = curpat.sand;
			param['sto'] = Math.ceil((curpat.sand * (curpat.main.width/100 * curpat.main.height/100))/25);
		}
		
		
		out = new Array();
		for(var e in grpcnt) {
			out.push(e + ":" + grpcnt[e]);
		}
		param['stones'] = out.join("|");
		
		var query = new Array();
		for(var k in param) {
			query.push(k + "=" + param[k]);	
		}
		open("/spec/print?" + query.join("&"));
		
		
}


jQuery(document).ready(function() {

	doCalc();
	jQuery('.current-retailer').click(
		function() {
			jQuery('.retailer-list').toggle();
		}
		
	)
	var dim = getSize();
	
	if (dim.area_in_m == 0) {
		/*jQuery('.pavepatterntype').hide();
		jQuery('.paveextras').hide();*/
	}

	
	jQuery('input[name=length]').keyup(
		function() {
			var result = getSize();
			doCalc();
		}
	)

	jQuery('input[name=width]').keyup(
		function() {
			var result = getSize();
			doCalc();
		}
	)
	
	jQuery(".pavetype").live('click',
		function() {
			doCalc();
		}
	)

	jQuery('#calc-kombitype-k2, #calc-kombitype-k3, #calc-kombitype-k1').click(
		function() {
			doCalc();
		}
	);
	
	
	jQuery('.stonetype').click(
		function() {
			if (this.checked) {

				var dim = getSize();
				
				if (dim.height_in_cm >0 && dim.width_in_cm >0) {
					
					var obj = eval('(' + this.value + ")");
					
					var patlst = findpatterns(obj.group, globalpatlib);
					
					// clear patterns
					
					
					var pathtml = "";
					jQuery.each(patlst, function() {
						pathtml += buildpatternhtml(this);
					})
					jQuery('#patterns').html(pathtml);
					if (patlst.length == 1) {
						// jQuery("input.pavetype").attr("checked","checked")
						// jQuery("input.pavetype").click();
					} 
					jQuery('#pattern-simple').hide();
					jQuery('#pattern-complex').hide();
					jQuery('.node-contact').hide();
					jQuery('.pavepatterntype').show();
					
				}
				else {
					jQuery('input[name=length]').toggleClass("error");
					jQuery('input[name=width]').toggleClass("error");
				}
			}
		}
	)
})

	function setCurrentRetailer(nid,obj) {
		jQuery('.current-retailer').html(obj.innerHTML);
		jQuery('.retailer-list').hide();
		global_cur_retail = nid;
	}
	
	function doCalc() {
		pattern_id = jQuery('input.pavetype:checked').val();
		stone_json = jQuery('input.stonetype:checked').val();
		stone = eval('(' + stone_json + ')');
		

		// fil out count with data from DB
		var dim = getSize();
		
		if (dim.height_in_cm > 0 &&  dim.width_in_cm > 0) {
			jQuery('.pavetiletype').show();
		}
		else {
			jQuery('.pavetiletype').hide();
		}
		
		pattern = globalpatlib[pattern_id];
		if(!pattern){
			return;
		}

		// fill out size from DB, count = 1.
		if (pattern_id == "repeat") {
			pattern.main.stones[0].tun = stone.name;
			pattern.main.width = stone.width;
			pattern.main.height = stone.height; 
		}
		

		var exactfill = calc(pattern, dim.height_in_cm, dim.width_in_cm,0);
		var morefill = calc(pattern, dim.height_in_cm, dim.width_in_cm,1);
		var lessfill = calc(pattern, dim.height_in_cm, dim.width_in_cm,-1);
		exactfill.curstone = stone;
		morefill.curstone = stone;
		lessfill.curstone = stone;
		
		
		
		if (count(exactfill.stones)==1) {
			jQuery('#pattern-simple').show();
			jQuery('#pattern-complex').hide();
			render_small_result(exactfill, 'exact');
		}
		else {
			jQuery('#pattern-simple').hide();
			jQuery('#pattern-complex').show();
			render_complex_start_result(exactfill, 'exact');
			render_result(lessfill, 'less');
			render_result(morefill, 'more');
			
			pat_selected = jQuery('input[name="calc-kombitype"]:checked').val();
			if (pat_selected == "k1") {
				jQuery('.alert').show();
			}
			else {
				jQuery('.alert').hide();
			}
		}
		
		// sand calculation
		var bagsize = 25;
		var pat= [];
			pat['k0'] = exactfill;
			pat['k1'] = exactfill;
			pat['k2'] = lessfill;
			pat['k3'] = morefill;
			curpat = pat[pat_selected];


		if (curpat.sand) {
			var area = (curpat.main.width/100) * (curpat.main.height/100) ; 
			jQuery('#weightpr2m').html(formatnum(curpat.sand,2));
			jQuery('#weighttotal').html(formatnum(area*curpat.sand,1));
			jQuery('#bagsprm2').html(formatnum(curpat.sand/bagsize, 2));
			jQuery('#bagstotal').html(formatnum(Math.ceil(curpat.sand*area/bagsize),1));
			jQuery('.paveextras').show();
		}
		else {
			jQuery('.paveextras').hide();
		}
		jQuery('.node-contact').show();
	}
	
	function count(haystack) {
		
		var cnt = 0;
		for(var e in haystack) {
			cnt ++;
		}
		return cnt;
		
	}
	function inArray(needle, haystack) {
	    var length = haystack.length;
	    for(var i = 0; i < length; i++) {
	        if(haystack[i] == needle) return true;
	    }
	    return false;
	}

	
	function findpatterns(group, patterns) {
		var out = new Array();
		for(var e in patterns) {
			
			if (patterns[e].groups) {
				if (inArray(group, patterns[e].groups) == true) {
					out.push(patterns[e]);
				}
			}
		}
		return out;
	}

	function formatnum(w,d) {
			 var out = "" + Math.round(w*Math.pow(10,d))/Math.pow(10,d);
			 return out.replace(".",",");
	}
	
	function formatdimension(w,h) {
			 return formatnum(w,2) + " x " + formatnum(h,2) 
	}

	
	function getSize() {
		var _length = jQuery('input[name=length]').val();
		var _width = jQuery('input[name=width]').val();
		
		_length = (""+_length).replace(",",".");
		_width = ("" + _width).replace(",",".");
		
		
		var _area_in_m = 0;
		var _area_in_cm = 0;
		
		if (1*_length >0 && 1*_width >0) {
			_area_in_m = 1*_length*1*_width
			_area_in_cm = _area_in_m*100*100;
		}
		
		jQuery('#calc-area strong').text(formatnum(_area_in_m,2));

		return {
				width_in_m: formatnum(_width,2),
				width_in_cm: formatnum(_width*100,0),
				height_in_m: formatnum(_length,2),
				height_in_cm: formatnum(_length*100,0),
				area_in_m: formatnum(_area_in_m,2), 
				area_in_cm: formatnum(_area_in_cm,0)
		}
	}

function debug(obj)
{
	var strTmp   = "";
	var strArray = new Array();
	var strContent = new Array();
	var intI 	 = 0;

	for(var e in obj)
	{
		strArray[intI] = e;
		strContent[intI] = obj[e];
		intI ++;
	}

	for (var i = 0; i < strArray.length; i++)
		strTmp += "<tr><td><b>" + strArray[i] + "</b></td><td>" + strContent[i] + "</td></tr>"

	var w = open("about:blank","_blank");
		w.document.write ("<table>");
		w.document.write (strTmp);
		w.document.write ("</table>");
		w.document.close();
	w.focus();
}

function buildpatternhtml(pat) {
	var tmpl = '<div class="tile checked"><label for="pavepatterntype-{id}"><img src="/sites/default/files/skitse/{id}.png" alt="{title}"><input class="form-radio pavetype" type="radio" name="pavepatterntype" id="pavepatterntype-{id}" value="{value}"></label></div>';
	
	var out = tmpl.replace(new RegExp('\{id\}', 'ig'),pat.id);
	out = out.replace(new RegExp('\{title\}', 'ig'),pat.main.title);
	out = out.replace(new RegExp('\{image\}', 'ig'),pat.id);
	out = out.replace(new RegExp('\{value\}', 'ig'),pat.id);
	
	return out;
}

function render_small_result(resultobj) {
	
	
	
	// jQuery(".js-title").html(resultobj.title);
	// alert(resultobj.title);
	jQuery(".js-usage").html(resultobj.curstone.m2 + " stk.");

	out = resultobj.curstone.name + " " + resultobj.curstone.width + " x  " + resultobj.curstone.height;
	jQuery(".js-stone").html(out);	
	
	for(var e in resultobj.stones) {
		// jQuery(".js-usage").html(e);
		jQuery(".js-amount").html(formatnum(resultobj.stones[e],0));

	}
	

	/*jQuery("js-stone").html()*/
	/*jQuery("js-usage").html();
	jQuery("js-amount").html();*/

}

function render_complex_start_result(resultobj) {
	
	
	var id = "#area_exact";
	
	jQuery(id + " .js-title").html(resultobj.title);
	
	jQuery(id + ' .dimension').html(formatdimension(resultobj.main.width/100, resultobj.main.height/100));
	
	
	// render stone amount	
	var cnt = 0;
	var out = "";

	for(var e in resultobj.stones) {
		cnt ++;
		out += "<tr><td class='k2'>"  + e + "</td></tr>";
		out += '<tr><td class="amount">ca. <strong>' + formatnum(resultobj.stones[e],0) +  ' stk.</strong></td></tr>'
	}
	jQuery(id + " table").html(out);
	
	
}

function render_result(resultobj, baseid) {
	// do a lot of magic with the base obj
	
	var id = "#area_" + baseid;
	 
	
	
	// jQuery(id + ' .dimension').html(formatdimension(resultobj.main.width/100, resultobj.main.height/100));
	
	
	// render stone amount	
	var cnt = 0;
	var out = "";
		out += "<tr><td>" + formatdimension(resultobj.main.width/100, resultobj.main.height/100) + "</td></tr>"

	for(var e in resultobj.stones) {
		cnt ++;
		if (cnt != 1) {
			out += "<tr><td class='k2'>&nbsp;</td></tr>"	
		}
		out += '<tr><td class="amount">ca. <strong>' + formatnum(resultobj.stones[e],0) +  ' stk.</strong></td></tr>'
	}
	
	jQuery(id + " table").html(out);
	
	/*
          <tr><td><strong class="dimension">9,8 x 20</strong> m</td></tr>
          <tr><td></td></tr>
          <tr><td class="amount">ca. <strong>610 stk.</strong></td></tr>
          
          <tr class="note"><td colspan="2"><label for="calc-kombitype-k2">Mindre end indtastede <input class="form-radio" type="radio" name="calc-kombitype" id="calc-kombitype-k2" value="k2" checked="checked"></label></td>          </tr>
	*/
	
	
	
	
	
	
	
}


	
		function debug(obj)
		{
			var strTmp   = "";
			var strArray = new Array();
			var strContent = new Array();
			var intI 	 = 0;

			for(var e in obj)
			{
				strArray[intI] = e;
				strContent[intI] = obj[e];
				intI ++;
			}

			for (var i = 0; i < strArray.length; i++)
				strTmp += "<tr><td><b>" + strArray[i] + "</b></td><td>" + strContent[i] + "</td></tr>"

			var w = open("about:blank","_blank");
				w.document.write ("<table>");
				w.document.write (strTmp);
				w.document.write ("</table>");
				w.document.close();
			w.focus();
		}
	

	
	function calc(pattern, width, height, calctype) {
	
	var info = ""; 
	
	// calctype = 1 : greater, calctype=0 : Exact, calctype=-1 : less
	
	
	// fjern border fra areal, så vi altid har plads til denne,
	var width_after_border = width - pattern.left.width - pattern.right.width;
	var height_after_border = height - pattern.top.height - pattern.bottom.height;
	
	
	// find antal tilings af main mønsteret.
	p_width = Math.floor(width_after_border / pattern.main.width);
	p_height = Math.floor(height_after_border / pattern.main.height);
	
	

	if (calctype == 0) {
		var inner_height = height_after_border;
		var inner_width  = width_after_border;
		var title = "Eksakt";
	}  
	
	if (calctype == -1) {
		var inner_height = p_height * pattern.main.height;
		var inner_width  = p_width * pattern.main.width;
		var title = "Mindre";
		
	}  

	if (calctype == 1) {
		var inner_height = (p_height+1) * pattern.main.height;
		var inner_width  = (p_width+1) * pattern.main.width;
		var title = "Støre";
	} 
	
	p_width = (inner_width / pattern.main.width);
	p_height = (inner_height / pattern.main.height);

	pattern.main.count = p_width * p_height; // hvor mange main tiles har vi gang i , ny attribute
	
	pattern.main.heightcnt = p_height;
	pattern.main.widthcnt = p_width;
	

		// hvor meget border skal der laves..
		pattern.bottom.count = 0;
		if (pattern.bottom.width > 0) {
			bottom_cnt = Math.floor(inner_width / pattern.bottom.width);	
			pattern.bottom.count = bottom_cnt;
		}
	
		pattern.top.count = 0;
		if (pattern.top.width > 0) {
			top_cnt = Math.floor(inner_width / pattern.top.width);
			pattern.top.count = top_cnt;	
		}
		
		pattern.left.count = 0;
		if (pattern.left.height > 0) {
			left_cnt = Math.floor(inner_height / pattern.left.height);
			pattern.left.count = left_cnt;
		}
		
		
		pattern.right.count = 0;
		if (pattern.right.height > 0) {
			right_cnt = Math.floor(inner_height / pattern.right.height);
			pattern.right.count = right_cnt;	
		}
	
		var sten = new Array();
		var map = new Array();
		for(var e in pattern) {
			
			if (pattern[e].stones) {
				for(var s in pattern[e].stones) {
					var astone = pattern[e].stones[s];
					if (!sten[astone.tun]) {
						sten[astone.tun] = 0;
					}
					// out(e + ":" + astone.count + " * " + pattern[e].count);
					// console.log(pattern[e]);
					if (astone.id) {
						// id can be given to any tun 
						map[astone.tun] =astone.id; 	
					}
					
					sten[astone.tun] = sten[astone.tun] + astone.count * pattern[e].count;
				}
			}
		}
		
	
	var rtobj = {
		sand: pattern.sand,
		title: pattern.main.title,
		main:{
			cnt_width: pattern.main.widthcnt,
			cnt_height: pattern.main.heightcnt,
			width: (pattern.main.width*pattern.main.widthcnt+1*pattern.left.width+1*pattern.right.width),
			height: (pattern.main.height*pattern.main.heightcnt+1*pattern.top.height+1*pattern.bottom.height)
		},
		stones: sten,
		grpmap: map
	};

	rtobj.fit = (rtobj.main.width == width) && (rtobj.main.height == height);
		
	return rtobj;
	
}

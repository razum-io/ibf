(function ($) {
  $(document).ready(function()
  {
	$(".node-nyheder-container .view-content").each(function() {
		$(this).css("display","none");
		var icon = $(this).parent().find(".title");
		icon.addClass("closed");
    });
    
    
    $(".node-nyheder-container div.view-header").die('click').live("click", function() 
    {
    	var viewContent = $(this).parent().find(".view-content");
    	if(viewContent.is(":visible"))
    	{
    		viewContent.slideUp();
    		var icon = $(this).find(".title");
    		icon.removeClass("opened");
    		icon.addClass("closed");
    		//icon.css("background-image","/sites/all/themes/ibf/grafik/arrow_right_black.png");
    	}
    	else
    	{
    		var icon = $(this).find(".title");
    		icon.removeClass("closed");
    		icon.addClass("opened");
	    	viewContent.slideDown();
	    }
    });
    
   });

})(jQuery);
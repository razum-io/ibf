(function ($) {
  $(document).ready(function(){
    
    /*
     * googlemaps integration magic for contact page
     */
	 
	 //if ($(".node-fabriksbeton #map-canvas").length > 0) {
	if (document.getElementById(".node-fabriksbeton")) {
		 var display_view_str = "ibf";
		 var display_retailer_view = $(".node-produkt .view-retailers-forhandlere");
		 //window.alert("IBF afdelinger");
	 }
	 else
	 {
		 var display_view_str = "forhandlere";
		 var display_retailer_view = $(".node-produkt .view-retailers-forhandlere");
		 //window.alert("Forhandlere");
	 }

	if ($(".node-produkt #map-canvas").length > 0) {
      var default_start = new google.maps.LatLng(56.212814, 10.667725); 
      if ($(".i18n-de").length > 0) default_start = new google.maps.LatLng(54.1581, 9.84396); 
      
      var gmap_markers = [];
      var gmap_options = {
        zoom: 7,
        center: default_start,
        mapTypeControl: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      var gmap = new google.maps.Map(document.getElementById("map-canvas"), gmap_options);
      
      // contact persons 
      if ($(".view-id-contactpersons").length > 0) { 
        $(".view-id-contactpersons h2.togglebar").each(function() {
          $(this).parents("div.views-table:first").find(".contentbody").hide();
        });
        // $(".view-id-contactpersons .views-table:first h2.togglebar").addClass("active");
        // $(".view-id-contactpersons .views-table:first .contentbody").show();
        
        $(".view-id-contactpersons .views-table h2.togglebar").click(function() {
          
          $(".view-id-contactpersons .views-table .contentbody").hide();
          $(".view-id-contactpersons .views-table h2.togglebar").removeClass("active");
          
          $(this).parents("div.views-table:first").find(".contentbody").show();
          $(this).addClass("active");
        });
      } 
      
      // retailers list
      //if ($(".node-produkt .view-retailers-ibf").length > 0) {
	  //if ($(".node-produkt .view-retailers").length > 0) {
	  if (display_retailer_view.length > 0) {	  
		var retailer_list = $(".node-produkt .retailer-list");
        var department_list = $(".node-produkt .department-list");
        retailer_list.hide();
        department_list.hide();
        //var retailer_view = $(".node-produkt .view-retailers-forhandlere");
		//var retailer_view = $(".node-produkt .view-retailers");
		var retailer_view = display_retailer_view;
		retailer_view.find(".views-row").hide();
        var retailer,lat,lon,pos,item_list;
        
        // populate retailer and department select list
		//$(".node-produkt .view-retailers .views-row .node").each(function(index, value) {	
		//$(".node-fabriksbeton .view-retailers-ibf .views-row .node").each(function(index, value) {
		$(display_retailer_view+".views-row .node").each(function(index, value) {	
		  //window.alert(retailer);
          retailer = "<li><a href='javascript:;' class='"+$(this).attr("id")+"'>"+$(this).find(".title").text()+"</a></li>";
          if ($(this).hasClass("node-retailer"))  {
            retailer_list.append(retailer);
          }
          else {

				if ($(this).hasClass("node-department"))  {
					retailer_list.append(retailer);
				}
		  
			//window.alert(retailer);
            department_list.append(retailer);
          }
          
          lat = $(this).attr("data-lat");
          lon = $(this).attr("data-lon");
          pos = new google.maps.LatLng(lat, lon);
          addMarker(pos, $(this).attr("id"));
          
        });
        
        // open retailer select list
        $(".node-produkt .current-retailer").live("click", function() {
		//	window.alert("sometext");
          if(retailer_list.is(":visible")) {
            retailer_list.show();
          }
          else {
            retailer_list.hide();
          }
        });
        
        // open department select list
        $(".node-produkt .current-department").live("click", function() {
          if(department_list.is(":visible")) {
            department_list.hide();
          }
          else {
            department_list.show();
          }
        });
        
        // "show on map" button
        retailer_view.find(".node .show-on-map").live("click", function() {
          lat = $(this).parents(".node:first").attr("data-lat");
          lon = $(this).parents(".node:first").attr("data-lon");
          pos = new google.maps.LatLng(lat, lon);
          showOnMap(pos);
        });
          
        // click on retailer list item
        $(".node-produkt .retailer-list a").live("click", function() {
		  //selectRetailerNode($(".node-produkt .view-retailers #"+$(this).attr("class")));
		  //window.alert(display_view_str);
		  selectRetailerNode($(".node-produkt .view-retailers-" + display_view_str +" #" + $(this).attr("class")));
        });
        
        // find items by zipcode
        $(".node-produkt .find-by-zip .submit").click(function() {
          var zipcode = $(this).parents(".find-by-zip").find("input:first").val();
          retailer_view.find(".views-row").hide();
          retailer_view.find(".node").each(function(index, value) {
            var item_zipcode = $(this).find(".postal-code").text();
            if (item_zipcode == zipcode) $(this).parents(".views-row").show();
            
            
          });
        });
        
        if (navigator.geolocation != undefined) {
          navigator.geolocation.getCurrentPosition(locationInitHandler, noLocation, {"timeout": 10000});
          
          $(".gmap-controls .find-nearest").click(function() {
            //navigator.geolocation.getCurrentPosition(foundLocation, noLocation, {"timeout": 10000});
            var node = $("#map-canvas").attr("data-nearest-node");
            if (node != undefined && node != "") {
              selectRetailerNode(retailer_view.find("#"+node));
            }
            
          });
        }
        // do some mobile testing
        if(navigator.userAgent.match(/Android/i) ||
          navigator.userAgent.match(/webOS/i) ||
          navigator.userAgent.match(/iPhone/i) ||
          navigator.userAgent.match(/iPod/i) ||
          navigator.userAgent.match(/iPad/i) ||
          navigator.userAgent.match(/BlackBerry/)) {
          $(".node-produkt #map-canvas").hide();
          
        }
        else {
          $(".node-produkt .show-on-map").each(function() {
            // remove links on non-mobile devices
            $(this).text($(this).text());
          });
        }
        
      }
      
    } // google maps
    
    // Add a marker to the map and push to the array.
    function addMarker(location, node_id) {
      marker = new google.maps.Marker({
        "position": location,
        "map": gmap
      });
      google.maps.event.addListener(marker, 'click', function() {
        //$(".node-contact").remove()
        selectRetailerNode($("#"+node_id));
        gmap.setCenter(location);
        gmap.setZoom(12);
      });
      
      gmap_markers.push(marker);
    }

    // Sets the map on all markers in the array.
    function setAllMap(map) {
      for (var i = 0; i < gmap_markers.length; i++) {
        gmap_markers[i].setMap(map);
      }
    }

    // Removes the overlays from the map, but keeps them in the array.
    function clearOverlays() {
      setAllMap(null);
    }
    
    function showOnMap(pos) {
      gmap.setCenter(pos);
      gmap.setZoom(12);
    }
    
    function selectRetailerNode(node) {
      var views_row = node.parents(".views-row:first");
      var retailer_list = node.parents(".node-produkt:first").find(".retailer-list");
      var department_list = node.parents(".node-produkt:first").find(".department-list");
      var retailer_view = node.parents(".node-produkt:first").find(".view-retailers-" + display_view_str);
	  //var retailer_view = node.parents(".node-produkt:first").find(".view-retailers");
      
      // show select item
      retailer_view.find(".views-row").hide();
      views_row.show();
      
      // set map position
      /*lat = views_row.find(".node").attr("data-lat");
      lon = views_row.find(".node").attr("data-lon");
      pos = new google.maps.LatLng(lat, lon);
      gmap.setCenter(pos);
      gmap.setZoom(10);
      */
      // set marker
      //addMarker(pos);
      
      // set current retailer text
      if (node.hasClass("node-retailer")) {
        node.parents(".node-produkt:first").find(".current-retailer").text(node.find(".title:first").text());
      }
      else {
        node.parents(".node-produkt:first").find(".current-department").text(node.find(".title:first").text());
      }
            
      // hide the list
      department_list.hide();
      retailer_list.hide();
    }
    
    function locationInitHandler(position) {
      var lat,lon,dist,loc,nid;
      var current_lat = position.coords.latitude;
      var current_lon = position.coords.longitude;
      var current_loc = new LatLon(current_lat, current_lon);
      //var view = $(".node-produkt .view-retailers-forhandlere");
	  //var view = $(".node-produkt .view-retailers");
	  var view = display_retailer_view;
      var map_canvas = $(".node-produkt #map-canvas");
      map_canvas.attr("data-nearest-dist", "");
      map_canvas.attr("data-nearest-lat", "");
      map_canvas.attr("data-nearest-lon", "");
      
      // enable find nearest button
      $(".gmap-controls .find-nearest").show();
       
      // zoom to current position
      var my_pos = new google.maps.LatLng(current_lat, current_lon);  
      gmap.setCenter(my_pos);
      gmap.setZoom(12);
      
      view.find(".views-row .node").each(function(index, value) {
        lat = $(this).attr("data-lat");
        lon = $(this).attr("data-lon");
        nid = $(this).attr("id");
        if (lat != undefined && lon != undefined) {
          loc = new LatLon(lat,lon);
          dist = current_loc.distanceTo(loc);
          $(this).attr("data-dist", dist);
          // check if we should update data attributes on #map-canvas
          if (map_canvas.attr("data-nearest-dist") == "" || (map_canvas.attr("data-nearest-dist") != "" && (parseFloat(dist) < parseFloat(map_canvas.attr("data-nearest-dist"))))) {
            map_canvas.attr("data-nearest-dist", dist);
            map_canvas.attr("data-nearest-lat", lat);
            map_canvas.attr("data-nearest-lon", lon);
            map_canvas.attr("data-nearest-node", nid);
          }
        }
      });
      
    }
    /*
    function foundLocation(position) {
      var lat = position.coords.latitude;
      var lon = position.coords.longitude;
      
      alert("lat" + lat);
      
    }*/
    
    function noLocation() {
      $(".gmap-controls .find-nearest").hide();
    }
    
    
    
  }); // document ready end

})(jQuery);